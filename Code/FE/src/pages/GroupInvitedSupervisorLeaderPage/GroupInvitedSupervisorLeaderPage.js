import React, { memo, useCallback, useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Button, Modal, message, Tooltip } from "antd";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";
import uuid from "uuid";
import DashboardLayout from "../../layouts/DashboardLayout";
import { TableBottomPaginationBlock } from "../../components/Common/Table";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import PageTitle from "../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
import { ForContent } from "./GroupInvitedSupervisorPageLeaderStyled";
import TableComponent from "../../components/Common/TableComponent";
import moment from "moment";
import PopupViewDetailGroup from "./PopupViewDetailGroup";
import PopupViewGroupSupervisor from "./PopupViewGroupSupervisor";
import { Client } from "@stomp/stompjs";
import SockJS from "sockjs-client";
import { apiUrl } from "../../config";

import {
  DATE_FORMAT_SLASH,
  SUPERVISOR_LEADER,
  SUPERVISOR_LEADER_STATUS,
  SUPERVISOR_STATUS,
} from "../../constants";
import { render } from "less";
import { set } from "lodash";

const GroupInvitedSupervisorLeaderPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    authenticationStore,
  } = props;
  const [
    isVisiblePopupViewDetailGroup,
    setIsVisiblePopupViewDetailGroup,
  ] = useState(false);
  const [
    isVisiblePopupViewGroupSupervisor,
    setIsVisiblePopupViewGroupSupervisor,
  ] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [
    listGroupInvitedSupervisorRegistered,
    setListGroupInvitedSupervisorRegistered,
  ] = useState();
  const [groupId, setGroupId] = useState();
  const [supervisorId, setSupervisorId] = useState();

  useEffect(() => {
    const client = new Client({
      brokerURL: `ws://${apiUrl}/ws`,
      webSocketFactory: () => new SockJS(`${apiUrl}/ws`),
      onConnect: () => {
        client.subscribe("/topic/updateStatusInvitation", (message) => {
          if (message.body) {
            groupStore.getGroupSupervisorByTwoAcceptStatus();
          }
        });
      },
    });
    client.activate();
    return () => client.deactivate();
  }, []);

  useEffect(() => {
    if (authenticationStore.currentUser) {
      getGroupSupervisorByTwoAcceptStatus();
    }
    return () => {
      groupStore.clearStore();
    };
  }, [authenticationStore.currentUser]);
  const getGroupSupervisorByTwoAcceptStatus = async () => {
    loadingAnimationStore.setTableLoading(true);
    const res = await groupStore
      .getGroupSupervisorByTwoAcceptStatus()
      .finally(() => {
        loadingAnimationStore.setTableLoading(false);
      });
    setListGroupInvitedSupervisorRegistered(res.data);
  };
  // console.log(listGroupInvitedSupervisorRegistered);
  const showConfirmModal = (action, record) => {
    console.log("123", record);
    Modal.confirm({
      title: `Do you want to ${action} lecturer "${record?.supervisor.user.name}" to guide the ${record?.group.groupCode} group ?`,
      onOk: () => {
        if (action === "agree") {
          handleAgree(record);
        } else {
          handleRefuse(record);
        }
      },
      onCancel: () => {},
      okText: "Yes",
      cancelText: "No",
    });
  };

  const handleAgree = async (values) => {
    updateInvitationStatus(values, SUPERVISOR_LEADER_STATUS.ACCEPT);
  };
  const handleRefuse = async (values) => {
    updateInvitationStatus(values, SUPERVISOR_LEADER_STATUS.REJECT);
  };

  const updateInvitationStatus = async (values, status) => {
    try {
      loadingAnimationStore.showSpinner(true);
      const response = await groupStore.updateGroupSupervisorStatus(
        values?.id,
        status
      );
      if (response.status === 200) {
        if (status == SUPERVISOR_LEADER_STATUS.ACCEPT) {
          message.success(`The supervisor has been supervising the group!`);
        } else {
          message.success(`You declined that team and supervisor's request!`);
        }
        await getGroupSupervisorByTwoAcceptStatus();
        loadingAnimationStore.showSpinner(false);
      }
    } catch (err) {
      message.error(err.en || "Error to accept/reject ");
      loadingAnimationStore.showSpinner(false);
    }
  };

  function navigateToView(groupId) {
    setGroupId(groupId);
    setIsVisiblePopupViewDetailGroup(true);
  }
  function navigateToViewGroupSupervisor(supervisorId) {
    setSupervisorId(supervisorId);
    setIsVisiblePopupViewGroupSupervisor(true);
  }

  const columns = [
    {
      title: "No.",
      width: "2%",
      render: (record, index, dataSource) => <p>{dataSource + 1}</p>,
    },
    {
      title: "Supervisor",
      width: "27%",
      render: (record) => (
        <div className="flex justify-between items-center">
          <p>
            {record?.supervisor.user.name} ({record.supervisor.user.username})
          </p>
          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
            onClick={() =>
              navigateToViewGroupSupervisor(record?.supervisor.user.id)
            }
          >
            View Group Supervisor
          </Button>
        </div>
      ),
    },
    {
      title: "Email of Supervisor",
      width: "12%",
      render: (record) => <p>{record?.supervisor.user.email}</p>,
    },
    {
      title: "Group Name",
      width: "30%",
      render: (record) => (
        <div className="flex justify-between items-center">
          <p>
            {record?.group.groupCode} ({record?.group.vietnameseTitle})
          </p>
          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
            onClick={() => navigateToView(record?.group.id)}
          >
            View Detail Group
          </Button>
        </div>
      ),
    },
    {
      title: "Create At",
      width: "12%",
      render: (record) => (
        <p>{moment(record?.group.createdAt).format(DATE_FORMAT_SLASH)}</p>
      ),
    },
    {
      title: "Status",
      width: "12%",
      render: (record) => {
        if (record?.status === SUPERVISOR_STATUS.ACCEPT) {
          return <p>Ready</p>;
        }
        if (record?.status === SUPERVISOR_LEADER_STATUS.ACCEPT) {
          return <p>Group approved</p>;
        }
        return null;
      },
    },
    {
      title: "Action",
      width: "10%",
      align: "center",
      render: (record) => {
        if (record?.status === SUPERVISOR_STATUS.ACCEPT) {
          return (
            <div className="flex justify-between items-center">
              <Tooltip title="Confirm">
                <Button
                  type="primary"
                  icon={<CheckCircleOutlined />}
                  onClick={() => showConfirmModal("agree", record)}
                  className="flex items-center"
                >
                  Accept
                </Button>
              </Tooltip>
              <Tooltip title="Reject">
                <Button
                  type="danger"
                  icon={<CloseCircleOutlined />}
                  onClick={() => showConfirmModal("refuse", record)}
                  className="flex items-center ml-2"
                >
                  Reject
                </Button>
              </Tooltip>
            </div>
          );
        }
      },
    },
  ];
  return (
    <DashboardLayout>
      <Helmet>
        <title>Registration | List Group Registered</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"List Group Registered"}
        hiddenGoBack
      ></PageTitle>
      <ContentBlockWrapper>
        <TableComponent
          rowKey={(record) => record.id || uuid()}
          dataSource={listGroupInvitedSupervisorRegistered}
          columns={columns}
          pagination={false}
          loading={loadingAnimationStore.tableLoading}
        />
      </ContentBlockWrapper>
      <PopupViewDetailGroup
        groupId={groupId}
        isVisiblePopupEdit={isVisiblePopupViewDetailGroup}
        setIsVisiblePopupEdit={setIsVisiblePopupViewDetailGroup}
        handleClosePopup={() => setIsVisiblePopupViewDetailGroup(false)}
        setRefresh={setRefresh}
      />
      <PopupViewGroupSupervisor
        supervisorId={supervisorId}
        isVisiblePopupEdit={isVisiblePopupViewGroupSupervisor}
        setIsVisiblePopupEdit={setIsVisiblePopupViewGroupSupervisor}
        handleClosePopup={() => setIsVisiblePopupViewGroupSupervisor(false)}
        setRefresh={setRefresh}
      />
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "groupStore"
    )(observer(GroupInvitedSupervisorLeaderPage))
  )
);
