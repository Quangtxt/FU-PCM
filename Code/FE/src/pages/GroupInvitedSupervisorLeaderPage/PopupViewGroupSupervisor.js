import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Modal,
  message,
  Space,
  Input,
  DatePicker,
  Select,
  Empty,
} from "antd";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";

dayjs.extend(customParseFormat);

const { RangePicker } = DatePicker;

const disabledDate = (current) => {
  // Can not select days before today and today
  return current && current < dayjs().endOf("day");
};

const PopupViewGroupSupervisor = (props) => {
  const {
    isVisiblePopupEdit,
    setIsVisiblePopupEdit,
    handleClosePopup,
    loadingAnimationStore,
    setRefresh,
    meetingStore,
    groupStore,
    authenticationStore,
    meetingList,
    meetingId,
    groupsOfSupervisor,
    supervisorId,
  } = props;
  const [form] = Form.useForm();
  const [error, setError] = useState("");
  const [data, setData] = useState();
  console.log("data", data);

  console.log(supervisorId);
  useEffect(() => {
    const getGroupInfo = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await groupStore.getGroupOfSupervisor(supervisorId);
        setData(res.data);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    getGroupInfo();
  }, [supervisorId]);

  return (
    <Modal
      title="Group Of Supervisor"
      footer={null}
      closable={true}
      visible={isVisiblePopupEdit}
      onCancel={handleClosePopup}
    >
      <p className="mr-2 p-2">
        <strong>List Group:</strong>{" "}
      </p>
      {data && data.length > 0 ? (
        data.map((group, i) => (
          <div key={i} className="border border-gray-300 rounded-md p-4 mb-4">
            <div>
              <p className="mr-2 p-2">
                <strong>Group:</strong> {group?.group.groupCode}
              </p>
              <p className="mr-2 p-2">
                <strong>VietNamese Name:</strong> {group?.group.vietnameseTitle}
              </p>
              <p className="mr-2 p-2">
                <strong>English Name:</strong> {group?.group.name}
              </p>
            </div>
          </div>
        ))
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}
      {data && data.length > 4 && (
        <p style={{ color: "red" }}>Supervisor has been monitoring 4 groups</p>
      )}
    </Modal>
  );
};

PopupViewGroupSupervisor.propTypes = {};

export default withRouter(
  inject(
    "loadingAnimationStore",
    "authenticationStore",
    "meetingStore",
    "authenticationStore",
    "groupStore"
  )(observer(PopupViewGroupSupervisor))
);
