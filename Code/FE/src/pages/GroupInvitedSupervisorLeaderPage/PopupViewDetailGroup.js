import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Modal,
  message,
  Space,
  Input,
  DatePicker,
  Select,
  Empty,
} from "antd";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";
import MemberItem from "../../components/ViewProgress/MemberItem";
import { DATE_FORMAT_SLASH, MEMBER_STATUS } from "../../constants";

dayjs.extend(customParseFormat);

const { RangePicker } = DatePicker;

const disabledDate = (current) => {
  // Can not select days before today and today
  return current && current < dayjs().endOf("day");
};

const PopupViewDetailGroup = (props) => {
  const {
    isVisiblePopupEdit,
    setIsVisiblePopupEdit,
    handleClosePopup,
    loadingAnimationStore,
    setRefresh,
    meetingStore,
    groupStore,
    authenticationStore,
    meetingList,
    meetingId,
    groupsOfSupervisor,
    groupId,
  } = props;
  const [form] = Form.useForm();
  const [error, setError] = useState("");
  const [members, setMembers] = useState([]);
  const [group, setGroup] = useState();
  // console.log("members", members);
  // console.log("group", group);

  useEffect(() => {
    const getGroupInfo = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await groupStore.getGroupByGroupId(groupId);
        setGroup(res.data);
        setMembers(res.data.members);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    groupId && getGroupInfo();
  }, [groupId]);

  return (
    <Modal
      title="Detail Group"
      footer={null}
      closable={true}
      visible={isVisiblePopupEdit}
      onCancel={handleClosePopup}
    >
      <div
        style={{
          justifyContent: "flex-end",
          alignItems: "center",
          marginBottom: 20,
        }}
      >
        <p className="mr-2 p-2">
          <strong>Group:</strong> {group?.groupCode}
        </p>
        <p className="mr-2 p-2">
          <strong>VietNamese Name:</strong> {group?.vietnameseTitle}
        </p>
        <p className="mr-2 p-2">
          <strong>English Name:</strong> {group?.name}
        </p>
      </div>
      <hr style={{ borderTop: "1px solid #ccc", marginBottom: 20 }} />
      <p className="mb-1">
        <strong>Member:</strong>{" "}
      </p>

      <div className="members">
        {members.length === 0 && <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />}
        {members
          ?.sort((a, b) => {
            // Ưu tiên thành viên có role là "OWNER"
            if (a.role.includes("OWNER")) return -1;
            if (b.role.includes("OWNER")) return 1;

            /// Nếu không phải "OWNER", ưu tiên "ingroup" giảm dần theo updateAt
            if (
              a.status === MEMBER_STATUS.INGROUP &&
              b.status === MEMBER_STATUS.INGROUP
            ) {
              return b.updateAt - a.updateAt;
            }
            if (a.status === MEMBER_STATUS.INGROUP) return -1;
            if (b.status === MEMBER_STATUS.INGROUP) return 1;

            // Cuối cùng là những thành viên "pending"
            if (
              a.status === MEMBER_STATUS.PENDING &&
              b.status !== MEMBER_STATUS.PENDING
            )
              return 1;
            if (
              b.status === MEMBER_STATUS.PENDING &&
              a.status !== MEMBER_STATUS.PENDING
            )
              return -1;

            // Nếu cùng status, giữ nguyên thứ tự
            return 0;
          })
          ?.map((member, index) => (
            <MemberItem key={index} member={member} group={group} />
          ))}
      </div>
    </Modal>
  );
};

PopupViewDetailGroup.propTypes = {};

export default withRouter(
  inject(
    "loadingAnimationStore",
    "authenticationStore",
    "meetingStore",
    "authenticationStore",
    "groupStore"
  )(observer(PopupViewDetailGroup))
);
