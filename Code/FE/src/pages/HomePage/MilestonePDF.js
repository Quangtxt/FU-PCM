import React, { memo, useCallback, useEffect, useState } from "react";
import { AudioOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Typography, List, Table, Divider, Row, Col, Collapse } from "antd";
import { DATE_FORMAT_SLASH } from "../../constants";
import {
  FontSize20px,
  FontSize17px,
  FontSize16px,
  FontSize15px,
} from "./HomePageStyled";
import uuid from "uuid";
const { Title, Paragraph } = Typography;
const MilestonePdf = (props) => {
  const { milestones } = props;
  const renderTable = () => {
    return (
      <table style={{ width: "100%", borderCollapse: "collapse" }}>
        <thead>
          <tr style={{}}>
            <th
              style={{
                border: "1px solid #ddd",
                padding: "8px",
                backgroundColor: "#006699",
                color: "#fff",
              }}
            >
              #
            </th>
            <th
              style={{
                border: "1px solid #ddd",
                padding: "8px",
                backgroundColor: "#006699",
                color: "#fff",
              }}
            >
              Milestone
            </th>
            <th
              style={{
                border: "1px solid #ddd",
                padding: "8px",
                backgroundColor: "#006699",
                color: "#fff",
              }}
            >
              From
            </th>
            <th
              style={{
                border: "1px solid #ddd",
                padding: "8px",
                backgroundColor: "#006699",
                color: "#fff",
              }}
            >
              To/Deadline
            </th>
            <th
              style={{
                border: "1px solid #ddd",
                padding: "8px",
                backgroundColor: "#006699",
                color: "#fff",
              }}
            >
              Notes
            </th>
          </tr>
        </thead>
        <tbody>
          {milestones?.map((milestone, index) => (
            <React.Fragment key={milestone.id || uuid()}>
              {milestone.detail[0]?.name == null ? (
                <React.Fragment key={milestone.id || uuid()}>
                  <tr>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {index + 1}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.name}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.fromDate.format(DATE_FORMAT_SLASH)}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.toDate.format(DATE_FORMAT_SLASH)}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.time}
                    </td>
                  </tr>
                  {milestone.detail?.map((detail, detailIndex) => (
                    <tr key={detail.id || uuid()}>
                      <td
                        style={{
                          border: "1px solid #ddd",
                          padding: "8px",
                          backgroundColor: "rgb(243 243 243)",
                        }}
                      ></td>
                      <td
                        style={{
                          border: "1px solid #ddd",
                          padding: "8px",
                          backgroundColor: "rgb(243 243 243)",
                        }}
                      >
                        {detail.product}
                      </td>
                      <td
                        style={{
                          border: "1px solid #ddd",
                          padding: "8px",
                          backgroundColor: "rgb(243 243 243)",
                        }}
                      >
                        {detail.fromDate?.format(DATE_FORMAT_SLASH)}
                      </td>
                      <td
                        style={{
                          border: "1px solid #ddd",
                          padding: "8px",
                          backgroundColor: "rgb(243 243 243)",
                        }}
                      >
                        {detail.toDate?.format(DATE_FORMAT_SLASH)}
                      </td>
                      <td
                        style={{
                          border: "1px solid #ddd",
                          padding: "8px",
                          backgroundColor: "rgb(243 243 243)",
                        }}
                      >
                        {detail.requirement}
                      </td>
                    </tr>
                  ))}
                </React.Fragment>
              ) : (
                <React.Fragment key={milestone.id || uuid()}>
                  <tr>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {index + 1}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.name}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.fromDate.format(DATE_FORMAT_SLASH)}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.toDate.format(DATE_FORMAT_SLASH)}
                    </td>
                    <td style={{ border: "1px solid #ddd", padding: "8px" }}>
                      {milestone.time}
                    </td>
                  </tr>
                  {milestone.detail?.map((item, itemIndex) => (
                    <React.Fragment key={item.id || uuid()}>
                      <tr>
                        <td
                          style={{
                            border: "1px solid #ddd",
                            padding: "8px",
                            backgroundColor: "rgb(243 243 243)",
                          }}
                        ></td>
                        <td
                          style={{
                            border: "1px solid #ddd",
                            padding: "8px",
                            backgroundColor: "rgb(243 243 243)",
                          }}
                        >
                          {item.name}
                        </td>
                        <td
                          style={{
                            border: "1px solid #ddd",
                            padding: "8px",
                            backgroundColor: "rgb(243 243 243)",
                          }}
                        >
                          {item.fromDate?.format(DATE_FORMAT_SLASH)}
                        </td>
                        <td
                          style={{
                            border: "1px solid #ddd",
                            padding: "8px",
                            backgroundColor: "rgb(243 243 243)",
                          }}
                        >
                          {item.toDate?.format(DATE_FORMAT_SLASH)}
                        </td>
                        <td
                          style={{
                            border: "1px solid #ddd",
                            padding: "8px",
                            backgroundColor: "rgb(243 243 243)",
                          }}
                        >
                          {item.time}
                        </td>
                      </tr>
                      {item.detail?.map((detail, detailIndex) => (
                        <tr key={detail.id || uuid()}>
                          <td
                            style={{
                              border: "1px solid #ddd",
                              padding: "8px",
                            }}
                          ></td>
                          <td
                            style={{
                              border: "1px solid #ddd",
                              padding: "8px",
                            }}
                          >
                            - {detail.product}
                          </td>
                          <td
                            style={{
                              border: "1px solid #ddd",
                              padding: "8px",
                            }}
                          >
                            {detail.fromDate?.format(DATE_FORMAT_SLASH)}
                          </td>
                          <td
                            style={{
                              border: "1px solid #ddd",
                              padding: "8px",
                            }}
                          >
                            {detail.toDate?.format(DATE_FORMAT_SLASH)}
                          </td>
                          <td
                            style={{
                              border: "1px solid #ddd",
                              padding: "8px",
                            }}
                          >
                            {detail.requirement}
                          </td>
                        </tr>
                      ))}
                    </React.Fragment>
                  ))}
                </React.Fragment>
              )}
            </React.Fragment>
          ))}
        </tbody>
      </table>
    );
  };

  return (
    <div
      id="divToPrint"
      style={{
        padding: "40px 60px",
        fontFamily: "Arial",
        backgroundColor: "#fff",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          fontFamily: "Arial, sans-serif",
          textAlign: "center",
          margin: "0",
          paddingBottom: "40px",
        }}
      >
        <div
          style={{
            marginBottom: "5px",
            width: "198px",
            aspectRatio: "198/108",
          }}
        >
          <img
            src={`/assets/photos/image1.png`}
            alt="logo"
            className="logoImg"
            style={{
              width: "198px",
              aspectRatio: "198/108",
            }}
          />
        </div>

        <Title
          level={1}
          style={{
            textAlign: "center",
            fontSize: "32px",
            color: "#0056b3",
            margin: "0",
          }}
        >
          SE CAPSTONE PROJECT (SEP490)
        </Title>
        <Title
          level={2}
          style={{
            textAlign: "center",
            fontSize: "28px",
            color: "#0056b3",
            margin: "0",
          }}
        >
          INTRODUCTION AND PLANNING – SUMMER 2024
        </Title>
      </div>
      <br />
      <br />
      <Typography>
        <div style={{ marginBottom: "15px", fontFamily: "Arial, sans-serif" }}>
          <h3
            style={{
              color: "#0056b3",
              fontSize: "30px",
              borderBottom: "2px solid #0056b3",
              paddingBottom: "10px",
            }}
          >
            Subject Overview
          </h3>
          <br />
          <Paragraph>
            <FontSize20px>
              <ul
                style={{ listStyleType: "disc", padding: 0, marginBottom: 0 }}
              >
                <li>
                  <strong>Target:</strong> students to work in teams of 5
                  students, to complete a specific software application or
                  system.
                </li>
                <li>
                  <strong>Product topic & scope:</strong> as provided/agreed
                  with the supervisor
                </li>
                <li>
                  Product size are required to be equivalent to 20-25 medium UCs
                  (3-7 transactions for each UC; action buttons on the screen,
                  end user interactions to the software screens/functions or
                  database transactions can be counted as transaction)
                </li>
                <li>
                  <strong>Process:</strong> full life cycle - requirement,
                  design, code, test (UT, IT, ST, AT)
                </li>
                <li>
                  Recommended methodology: iterative & incremental (or RUP)
                </li>
                <li>
                  Templates & guides:{" "}
                  <a href="https://cmshn.fpt.edu.vn/mod/folder/view.php?id=150581">
                    https://cmshn.fpt.edu.vn/mod/folder/view.php?id=150581
                  </a>
                </li>
                <li>
                  <strong>Tools & Technologies:</strong>
                  <FontSize17px>
                    <ul
                      style={{
                        listStyleType: "none",
                        padding: 0,
                        marginBottom: 0,
                      }}
                    >
                      <li>
                        <strong>Technologies/Platforms:</strong> decided by the
                        students, with consulting from supervisor
                      </li>
                      <li>
                        <strong>Recommended Tools</strong> (using student’s FPT
                        email account):
                        <FontSize15px>
                          <ul
                            style={{
                              listStyleType: "none",
                              padding: 0,
                              marginBottom: 0,
                            }}
                          >
                            <li>
                              -{" "}
                              <span style={{ textDecoration: "underline" }}>
                                Diagrams
                              </span>
                              : Visual Paradigm (diagrams.net)
                            </li>
                            <li>
                              -{" "}
                              <span style={{ textDecoration: "underline" }}>
                                Mockup Prototype
                              </span>
                              : Pencil, Figma, ..
                            </li>
                            <li>
                              -{" "}
                              <span style={{ textDecoration: "underline" }}>
                                Sources & Project Issue Management
                              </span>
                              : GitLab
                            </li>
                            <li>
                              -{" "}
                              <span style={{ textDecoration: "underline" }}>
                                Document/Files Management
                              </span>
                              : OneDrive or Google Drive
                            </li>
                            <li>
                              -{" "}
                              <span style={{ textDecoration: "underline" }}>
                                Email notifications
                              </span>
                              : integrate with the MailTrap
                              <a href="https://mailtrap.io">
                                (https://mailtrap.io)
                              </a>
                            </li>
                            <li>
                              -{" "}
                              <span style={{ textDecoration: "underline" }}>
                                SMS OTP verification
                              </span>
                              : integrate with Google Firebase
                            </li>
                          </ul>
                        </FontSize15px>
                      </li>
                    </ul>
                  </FontSize17px>
                </li>
              </ul>
            </FontSize20px>
          </Paragraph>
        </div>
        <br />
        <br />
        {/* <Title level={3}></Title> */}
        <div style={{ paddingBottom: "30px" }}>
          <h3
            style={{
              color: "#0056b3",
              fontSize: "30px",
              borderBottom: "2px solid #0056b3",
              paddingBottom: "10px",
              marginTop: 0,
              textAlign: "center",
            }}
          >
            Milestones & Deliverables
          </h3>
          <br />
          <FontSize16px>{renderTable()}</FontSize16px>
        </div>
        <br />
        <br />
        <div style={{ paddingBottom: "30px" }}>
          <h3
            style={{
              color: "#0056b3",
              fontSize: "30px",
              borderBottom: "2px solid #0056b3",
              paddingBottom: "10px",
              marginTop: 0,
            }}
          >
            Daily Project Tracking
          </h3>
          <br />
          <div
            style={{
              display: "flex",
              alignItems: "flex-start",
              marginBottom: "10px",
            }}
          >
            <Paragraph style={{ width: "calc(100% - 647px)" }}>
              <FontSize20px>
                <ul style={{ listStyle: "none", padding: 0, margin: 0 }}>
                  <li style={{ margin: 0 }}>
                    <strong>WP Status</strong>
                  </li>
                  <FontSize17px>
                    <ul style={{ listStyle: "disc", padding: 0, margin: 0 }}>
                      <li>Pending</li>
                      <li>Doing</li>
                      <li>Deferred</li>
                      <li>Cancelled</li>
                      <li>Done</li>
                    </ul>
                  </FontSize17px>
                </ul>
              </FontSize20px>
            </Paragraph>
            <div
              style={{
                marginBottom: "5px",
                aspectRatio: "647/198",
                width: "647px",
              }}
            >
              <img
                src={`/assets/photos/image2.png`}
                alt="logo"
                className="logoImg"
                style={{
                  objectFit: "cover",
                  aspectRatio: "647/198",
                  width: "647px",
                }}
              />
            </div>
          </div>
          <div
            style={{
              marginBottom: "5px",
              aspectRatio: "533/263",
              width: "100%",
            }}
          >
            <img
              src={`/assets/photos/image3.png`}
              alt="logo"
              className="logoImg"
              style={{
                objectFit: "cover",
                aspectRatio: "533/263",
                width: "533px",
              }}
            />
          </div>
        </div>
        <br />
        <br />
        <div>
          <h3
            style={{
              color: "#0056b3",
              fontSize: "30px",
              borderBottom: "2px solid #0056b3",
              paddingBottom: "10px",
              marginTop: 0,
            }}
          >
            Student Evaluation
          </h3>
          <br />
          <Paragraph>
            <FontSize20px>
              <ul
                style={{ listStyleType: "disc", padding: 0, marginBottom: 0 }}
              >
                <li>
                  On going evaluation (<strong>50% of student GPA</strong>,
                  other 50% is by the thesis presentation council in the final
                  presentation)
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      gap: " 20px",
                      padding: "10px",
                    }}
                  >
                    <div
                      style={{
                        aspectRatio: "215/108",
                        width: "50%",
                      }}
                    >
                      <img
                        src={`/assets/photos/image4.png`}
                        alt="logo"
                        className="logoImg"
                        style={{
                          objectFit: "cover",
                          width: "215px",
                          aspectRatio: "215/108",
                        }}
                      />
                    </div>
                    <FontSize17px style={{ width: "50%" }}>
                      <ul
                        style={{
                          listStyleType: "none",
                          padding: 0,
                          marginBottom: 0,
                        }}
                      >
                        <li>
                          <strong>Who:</strong> supervisor/supervisor/GVHD
                        </li>
                        <li>
                          <strong>When:</strong>
                          <ul
                            style={{
                              listStyleType: "none",
                              padding: 0,
                              marginBottom: 0,
                            }}
                          >
                            <li>- After each report submission</li>
                            <li>
                              - Might be updated right after report 7 submission
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </FontSize17px>
                  </div>
                </li>
                <li>
                  Comments for thesis: supervisors to be requested to send in
                  the week 13
                  <div
                    style={{
                      aspectRatio: "587/268",
                      width: "100%",
                    }}
                  >
                    <img
                      src={`/assets/photos/image5.png`}
                      alt="logo"
                      className="logoImg"
                      style={{
                        objectFit: "cover",
                        width: "587px",
                        aspectRatio: "587/268",
                      }}
                    />
                  </div>
                </li>
              </ul>
            </FontSize20px>
          </Paragraph>
        </div>
      </Typography>
    </div>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "studentStore",
      "groupStore"
    )(observer(MilestonePdf))
  )
);
