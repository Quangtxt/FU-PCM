import React, { memo, useCallback, useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Button, Modal, message, Tooltip } from "antd";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";
import uuid from "uuid";
import DashboardLayout from "../../layouts/DashboardLayout";
import { TableBottomPaginationBlock } from "../../components/Common/Table";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import PageTitle from "../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
import { ForContent } from "./GroupInvitedSupervisorPageStyled";
import TableComponent from "../../components/Common/TableComponent";
import moment from "moment";
import {
  DATE_FORMAT_SLASH,
  SUPERVISOR_STATUS,
  SUPERVISOR_LEADER_STATUS,
} from "../../constants";
import PopupViewDetailGroup from "../GroupInvitedSupervisorLeaderPage/PopupViewDetailGroup";
import { Client } from "@stomp/stompjs";
import SockJS from "sockjs-client";
import { apiUrl } from "../../config";

const GroupInvitedSupervisorPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    authenticationStore,
  } = props;

  const { groupInvitation } = groupStore;

  const [
    listGroupSupervisorRegistered,
    setListGroupSupervisorRegistered,
  ] = useState();
  const [groupId, setGroupId] = useState();
  const [
    isVisiblePopupViewDetailGroup,
    setIsVisiblePopupViewDetailGroup,
  ] = useState(false);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    const client = new Client({
      brokerURL: `ws://${apiUrl}/ws`,
      webSocketFactory: () => new SockJS(`${apiUrl}/ws`),
      onConnect: () => {
        client.subscribe("/topic/submitGroup", (message) => {
          if (message.body) {
            getListInvitation();
          }
        });
      },
    });
    client.activate();
    return () => client.deactivate();
  }, []);
  useEffect(() => {
    if (authenticationStore.currentUser) {
      getListInvitation();
    }
    return () => {
      groupStore.clearStore();
    };
  }, [authenticationStore.currentUser]);
  const getListInvitation = async () => {
    loadingAnimationStore.setTableLoading(true);
    const res = await groupStore
      .getGroupSupervisorBySupervisorId(authenticationStore?.currentUser.id)
      .finally(() => {
        loadingAnimationStore.setTableLoading(false);
      });
    setListGroupSupervisorRegistered(res.data);
  };

  const showConfirmModal = (action, record) => {
    Modal.confirm({
      title: `Do you want to ${action} to be a supervisor of this group?`,
      onOk: () => {
        if (action === "agree") {
          handleAgree(record);
        } else {
          handleRefuse(record);
        }
      },
      onCancel: () => {},
      okText: "Yes",
      cancelText: "No",
    });
  };

  const handleAgree = async (values) => {
    updateInvitationStatus(values, SUPERVISOR_STATUS.ACCEPT);
  };
  const handleRefuse = async (values) => {
    updateInvitationStatus(values, SUPERVISOR_STATUS.REJECT);
  };

  const updateInvitationStatus = async (values, status) => {
    try {
      loadingAnimationStore.showSpinner(true);
      const response = await groupStore.updateGroupSupervisorStatus(
        values?.id,
        status
      );
      if (response.status === 200) {
        if (status == SUPERVISOR_STATUS.ACCEPT) {
          message.success(
            `You have successfully accepted to become a supervisor for this group!`
          );
        } else {
          message.success(`You refused to become a supervisor for this group!`);
        }
        await getListInvitation();
        loadingAnimationStore.showSpinner(false);
      }
    } catch (err) {
      message.error(err.en || "Login failed response status!");
      loadingAnimationStore.showSpinner(false);
    }
  };
  function navigateToView(groupId) {
    setGroupId(groupId);
    setIsVisiblePopupViewDetailGroup(true);
  }
  const columns = [
    {
      title: "No.",
      width: "5%",
      render: (record, index, dataSource) => <p>{dataSource + 1}</p>,
    },
    {
      title: "Group Name",
      width: "25%",
      render: (record) => (
        <div className="flex justify-between items-center">
          <p className="w-1/2">
            {record?.group.groupCode} ({record?.group.vietnameseTitle})
          </p>

          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
            onClick={() => navigateToView(record?.group.id)}
          >
            View Detail Group
          </Button>
        </div>
      ),
    },
    {
      title: "Description",
      width: "25%",
      render: (record) => <p>{record?.group.description}</p>,
    },
    {
      title: "Create At",
      width: "25%",
      render: (record) => (
        <p>{moment(record?.group.createdAt).format(DATE_FORMAT_SLASH)}</p>
      ),
    },
    {
      title: "Status",
      width: "12%",
      render: (record) => {
        if (record?.status === SUPERVISOR_STATUS.ACCEPT) {
          return <p>Wait for the leader supervisor approval</p>;
        }
        if (record?.status === SUPERVISOR_LEADER_STATUS.ACCEPT) {
          return <p>Group approved</p>;
        }
        return <p>Ready</p>;
      },
    },
    {
      title: "Action",
      width: "10%",
      align: "center",
      render: (record) => {
        if (record?.status === SUPERVISOR_STATUS.PENDING) {
          return (
            <div className="flex justify-between items-center">
              <Tooltip title="Confirm">
                <Button
                  type="primary"
                  icon={<CheckCircleOutlined />}
                  onClick={() => showConfirmModal("agree", record)}
                  className="flex items-center"
                >
                  Accept
                </Button>
              </Tooltip>
              <Tooltip title="Reject">
                <Button
                  type="danger"
                  icon={<CloseCircleOutlined />}
                  onClick={() => showConfirmModal("refuse", record)}
                  className="flex items-center ml-2"
                >
                  Reject
                </Button>
              </Tooltip>
            </div>
          );
        }
      },
    },
  ];
  return (
    <DashboardLayout>
      <Helmet>
        <title>Registration | List Group Registered</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"List Group Registered"}
        hiddenGoBack
      ></PageTitle>
      <ContentBlockWrapper>
        <TableComponent
          rowKey={(record) => record.id || uuid()}
          dataSource={listGroupSupervisorRegistered}
          columns={columns}
          pagination={false}
          loading={loadingAnimationStore.tableLoading}
        />
      </ContentBlockWrapper>
      <PopupViewDetailGroup
        groupId={groupId}
        isVisiblePopupEdit={isVisiblePopupViewDetailGroup}
        setIsVisiblePopupEdit={setIsVisiblePopupViewDetailGroup}
        handleClosePopup={() => setIsVisiblePopupViewDetailGroup(false)}
        setRefresh={setRefresh}
      />
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "groupStore"
    )(observer(GroupInvitedSupervisorPage))
  )
);
