import React, { memo, useEffect, useState } from "react";
import {
  Typography,
  Select,
  Table,
  Modal,
  Divider,
  Button,
  notification,
} from "antd";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import DashboardLayout from "../../layouts/DashboardLayout";
import { Helmet } from "react-helmet/es/Helmet";
import { withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { DownloadOutlined } from "@ant-design/icons";
import { apiUrl } from "../../config";
import moment from "moment";
import CPCommentFormPopup from "./CPCommentFormPopup";
import { DD_MM_YYYY_HH_mm } from "../../constants";

const { Title, Text, Paragraph } = Typography;

const CapstoneProjectSubmitPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    semesterStore,
    authenticationStore,
    match,
  } = props;
  const [selectedSemesterId, setSelectedSemesterId] = useState(null);
  const [semesters, setSemesters] = useState([]);
  const [groupSubmission, setGroupSubmission] = useState();
  const [refresh, setRefresh] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isCommentModalVisible, setIsCommentModalVisible] = useState(false);
  const [actionType, setActionType] = useState("");
  const [groupId, setGroupId] = useState(null);
  const [groupSelected, setGroupSelected] = useState();
  const [filesSelected, setFileSelected] = useState(null);
  const [isModalVisibleAll, setIsModalVisibleAll] = useState(false);

  useEffect(() => {
    getSemester();
  }, []);
  const getSemester = async () => {
    try {
      const res = await semesterStore.getSemesters();
      setSemesters(res.data);
      setSemesterCurrent(res.data);
    } catch (e) {
      console.log(e);
    }
  };

  const onSelectChange = (value) => {
    setSelectedSemesterId(value);
  };

  const setSemesterCurrent = (semesters) => {
    if (semesters.length > 0) {
      const currentDate = moment();
      const closestSemester = semesters.reduce((prev, curr) => {
        const startDate = moment(curr.beginAt);
        const endDate = moment(curr.endAt);
        if (currentDate.isBetween(startDate, endDate, null, "[]")) {
          return curr;
        } else {
          if (prev !== null) {
            const prevDiff = Math.abs(
              moment(prev.beginAt).diff(currentDate, "days")
            );
            const currDiff = Math.abs(startDate.diff(currentDate, "days"));
            return currDiff < prevDiff ? curr : prev;
          } else {
            return curr;
          }
        }
      }, null);
      if (closestSemester) {
        setSelectedSemesterId(closestSemester.id);
      }
    }
  };

  useEffect(() => {
    selectedSemesterId && getGroupSubmissionBySemester(selectedSemesterId);
  }, [selectedSemesterId, refresh]);
  const getGroupSubmissionBySemester = async (semesterId) => {
    try {
      const res = await groupStore.getGroupSubmissionBySemester(semesterId);
      setGroupSubmission(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  const showModal = (files, group) => {
    setGroupSelected(group);
    setFileSelected(files);
    setIsModalVisibleAll(true);
  };

  const columns = [
    {
      title: "Group",
      render: (record) => record.group.groupCode,
    },
    {
      title: "Deadline",
      render: (record) => (
        <Text style={{ color: "red" }}>
          {record?.startSubmit
            ? `${moment(record.startSubmit).format(
                DD_MM_YYYY_HH_mm
              )} - ${moment(record.endSubmit).format(DD_MM_YYYY_HH_mm)}`
            : "Not set"}
        </Text>
      ),
    },
    {
      title: "Supervisor's approval",
      dataIndex: "approvedBySupervisor",
      key: "approvedBySupervisor",
      render: (approvedBySupervisor) => (
        <Text
          style={{
            color:
              approvedBySupervisor === "Approved"
                ? "black"
                : approvedBySupervisor === "Rejected"
                ? "red"
                : "orange",
          }}
        >
          {approvedBySupervisor === "APPROVED"
            ? "Confirm"
            : approvedBySupervisor === "REJECT"
            ? "Reject"
            : "Not Confirm"}
        </Text>
      ),
    },
    {
      title: "Directories",
      render: (record) => (
        <div>
          {record?.group && (
            <Button onClick={() => showModal(record?.files, record?.group)}>
              Group resource
            </Button>
          )}
        </div>
      ),
    },
    {
      title: "Group Comment",
      render: (record) => (
        <div>
          {record?.group && (
            <Button
              onClick={() => handleViewComment(record.group)}
              style={{
                backgroundColor: record?.comment ? "#4CAF50" : "#2196F3",
                color: "#fff",
              }}
            >
              {record?.comment ? "Open Comment" : "Create Comment"}
            </Button>
          )}
        </div>
      ),
    },
    {
      title: "Actions",
      render: (record) => (
        <div>
          {record?.approvedBySupervisor === "PENDING" && (
            <>
              <Button
                onClick={() => {
                  if (record.comment) {
                    showConfirmModal(record.group.id, "Approve");
                  } else {
                    notification.warning({
                      message: "Unable to approve group",
                      description:
                        "You must have a comment form before approving the group.",
                    });
                  }
                }}
              >
                Approve
              </Button>
              <Button
                onClick={() => {
                  if (record.comment) {
                    showConfirmModal(record.group.id, "Reject");
                  } else {
                    notification.warning({
                      message: "Unable to approve group",
                      description:
                        "You must have a comment form before approving the group.",
                    });
                  }
                }}
                style={{ marginLeft: 8, backgroundColor: "red" }}
              >
                Reject
              </Button>
            </>
          )}
        </div>
      ),
    },
  ];

  const showConfirmModal = (id, action) => {
    setGroupId(id);
    setActionType(action);
    setIsModalVisible(true);
  };
  const handleViewComment = (group) => {
    setIsCommentModalVisible(true);
    setGroupId(group?.id);
  };

  const handleOk = async () => {
    setIsModalVisible(false);
    loadingAnimationStore.showSpinner(true);
    try {
      if (actionType === "Approve") {
        await groupStore.confirmGroupSubmission(groupId);
      } else if (actionType === "Reject") {
        await groupStore.rejectGroupSubmission(groupId);
      }
      setRefresh(true);
    } catch (err) {
      console.log(err);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const handleOkAll = async () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleCancelAll = () => {
    setIsModalVisibleAll(false);
  };
  const handleDownloadAll = async (group, type) => {
    try {
      loadingAnimationStore.showSpinner(true);
      const downloadUrl = `${apiUrl}/api/v1/group/download/all/${group?.id}/${type}`;
      const response = await fetch(downloadUrl, {
        method: "GET",
        headers: {
          "Content-Type": "application/octet-stream",
        },
      });
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", `${group?.groupCode} - ${type}`);
      document.body.appendChild(link);
      link.click();
      link.remove();
      window.URL.revokeObjectURL(url);
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const handleDownload = async (file) => {
    try {
      loadingAnimationStore.showSpinner(true);
      const downloadUrl = `${apiUrl}/api/v1/group/download/file/${file.id}`;
      const response = await fetch(downloadUrl, {
        method: "GET",
        headers: {
          "Content-Type": "application/octet-stream",
        },
      });
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", file.filename);
      document.body.appendChild(link);
      link.click();
      link.remove();
      window.URL.revokeObjectURL(url);
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const renderFileList = (files) => {
    const filesByType = files.reduce((acc, file) => {
      const type = file.submissionType || "Others";
      if (!acc[type]) {
        acc[type] = [];
      }
      acc[type].push(file);
      return acc;
    }, {});
    return (
      <div>
        {Object.keys(filesByType).length > 0 ? (
          <div>
            {Object.entries(filesByType).map(([type, files]) => (
              <div key={type}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <span>{type.toUpperCase()}:</span>
                  <Button
                    icon={<DownloadOutlined />}
                    onClick={(e) => {
                      e.preventDefault();
                      handleDownloadAll(groupSelected, type);
                    }}
                  >
                    Download All
                  </Button>
                </div>
                <Divider />
                {files.map((file) => (
                  <div
                    key={file.id}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <div>{file.filename}</div>
                    <div>
                      <Button
                        icon={<DownloadOutlined />}
                        size="small"
                        onClick={(e) => {
                          e.preventDefault();
                          handleDownload(file);
                        }}
                      />
                    </div>
                  </div>
                ))}
                <Divider />
              </div>
            ))}
          </div>
        ) : (
          <div>
            <div style={{ textAlign: "center" }}>No files submitted yet.</div>
            <Divider />
          </div>
        )}
      </div>
    );
  };
  return (
    <DashboardLayout>
      <Helmet>
        <title>Capstone project submit</title>
      </Helmet>
      <ContentBlockWrapper>
        <div style={{ padding: "20px" }}>
          <Title level={3}>Capstone project submit</Title>
          <Paragraph>
            <Text strong>Permission:</Text> Supervisor
          </Paragraph>
          <div className="flex items-stretch gap-2 mb-5 bg-white  rounded-md">
            <div className="flex items-center justify-center">
              <Title level={5}>Semester</Title>
            </div>
            <Select
              value={selectedSemesterId}
              onChange={onSelectChange}
              style={{ width: "200px" }}
              options={semesters?.map((semester) => ({
                value: semester.id,
                label: semester.name,
              }))}
            />
          </div>
        </div>
        <div style={{ padding: "20px" }}>
          <Typography.Title level={4}>Secretary note:</Typography.Title>
          {groupSubmission && (
            <Table
              columns={columns}
              dataSource={groupSubmission}
              pagination={false}
            />
          )}
        </div>
        <Modal
          title="Confirm Action"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          okText={actionType}
          cancelText="Cancel"
        >
          <p>
            Are you sure you want to {actionType.toLowerCase()} this submission?
          </p>
        </Modal>
        <CPCommentFormPopup
          isModalVisible={isCommentModalVisible}
          setIsModalVisible={setIsCommentModalVisible}
          groupId={groupId}
          selectedSemesterId={selectedSemesterId}
          semesters={semesters}
        >
          <p>
            Are you sure you want to {actionType.toLowerCase()} this submission?
          </p>
        </CPCommentFormPopup>
        <Modal
          title={`Resource file`}
          visible={isModalVisibleAll}
          footer={null}
          onOk={handleOkAll}
          onCancel={handleCancelAll}
        >
          {renderFileList(filesSelected || [])}
        </Modal>
      </ContentBlockWrapper>
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "authenticationStore"
    )(observer(CapstoneProjectSubmitPage))
  )
);
