// App.js
import React, { memo, useState, useEffect } from "react";
import {
  Form,
  Input,
  Select,
  Table,
  Button,
  Modal,
  List,
  Radio,
  message,
} from "antd";
import { withRouter } from "react-router-dom/cjs/react-router-dom";
import { inject, observer } from "mobx-react";
import utils from "../../utils";

const { TextArea } = Input;
const { Option } = Select;

const CPCommentFormPopup = (props) => {
  const {
    history,
    isModalVisible,
    setIsModalVisible,
    groupId,
    groupStore,
    loadingAnimationStore,
    match,
    semesters,
    selectedSemesterId,
  } = props;
  const [form] = Form.useForm();

  const [group, setGroup] = useState([]);
  const [groupCpComment, setGroupCpComment] = useState();
  const [members, setMembers] = useState([]);

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  useEffect(() => {
    if (groupId) {
      getGroupInfo(groupId);
    }
  }, [groupId, isModalVisible]);

  const getGroupInfo = async (groupId) => {
    loadingAnimationStore.showSpinner(true);
    try {
      const res = await groupStore.getGroupByGroupId(groupId);
      form.setFieldsValue({
        vietnameseTitle: res.data.vietnameseTitle,
        englishTitle: res.data.name,
      });
      setGroup(res.data);
      setMembers(res.data.members);

      const res1 = await groupStore.getGroupCpCommentByGroupId(groupId);
      if (res1.data) {
        setGroupCpComment(res1.data);
        form.setFieldsValue({
          contentEvaluation: res1.data.contentEvaluation,
          formatEvaluation: res1.data.formatEvaluation,
          attitudeEvaluation: res1.data.attitudeEvaluation,
          achievementLevel: res1.data.achievementLevel,
          limitation: res1.data.limitation,
        });
        const membersMap = {};
        res.data.members.forEach((member) => {
          membersMap[member.student.id] = member;
        });
        res1.data.groupCpCommentStudents.forEach((student) => {
          if (membersMap[student.student.id]) {
            membersMap[student.student.id] = {
              ...membersMap[student.student.id],
              selectedOption: utils.mapStatusToOption(student.defenseStatus),
              defenseStatus: student.defenseStatus,
              note: student.note,
            };
          } else {
            membersMap[student.student.id] = {
              student: student.student,
              selectedOption: utils.mapStatusToOption(student.defenseStatus),
              defenseStatus: student.defenseStatus,
              note: student.note,
            };
          }
        });
        const updatedMembers = Object.values(membersMap);
        setMembers(updatedMembers);
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };

  const handleSubmit = async (values) => {
    const groupCpCommentRequest = {
      groupId: group.id,
      vietnameseTitle: values.vietnameseTitle,
      englishTitle: values.englishTitle,
      contentEvaluation: values.contentEvaluation,
      formatEvaluation: values.formatEvaluation,
      attitudeEvaluation: values.attitudeEvaluation,
      achievementLevel: values.achievementLevel,
      limitation: values.limitation,
      groupCpCommentStudents: members.map((member) => ({
        studentId: member.student.id,
        defenseStatus: member.defenseStatus,
        note: member.note,
      })),
    };
    if (groupCpComment) {
      await updateGroupCpComment(groupCpComment?.id, groupCpCommentRequest);
    } else {
      await addGroupCpComment(groupCpCommentRequest);
    }
  };
  const addGroupCpComment = async (groupCpCommentRequest) => {
    loadingAnimationStore.showSpinner(true);
    try {
      const res = await groupStore.addGroupCpComment(groupCpCommentRequest);
      if (res.status == 200) {
        message.success(
          "Create comment form successful for" + group?.groupCode
        );
        form.resetFields();
        setIsModalVisible(false);
        setMembers([]);
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const updateGroupCpComment = async (id, groupCpCommentRequest) => {
    loadingAnimationStore.showSpinner(true);
    try {
      const res = await groupStore.updateGroupCpComment(
        id,
        groupCpCommentRequest
      );
      if (res.status == 200) {
        message.success(
          "Update comment form successful for" + group?.groupCode
        );
        form.resetFields();
        setIsModalVisible(false);
        setMembers([]);
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const handleStudentEvaluationChange = (record, option) => {
    const updatedMembers = members.map((member) =>
      member.student.id === record.student.id
        ? {
            ...member,
            selectedOption: option,
            defenseStatus: utils.mapOptionToDefenseStatus(option),
          }
        : member
    );
    setMembers(updatedMembers);
  };

  const handleNoteChange = (record, note) => {
    const updatedMembers = members.map((member) =>
      member.student.id === record.student.id ? { ...member, note } : member
    );
    setMembers(updatedMembers);
  };

  const columns = [
    {
      title: "Roll",
      render: (record) =>
        utils.getRollNumberFromEmail(record?.student?.user?.email),
    },
    {
      title: "Name",
      render: (record) => record?.student?.user?.name,
    },
    {
      title: "Agree to defense",
      dataIndex: "agreeToDefense",
      key: "agreeToDefense",
      render: (text, record) => (
        <Radio
          name={`option-${record.student.id}`}
          checked={record.selectedOption === "agreeToDefense"}
          onChange={() =>
            handleStudentEvaluationChange(record, "agreeToDefense")
          }
        />
      ),
    },
    {
      title: "Review for the second defense",
      dataIndex: "reviewForSecondDefense",
      key: "reviewForSecondDefense",
      render: (text, record) => (
        <Radio
          name={`option-${record.student.id}`}
          checked={record.selectedOption === "reviewForSecondDefense"}
          onChange={() =>
            handleStudentEvaluationChange(record, "reviewForSecondDefense")
          }
        />
      ),
    },
    {
      title: "Disagree to defense",
      dataIndex: "disagreeToDefense",
      key: "disagreeToDefense",
      render: (text, record) => (
        <Radio
          name={`option-${record.student.id}`}
          checked={record.selectedOption === "disagreeToDefense"}
          onChange={() =>
            handleStudentEvaluationChange(record, "disagreeToDefense")
          }
        />
      ),
    },
    {
      title: "Note",
      dataIndex: "note",
      key: "note",
      render: (text, record) => (
        <Input
          value={record.note}
          onChange={(e) => handleNoteChange(record, e.target.value)}
        />
      ),
    },
  ];
  const getSemesterById = (id) => {
    return semesters.find((semester) => semester.id === id);
  };
  return (
    <Modal
      title={`Subject code: SEP490, Class: ${group?.groupCode} , Semester: ${
        getSemesterById(selectedSemesterId)?.code
      }`}
      visible={isModalVisible}
      onCancel={handleCancel}
      footer={[
        <Button
          key="submit"
          type="primary"
          onClick={() => form.submit()}
          className="m-3"
        >
          {groupCpComment
            ? "Update capstone project group comment "
            : "Submit capstone project group comment "}
        </Button>,
      ]}
      width={1000}
    >
      <Form layout="vertical" form={form} onFinish={handleSubmit}>
        <div className="flex gap-3">
          <div className="w-2/3">
            <label>1.Thesis title</label>
            <div className="ml-4">
              <Form.Item label="-Vietnamese" name="vietnameseTitle">
                <TextArea rows={1} readOnly />
              </Form.Item>
              <Form.Item label="-English" name="englishTitle">
                <TextArea rows={1} readOnly />
              </Form.Item>
            </div>
          </div>
          <div className="w-1/3">
            <Form.Item label="2.Student of thesis defense">
              <List
                style={{ minHeight: "150px" }}
                size="small"
                bordered
                dataSource={group?.members}
                renderItem={(item) => (
                  <List.Item>
                    {item?.student?.user?.name} -{" "}
                    {utils.getRollNumberFromEmail(item?.student?.user?.email)}
                  </List.Item>
                )}
              />
            </Form.Item>
          </div>
        </div>
        <div>
          <label>3.Comment from proposed Supervisor</label>
          <div className="ml-4">
            <Form.Item label="3.1 - Thesis content" name="contentEvaluation">
              <TextArea rows={3} />
            </Form.Item>
            <Form.Item label="3.2 - Thesis form" name="formatEvaluation">
              <TextArea rows={2} />
            </Form.Item>
            <Form.Item
              label="3.3 - Students' attitudes"
              name="attitudeEvaluation"
            >
              <TextArea rows={2} />
            </Form.Item>
          </div>
        </div>
        <div>
          <label>4.Conclusion: Pass at what stage? (Or not pass)</label>
          <div className="ml-4">
            <div className="flex gap-3">
              <div className="w-2/3">
                <Form.Item
                  label="4.1 - Achievement level"
                  name="achievementLevel"
                >
                  <TextArea rows={2} />
                </Form.Item>
              </div>
              <div className="w-1/3">
                <Form.Item label="4.2 - Limitation" name="limitation">
                  <TextArea rows={2} />
                </Form.Item>
              </div>
            </div>
          </div>
          <Table dataSource={members} columns={columns} pagination={false} />
        </div>
      </Form>
    </Modal>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "authenticationStore"
    )(observer(CPCommentFormPopup))
  )
);
