import React, { memo, useCallback, useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Button, Table, Typography, Select, Tooltip } from "antd";
import uuid from "uuid";
import DashboardLayout from "../../../layouts/DashboardLayout";
import ContentBlockWrapper from "../../../components/ContentBlockWrapper";
import { NoMarginBottom } from "./ManageGroupPageStyled";
import PageTitle from "../../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
import ViewProgress from "../../../components/ViewProgress/ViewProgress";
import moment from "moment";
import PopupImportExcel from "./PopupImportExcel";
import ViewAllEvaluationModal from "../ManageGroupProgressPage/ViewAllEvaluationModal";

const { Option } = Select;
const { Title } = Typography;

const ManageGroupPage = (props) => {
  const { history, loadingAnimationStore, groupStore, semesterStore } = props;

  const [selectedSemesterId, setSelectedSemesterId] = useState(null);
  const [semesters, setSemesters] = useState([]);
  const [groups, setGroups] = useState([]);
  const [isVisiblePopupImportExcel, setIsVisiblePopupImportExcel] = useState(
    false
  );
  const [visiblePopupAll, setVisiblePopupAll] = useState(false);
  useEffect(() => {
    getSemester();
  }, []);
  useEffect(() => {
    if (selectedSemesterId) {
      GetGroupListBySemester(selectedSemesterId);
    }
  }, [selectedSemesterId]);
  const getSemester = async () => {
    try {
      const res = await semesterStore.getSemesters();
      setSemesters(res.data);
      setSemesterCurrent(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  const GetGroupListBySemester = async (semesterId) => {
    try {
      const res = await groupStore.getGroupOfSupervisorBySemester(semesterId);
      setGroups(res.data);
    } catch (e) {
      console.log(e);
    }
  };

  const onSelectChange = (value) => {
    setSelectedSemesterId(value);
  };

  const setSemesterCurrent = (semesters) => {
    if (semesters.length > 0) {
      const currentDate = moment();
      const closestSemester = semesters.reduce((prev, curr) => {
        const startDate = moment(curr.beginAt);
        const endDate = moment(curr.endAt);
        if (currentDate.isBetween(startDate, endDate, null, "[]")) {
          return curr;
        } else {
          if (prev !== null) {
            const prevDiff = Math.abs(
              moment(prev.beginAt).diff(currentDate, "days")
            );
            const currDiff = Math.abs(startDate.diff(currentDate, "days"));
            return currDiff < prevDiff ? curr : prev;
          } else {
            return curr;
          }
        }
      }, null);
      if (closestSemester) {
        setSelectedSemesterId(closestSemester.id);
      }
    }
  };
  const [currentIndex, setCurrentIndex] = useState(null);
  const [selectedGroupId, setSelectedGroupId] = useState();
  const openPopup = (groupId) => {
    setSelectedGroupId(groupId);
    setVisiblePopupAll(true);
  };
  const columns = [
    {
      title: "Group Code",
      width: "100",
      render: (record) => record?.group.groupCode,
    },
    {
      title: "Group Name",
      width: "200",
      render: (record) => record?.group.name,
    },
    {
      title: "Vietnamese Title",
      width: "25%",
      render: (record) => record?.group.vietnameseTitle,
    },
    {
      title: "Action",
      width: "20%",
      align: "center",
      render: (record) => (
        <div className="flex justify-center gap-3">
          <Tooltip title="View group detail">
            <Button
              onClick={() =>
                history.push(`/guidance/group/progress/${record.group.id}`)
              }
            >
              View
            </Button>
          </Tooltip>
          {currentIndex > 5 && (
            <Tooltip title="View evaluation of group">
              <Button onClick={() => openPopup(record.group.id)}>
                View Evaluation
              </Button>
            </Tooltip>
          )}
        </div>
      ),
    },
  ];
  return (
    <DashboardLayout>
      <Helmet>
        <title>Manager Group</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"Manager group"}
        hiddenGoBack
      ></PageTitle>
      <ContentBlockWrapper>
        {selectedSemesterId && (
          <ViewProgress
            id={selectedSemesterId}
            setCurrentIndex={setCurrentIndex}
          />
        )}
        <div className="gap-5 items-start">
          <div
            className="bg-white flex items-center justify-between"
            style={{ backgroundColor: "unset" }}
          >
            <div className="flex items-stretch justify-between gap-2 bg-white py-5 rounded-md">
              <NoMarginBottom className="flex items-center justify-center">
                <Title level={5}>Semester</Title>
              </NoMarginBottom>
              <Select
                value={selectedSemesterId}
                onChange={onSelectChange}
                style={{ width: "200px" }}
                options={semesters?.map((semester) => ({
                  value: semester.id,
                  label: semester.name,
                }))}
              />
            </div>
            <Button type="primary" onClick={setIsVisiblePopupImportExcel}>
              Import group
            </Button>
          </div>
          <div className="bg-white pt-4 rounded-md">
            <Table
              columns={columns}
              dataSource={groups}
              rowKey={(record) => record.id || uuid()}
              pagination={false}
            />
          </div>
        </div>
        <PopupImportExcel
          isVisiblePopup={isVisiblePopupImportExcel}
          setIsVisiblePopup={setIsVisiblePopupImportExcel}
          handleClosePopup={() => setIsVisiblePopupImportExcel(false)}
        />
        <ViewAllEvaluationModal
          visible={visiblePopupAll}
          semesterId={selectedSemesterId}
          setVisiblePopup={setVisiblePopupAll}
          groupId={selectedGroupId}
          handleCancel={() => setVisiblePopupAll(false)}
        />
      </ContentBlockWrapper>
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore"
    )(observer(ManageGroupPage))
  )
);
