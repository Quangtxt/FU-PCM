// App.js
import React, { memo, useState, useEffect } from "react";
import {
  Form,
  Input,
  Select,
  Table,
  Button,
  Modal,
  List,
  Radio,
  message,
} from "antd";
import { withRouter } from "react-router-dom/cjs/react-router-dom";
import { inject, observer } from "mobx-react";
import utils from "../../../utils";

const { TextArea } = Input;
const { Option } = Select;

const TabComment = (props) => {
  const {
    history,
    isModalVisible,
    setIsModalVisible,
    group,
    groupStore,
    loadingAnimationStore,
    match,
  } = props;
  const [form] = Form.useForm();
  const [members, setMembers] = useState([]);

  useEffect(() => {
    if (group) {
      getGroupInfo(group?.id);
    }
  }, [group]);

  const getGroupInfo = async () => {
    loadingAnimationStore.showSpinner(true);
    try {
      form.setFieldsValue({
        vietnameseTitle: group.vietnameseTitle,
        englishTitle: group.name,
      });
      setMembers(group.members);

      const res1 = await groupStore.getGroupCpCommentByGroupId(group?.id);
      if (res1.data) {
        form.setFieldsValue({
          contentEvaluation: res1.data.contentEvaluation,
          formatEvaluation: res1.data.formatEvaluation,
          attitudeEvaluation: res1.data.attitudeEvaluation,
          achievementLevel: res1.data.achievementLevel,
          limitation: res1.data.limitation,
        });
        const membersMap = {};
        group.members.forEach((member) => {
          membersMap[member.student.id] = member;
        });
        res1.data.groupCpCommentStudents.forEach((student) => {
          if (membersMap[student.student.id]) {
            membersMap[student.student.id] = {
              ...membersMap[student.student.id],
              selectedOption: utils.mapStatusToOption(student.defenseStatus),
              defenseStatus: student.defenseStatus,
              note: student.note,
            };
          } else {
            membersMap[student.student.id] = {
              student: student.student,
              selectedOption: utils.mapStatusToOption(student.defenseStatus),
              defenseStatus: student.defenseStatus,
              note: student.note,
            };
          }
        });
        const updatedMembers = Object.values(membersMap);
        setMembers(updatedMembers);
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const columns = [
    {
      title: "Roll",
      render: (record) =>
        utils.getRollNumberFromEmail(record?.student?.user?.email),
    },
    {
      title: "Name",
      render: (record) => record?.student?.user?.name,
    },
    {
      title: "Agree to defense",
      dataIndex: "agreeToDefense",
      render: (text, record) => (
        <Radio
          name={`option-${record.student.id}`}
          checked={record.selectedOption === "agreeToDefense"}
          disabled
        />
      ),
    },
    {
      title: "Review for the second defense",
      dataIndex: "reviewForSecondDefense",
      render: (text, record) => (
        <Radio
          name={`option-${record.student.id}`}
          checked={record.selectedOption === "reviewForSecondDefense"}
          disabled
        />
      ),
    },
    {
      title: "Disagree to defense",
      dataIndex: "disagreeToDefense",
      render: (text, record) => (
        <Radio
          name={`option-${record.student.id}`}
          checked={record.selectedOption === "disagreeToDefense"}
          disabled
        />
      ),
    },
    {
      title: "Note",
      dataIndex: "note",
      render: (text, record) => <Input value={record.note} readOnly />,
    },
  ];

  return (
    <Form layout="vertical" form={form}>
      <div className="flex gap-3">
        <div className="w-2/3">
          <label>1.Thesis title</label>
          <div className="ml-4">
            <Form.Item label="-Vietnamese" name="vietnameseTitle">
              <TextArea rows={1} readOnly />
            </Form.Item>
            <Form.Item label="-English" name="englishTitle">
              <TextArea rows={1} readOnly />
            </Form.Item>
          </div>
        </div>
        <div className="w-1/3">
          <Form.Item label="2.Student of thesis defense">
            <List
              style={{ minHeight: "150px" }}
              size="small"
              bordered
              dataSource={group?.members}
              renderItem={(item) => (
                <List.Item>
                  {item?.student?.user?.name} -{" "}
                  {utils.getRollNumberFromEmail(item?.student?.user?.email)}
                </List.Item>
              )}
            />
          </Form.Item>
        </div>
      </div>
      <div>
        <label>3.Comment from proposed Supervisor</label>
        <div className="ml-4">
          <Form.Item label="3.1 - Thesis content" name="contentEvaluation">
            <TextArea rows={3} readOnly />
          </Form.Item>
          <Form.Item label="3.2 - Thesis form" name="formatEvaluation">
            <TextArea rows={2} readOnly />
          </Form.Item>
          <Form.Item
            label="3.3 - Students' attitudes"
            name="attitudeEvaluation"
          >
            <TextArea rows={2} readOnly />
          </Form.Item>
        </div>
      </div>
      <div>
        <label>4.Conclusion: Pass at what stage? (Or not pass)</label>
        <div className="ml-4">
          <div className="flex gap-3">
            <div className="w-2/3">
              <Form.Item
                label="4.1 - Achievement level"
                name="achievementLevel"
              >
                <TextArea rows={2} readOnly />
              </Form.Item>
            </div>
            <div className="w-1/3">
              <Form.Item label="4.2 - Limitation" name="limitation">
                <TextArea rows={2} readOnly />
              </Form.Item>
            </div>
          </div>
        </div>
        <Table dataSource={members} columns={columns} pagination={false} />
      </div>
    </Form>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "authenticationStore"
    )(observer(TabComment))
  )
);
