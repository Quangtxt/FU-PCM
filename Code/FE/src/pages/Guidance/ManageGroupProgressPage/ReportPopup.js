import React, { useState } from "react";
import { Modal, Button, notification } from "antd";
import { memo } from "react";
import { withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import mammoth from "mammoth";

const ReportPopup = ({
  currentMilestone,
  group,
  handleCancel,
  scoreStore,
  loadingAnimationStore,
}) => {
  const [docContent, setDocContent] = useState("");
  const [visible, setVisible] = useState(false);
  const [downloadLink, setDownloadLink] = useState("");

  const fetchDocContent = async () => {
    try {
      const response = await fetch(
        "https://gitlab.com/api/v4/projects/57799309/repository/files/Report%2FReport2_Project%20Management%20Plan.docx?ref=main"
      );
      const data = await response.json();
      const decodedContent = atob(data.content);
      const arrayBuffer = Uint8Array.from(decodedContent, (c) =>
        c.charCodeAt(0)
      ).buffer;

      mammoth
        .convertToHtml({ arrayBuffer: arrayBuffer })
        .then((result) => {
          setDocContent(result.value);
          setVisible(true); // Show the modal after content is ready
        })
        .catch((err) => {
          console.error("Error converting DOCX to HTML:", err);
        });

      // Create a download link for the file
      const blob = new Blob([arrayBuffer], {
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      });
      const url = URL.createObjectURL(blob);
      setDownloadLink(url);

      // Notify user that the file has been downloaded
      notification.success({
        message: "Download Complete",
        description: "Click the button below to open the file manually.",
      });
    } catch (error) {
      console.error("Error fetching document:", error);
    }
  };

  return (
    <>
      <Button type="primary" onClick={fetchDocContent}>
        Download and View Report
      </Button>
      <Modal
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        title={`Report preview`}
        width={1000}
      >
        <div dangerouslySetInnerHTML={{ __html: docContent }} />
        <a href={downloadLink} download="Report2_Project_Management_Plan.docx">
          <Button type="primary" style={{ marginTop: 20 }}>
            Download File
          </Button>
        </a>
      </Modal>
    </>
  );
};

export default memo(
  withRouter(
    inject("loadingAnimationStore", "scoreStore")(observer(ReportPopup))
  )
);
