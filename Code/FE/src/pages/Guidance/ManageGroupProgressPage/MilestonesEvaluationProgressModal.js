import React, { useState, memo, useEffect } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Modal, Table, Input, message, Select } from "antd";
import { DATE_FORMAT_SLASH, MEMBER_STATUS } from "../../../constants";

const MilestoneEvaluationProgressModal = ({
  visible,
  setVisiblePopup,
  currentMilestone,
  semesterStore,
  group,
  handleCancel,
  milestoneStatus,
}) => {
  useEffect(() => {
    const initialSelected = {};
    const initialStudentStatus = {};
    milestoneStatus?.forEach((status) => {
      initialSelected[status.milestoneId] = status.status;
      initialStudentStatus[status.milestoneId] = status.studentRequestGrading;
    });
    setSelectedRecords(initialSelected);
    setStudentStatus(initialStudentStatus);
  }, [milestoneStatus]);

  const [selectedRecords, setSelectedRecords] = useState({});
  const [studentStatus, setStudentStatus] = useState({});

  const handleStatusChange = (record, newStatus) => {
    setSelectedRecords((prev) => ({
      ...prev,
      [record.id]: newStatus,
    }));
  };

  const handleSubmit = async () => {
    const results = Object.entries(selectedRecords).map(([id, status]) => ({
      milestoneId: id,
      status: status,
    }));
    try {
      await semesterStore.setProcessOfMilestone(group.id, results);
      message.success("Status updated successfully");
      setVisiblePopup(false);
    } catch (e) {
      console.log(e);
    }
    console.log(results);
  };
  const column2 = [
    {
      title: "Product",
      dataIndex: "product",
      key: "product",
      width: 155,
    },
    {
      title: "To/Deadline",
      render: (record) => record.toDate.format(DATE_FORMAT_SLASH),
      width: 160,
    },
    {
      title: "Done",
      key: "done",
      render: (record) =>
        studentStatus[record.id] ? (
          <Select
            value={selectedRecords[record.id] || "Not set"}
            onChange={(value) => handleStatusChange(record, value)}
            style={{ width: "200px" }}
          >
            <Select.Option value="DONE">Done</Select.Option>
            <Select.Option value="REDO">Redo</Select.Option>
          </Select>
        ) : (
          <div>Not request</div>
        ),
      width: 100,
    },
  ].filter(Boolean);
  const column1 = [
    {
      title: "Inter",
      dataIndex: "name",
      key: "name",
    },
  ].filter(Boolean);
  const [autoExpandAll, setAutoExpandAll] = useState(true);
  return (
    <Modal
      visible={visible}
      onOk={handleSubmit}
      onCancel={handleCancel}
      title={`Mark for ${currentMilestone?.name}`}
      cancelText="Cancel"
      okText="Submit"
      width={800}
    >
      {currentMilestone?.id == 10 ? (
        <Table
          columns={column1}
          dataSource={currentMilestone?.detail}
          rowKey="id"
          pagination={false}
          expandable={{
            expandedRowRender: (record) => (
              <Table
                columns={column2}
                dataSource={record?.detail}
                rowKey={(record) => record.id || uuid()}
                expandable={false}
                pagination={false}
                showHeader={true}
              />
            ),
            expandIconColumnIndex: 0,
            expandedRowKeys: autoExpandAll
              ? currentMilestone?.detail.map((record) => record.id)
              : [],
            onExpand: (expanded, record) => {
              if (!autoExpandAll) {
                if (expanded) {
                  setExpandedRows([...expandedRows, record.id]);
                } else {
                  setExpandedRows(
                    expandedRows.filter((id) => id !== record.id)
                  );
                }
              }
            },
          }}
        />
      ) : (
        <Table
          columns={column2}
          dataSource={currentMilestone?.detail}
          rowKey="id"
          pagination={false}
        />
      )}
    </Modal>
  );
};
export default memo(
  withRouter(
    inject(
      "loadingAnimationStore",
      "scoreStore",
      "semesterStore"
    )(observer(MilestoneEvaluationProgressModal))
  )
);
