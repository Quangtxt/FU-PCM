import React, { useState, memo, useEffect, useCallback } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Modal, Table, message, Tabs, List, Radio, Form, Input } from "antd";
import utils from "../../../utils";
import moment from "moment";
import TabComment from "./TabComment";
const { TabPane } = Tabs;

const { TextArea } = Input;
const ViewAllEvaluationModal = ({
  visible,
  setVisiblePopup,
  groupId,
  semesterId,
  handleCancel,
  scoreStore,
  semesterStore,
  groupStore,
  loadingAnimationStore,
}) => {
  const [scores, setScores] = useState(null);
  const [group, setGroup] = useState();
  const [comments, setComments] = useState({});
  const [evaluation, setEvaluation] = useState();

  const [report, setReport] = useState([]);
  useEffect(() => {
    const getGroupInfo = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await groupStore.getGroupByGroupId(groupId);
        setGroup(res.data);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    groupId && getGroupInfo();
  }, [groupId]);
  useEffect(() => {
    semesterId && getReports(semesterId);
  }, [semesterId]);
  const getReports = async (semesterId) => {
    try {
      const res = await semesterStore.getReports(semesterId);
      setReport(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    const getScores = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await scoreStore.getScoreByGroupId(groupId);
        setEvaluation(res.data);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    visible && groupId && getScores();
  }, [visible, groupId]);
  useEffect(() => {
    const getScore = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const newScores = {};
        const newComments = {};
        evaluation?.forEach((score) => {
          if (!newScores[score.member.id]) {
            newScores[score.member.id] = {};
            newComments[score.member.id] = {};
          }
          newScores[score.member.id][score.report?.id] = score.score;
          newComments[score.member.id] = score.comment;
        });
        setScores(newScores);
        setComments(newComments);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    if (evaluation) {
      getScore();
    }
  }, [evaluation]);

  const [exportLoading, setExportLoading] = useState(false);

  const exportScore = useCallback(async () => {
    setExportLoading(true);

    const col = scores && tableColumns.map((col) => col.title);
    let rows = [];
    rows.push(col);
    try {
      setExportLoading(true);
      if (group?.members) {
        group?.members.map((member) => {
          rows.push([
            member?.student?.user?.name,
            utils.getRollNumberFromEmail(member?.student?.user?.email),
            ...report?.map((report) => scores?.[member.id]?.[report.id] || ""),
            report
              .reduce((sum, report) => {
                const score = scores?.[member.id]?.[report.id] || 0;
                const percent = report?.percentScore || 0;
                return sum + (score * percent) / 100;
              }, 0)
              .toFixed(2),
          ]);
        });
      }
      utils.exportExcel(
        rows,
        `Score - ${group?.name} ` + moment().format("YYYY-MM-DD")
      );
    } catch (error) {
      console.log(error);
      message.error(error.en || "An error occurred!");
    } finally {
      setExportLoading(false);
    }
  }, [group, scores, report]);
  return (
    <Modal
      visible={visible}
      onCancel={handleCancel}
      onOk={handleCancel}
      title={`Evaluation - ` + group?.groupCode}
      cancelText="Cancel"
      width={1300}
    >
      <Tabs defaultActiveKey="tab1">
        <TabPane tab="Score" key="tab1">
          <Table
            columns={[
              {
                title: "Member Name",
                render: (_, member) => member?.student?.user?.name,
                width: 100,
              },
              {
                title: "Comment",
                dataIndex: "comment",
                key: "comment",
                width: 100,
                render: (_, member) => comments[member.id],
              },
              ...report?.map((report) => ({
                title: (
                  <div
                    style={{
                      whiteSpace: "wrap",
                      textOverflow: "ellipsis",
                      maxWidth: "100px",
                    }}
                  >
                    {`${report?.name} - ${report?.percentScore}%`}
                  </div>
                ),
                key: report?.id,
                width: 100,
                render: (_, member) => (
                  <div>{scores?.[member?.id]?.[report.id]}</div>
                ),
              })),
              scores && {
                title: "Total",
                render: (_, member) => {
                  const totalScore = report.reduce((sum, report) => {
                    const score = scores?.[member.id]?.[report.id] || 0;
                    const percent = report?.percentScore || 0;
                    return sum + (score * percent) / 100;
                  }, 0);
                  return totalScore.toFixed(2);
                },
              },
            ].filter(Boolean)}
            dataSource={group?.members}
            rowKey="id"
            pagination={false}
          />
        </TabPane>
        <TabPane tab="Comment" key="tab2">
          <TabComment group={group} />
        </TabPane>
      </Tabs>
      {/* <Button
        loading={exportLoading}
        onClick={exportScore}
        style={{ marginLeft: "10px" }}
      >
        Export excel
      </Button> */}
    </Modal>
  );
};
export default memo(
  withRouter(
    inject(
      "loadingAnimationStore",
      "scoreStore",
      "semesterStore",
      "groupStore"
    )(observer(ViewAllEvaluationModal))
  )
);
