import { message } from "antd";
import { inject, observer } from "mobx-react";
import React, { memo, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { withRouter } from "react-router-dom/cjs/react-router-dom";

const DownloadReportPage = (props) => {
  const { id, groupId } = useParams();
  const {
    loadingAnimationStore,
    groupStore,
    semesterStore,
    scoreStore,
    match,
  } = props;
  const [report, setReport] = useState();
  const [group, setGroup] = useState();

  const history = useHistory();
  useEffect(() => {
    const getGroupInfo = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await groupStore.getGroupByGroupId(groupId);
        setGroup(res.data);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    getGroupInfo();
  }, [groupId]);
  useEffect(() => {
    id && getReports(id);
  }, [id]);
  const getReports = async (id) => {
    try {
      const res = await semesterStore.getReportByMilestoneId(id);
      setReport(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    const fileName = `${report?.name}.docx`;
    const checkAndDownloadFile = async () => {
      try {
        const commitResponse = await fetch(
          `https://gitlab.com/api/v4/projects/${group?.gitId}/repository/files/Report%2F${report?.name}.docx?ref=main`
        );
        if (commitResponse.status == 404) {
          message.error("File not found in git");
          history.goBack();
        } else {
          const commitData = await commitResponse.json();
          const currentCommitId = commitData.commit_id;

          const storedFileInfo = JSON.parse(localStorage.getItem(fileName));
          const storedCommitId = storedFileInfo
            ? storedFileInfo.commit_id
            : null;

          if (storedCommitId === currentCommitId) {
            message.warn("File not change");
            history.goBack();
          } else {
            const response = await fetch(
              `https://gitlab.com/api/v4/projects/57799309/repository/files/Report%2F${report?.name}.docx/raw?ref=main`
            );
            if (response.status == 404) {
              message.error("File not found in git");
              history.goBack();
            }
            const arrayBuffer = await response.arrayBuffer();
            const blob = new Blob([arrayBuffer], {
              type:
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            });
            const url = window.URL.createObjectURL(blob);

            const a = document.createElement("a");
            a.href = url;
            a.download = fileName;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);

            const fileInfo = {
              commit_id: currentCommitId,
              downloadedAt: new Date().toISOString(),
            };
            localStorage.setItem(fileName, JSON.stringify(fileInfo));

            history.goBack();
          }
        }
      } catch (error) {
        console.error("Lỗi khi tải file:", error);
      }
    };

    report && group && checkAndDownloadFile();
  }, [id, history, report, group]);

  return (
    <div>
      <p>File của bạn đang được tải xuống...</p>
    </div>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "scoreStore"
    )(observer(DownloadReportPage))
  )
);
