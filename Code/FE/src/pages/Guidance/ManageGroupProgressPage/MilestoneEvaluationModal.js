import React, { useState, memo, useEffect } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Modal, Table, Input, message } from "antd";

const MilestoneEvaluationModal = ({
  visible,
  setVisiblePopup,
  currentMilestone,
  report,
  group,
  evaluation,
  handleCancel,
  scoreStore,
  loadingAnimationStore,
  semesterId,
}) => {
  const [scores, setScores] = useState(null);
  const [comments, setComments] = useState({});
  const [reasons, setReasons] = useState({});
  const [isUpdate, setIsUpdate] = useState(false);
  const [isValidateScore, setIsValidateScore] = useState(true);

  useEffect(() => {
    const getScore = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await scoreStore.getScore(
          group?.id,
          currentMilestone?.id,
          semesterId
        );
        const newScores = {};
        const newComments = {};
        const eva = currentMilestone?.id === 12 ? evaluation : res.data;
        eva.forEach((score) => {
          if (!newScores[score.member.id]) {
            newScores[score.member.id] = {};
            newComments[score.member.id] = {};
          }
          newScores[score.member.id][score.report?.id] = score.score;
          newComments[score.member.id] = score.comment;
        });
        setScores(newScores);
        setComments(newComments);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    if (visible) {
      getScore();
    }
  }, [currentMilestone, visible]);
  const handleMarkChange = (e, memberId, reportId) => {
    setScores({
      ...scores,
      [memberId]: {
        ...scores[memberId],
        [reportId]: parseFloat(e.target.value),
      },
    });
    setIsUpdate(true);
  };
  const handleReasonChange = (e, memberId, reportId) => {
    setReasons((prevReasons) => ({
      ...prevReasons,
      [memberId]: {
        ...(prevReasons[memberId] || {}),
        [reportId]: e.target.value,
      },
    }));
  };
  const handleMarkValidation = (e, memberId) => {
    const value = parseFloat(e.target.value);
    if (value < 0 || value > 10) {
      message.error("Mark should be between 0 and 10");
      setIsUpdate(false);
      setIsValidateScore(false);
    } else {
      setIsUpdate(true);
      setIsValidateScore(true);
    }
  };
  const handleCommentChange = (e, id) => {
    if (e.target.value) {
      setComments({
        ...comments,
        [id]: e.target.value,
      });
      setIsUpdate(true);
    }
  };

  const handleEvaluate = async () => {
    const scoreEvaluateRequests = Object.keys(scores).flatMap((memberId) => {
      return Object.keys(scores[memberId]).map((reportId) => ({
        memberId: memberId,
        reportId: reportId,
        score: scores[memberId][reportId],
        comment: comments[memberId] ?? "",
        reason: reasons?.[memberId]?.[reportId] || "",
      }));
    });
    try {
      await scoreStore.saveEvaluation(scoreEvaluateRequests);
      message.success("Scores updated successfully");
      setVisiblePopup(false);
    } catch (e) {
      console.log(e);
    }
  };
  const lastMilestone = currentMilestone?.id === 12;
  const currentReports = lastMilestone
    ? report
    : report?.filter((r) => r.milestoneSubmit.id === currentMilestone?.id);

  return (
    <Modal
      visible={visible}
      onOk={handleEvaluate}
      onCancel={handleCancel}
      title={`Mark for ${currentMilestone?.name}`}
      okText={scores?.length > 0 ? "Update evaluation member" : "Evaluate"}
      cancelText="Cancel"
      okButtonProps={{ disabled: !(isUpdate && isValidateScore) }}
      width={currentReports?.length > 1 ? 1200 : undefined}
    >
      <Table
        columns={[
          {
            title: "Member Name",
            render: (_, member) => member?.student?.user?.name,
          },
          {
            title: "Comment",
            dataIndex: "comment",
            key: "comment",
            render: (_, member) => (
              <Input
                placeholder="Enter comment"
                value={comments[member.id] || ""}
                onChange={(e) => handleCommentChange(e, member.id)}
              />
            ),
          },
          ...currentReports?.map((report) => ({
            title: (
              <div
                style={{
                  whiteSpace: "wrap",
                  textOverflow: "ellipsis",
                  maxWidth: lastMilestone && "100px",
                }}
              >
                {`${report?.name} - ${report?.percentScore}%`}
              </div>
            ),
            key: report?.id,
            render: (_, member) => (
              <div>
                <Input
                  placeholder="Enter mark"
                  value={scores?.[member?.id]?.[report.id] || ""}
                  onChange={(e) => handleMarkChange(e, member.id, report.id)}
                  type="number"
                  step="0.01"
                  min="0"
                  max="10"
                  onBlur={(e) => handleMarkValidation(e, member.id)}
                />
                {scores?.[member?.id]?.[report.id] && (
                  <Input
                    placeholder="Enter reason fix score"
                    onBlur={(e) => handleReasonChange(e, member.id, report.id)}
                  />
                )}
              </div>
            ),
          })),
          lastMilestone &&
            scores && {
              title: "Total",
              render: (_, member) => {
                const totalScore = currentReports.reduce((sum, report) => {
                  const score = scores?.[member.id]?.[report.id] || 0;
                  const percent = report?.percentScore || 0;
                  return sum + (score * percent) / 100;
                }, 0);
                return totalScore.toFixed(2);
              },
            },
        ].filter(Boolean)}
        dataSource={group?.members}
        rowKey="id"
        pagination={false}
      />
    </Modal>
  );
};
export default memo(
  withRouter(
    inject(
      "loadingAnimationStore",
      "scoreStore"
    )(observer(MilestoneEvaluationModal))
  )
);
