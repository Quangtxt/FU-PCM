import React, { memo, useCallback, useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import {
  Button,
  Table,
  Typography,
  Select,
  Progress,
  Drawer,
  Collapse,
  Row,
  Col,
  Modal,
  Input,
} from "antd";
import uuid from "uuid";
import DashboardLayout from "../../../layouts/DashboardLayout";
import ContentBlockWrapper from "../../../components/ContentBlockWrapper";
import PageTitle from "../../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
import ViewProgress from "../../../components/ViewProgress/ViewProgress";
import moment from "moment";
import utils from "../../../utils";
import { DATE_FORMAT_SLASH, MEMBER_STATUS } from "../../../constants";
import MilestoneEvaluationModal from "./MilestoneEvaluationModal";
import MilestonesEvaluationProgressModal from "./MilestonesEvaluationProgressModal";
const { Panel } = Collapse;
const { TextArea } = Input;

const ManageGroupProgressPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    semesterStore,
    scoreStore,
    match,
  } = props;
  const [data, setData] = useState([]);
  const { id } = match.params;
  const [members, setMembers] = useState([]);
  const [group, setGroup] = useState();
  const [report, setReport] = useState([]);
  const [evaluation, setEvaluation] = useState();
  const [visiblePopup, setVisiblePopup] = useState(false);
  const [
    visiblePopupEvaluateProgress,
    setVisiblePopupEvaluateProgress,
  ] = useState(false);
  const [visiblePopupAll, setVisiblePopupAll] = useState(false);
  const [currentMilestone, setCurrentMilestone] = useState("");
  const [milestoneStatus, setMilestoneStatus] = useState();
  useEffect(() => {
    const getGroupInfo = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await groupStore.getGroupByGroupId(id);
        setGroup(res.data);
        setMembers(res.data.members);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    getGroupInfo();
  }, [id]);
  useEffect(() => {
    const getScores = async () => {
      loadingAnimationStore.showSpinner(true);
      try {
        const res = await scoreStore.getScoreByGroupId(id);
        setEvaluation(res.data);
      } catch (err) {
        console.log(err);
        loadingAnimationStore.showSpinner(false);
      } finally {
        loadingAnimationStore.showSpinner(false);
      }
    };
    getScores();
  }, [visiblePopup]);
  const [selectedSemesterId, setSelectedSemesterId] = useState(null);
  useEffect(() => {
    getSemester();
  }, []);
  useEffect(() => {
    selectedSemesterId && getReports(selectedSemesterId);
  }, [selectedSemesterId]);
  const getSemester = async () => {
    try {
      const res = await semesterStore.getSemesters();
      setSemesterCurrent(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  const getReports = async (selectedSemesterId) => {
    try {
      const res = await semesterStore.getReports(selectedSemesterId);
      setReport(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  const setSemesterCurrent = (semesters) => {
    if (semesters.length > 0) {
      const currentDate = moment();
      const closestSemester = semesters.reduce((prev, curr) => {
        const startDate = moment(curr.beginAt);
        const endDate = moment(curr.endAt);
        if (currentDate.isBetween(startDate, endDate, null, "[]")) {
          return curr;
        } else {
          if (prev !== null) {
            const prevDiff = Math.abs(
              moment(prev.beginAt).diff(currentDate, "days")
            );
            const currDiff = Math.abs(startDate.diff(currentDate, "days"));
            return currDiff < prevDiff ? curr : prev;
          } else {
            return curr;
          }
        }
      }, null);
      if (closestSemester) {
        setData(utils.transformData(closestSemester.milestones));
        setSelectedSemesterId(closestSemester.id);
      }
    }
  };
  const currentDate = moment();

  const isCurrentMilestone = (record) => {
    return currentDate.isBetween(record.fromDate, record.toDate, "day", "[]");
  };
  const [progressStatus, setProgressStatus] = useState({});

  useEffect(() => {
    const fetchProgressStatus = async () => {
      const progressData = await Promise.all(
        data[4]?.detail.map(async (record) => ({
          id: record.id,
          progress: await getProgressStatus(record),
        }))
      );
      setProgressStatus(
        progressData.reduce((acc, curr) => {
          acc[curr.id] = curr.progress;
          return acc;
        }, {})
      );
    };
    data?.length > 0 && fetchProgressStatus();
  }, [data, id, visiblePopupEvaluateProgress, group]);
  const columnMilestoneGuidance = [
    {
      title: "#",
      dataIndex: "key",
      key: "key",
      width: 50,
    },
    {
      title: "Milestone",
      dataIndex: "name",
      key: "name",
      width: 200,
    },
    {
      title: "From",
      render: (record) => record.fromDate.format(DATE_FORMAT_SLASH),
      width: 150,
    },
    {
      title: "To/Deadline",
      render: (record) => record.toDate.format(DATE_FORMAT_SLASH),
      width: 150,
    },
    {
      title: "Status",
      width: 250,
      render: (record) => (
        <div className="flex">
          <Progress percent={progressStatus[record.id] || 0} />
        </div>
      ),
    },
    {
      title: "Evaluate progress",
      render: (record) => (
        <div className="flex">
          <Button
            type="primary"
            onClick={() => handleOpenPopupEvaluateProgress(record)}
          >
            Evaluate
          </Button>
        </div>
      ),
    },
    {
      title: "Mark",
      width: 200,
      render: (record) => {
        const isMarked = report?.some(
          (item) =>
            item.milestoneSubmit.id === record.id &&
            evaluation.some(
              (evaluation) =>
                evaluation.report.id === item.id && evaluation.score
            )
        );
        return (
          <div>
            {record?.id != 12 ? (
              report.some((item) => item.milestoneSubmit.id === record.id) &&
              (record.toDate > new Date() ? (
                <Button
                  type="primary"
                  onClick={() => handleOpenPopup(record)}
                  // disabled
                >
                  Mark
                </Button>
              ) : (
                <Button
                  type={isMarked ? "default" : "primary"}
                  onClick={() => handleOpenPopup(record)}
                  style={
                    isMarked ? { backgroundColor: "green", color: "white" } : {}
                  }
                >
                  {isMarked ? "Re-mark score" : "Mark score"}
                </Button>
              ))
            ) : record.toDate > new Date().setDate(new Date().getDate() - 7) ? (
              <Button
                type="primary"
                onClick={() => handleOpenPopup(record)}
                // disabled
              >
                Mark
              </Button>
            ) : (
              <Button
                type={isMarked ? "default" : "primary"}
                onClick={() => handleOpenPopup(record)}
                style={
                  isMarked ? { backgroundColor: "green", color: "white" } : {}
                }
              >
                {isMarked ? "Re-mark" : "Mark"}
              </Button>
            )}
          </div>
        );
      },
    },
  ];
  async function getProgressStatus(record) {
    if (group) {
      try {
        const response = await semesterStore.getProcessOfMilestone(
          group?.id,
          record?.id
        );
        if (response.status == 200) {
          setMilestoneStatus(response.data);
          const { data } = response;
          let milestoneDetails = record?.detail || [];
          if (record?.name === "Implementation") {
            milestoneDetails = milestoneDetails.reduce(
              (acc, milestone) => acc.concat(milestone.detail || []),
              []
            );
          }

          const completedMilestones = milestoneDetails.filter(
            (milestone) =>
              data.find((m) => m.milestoneId === milestone.id)?.status ===
              "DONE"
          );
          const totalMilestones = milestoneDetails.length;
          const progressStatus =
            (completedMilestones.length / totalMilestones) * 100;

          return progressStatus;
        } else {
          return 0;
        }
      } catch (error) {
        console.error(error);
        return 0;
      }
    }
  }
  const getMilestoneStatus = (recordId) => {
    const milestone = milestoneStatus?.find(
      (item) => item.milestoneId === recordId
    );
    return milestone ? milestone.status : false;
  };
  const columnMilestone2 = [
    {
      title: "#",
      width: 45,
    },
    {
      title: "Product",
      dataIndex: "product",
      key: "product",
      width: 155,
    },
    {
      title: "From",
      render: (record) => record.fromDate.format(DATE_FORMAT_SLASH),
      width: 145,
    },
    {
      title: "To/Deadline",
      render: (record) => record.toDate.format(DATE_FORMAT_SLASH),
      width: 160,
    },
    {
      title: "submit",
      render: (record) => (
        <div className="flex">
          {record.product.toLowerCase().includes("report") ? (
            getMilestoneStatus(record.id) ? (
              <div>
                submitted -
                <Button
                  className="ml-2"
                  onClick={() => handleViewReport(record)}
                >
                  Download in git
                </Button>
              </div>
            ) : (
              "Not submitted"
            )
          ) : (
            ""
          )}
        </div>
      ),
    },
  ];

  const handleOpenPopup = (milestone) => {
    setCurrentMilestone(milestone);
    setVisiblePopup(true);
  };
  const handleOpenPopupEvaluateProgress = (milestone) => {
    setCurrentMilestone(milestone);
    setVisiblePopupEvaluateProgress(true);
  };

  const handleCancel = () => {
    setVisiblePopup(false);
  };
  const handleCancelEvaluateProgress = () => {
    setVisiblePopupEvaluateProgress(false);
  };

  const handleViewReport = (record) => {
    history.push(`/download-report/${group?.id}/${record.id}`);
  };
  return (
    <DashboardLayout>
      <Helmet>
        <title>Manager Group</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"Manager group"}
        hiddenGoBack
      ></PageTitle>
      <ContentBlockWrapper>
        {selectedSemesterId && (
          <ViewProgress
            id={selectedSemesterId}
            group={group}
            members={members}
          />
        )}

        <div className="">
          <div className="border rounded-md shadow-md m-4 w-1/2 mx-auto">
            <div className="p-4 flex items-center justify-center">
              <div>GROUP: {group?.groupCode}</div>
            </div>
            <div className="flex items-center justify-center">
              <div className="p-4">English name:{group?.name}</div>
              <div className="p-4">
                Vietnamese name: {group?.vietnameseTitle}
              </div>
            </div>
          </div>
        </div>
        {report.length > 0 && (
          <Table
            columns={columnMilestoneGuidance}
            dataSource={data[4]?.detail}
            rowKey={(record) => record.id || uuid()}
            pagination={false}
            expandable={{
              expandedRowRender: (record) => (
                <>
                  {record?.detail[0]?.name == null ? (
                    <Table
                      columns={columnMilestone2}
                      dataSource={record?.detail}
                      rowKey={(record) => record.id || uuid()}
                      expandable={false}
                      pagination={false}
                      showHeader={false}
                      rowClassName={(record) =>
                        isCurrentMilestone(record) ? "highlight" : ""
                      }
                    />
                  ) : (
                    <Collapse accordion>
                      {record?.detail.map((item) => (
                        <Panel
                          header={
                            <Row
                              style={{
                                width: "100%",
                                paddingLeft: "55px",
                              }}
                            >
                              <Col span={4}>{item.name}</Col>
                              <Col span={4}>
                                {item?.fromDate?.format(DATE_FORMAT_SLASH)}
                              </Col>
                              <Col span={4}>
                                {item?.toDate?.format(DATE_FORMAT_SLASH)}
                              </Col>
                              <Col span={8}>{item.time}</Col>
                              <Col span={4}></Col>
                            </Row>
                          }
                          key={item.key}
                        >
                          {item.detail.map((detail, index) => (
                            <Row key={index} style={{ paddingLeft: "80px" }}>
                              <Col xs={4}>{detail.product}</Col>
                              <Col xs={4}>
                                {detail?.fromDate?.format(DATE_FORMAT_SLASH)}
                              </Col>
                              <Col xs={4}>
                                {detail?.toDate?.format(DATE_FORMAT_SLASH)}
                              </Col>
                              <Col xs={8}>{detail.time}</Col>
                              <Col xs={4}>
                                <div className="flex">
                                  {detail.product
                                    .toLowerCase()
                                    .includes("report") ? (
                                    getMilestoneStatus(detail.id) ? (
                                      <div>
                                        submitted -
                                        <Button
                                          className="ml-2"
                                          onClick={() =>
                                            handleViewReport(detail)
                                          }
                                        >
                                          Download in git
                                        </Button>
                                      </div>
                                    ) : (
                                      "Not submitted"
                                    )
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </Col>
                            </Row>
                          ))}
                        </Panel>
                      ))}
                    </Collapse>
                  )}
                </>
              ),
              rowExpandable: (record) => record.detail?.length > 0,
              expandIconColumnIndex: 0,
              rowClassName: (record) =>
                isCurrentMilestone(record) ? "highlight" : "",
            }}
            rowClassName={(record) =>
              isCurrentMilestone(record) ? "highlight" : ""
            }
          />
        )}
        {report.length > 0 && (
          <MilestoneEvaluationModal
            visible={visiblePopup}
            setVisiblePopup={setVisiblePopup}
            currentMilestone={currentMilestone}
            report={report}
            group={group}
            evaluation={evaluation}
            semesterId={selectedSemesterId}
            handleCancel={handleCancel}
          />
        )}
        {report.length > 0 && (
          <MilestonesEvaluationProgressModal
            visible={visiblePopupEvaluateProgress}
            setVisiblePopup={setVisiblePopupEvaluateProgress}
            currentMilestone={currentMilestone}
            report={report}
            group={group}
            handleCancel={handleCancelEvaluateProgress}
            milestoneStatus={milestoneStatus}
          />
        )}
      </ContentBlockWrapper>
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "scoreStore"
    )(observer(ManageGroupProgressPage))
  )
);
