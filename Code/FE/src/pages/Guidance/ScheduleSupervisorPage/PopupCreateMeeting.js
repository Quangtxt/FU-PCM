import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Modal,
  message,
  Space,
  Input,
  DatePicker,
  TimePicker,
  Select,
} from "antd";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";

dayjs.extend(customParseFormat);
const PopupCreateMeeting = (props) => {
  const {
    isVisiblePopup,
    setIsVisiblePopup,
    handleClosePopup,
    loadingAnimationStore,
    setRefresh,
    meetingStore,
    groupStore,
    authenticationStore,
    groupsOfSupervisor,
    selectedDate,
    milestones,
  } = props;
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const [selectedGroup, setSelectedGroup] = useState(null);
  const [selectedRange, setSelectedRange] = useState(false);
  const [weekMilestone, setWeekMilestone] = useState();
  const [dateSelected, setDateSelected] = useState();
  const [meetingDate, setMeetingDate] = useState(null);

  useEffect(() => {
    if (selectedDate) {
      setMeetingDate(moment(selectedDate));
      form.setFieldsValue({
        meetingTime: moment(selectedDate),
      });
    }
  }, [selectedDate]);

  const findWeekMilestoneAndSelectedWeek = (selectedDate) => {
    const firstMilestoneStartDate = moment(milestones[0]?.startDate);
    const lastMilestoneEndDate = moment(
      milestones[milestones.length - 1]?.endDate
    );
    const daysDifference = lastMilestoneEndDate.diff(
      firstMilestoneStartDate,
      "days"
    );
    const weeksDifference = Math.ceil(daysDifference / 7);
    setWeekMilestone(weeksDifference);

    if (selectedDate.isAfter(firstMilestoneStartDate)) {
      const daysPassed = selectedDate.diff(firstMilestoneStartDate, "days");
      const specificWeek = Math.ceil(daysPassed / 7);
      setDateSelected(specificWeek);
    } else {
      if (dateSelected !== null) {
        setDateSelected(null);
      }
    }
  };
  useEffect(() => {
    if (!isVisiblePopup) {
      form.resetFields();
      setSelectedRange(false);
      setSelectedGroup(null);
    }
  }, [isVisiblePopup, form]);

  useEffect(() => {
    if (meetingDate) {
      findWeekMilestoneAndSelectedWeek(meetingDate);
    }
  }, [meetingDate]);

  const disabledDate = (current) => {
    return current && current < dayjs().startOf("day");
  };

  const range = (start, end) => {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  };

  // const disabledRangeTime = (dates, type) => {
  //   const now = moment();
  //   const isToday = dates && dates[0] && dates[0].isSame(now, "day");

  //   if (type === "start") {
  //     return {
  //       disabledHours: () => {
  //         const hours = range(0, 7).concat(range(19, 24));
  //         return hours;
  //       },
  //       disabledMinutes: () => {
  //         return [];
  //       },
  //     };
  //   }

  //   return {
  //     disabledHours: () => {
  //       const hours = range(0, 7).concat(range(19, 24));
  //       return hours;
  //     },
  //     disabledMinutes: () => {
  //       return [];
  //     },
  //   };
  // };
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);

  const handleStartTimeChange = (time) => {
    if (!time || (time.hour() >= 7 && time.hour() < 18)) {
      setStartTime(time);
    } else {
      message.error("Please select a time between 7:00 AM and 6:59 PM");
      setStartTime(null); // Reset if invalid
    }
  };

  const handleEndTimeChange = (time) => {
    setEndTime(time);
  };

  const handleDateChange = (date) => {
    setMeetingDate(date);
  };
  const handleCreate = async (values) => {
    try {
      const { meetingTime, week, type, location, startTime, endTime } = values;
      let startAt = undefined;
      let endAt = undefined;

      if (meetingDate && startTime && endTime) {
        startAt = moment(meetingDate).set({
          hour: startTime.hour(),
          minute: startTime.minute(),
        });
        endAt = moment(meetingDate).set({
          hour: endTime.hour(),
          minute: endTime.minute(),
        });
      }
      const meetings = [];
      //tạo cuộc họp theo tuần
      if (week) {
        for (let i = 0; i < week; i++) {
          const meeting = {
            startAt: startAt.clone().add(i + 1, "weeks"),
            endAt: endAt.clone().add(i + 1, "weeks"),
            type: type,
            location: location,
            groupId: selectedGroup,
          };
          meetings.push(meeting);
        }
      }
      //tạo cuộc họp hiện tại
      const meeting = {
        startAt: startAt,
        endAt: endAt,
        type: type,
        location: location,
        groupId: selectedGroup,
      };
      meetings.push(meeting);

      loadingAnimationStore.showSpinner(true);
      const response = await meetingStore.createMeeting(
        meetings,
        selectedGroup
      );
      if (response.status === 200) {
        setRefresh(true);
        setIsVisiblePopup(false);
        form.resetFields();
        message.success("Create successfully");
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
      message.error(err.en || "Error not create meeting!");
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };

  const getOptions = () => {
    if (groupsOfSupervisor?.length > 0) {
      return groupsOfSupervisor.map((item) => ({
        value: item?.group.id,
        label: item?.group.vietnameseTitle,
      }));
    } else {
      return [];
    }
  };

  const handleGroupChange = (option) => {
    setSelectedGroup(option);
  };
  const handleRangeChange = () => {
    setSelectedRange(true);
  };

  const disabledTime = () => {
    return {
      disabledHours: () =>
        [...Array(24).keys()].filter((hour) => hour < 7 || hour >= 18),
      disabledMinutes: () => [],
    };
  };
  return (
    <Modal
      title="Create Meeting"
      footer={null}
      closable={true}
      visible={isVisiblePopup}
      onCancel={handleClosePopup}
    >
      <Form onFinish={handleCreate} form={form} scrollToFirstError>
        <Space
          size={12}
          direction="vertical"
          style={{
            marginTop: 20,
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Form.Item
            label="Meeting Time (1 DAY)"
            name="meetingTime"
            rules={[{ required: true, message: "Please select meeting time!" }]}
          >
            <DatePicker
              disabledDate={disabledDate}
              onChange={handleDateChange}
              onCancel={() => setSelectedRange(false)}
            />
          </Form.Item>

          <div className="flex justify-center">
            <Form.Item
              label="Start Time:"
              name="startTime"
              rules={[
                { required: true, message: "Please select meeting time!" },
              ]}
            >
              <TimePicker
                format="HH:mm"
                value={startTime}
                onChange={handleStartTimeChange}
              />
            </Form.Item>
            <Form.Item
              label="End Time"
              name="endTime"
              rules={[
                { required: true, message: "Please select meeting time!" },
              ]}
            >
              <TimePicker
                format="HH:mm"
                disabledTime={disabledTime}
                onChange={handleEndTimeChange}
              />
            </Form.Item>
          </div>
          {meetingDate && startTime && endTime && (
            <div>
              <Form.Item
                label="Do you want to schedule multiple weeks(not required)"
                name="week"
              >
                <Select>
                  <Option value="">none</Option>
                  <Option value={2}>2 weeks</Option>
                  <Option value={4}>1 month</Option>
                  {weekMilestone - dateSelected > 0 ? (
                    <Option value={weekMilestone - dateSelected}>
                      During semester - {weekMilestone - dateSelected} weeks
                    </Option>
                  ) : (
                    <></>
                  )}
                </Select>
              </Form.Item>
            </div>
          )}
          <Form.Item
            label="Type of meeting"
            name="type"
            rules={[
              { required: true, message: "Please select type of meeting!" },
            ]}
          >
            <Select>
              <Option value={"Online"}>Online</Option>
              <Option value={"Offline"}>Offline</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Location or Link of meeting"
            name="location"
            rules={[{ required: true, message: "Please text the location!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Choose the group"
            name="group"
            rules={[{ required: true, message: "Please select group!" }]}
          >
            <Select
              value={selectedGroup}
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              onChange={handleGroupChange}
              options={getOptions()}
              placeholder="Group selected"
            />
          </Form.Item>
        </Space>
        <div className="flex justify-end items-center">
          <Button
            icon={<CloseOutlined />}
            className="rounded flex items-center"
            onClick={handleClosePopup}
            danger
          >
            Cancel
          </Button>
          <Button
            icon={<CheckOutlined />}
            className="rounded flex items-center m-2"
            htmlType={"submit"}
            type={"primary"}
          >
            Create
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

PopupCreateMeeting.propTypes = {};

export default withRouter(
  inject(
    "loadingAnimationStore",
    "authenticationStore",
    "meetingStore",
    "authenticationStore",
    "groupStore"
  )(observer(PopupCreateMeeting))
);
