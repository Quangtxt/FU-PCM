import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Modal,
  message,
  Space,
  Input,
  DatePicker,
  TimePicker,
  Select,
} from "antd";
import dayjs from "dayjs";
import { withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";

const PopupEditMeeting = (props) => {
  const {
    isVisiblePopupEdit,
    setIsVisiblePopupEdit,
    handleClosePopup,
    loadingAnimationStore,
    setRefresh,
    meetingStore,
    meetingList,
    meetingId,
    groupsOfSupervisor,
  } = props;
  const [form] = Form.useForm();
  const [meetingTime, setMeetingTime] = useState(null);
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);

  const disabledDate = (current) => {
    return current && current < dayjs().startOf("day");
  };

  const handleDateChange = (date) => {
    setMeetingTime(date);
  };

  useEffect(() => {
    if (isVisiblePopupEdit && meetingList) {
      meetingList.filter((meeting) => {
        if (meeting.id === meetingId) {
          form.setFieldsValue({
            meetingTime: moment(meeting?.startAt),
            startTime: moment(meeting?.startAt),
            endTime: moment(meeting?.endAt),
            type: meeting?.type,
            location: meeting?.location,
            group: meeting?.group?.id,
          });
        }
      });
    }
  }, [meetingList, isVisiblePopupEdit]);
  const handleEdit = async (values) => {
    try {
      const { meetingTime, type, location, startTime, endTime } = values;
      let startAt = undefined;
      let endAt = undefined;
      console.log(meetingTime, startTime, endTime);
      if (meetingTime && startTime && endTime) {
        startAt = moment(meetingTime).set({
          hour: startTime.hour(),
          minute: startTime.minute(),
        });
        endAt = moment(meetingTime).set({
          hour: endTime.hour(),
          minute: endTime.minute(),
        });
      }
      const meeting = {
        startAt: startAt,
        endAt: endAt,
        type: type,
        location: location,
        id: meetingId,
      };
      loadingAnimationStore.showSpinner(true);
      const response = await meetingStore.updateMeeting_One(meeting);
      if (response.status === 200) {
        setRefresh(true);
        setIsVisiblePopupEdit(false);
        message.success("Edit successfully");
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
      message.error(err.en || "Error not edit meeting!");
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const getOptions = () => {
    if (groupsOfSupervisor?.length > 0) {
      return groupsOfSupervisor.map((item) => ({
        value: item?.group.id,
        label: item?.group.vietnameseTitle,
      }));
    } else {
      return [];
    }
  };
  const handleStartTimeChange = (time) => {
    if (!time || (time.hour() >= 7 && time.hour() < 18)) {
      setStartTime(time);
    } else {
      message.error("Please select a time between 7:00 AM and 6:59 PM");
      setStartTime(null); // Reset if invalid
    }
  };

  const disabledTime = () => {
    return {
      disabledHours: () =>
        [...Array(24).keys()].filter((hour) => hour < 7 || hour >= 18),
      disabledMinutes: () => [],
    };
  };

  const handleEndTimeChange = (time) => {
    setEndTime(time);
  };

  return (
    <Modal
      title="Edit Meeting"
      footer={null}
      closable={true}
      visible={isVisiblePopupEdit}
      onCancel={handleClosePopup}
    >
      <Form onFinish={handleEdit} form={form} scrollToFirstError>
        <Space
          size={12}
          direction="vertical"
          style={{
            marginTop: 20,
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Form.Item
            label="Meeting Time (1 DAY)"
            name="meetingTime"
            rules={[{ required: true, message: "Please select meeting time!" }]}
          >
            <DatePicker
              value={meetingTime}
              disabledDate={disabledDate}
              onChange={handleDateChange}
            />
          </Form.Item>

          <div className="flex justify-center">
            <Form.Item
              label="Start Time:"
              name="startTime"
              rules={[
                { required: true, message: "Please select meeting time!" },
              ]}
            >
              <TimePicker
                format="HH:mm"
                value={startTime}
                onChange={handleStartTimeChange}
              />
            </Form.Item>
            <Form.Item
              label="End Time"
              name="endTime"
              rules={[
                { required: true, message: "Please select meeting time!" },
              ]}
            >
              <TimePicker
                format="HH:mm"
                disabledTime={disabledTime}
                onChange={handleEndTimeChange}
              />
            </Form.Item>
          </div>
          <Form.Item
            label="Type of meeting"
            name="type"
            rules={[
              { required: true, message: "Please select type of meeting!" },
            ]}
          >
            <Select>
              <Option value={"Online"}>Online</Option>
              <Option value={"Offline"}>Offline</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Location or Link of meeting"
            name="location"
            rules={[{ required: true, message: "Please text the location!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Group" name="group">
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              options={getOptions()}
              disabled
            />
          </Form.Item>
        </Space>
        <div className="flex justify-end items-center">
          <Button
            icon={<CloseOutlined />}
            onClick={handleClosePopup}
            danger
            className="rounded flex items-center"
          >
            Cancel
          </Button>
          <Button
            icon={<CheckOutlined />}
            htmlType={"submit"}
            type={"primary"}
            className="rounded flex items-center m-2"
          >
            Edit
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

PopupEditMeeting.propTypes = {};

export default withRouter(
  inject(
    "loadingAnimationStore",
    "authenticationStore",
    "meetingStore",
    "authenticationStore",
    "groupStore"
  )(observer(PopupEditMeeting))
);
