import React, { memo, useCallback, useEffect, useState } from "react";
import {
  AudioOutlined,
  UserAddOutlined,
  FolderAddOutlined,
} from "@ant-design/icons";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Button, Select, Input, Tabs, Typography, Table } from "antd";
import uuid from "uuid";
import DashboardLayout from "../../layouts/DashboardLayout";
import { TableBottomPaginationBlock } from "../../components/Common/Table";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import PageTitle from "../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
import PopupImportExcel from "./PopupImportExcel";
import moment from "moment";
const { Title } = Typography;
const { TabPane } = Tabs;

const ManagerCommitteePage = (props) => {
  const {
    history,
    loadingAnimationStore,
    committeeStore,
    semesterStore,
  } = props;

  const {
    committeeList,
    committeeListTotalCount,
    committeeListPageSize,
    committeeListPageIndex,
    setFilter,
    committeeAssignmentList,
  } = committeeStore;
  const [selectedSemesterId, setSelectedSemesterId] = useState(null);
  const [semesters, setSemesters] = useState([]);
  useEffect(() => {
    getSemester();
  }, []);
  const getSemester = async () => {
    try {
      const res = await semesterStore.getSemesters();
      setSemesters(res.data);
      setSemesterCurrent(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  const setSemesterCurrent = (semesters) => {
    if (semesters.length > 0) {
      const currentDate = moment();
      const closestSemester = semesters.reduce((prev, curr) => {
        const startDate = moment(curr.beginAt);
        const endDate = moment(curr.endAt);
        if (currentDate.isBetween(startDate, endDate, null, "[]")) {
          return curr;
        } else {
          if (prev !== null) {
            const prevDiff = Math.abs(
              moment(prev.beginAt).diff(currentDate, "days")
            );
            const currDiff = Math.abs(startDate.diff(currentDate, "days"));
            return currDiff < prevDiff ? curr : prev;
          } else {
            return curr;
          }
        }
      }, null);
      if (closestSemester) {
        setSelectedSemesterId(closestSemester.id);
      }
    }
  };
  useEffect(() => {
    if (selectedSemesterId) {
      loadingAnimationStore.setTableLoading(true);
      committeeStore.getCommitteeList(selectedSemesterId).finally(() => {
        loadingAnimationStore.setTableLoading(false);
      });
      committeeStore
        .getCommitteeAssignmentList(selectedSemesterId)
        .finally(() => {
          loadingAnimationStore.setTableLoading(false);
        });
    }
    return () => {
      committeeStore.clearStore();
    };
  }, [selectedSemesterId]);
  const onSelectChange = (value) => {
    setSelectedSemesterId(value);
  };
  const [isVisiblePopupImportExcel, setIsVisiblePopupImportExcel] = useState(
    false
  );
  console.log("committeeAssignmentList", committeeAssignmentList);
  const columns = [
    {
      title: "Committee Name",
      dataIndex: "committeeName",
      key: "committeeName",
    },
    {
      title: "Employee Name",
      dataIndex: "employeeName",
      key: "employeeName",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Mission",
      dataIndex: "mission",
      key: "mission",
    },
    {
      title: "Committee Code",
      dataIndex: "committeeCode",
      key: "committeeCode",
    },
  ];

  const data = committeeList.flatMap((committee, index) =>
    committee.committeeDetails.map((detail, idx) => ({
      key: `${index}-${idx}`,
      committeeName: committee.committeeName,
      employeeName: detail.employee.name,
      email: detail.employee.email,
      mission: detail.mission,
      committeeCode: committee.committeeCode,
    }))
  );
  const column2 = [
    {
      title: "Group",
      render: (record) => record?.group?.groupCode,
    },
    {
      title: "Committee",
      render: (record) => record?.committee?.committeeCode,
    },
    {
      title: "Time",
      dataIndex: "time",
      key: "time",
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
    },
  ];
  return (
    <DashboardLayout>
      <Helmet>
        <title>Registration | Committee</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"List committees"}
        hiddenGoBack
      ></PageTitle>
      <ContentBlockWrapper>
        <div className="flex items-stretch  gap-2 mb-5 bg-white p-8 rounded-md">
          <div className="flex items-center justify-center">
            <Title level={5}>Semester</Title>
          </div>
          <Select
            value={selectedSemesterId}
            onChange={onSelectChange}
            style={{ width: "200px" }}
            options={semesters?.map((semester) => ({
              value: semester.id,
              label: semester.name,
            }))}
          />
        </div>
        <Tabs
          defaultActiveKey="tab1"
          tabBarExtraContent={
            <Button
              className="flex items-center justify-center"
              onClick={setIsVisiblePopupImportExcel}
            >
              <FolderAddOutlined />
              Import Excel
            </Button>
          }
        >
          <TabPane tab="Committee" key="tab1">
            <Table
              columns={columns}
              dataSource={data}
              rowKey={(record) => record.id || uuid()}
              pagination={false}
            />
          </TabPane>
          <TabPane tab="Committee Assignment" key="tab2">
            <Table
              columns={column2}
              dataSource={committeeAssignmentList}
              rowKey={(record) => record.id || uuid()}
              expandable={false}
              pagination={false}
            />
          </TabPane>
        </Tabs>
        <PopupImportExcel
          isVisiblePopup={isVisiblePopupImportExcel}
          setIsVisiblePopup={setIsVisiblePopupImportExcel}
          handleClosePopup={() => setIsVisiblePopupImportExcel(false)}
        />
      </ContentBlockWrapper>
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "committeeStore",
      "semesterStore"
    )(observer(ManagerCommitteePage))
  )
);
