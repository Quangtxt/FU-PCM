import React, { useEffect, useState, useMemo } from "react";
// Ant design
// Mobx
import { inject, observer } from "mobx-react";
import { toJS } from "mobx";
import {
  Avatar,
  Button,
  Col,
  Comment,
  Input,
  message,
  Pagination,
  Row,
  Switch,
  Tooltip,
  Upload,
  Modal,
} from "antd";
import {
  CloseCircleOutlined,
  EditOutlined,
  FilterOutlined,
  PlusCircleOutlined,
  FileAddOutlined,
  InboxOutlined,
} from "@ant-design/icons";
import EmptyContent from "../../components/EmptyContent";
import { EmptyText } from "../../components/Common/CellText";
import {
  CellEclipseBox,
  TableBottomPaginationBlock,
} from "../../components/Common/Table";
import TableComponent from "../../components/Common/TableComponent";
import fileStore from "../../stores/fileStore";

const ModalPreView = (props) => {
  const {
    handleCancelImport,
    data,
    onCancel,
    setPreViewVisible,
    loadingAnimationStore,
    fileStore,
  } = props;

  const [checkErrorData, setCheckErrorData] = useState(false);
  useEffect(() => {
    if (data == null) return;
    data.data.committees.forEach((info) => {
      if (info.note !== "") {
        setCheckErrorData(true);
        return false;
      }
    });
    data.data.committeeAssignments.forEach((info) => {
      if (info.note !== "") {
        setCheckErrorData(true);
        return false;
      }
    });
  }, [data]);
  const tableColumn1 = useMemo(
    () => [
      {
        title: "Committee Name",
        key: "committeeName",
        render: (record) =>
          record.committeeName ? (
            <span>{record.committeeName}</span>
          ) : (
            <EmptyText>Unclear</EmptyText>
          ),
      },
      {
        title: "Employee name",
        width: 150,
        key: "employeeName",
        render: (record) =>
          record.employeeName ? (
            <Comment
              author={
                <span style={{ textTransform: "capitalize" }}>
                  {record.employeeName}
                </span>
              }
            />
          ) : (
            <EmptyText>Unclear</EmptyText>
          ),
      },

      {
        title: "Email",
        key: "email",
        render: (record) =>
          record.email ? record?.email : <EmptyText>Unclear</EmptyText>,
      },
      {
        title: "Mission",
        key: "mission",
        render: (record) =>
          record.mission ? record.mission : <EmptyText>Unclear</EmptyText>,
      },
      {
        title: "Committee code",
        key: "committeeCode",
        render: (record) =>
          record.committeeCode ? (
            <strong>{record.committeeCode}</strong>
          ) : (
            <EmptyText>Unclear</EmptyText>
          ),
      },
      {
        title: "Note",
        key: "note",
        width: 200,
        render: (record) => (
          <div style={{ color: "red" }}>
            {record?.note &&
              record?.note
                .replace(/\.$/, "")
                .split(".")
                .map((sentence, index) => (
                  <React.Fragment key={index}>
                    {sentence.trim()}
                    <br />
                  </React.Fragment>
                ))}
          </div>
        ),
      },
    ],
    []
  );
  const tableColumn2 = useMemo(
    () => [
      {
        title: "Group",
        key: "group",
        render: (record) =>
          record.groupCode ? (
            <span>{record.groupCode}</span>
          ) : (
            <EmptyText>Unclear</EmptyText>
          ),
      },
      {
        title: "Committee",
        width: 150,
        key: "committee",
        render: (record) =>
          record.committee ? (
            <span>{record.committee}</span>
          ) : (
            <EmptyText>Unclear</EmptyText>
          ),
      },

      {
        title: "Time",
        key: "time",
        render: (record) =>
          record.time ? record?.time : <EmptyText>Unclear</EmptyText>,
      },
      {
        title: "Date",
        key: "date",
        render: (record) =>
          record.date ? record.date : <EmptyText>Unclear</EmptyText>,
      },
      {
        title: "Note",
        key: "note",
        width: 200,
        render: (record) => (
          <div style={{ color: "red" }}>
            {record?.note &&
              record?.note
                .replace(/\.$/, "")
                .split(".")
                .map((sentence, index) => (
                  <React.Fragment key={index}>
                    {sentence.trim()}
                    <br />
                  </React.Fragment>
                ))}
          </div>
        ),
      },
    ],
    []
  );
  const HandelCreateCommittee = async () => {
    try {
      loadingAnimationStore.showSpinner(true);
      await fileStore.createCommitteesFromExcel(data.data);
      loadingAnimationStore.showSpinner(false);
      message.success("Import successfully.");
      setPreViewVisible(false);
      handleCancelImport();
    } catch (error) {
      loadingAnimationStore.showSpinner(false);
      console.error("Error importing Excel:", error);
    }
  };

  return (
    <Modal
      visible={true}
      width={1200}
      title="Preview"
      onCancel={onCancel}
      style={{ top: 20 }}
      footer={[
        checkErrorData && (
          <span style={{ color: "red", marginRight: "10px" }}>
            Please edit the file to the correct format before importing.
          </span>
        ),
        <Button danger ghost onClick={onCancel}>
          Cancel
        </Button>,
        !checkErrorData && (
          <Button type="primary" onClick={HandelCreateCommittee}>
            Import
          </Button>
        ),
      ]}
    >
      <TableComponent
        rowKey={(record) => record?.code}
        dataSource={data && data.data.committees}
        columns={tableColumn1}
        pagination={false}
        scroll={{ x: 1000 }}
        locale={{
          emptyText: <EmptyContent />,
        }}
      />
      <TableComponent
        rowKey={(record) => record?.code}
        dataSource={data && data.data.committeeAssignments}
        columns={tableColumn2}
        pagination={false}
        scroll={{ x: 1000 }}
        locale={{
          emptyText: <EmptyContent />,
        }}
      />
    </Modal>
  );
};

ModalPreView.propTypes = {};

export default inject(
  "authenticationStore",
  "loadingAnimationStore",
  "fileStore"
)(observer(ModalPreView));
