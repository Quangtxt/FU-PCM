import React, { memo, useCallback, useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Button, Table, Typography, Select, Tooltip } from "antd";
import uuid from "uuid";
import DashboardLayout from "../../layouts/DashboardLayout";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import PageTitle from "../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
import ViewProgress from "../../components/ViewProgress/ViewProgress";
import moment from "moment";
import ViewAllEvaluationModal from "../Guidance/ManageGroupProgressPage/ViewAllEvaluationModal";

const { Option } = Select;
const { Title } = Typography;

const ManageGroupPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    semesterStore,
    committeeStore,
  } = props;
  const { committeeAssignmentList } = committeeStore;
  const [selectedSemesterId, setSelectedSemesterId] = useState(null);
  const [semesters, setSemesters] = useState([]);
  const [groups, setGroups] = useState([]);
  const [visiblePopupAll, setVisiblePopupAll] = useState(false);
  useEffect(() => {
    getSemester();
  }, []);
  useEffect(() => {
    if (selectedSemesterId) {
      GetGroupListBySemester(selectedSemesterId);
    }
  }, [selectedSemesterId]);
  const getSemester = async () => {
    try {
      const res = await semesterStore.getSemesters();
      setSemesters(res.data);
      setSemesterCurrent(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  // console.log("committeeAssignmentList", committeeAssignmentList);
  useEffect(() => {
    if (selectedSemesterId) {
      loadingAnimationStore.setTableLoading(true);
      committeeStore
        .getCommitteeAssignmentList(selectedSemesterId)
        .finally(() => {
          loadingAnimationStore.setTableLoading(false);
        });
    }
    return () => {
      committeeStore.clearStore();
    };
  }, [selectedSemesterId]);
  const GetGroupListBySemester = async (semesterId) => {
    try {
      const res = await groupStore.getGroupOfCommittee(semesterId);
      setGroups(res.data);
    } catch (e) {
      console.log(e);
    }
  };

  const setSemesterCurrent = (semesters) => {
    if (semesters.length > 0) {
      const currentDate = moment();
      const closestSemester = semesters.reduce((prev, curr) => {
        const startDate = moment(curr.beginAt);
        const endDate = moment(curr.endAt);
        if (currentDate.isBetween(startDate, endDate, null, "[]")) {
          return curr;
        } else {
          if (prev !== null) {
            const prevDiff = Math.abs(
              moment(prev.beginAt).diff(currentDate, "days")
            );
            const currDiff = Math.abs(startDate.diff(currentDate, "days"));
            return currDiff < prevDiff ? curr : prev;
          } else {
            return curr;
          }
        }
      }, null);
      if (closestSemester) {
        setSelectedSemesterId(closestSemester.id);
      }
    }
  };
  const [currentIndex, setCurrentIndex] = useState(null);
  const [selectedGroupId, setSelectedGroupId] = useState();
  const openPopup = (groupId) => {
    setSelectedGroupId(groupId);
    setVisiblePopupAll(true);
  };
  const columns = [
    {
      title: "Group Code",
      width: "100",
      render: (record) => record?.group.groupCode,
    },
    {
      title: "Group Name",
      width: "200",
      render: (record) => record?.group.name,
    },
    {
      title: "Vietnamese Title",
      render: (record) => record?.group.vietnameseTitle,
    },
    {
      title: "Date",
      render: (record) => record?.date,
    },
    {
      title: "Time",
      render: (record) => record?.time,
    },
    {
      title: "Status",
      render: (record) => record?.group.approvedBySupervisor,
    },
    {
      title: "Action",
      width: "20%",
      align: "center",
      render: (record) => (
        <div className="flex justify-center ">
          {/* {currentIndex > 5 && ( */}
          {record?.group.approvedBySupervisor === "APPROVED" && (
            <Tooltip title="View evaluation of group">
              <Button onClick={() => openPopup(record.group.id)}>
                View Evaluation - Comment
              </Button>
            </Tooltip>
          )}

          {/* )} */}
        </div>
      ),
    },
  ];
  return (
    <DashboardLayout>
      <Helmet>
        <title>Manager Group</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"Manager group"}
        hiddenGoBack
      ></PageTitle>
      <ContentBlockWrapper>
        {selectedSemesterId && (
          <ViewProgress
            id={selectedSemesterId}
            setCurrentIndex={setCurrentIndex}
          />
        )}
        <div className="gap-5 items-start">
          <div className="bg-white p-8 rounded-md mb-5">
            <Table
              columns={columns}
              dataSource={committeeAssignmentList}
              rowKey={(record) => record.id || uuid()}
              pagination={false}
            />
          </div>
        </div>
        <ViewAllEvaluationModal
          visible={visiblePopupAll}
          semesterId={selectedSemesterId}
          setVisiblePopup={setVisiblePopupAll}
          groupId={selectedGroupId}
          handleCancel={() => setVisiblePopupAll(false)}
        />
      </ContentBlockWrapper>
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "committeeStore"
    )(observer(ManageGroupPage))
  )
);
