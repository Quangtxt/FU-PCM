import React, { memo, useCallback, useEffect, useState, useRef } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { UserOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Form, Input, message, Row, Col, Modal } from "antd";
import ContentBlockWrapper from "../../../components/ContentBlockWrapper";
import DashboardLayout from "../../../layouts/DashboardLayout";
import { Helmet } from "react-helmet/es/Helmet";
import { PortalContent } from "./CreateIdeaNoteStyled";
import PageTitle from "../../../components/PageTitle";
import { FormActionFooter } from "./CreateIdeaNoteStyled";
import EmptyPage from "../../EmptyPage/EmptyPage";
import PopupViewDetail from "./PopupViewDetail";
import TableComponent from "../../../components/Common/TableComponent";

import { Container } from "../../../layouts/Container/Container";
import CreateNotePage from "./CreateNotePage";
import moment from "moment";
import { Client } from "@stomp/stompjs";
import SockJS from "sockjs-client";
import { apiUrl } from "../../../config";

const NotePage = (props) => {
  const {
    studentStore,
    loadingAnimationStore,
    meetingStore,
    groupStore,
    authenticationStore,
    history,
    match,
  } = props;
  const { meetingId } = match.params;
  const { currentUser } = authenticationStore;
  const EDITOR_REF = useRef();
  const [noteList, setNoteList] = useState();
  const [note, setNote] = useState();
  const [meeting, setMeeting] = useState();
  const [isVisiblePopup, setIsVisiblePopup] = useState(false);
  const [isVisiblePopupCreate, setIsVisiblePopupCreate] = useState(false);
  const currentDate = moment();
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    if (authenticationStore.currentUser) {
      getNoteListByMeeting();
      getMeetingByMeeting();
    }
  }, [authenticationStore.currentUser, isVisiblePopupCreate, refresh]);
  useEffect(() => {
    const client = new Client({
      brokerURL: `ws://${apiUrl}/ws`,
      webSocketFactory: () => new SockJS(`${apiUrl}/ws`),
      onConnect: () => {
        client.subscribe("/topic/createNote", (message) => {
          if (message.body) {
            getNoteListByMeeting();
          }
        });
        client.subscribe("/topic/editNote", (message) => {
          if (message.body) {
            getNoteListByMeeting();
          }
        });
        client.subscribe("/topic/removeNote", (message) => {
          if (message.body) {
            getNoteListByMeeting();
          }
        });
      },
    });
    client.activate();
    return () => client.deactivate();
  }, []);
  const getNoteListByMeeting = async () => {
    loadingAnimationStore.setTableLoading(true);
    const res = await meetingStore
      .getNoteListByMeeting(meetingId)
      .finally(() => {
        loadingAnimationStore.setTableLoading(false);
      });
    setNoteList(res.data);
    setRefresh(false);
  };

  const getMeetingByMeeting = async () => {
    loadingAnimationStore.setTableLoading(true);
    const res = await meetingStore
      .getMeetingByMeeting(meetingId)
      .finally(() => {
        loadingAnimationStore.setTableLoading(false);
      });
    setMeeting(res.data);
    setRefresh(false);
  };

  const showConfirmModal = (action, noteId, handleRemoveNote) => {
    Modal.confirm({
      title: `Are you sure to delete this meeting?`,
      onOk: () => {
        if (action === "delete") {
          handleRemoveNote(noteId);
          message.success("Delete action was successfully");
        }
      },
      onCancel: () => {
        message.info("Delete action was cancelled");
      },
      okText: "Yes",
      cancelText: "No",
    });
  };

  const handleRemoveNote = async (noteId) => {
    try {
      await meetingStore.deleteNote(noteId);
      setRefresh(true);
    } catch (err) {
      console.log(err);
    }
  };

  const columns = [
    {
      title: "No.",
      width: 100,
      render: (record, index, dataSource) => <strong>{dataSource + 1}</strong>,
    },
    {
      title: "Title",
      width: "80%",
      render: (record) => record?.title,
    },
    {
      title: "Action",
      render: (record) => (
        <div className="flex justify-start">
          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center"
            onClick={() => {
              setNote(record);
              setIsVisiblePopup(true);
            }}
          >
            View
          </Button>
          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
            onClick={() => {
              showConfirmModal("delete", record?.id, handleRemoveNote);
            }}
          >
            Delete
          </Button>
        </div>
      ),
    },
  ];
  return (
    <DashboardLayout>
      <Helmet>
        <title>Guidance || Note</title>
      </Helmet>

      <PageTitle
        location={props.location}
        title={"Note"}
        hiddenGoBack
      ></PageTitle>

      <ContentBlockWrapper>
        <div className="flex flex-col mb-3">
          <h2 className="text-3xl font-bold text-blue-600 text-center">NOTE</h2>
          <div className="flex items-center justify-end">
            {moment().diff(moment(meeting?.endAt), "days") <= 3 ? (
              <Button
                className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
                onClick={() => {
                  setIsVisiblePopupCreate(true);
                }}
              >
                Create new note
              </Button>
            ) : (
              <></>
            )}
          </div>
        </div>
        <TableComponent
          // onRow={(record, rowIndex) => {
          //   return {
          //     onClick: (event) => {
          //       setNote(record);
          //       openEditPopUp();
          //     },
          //   };
          // }}
          rowKey={(record) => record.id || uuid()}
          dataSource={noteList}
          columns={columns}
          pagination={false}
          loading={loadingAnimationStore.tableLoading}
        />
      </ContentBlockWrapper>
      <PopupViewDetail
        meetingId={meetingId}
        note={note}
        isVisiblePopup={isVisiblePopup}
        setIsVisiblePopup={setIsVisiblePopup}
        handleClosePopup={() => setIsVisiblePopup(false)}
      />
      <CreateNotePage
        meetingId={meetingId}
        isVisiblePopup={isVisiblePopupCreate}
        setIsVisiblePopup={setIsVisiblePopupCreate}
        handleClosePopup={() => setIsVisiblePopupCreate(false)}
      />
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "groupStore",
      "studentStore",
      "meetingStore"
    )(observer(NotePage))
  )
);
