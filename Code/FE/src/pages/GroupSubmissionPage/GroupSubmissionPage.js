import React, { memo, useEffect, useState } from "react";
import {
  Typography,
  Select,
  Table,
  Modal,
  Form,
  Button,
  DatePicker,
} from "antd";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import DashboardLayout from "../../layouts/DashboardLayout";
import { Helmet } from "react-helmet/es/Helmet";
import { withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { DownloadOutlined } from "@ant-design/icons";
import { apiUrl } from "../../config";
import moment from "moment";
import { DD_MM_YYYY_HH_mm } from "../../constants";

const { Title, Text, Paragraph } = Typography;

const GroupSubmissionPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    semesterStore,
    authenticationStore,
    match,
  } = props;
  const [form] = Form.useForm();

  const [selectedSemesterId, setSelectedSemesterId] = useState(null);
  const [semesters, setSemesters] = useState([]);
  const [groupSubmission, setGroupSubmission] = useState();
  const [selectedSubmission, setSelectedSubmission] = useState();
  const [refresh, setRefresh] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [actionType, setActionType] = useState("");
  const [groupId, setGroupId] = useState(null);
  const [isModalVisibleAll, setIsModalVisibleAll] = useState(false);
  const [startSubmit, setStartSubmit] = useState(null);
  const [endSubmit, setEndSubmit] = useState(null);

  useEffect(() => {
    getSemester();
  }, []);
  const getSemester = async () => {
    try {
      const res = await semesterStore.getSemesters();
      setSemesters(res.data);
      setSemesterCurrent(res.data);
    } catch (e) {
      console.log(e);
    }
  };

  const onSelectChange = (value) => {
    setSelectedSemesterId(value);
  };

  const setSemesterCurrent = (semesters) => {
    if (semesters.length > 0) {
      const currentDate = moment();
      const closestSemester = semesters.reduce((prev, curr) => {
        const startDate = moment(curr.beginAt);
        const endDate = moment(curr.endAt);
        if (currentDate.isBetween(startDate, endDate, null, "[]")) {
          return curr;
        } else {
          if (prev !== null) {
            const prevDiff = Math.abs(
              moment(prev.beginAt).diff(currentDate, "days")
            );
            const currDiff = Math.abs(startDate.diff(currentDate, "days"));
            return currDiff < prevDiff ? curr : prev;
          } else {
            return curr;
          }
        }
      }, null);
      if (closestSemester) {
        setSelectedSemesterId(closestSemester.id);
      }
    }
  };

  useEffect(() => {
    selectedSemesterId && getGroupSubmissionToSetTime(selectedSemesterId);
  }, [selectedSemesterId, refresh]);
  const getGroupSubmissionToSetTime = async (semesterId) => {
    try {
      const res = await groupStore.getGroupSubmissionToSetTime(semesterId);
      setGroupSubmission(res.data);
    } catch (e) {
      console.log(e);
    }
  };
  const columns = [
    {
      title: "Group",
      render: (record) => record.group.groupCode,
    },
    {
      title: "Group Name",
      render: (record) => record.group.name,
    },
    {
      title: "Start submit",
      render: (record) => (
        <div>
          {record?.startSubmit
            ? moment(record?.startSubmit).format(DD_MM_YYYY_HH_mm)
            : "Not set"}
        </div>
      ),
    },
    {
      title: "End submit",
      render: (record) => (
        <div>
          {record?.endSubmit
            ? moment(record?.endSubmit).format(DD_MM_YYYY_HH_mm)
            : "Not set"}
        </div>
      ),
    },
    {
      title: "Actions",
      render: (record) => (
        <div>
          <Button
            onClick={() => {
              showModalTime(record);
            }}
          >
            Set time submit
          </Button>
        </div>
      ),
    },
  ];

  const showModalTime = (record) => {
    setSelectedSubmission(record);
    setStartSubmit(record?.startSubmit ? moment(record.startSubmit) : null);
    setEndSubmit(record?.endSubmit ? moment(record.endSubmit) : null);
    setIsModalVisible(true);
  };
  const showModalTimeDeadlineAll = () => {
    setIsModalVisibleAll(true);
  };
  const handleOk = async () => {
    setIsModalVisible(false);
    loadingAnimationStore.showSpinner(true);
    try {
      await groupStore.setForGroupTimeSubmit(
        startSubmit,
        endSubmit,
        selectedSubmission?.group?.id
      );
      setRefresh(true);
    } catch (err) {
      console.log(err);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const handleTimeSubmitAll = async () => {
    console.log("gigi");
    loadingAnimationStore.showSpinner(true);
    try {
      await groupStore.setAllForGroupTimeSubmit(
        selectedSemesterId,
        startSubmit,
        endSubmit
      );
      setRefresh(true);
      setIsModalVisibleAll(false);
    } catch (err) {
      console.log(err);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };
  const handleOkAll = async () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleCancelAll = () => {
    setIsModalVisibleAll(false);
  };
  return (
    <DashboardLayout>
      <Helmet>
        <title>Capstone project submit</title>
      </Helmet>
      <ContentBlockWrapper>
        <div style={{ padding: "20px" }}>
          <Title level={3}>Capstone project submit</Title>
          <div className="flex justify-between">
            <div className="flex items-stretch gap-2 mb-5 bg-white  rounded-md">
              <div className="flex items-center justify-center">
                <Title level={5}>Semester</Title>
              </div>
              <Select
                value={selectedSemesterId}
                onChange={onSelectChange}
                style={{ width: "200px" }}
                options={semesters?.map((semester) => ({
                  value: semester.id,
                  label: semester.name,
                }))}
              />
            </div>
            <div>
              <Button
                onClick={() => {
                  showModalTimeDeadlineAll();
                }}
              >
                Set time deadline submission for all group
              </Button>
            </div>
          </div>
        </div>
        <div style={{ padding: "20px" }}>
          <Typography.Title level={4}>Secretary note:</Typography.Title>
          {groupSubmission && (
            <Table
              columns={columns}
              dataSource={groupSubmission}
              pagination={false}
            />
          )}
        </div>
        <Modal
          title={`${selectedSubmission?.group?.groupCode} - Time to deadline submission`}
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          okText={"Submit"}
          cancelText="Cancel"
        >
          <Form layout="vertical">
            <Form.Item label="Start Submit">
              <DatePicker
                showTime
                value={startSubmit}
                onChange={(value) => setStartSubmit(value)}
                format="DD/MM/YYYY HH:mm"
              />
            </Form.Item>
            <Form.Item label="End Submit">
              <DatePicker
                showTime
                value={endSubmit}
                onChange={(value) => setEndSubmit(value)}
                format="DD/MM/YYYY HH:mm"
              />
            </Form.Item>
          </Form>
        </Modal>

        <Modal
          title={`Set time deadline submission for all group`}
          visible={isModalVisibleAll}
          footer={null}
          onOk={handleOkAll}
          onCancel={handleCancelAll}
        >
          <Form form={form} onFinish={handleTimeSubmitAll}>
            <Form.Item label="Start submit">
              <DatePicker
                showTime
                onChange={(value) => setStartSubmit(value)}
                format="DD/MM/YYYY HH:mm"
              />
            </Form.Item>
            <Form.Item label="End submit">
              <DatePicker
                showTime
                onChange={(value) => setEndSubmit(value)}
                format="DD/MM/YYYY HH:mm"
              />
            </Form.Item>
            <Button htmlType={"submit"} type={"primary"}>
              Submit
            </Button>
          </Form>
        </Modal>
      </ContentBlockWrapper>
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "authenticationStore"
    )(observer(GroupSubmissionPage))
  )
);
