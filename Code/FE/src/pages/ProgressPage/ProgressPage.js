import React, { memo, useCallback, useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { withRouter, Link } from "react-router-dom";
import {
  Button,
  Table,
  Typography,
  Progress,
  Input,
  Form,
  message,
} from "antd";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";
import uuid from "uuid";
import DashboardLayout from "../../layouts/DashboardLayout";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import { NoMarginBottom } from "./ProgressPageStyled";
import PageTitle from "../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
import ViewProgress from "../../components/ViewProgress/ViewProgress";
import moment from "moment";
import utils from "../../utils";
import { DATE_FORMAT_SLASH } from "../../constants";
const { TextArea } = Input;
const { Title } = Typography;
import MemberItem from "../../components/ViewProgress/MemberItem";
import {
  GroupBtn,
  ContentInformation,
  FontSize14px,
} from "../ProfilePage/ProfilePageStyled";

const ProgressPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    semesterStore,
    authenticationStore,
    match,
  } = props;
  const [data, setData] = useState([]);
  const { id } = match.params;
  const [visible, setVisible] = useState(false);
  const [members, setMembers] = useState([]);
  const [group, setGroup] = useState();
  const [refresh, setRefresh] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const [currentStepIndex, setCurrentStepIndex] = useState(0);
  const onClose = () => {
    setVisible(false);
  };
  const [selectedSemesterId, setSelectedSemesterId] = useState(null);
  useEffect(() => {
    getSemester();
  }, []);

  const [form] = Form.useForm();

  useEffect(() => {
    if (authenticationStore.currentUser?.group) {
      getGroupInfo();
    }
  }, [refresh, authenticationStore.currentUser]);

  const getGroupInfo = async () => {
    loadingAnimationStore.showSpinner(true);
    try {
      const res = await groupStore.getGroupByMemberId();
      setGroup(res.data);
      setMembers(res.data.members);
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };

  const getSemester = async () => {
    try {
      const res = await semesterStore.getSemesters();
      setSemesterCurrent(res.data);
    } catch (e) {
      console.log(e);
    }
  };

  const setSemesterCurrent = (semesters) => {
    if (semesters.length > 0) {
      const currentDate = moment();
      const closestSemester = semesters.reduce((prev, curr) => {
        const startDate = moment(curr.beginAt);
        const endDate = moment(curr.endAt);
        if (currentDate.isBetween(startDate, endDate, null, "[]")) {
          return curr;
        } else {
          if (prev !== null) {
            const prevDiff = Math.abs(
              moment(prev.beginAt).diff(currentDate, "days")
            );
            const currDiff = Math.abs(startDate.diff(currentDate, "days"));
            return currDiff < prevDiff ? curr : prev;
          } else {
            return curr;
          }
        }
      }, null);
      if (closestSemester) {
        setData(utils.transformData(closestSemester.milestones));
        setSelectedSemesterId(closestSemester.id);
      }
    }
  };

  const rowExpandable = (record) => {
    const currentDate = moment();
    const fromDate = moment(record.fromDate);
    const toDate = moment(record.toDate);

    return (
      currentDate.isBetween(fromDate, toDate, null, "[]") ||
      record.detail?.length > 0
    );
  };
  const [progressStatus, setProgressStatus] = useState({});
  useEffect(() => {
    const fetchProgressStatus = async () => {
      // console.log("data", data);
      const progressData = await Promise.all(
        data[4]?.detail.map(async (record) => ({
          id: record.id,
          progress: await getProgressStatus(record),
        }))
      );
      setProgressStatus(
        progressData.reduce((acc, curr) => {
          acc[curr.id] = curr.progress;
          return acc;
        }, {})
      );
    };
    data?.length > 0 && fetchProgressStatus();
  }, [data]);

  const columnMilestoneGuidance = [
    {
      title: "#",
      dataIndex: "key",
      key: "key",
      width: 50,
    },
    {
      title: "Milestone",
      dataIndex: "name",
      key: "name",
      width: 200,
    },
    {
      title: "Start at",
      render: (record) => record.fromDate.format(DATE_FORMAT_SLASH),
      width: 125,
    },
    {
      title: "To/Deadline",
      render: (record) => record.toDate.format(DATE_FORMAT_SLASH),
      width: 125,
    },
    {
      title: "Status",
      render: (record) => (
        <div className="flex">
          <Progress percent={progressStatus[record.id] || 0} />
        </div>
      ),
    },
  ];
  async function getProgressStatus(record) {
    if (group) {
      try {
        // Gọi API để lấy tiến độ
        const response = await semesterStore.getProcessOfMilestone(
          group?.id,
          record?.id
        );
        if (response.status == 200) {
          return convertProgressString(response.data);
        } else {
          return 0;
        }
      } catch (error) {
        console.error(error);
        return 0;
      }
    }
  }
  function convertProgressString(progressString) {
    const [numerator, denominator] = progressString.split("/").map(Number);
    if (denominator !== 0) {
      const progressValue = (numerator / denominator) * 100;
      return progressValue;
    } else {
      return 0;
    }
  }

  const columnMilestone2 = [
    {
      title: "#",
      width: 50,
    },
    {
      title: "Product",
      dataIndex: "product",
      key: "product",
      width: 200,
    },
    {
      title: "Start at",
      render: (record) => record.fromDate.format(DATE_FORMAT_SLASH),
      width: 125,
    },
    {
      title: "To/Deadline",
      render: (record) => record.toDate.format(DATE_FORMAT_SLASH),
      width: 125,
    },
    {
      title: "submit",
      render: (record) => (
        <div className="flex">
          <p>submitted</p>
        </div>
      ),
    },
  ];

  const [visiblePopup, setVisiblePopup] = useState(false);

  const handleEvaluate = () => {
    // Save the evaluation to the record
    setVisiblePopup(false);
  };

  const handleCancel = () => {
    setVisiblePopup(false);
  };
  const handleStepChange = (index) => {
    setCurrentStepIndex(index);
  };
  const handleSubmit = async (values) => {
    try {
      console.log("value", values);
      loadingAnimationStore.showSpinner(true);
      const response = await groupStore.addGit(values.gitID, group?.id);
      if (response.status === 200) {
        message.success("Add git successfully");
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
      message.error(err.en || "GitID is not exist!");
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  };

  console.log("group", group);
  return (
    <DashboardLayout>
      <Helmet>
        <title>Manager Group</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"Manager group"}
        hiddenGoBack
      ></PageTitle>
      <div className="flex items-start gap-5">
        <div className="lg:w-8/12 max-lg:w-full">
          <ContentInformation className="p-4 mb-5 max-lg:sticky">
            {selectedSemesterId && (
              <ViewProgress
                id={selectedSemesterId}
                onStepChange={handleStepChange}
              />
            )}
          </ContentInformation>
          <ContentInformation>
            <FontSize14px>
              <Table
                columns={columnMilestoneGuidance}
                dataSource={data[4]?.detail}
                rowKey={(record) => record.id || uuid()}
                pagination={false}
                expandable={{
                  expandedRowRender: (record) => (
                    <>
                      {record?.detail[0]?.name == null ? (
                        <Table
                          columns={columnMilestone2}
                          dataSource={record?.detail}
                          rowKey={(record) => record.id || uuid()}
                          expandable={false}
                          pagination={false}
                          showHeader={false}
                        />
                      ) : (
                        <></>
                      )}
                    </>
                  ),
                  rowExpandable: (record) => record.detail?.length > 0,
                  expandIconColumnIndex: 0,
                }}
              />
            </FontSize14px>
          </ContentInformation>
        </div>
        <div className="w-4/12">
          {currentStepIndex === 0 && (
            <ContentInformation className="w-full p-8 mb-5">
              <Form
                form={form}
                onFinish={handleSubmit}
                labelAlign="left"
                layout="horizontal"
                className="flex items-center gap-2 w-full"
              >
                <NoMarginBottom>
                  <Title level={5} style={{ whiteSpace: "nowrap" }}>
                    Git ID
                  </Title>
                </NoMarginBottom>
                <Form.Item
                  name="gitID"
                  className="w-full"
                  style={{ marginBottom: "0" }}
                >
                  <Input
                    style={{
                      maxWidth: "100%",
                    }}
                  />
                </Form.Item>
                <Button
                  type="primary"
                  htmlType={"submit"}
                  className="flex items-center justify-center"
                >
                  Submit
                </Button>
              </Form>
            </ContentInformation>
          )}
          <ContentInformation className="w-full p-8">
            <Title className="text-center" level={4}>
              GROUP {group?.name}
            </Title>
            <FontSize14px>
              <div className="mb-2">English name: {group?.name}</div>
              <div className="mb-2">
                Vietnamese name: {group?.vietnameseTitle}
              </div>
              <div className="mb-2">
                Created at: {moment(group?.createdAt).format(DATE_FORMAT_SLASH)}
              </div>
              <div className="mb-2">Abbreviations: {group?.abbreviations}</div>
              <div className="mb-2">
                Profession: Information Technology A (K15 trở đi)
              </div>
              <div className="mb-2">Specialty: Lập trình .NET</div>
              <div>Description: </div>
              <div className="mb-2">{group?.description}</div>
              <div className="mb-2">Keywords: {group?.keywords}</div>
              <div>Members:</div>
              <div className="members">
                {members.map((member, index) => (
                  <div>{member?.student.user.name}</div>
                ))}
              </div>
              <div className="mb-2"></div>
              <div className="flex items-center justify-center gap-5">
                <Button
                  className="flex items-center justify-center"
                  type="primary"
                >
                  <Link to={"/guidance/schedule"}>View Schedule</Link>
                </Button>
                <Button
                  className="flex items-center justify-center"
                  type="primary"
                >
                  <Link to={"/registration/team"}> View Group Detail</Link>
                </Button>
              </div>
            </FontSize14px>
          </ContentInformation>
        </div>
      </div>
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "semesterStore",
      "groupStore",
      "authenticationStore"
    )(observer(ProgressPage))
  )
);
