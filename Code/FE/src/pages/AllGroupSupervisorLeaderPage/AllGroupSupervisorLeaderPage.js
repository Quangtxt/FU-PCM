import React, { memo, useCallback, useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import { Button, Modal, message, Select, Tooltip, Tabs } from "antd";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";
import uuid from "uuid";
import DashboardLayout from "../../layouts/DashboardLayout";
import { TableBottomPaginationBlock } from "../../components/Common/Table";
import ContentBlockWrapper from "../../components/ContentBlockWrapper";
import PageTitle from "../../components/PageTitle";
import { Helmet } from "react-helmet/es/Helmet";
// import {} from "./AllGroupSupervisorLeaderPageStyled";
import TableComponent from "../../components/Common/TableComponent";
import moment from "moment";
import PopupViewDetailGroup from "../GroupInvitedSupervisorLeaderPage/PopupViewDetailGroup";
import PopupViewGroupSupervisor from "../GroupInvitedSupervisorLeaderPage/PopupViewGroupSupervisor";

import {
  DATE_FORMAT_SLASH,
  SUPERVISOR_LEADER,
  SUPERVISOR_LEADER_STATUS,
  SUPERVISOR_STATUS,
} from "../../constants";
import { render } from "less";
import { set } from "lodash";

const AllGroupSupervisorLeaderPage = (props) => {
  const {
    history,
    loadingAnimationStore,
    groupStore,
    authenticationStore,
  } = props;
  const [
    isVisiblePopupViewDetailGroup,
    setIsVisiblePopupViewDetailGroup,
  ] = useState(false);
  const [
    isVisiblePopupViewGroupSupervisor,
    setIsVisiblePopupViewGroupSupervisor,
  ] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [listGroupHaveSupervisor, setListGroupHaveSupervisor] = useState([]);
  const [listGroupNoSupervisor, setListGroupNoSupervisor] = useState([]);
  const [groupId, setGroupId] = useState();
  const [listSupervisor, setListSupervisor] = useState([]);
  const [supervisorId, setSupervisorId] = useState();
  const [selectedSupervisors, setSelectedSupervisors] = useState({});
  const [activeTab, setActiveTab] = useState("tab1");
  const { TabPane } = Tabs;
  const onChange = (key) => {
    setActiveTab(key);
  };

  const isAllConditionsMet = () => {
    let allSelectedAndValid = true;
    listSupervisor.forEach((item) => {
      const remainingGroups = 4 - item?.count;
      if (remainingGroups < 0) {
        allSelectedAndValid = false;
      }
    });
    return allSelectedAndValid;
  };

  const operations =
    activeTab === "tab2" && isAllConditionsMet ? (
      <Button
        className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
        onClick={handleCreateButtonClick}
      >
        Assign supervisors to the group
      </Button>
    ) : null;

  // console.log("listSupervisor", listSupervisor);
  useEffect(() => {
    if (authenticationStore.currentUser) {
      getAllGroup();
    }
    return () => {
      groupStore.clearStore();
    };
  }, [authenticationStore.currentUser]);
  const getAllGroup = async () => {
    loadingAnimationStore.setTableLoading(true);
    const res = await groupStore.getAllGroup().finally(() => {
      loadingAnimationStore.setTableLoading(false);
    });
    setListGroupHaveSupervisor(res.data?.groupHaveSupervisor);
    setListGroupNoSupervisor(res.data?.groupNoSupervisor);
    setListSupervisor(res.data?.supervisorDTO1List);
  };

  function navigateToView(groupId) {
    setGroupId(groupId);
    setIsVisiblePopupViewDetailGroup(true);
  }

  function navigateToViewGroupSupervisor(supervisorId) {
    setSupervisorId(supervisorId);
    setIsVisiblePopupViewGroupSupervisor(true);
  }
  async function handleCreateButtonClick() {
    try {
      loadingAnimationStore.showSpinner(true);
      const groupSupervisorPairs = listGroupNoSupervisor
        .map((group) => {
          const selectedSupervisor = selectedSupervisors[group.id];
          return {
            groupId: group?.id,
            supervisorId: selectedSupervisor?.value || null,
          };
        })
        .filter((pair) => pair.supervisorId);
      const response = await groupStore.assignTeacher(groupSupervisorPairs);
      if (response.status === 200) {
        message.success("Add supervisors successfully");
        getAllGroup();
      }
    } catch (err) {
      console.log(err);
      loadingAnimationStore.showSpinner(false);
      message.error(err.en || "Error add supervisors!");
    } finally {
      loadingAnimationStore.showSpinner(false);
    }
  }

  const columns = [
    {
      title: "No.",
      width: "2%",
      render: (record, index, dataSource) => <p>{dataSource + 1}</p>,
    },
    {
      title: "Group Code",
      width: "30%",
      render: (record) => (
        <div className="flex justify-between items-center">
          <p>{record?.group.groupCode}</p>
          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
            onClick={() => navigateToView(record?.group.id)}
          >
            View Detail Group
          </Button>
        </div>
      ),
    },
    {
      title: "Create At",
      width: "12%",
      render: (record) => (
        <p>{moment(record?.group.createdAt).format(DATE_FORMAT_SLASH)}</p>
      ),
    },
    {
      title: "Status",
      width: "12%",
      render: (record) => {
        if (record?.status === SUPERVISOR_STATUS.ACCEPT) {
          return <p>Ready</p>;
        }
        if (record?.status === SUPERVISOR_LEADER_STATUS.ACCEPT) {
          return <p>Group approved</p>;
        }
        return null;
      },
    },
    {
      title: "Supervisor",
      width: "27%",
      render: (record) => (
        <div className="flex justify-between items-center">
          <p>
            {record?.supervisor.user.name} ({record.supervisor.user.username})
          </p>
          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
            onClick={() =>
              navigateToViewGroupSupervisor(record?.supervisor.user.id)
            }
          >
            View Group Supervisor
          </Button>
        </div>
      ),
    },
  ];

  const getOptions = () => {
    if (listSupervisor.length > 0) {
      return listSupervisor.map((supervisor) => ({
        value: supervisor?.supervisor?.id,
        label: supervisor?.supervisor?.user?.email,
        count: supervisor?.count,
      }));
    } else {
      return [];
    }
  };
  const handleOptionChange1 = (value, option, record) => {
    const previousValue = selectedSupervisors[record.id]?.value;

    if (previousValue && previousValue !== value.value) {
      // Trừ đi 1 từ count của supervisor trước đó
      updateSupervisorCount(previousValue, -1);
    }

    setSelectedSupervisors((prev) => ({
      ...prev,
      [record.id]: value,
    }));

    if (value.value) {
      updateSupervisorCount(value.value, 1);
    }
  };
  const updateSupervisorCount = (supervisorId, delta) => {
    setListSupervisor((prevList) =>
      prevList.map((item) =>
        item.supervisor.id === supervisorId
          ? { ...item, count: item.count + delta }
          : item
      )
    );
  };

  const columns2 = [
    {
      title: "No.",
      width: "2%",
      render: (record, index, dataSource) => <p>{dataSource + 1}</p>,
    },
    {
      title: "Group Name",
      width: "30%",
      render: (record) => (
        <div className="flex justify-between items-center">
          <p>
            {record?.groupCode} ({record?.vietnameseTitle})
          </p>
          <Button
            className="bg-blue-500 text-white font-bold py-2 px-4 rounded flex items-center ml-2"
            onClick={() => navigateToView(record?.id)}
          >
            View Detail Group
          </Button>
        </div>
      ),
    },
    {
      title: "Create At",
      width: "12%",
      render: (record) => (
        <p>{moment(record?.createdAt).format(DATE_FORMAT_SLASH)}</p>
      ),
    },
    {
      title: "Supervisor",
      width: "27%",
      render: (record) => {
        let allSelected = true;
        let validCounts = true;
        return (
          <div className="flex justify-between items-center">
            <Select
              labelInValue
              value={
                selectedSupervisors[record.id] || {
                  value: "",
                  label: "Select Supervisor",
                }
              }
              onChange={(value, option) =>
                handleOptionChange1(value, option, record)
              }
              options={getOptions()}
              placeholder="Example@fpt.edu.vn"
            />
            {listSupervisor.map((item) => {
              const supervisorOption = getOptions();
              const selectedOption = supervisorOption.find(
                (opt) =>
                  opt.value === (selectedSupervisors[record.id]?.value || "")
              );
              if (!selectedOption) {
                allSelected = false;
              }
              const remainingGroups = 4 - item?.count;
              if (remainingGroups < 0) {
                validCounts = false;
              }

              if (
                selectedOption &&
                item?.supervisor?.id === selectedOption.value
              ) {
                if (remainingGroups < 0) {
                  return (
                    <p className="p-1 text-red-500 ">
                      The supervisor has already reached the maximum number of
                      group options.
                    </p>
                  );
                } else if (remainingGroups === 0) {
                  return (
                    <p className="p-1 text-green-500">
                      The supervisor has no more group options remaining.
                    </p>
                  );
                } else {
                  return (
                    <p className="p-1">
                      The supervisor has {remainingGroups} more group options
                      remaining.
                    </p>
                  );
                }
              }
              return null;
            })}
          </div>
        );
      },
    },
  ];

  return (
    <DashboardLayout>
      <Helmet>
        <title>Registration | All Groups</title>
      </Helmet>
      <PageTitle
        location={props.location}
        title={"All Groups"}
        hiddenGoBack
      ></PageTitle>
      <Tabs
        tabBarExtraContent={operations}
        activeKey={activeTab}
        onChange={onChange}
      >
        <TabPane
          tab={
            <span style={{ fontWeight: "bold" }}>Group have Supervisor</span>
          }
          key="tab1"
        >
          <ContentBlockWrapper>
            <TableComponent
              rowKey={(record) => record.id || uuid()}
              dataSource={listGroupHaveSupervisor}
              columns={columns}
              pagination={false}
              loading={loadingAnimationStore.tableLoading}
            />
          </ContentBlockWrapper>
        </TabPane>

        <TabPane
          tab={
            <span style={{ fontWeight: "bold" }}>
              Group don't have Supervisor
            </span>
          }
          key="tab2"
        >
          <ContentBlockWrapper>
            <TableComponent
              rowKey={(record) => record.id || uuid()}
              dataSource={listGroupNoSupervisor}
              columns={columns2}
              pagination={false}
              loading={loadingAnimationStore.tableLoading}
            />
          </ContentBlockWrapper>
        </TabPane>
      </Tabs>
      <PopupViewDetailGroup
        groupId={groupId}
        isVisiblePopupEdit={isVisiblePopupViewDetailGroup}
        setIsVisiblePopupEdit={setIsVisiblePopupViewDetailGroup}
        handleClosePopup={() => setIsVisiblePopupViewDetailGroup(false)}
        setRefresh={setRefresh}
      />
      <PopupViewGroupSupervisor
        supervisorId={supervisorId}
        isVisiblePopupEdit={isVisiblePopupViewGroupSupervisor}
        setIsVisiblePopupEdit={setIsVisiblePopupViewGroupSupervisor}
        handleClosePopup={() => setIsVisiblePopupViewGroupSupervisor(false)}
        setRefresh={setRefresh}
      />
    </DashboardLayout>
  );
};
export default memo(
  withRouter(
    inject(
      "authenticationStore",
      "loadingAnimationStore",
      "groupStore"
    )(observer(AllGroupSupervisorLeaderPage))
  )
);
