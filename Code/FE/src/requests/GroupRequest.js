import axios from "axios";
import { apiUrl, oauth } from "../config";
import authenticationStore from "../stores/authenticationStore";

export const GroupRequest = {
  createGroup: (
    abbreviations,
    description,
    keywords,
    name,
    vietnameseTitle,
    selectedStudent
  ) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/create`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        abbreviations: abbreviations,
        description: description,
        keywords: keywords,
        name: name,
        vietnameseTitle: vietnameseTitle,
        listStudentID: selectedStudent,
      },
    }),

  editGroup: (
    id,
    name,
    description,
    abbreviations,
    vietnameseTitle,
    keywords
  ) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/edit`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        groupId: id,
        name: name,
        description: description,
        abbreviations: abbreviations,
        vietnameseTitle: vietnameseTitle,
        keywords: keywords,
      },
    }),
  getListInvitationToJoinGroup: () =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group/invitations`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),
  getGroupByMemberId: () =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group/view-group`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),
  getGroupByGroupId: (id) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group/view`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        groupId: id,
      },
    }),

  updateInvitationStatus: (groupId, status, studentId) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/update/invitation/status`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        groupId: groupId,
        status: status,
        studentId: studentId,
      },
    }),
  inviteMember: (groupId, listStudentID) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/inviteMember`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        groupId: groupId,
        listStudentID: listStudentID,
      },
    }),
  updateStatus: (groupId, status, studentId) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/update/invitation/status`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        groupId: groupId,
        status: status,
        studentId: studentId,
      },
    }),

  submitGroup: (groupId, listSupervisorId) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/submitGroup`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        groupId: groupId,
        listSupervisorId: listSupervisorId,
      },
    }),
  empowerOwner: (groupId, studentId) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/empowerOwner`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        groupId: groupId,
        studentId: studentId,
      },
    }),

  //memtor
  getGroupOfSupervisorBySemester: (semesterId) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group-supervisor/list/${semesterId}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        semesterId: semesterId,
      },
    }),
  getGroupOfCommittee: (semesterId) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group/committee/view`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        semesterId: semesterId,
      },
    }),

  getGroupList: (pageSize, pageNumber) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/staff/groups`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        page: pageNumber,
        size: pageSize,
        // keyword: keyword || "",
      },
    }),

  getGroupInvitation: () =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group-supervisor/list/invitation`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),

  updateGroupSupervisorStatus: (id, status) =>
    axios({
      method: "put",
      url: `${apiUrl}/api/v1/group-supervisor/${id}/status`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        status: status,
      },
    }),

  changeStatusGroupSupervisorByGroupId: (groupId, status) =>
    axios({
      method: "put",
      url: `${apiUrl}/api/v1/group-supervisor/put/${groupId}/status`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        newStatus: status,
      },
    }),
  addGit: (gitId, groupId) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group/addGit`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        gitId: gitId,
        groupId: groupId,
      },
    }),
  //SupervisorLeader
  getGroupSupervisorByTwoAcceptStatus: () =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group-supervisor/list-group-supervisor/by-two-status`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),

  getGroupOfSupervisor: (supervisorId) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group-supervisor/groups-of-supervisor/${supervisorId}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),

  getGroupSupervisorBySupervisorId: (supervisorId) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group-supervisor/list-group-supervisor/by-supervisor-id/${supervisorId}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),

  getAllGroup: () =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group-supervisor/all-group`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),
  assignTeacher: (assignTeacherRequests) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/group-supervisor/assignTeacher`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: assignTeacherRequests,
    }),
  getGroupSubmissionBySemester: (id) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/group/submission/final/${id}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),
  confirmGroupSubmission: (id) =>
    axios({
      method: "put",
      url: `${apiUrl}/api/v1/group/confirm/${id}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: "APPROVED",
    }),
  rejectGroupSubmission: (id) =>
    axios({
      method: "put",
      url: `${apiUrl}/api/v1/group/confirm/${id}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: "REJECT",
    }),
  addGroupCpComment: (req) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/g/comment`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: req,
    }),
  getGroupCpCommentByGroupId: (id) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/g/comment/${id}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
    }),
  updateGroupCpComment: (id, req) =>
    axios({
      method: "put",
      url: `${apiUrl}/api/v1/g/comment/${id}`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: req,
    }),
  getGroupSubmissionToSetTime: (id) =>
    axios({
      method: "get",
      url: `${apiUrl}/api/v1/staff/getGroupSubmissionToSetTime`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      params: {
        semesterId: id,
      },
    }),
  setForGroupTimeSubmit: (startSubmit, endSubmit, groupId) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/staff/setForGroupTimeSubmit`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        startSubmit: startSubmit,
        endSubmit: endSubmit,
        groupId: groupId,
      },
    }),
  setAllForGroupTimeSubmit: (id, startSubmit, endSubmit) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/staff/setAllForGroupTimeSubmit`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        semesterId: id,
        startSubmit: startSubmit,
        endSubmit: endSubmit,
      },
    }),
  setAllForGroupTimeApprove: (id, startApprove, endApprove) =>
    axios({
      method: "post",
      url: `${apiUrl}/api/v1/staff/getGroupSubmissionToSetTime`,
      headers: {
        Authorization: `Bearer ${JSON.parse(authenticationStore.appToken)}`,
        "Content-Type": "application/json",
      },
      data: {
        semesterId: id,
        startApprove: startApprove,
        endApprove: endApprove,
      },
    }),
};
