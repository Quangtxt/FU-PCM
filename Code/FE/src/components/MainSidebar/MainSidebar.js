import React, { memo, useCallback, useEffect, useState } from "react";
import { Badge, Menu } from "antd";
import { Link, withRouter } from "react-router-dom";
import { inject, observer } from "mobx-react";
import {
  ACL_ACTION_TYPE,
  MODULE_CODE,
  VAN_PHONG_DIEN_TU,
} from "../../constants";
import {
  ApartmentOutlined,
  BarChartOutlined,
  CalendarOutlined,
  FileProtectOutlined,
  FileTextOutlined,
  HomeOutlined,
  ProfileOutlined,
  SnippetsOutlined,
  SolutionOutlined,
  ScheduleOutlined,
  ReconciliationOutlined,
} from "@ant-design/icons";
import { SiderbarWrapper } from "./MainSidebarStyled";
import { toJS } from "mobx";

const sortCommands = (a, b) => {
  const nameA = a.name.toUpperCase();
  const nameB = b.name.toUpperCase();
  let comparison = 0;
  if (nameA > nameB) {
    comparison = 1;
  } else if (nameA < nameB) {
    comparison = -1;
  }
  return comparison;
};

const MainSidebar = (props) => {
  const {
    location,
    history,
    commandStore,
    accountStore,
    authenticationStore,
    commonStore,
    moduleStore,
    aclStore,
  } = props;
  const {
    currentUser,
    isAccountAdmin,
    isSuperAdmin,
    isSupervisor,
    isStaff,
    isCommittee,
    isSupervisorLeader,
  } = authenticationStore;
  const { openedSubMenu, setOpenedSubMenu, collapsedMenu } = commonStore;
  const { moduleList } = moduleStore;
  const { aclActionsByUser } = aclStore;

  const [moduleFlatList, setModuleFlatList] = useState([]);

  useEffect(() => {
    // commandStore.getSideMenuCounter(["WORK", "PROPOSAL", "INTERNAL_MESSAGE"]);
  }, [location.pathname]);

  useEffect(() => {
    const moduleFlatListConvert = [];
    moduleList.forEach((item) => {
      moduleFlatListConvert.push({
        ...toJS(item),
        sub_modules: null,
      });
      if (item.sub_modules?.length > 0) {
        item.sub_modules.forEach((el) => {
          moduleFlatListConvert.push(toJS(el));
        });
      }
    });
    setModuleFlatList(moduleFlatListConvert);
  }, [moduleList]);

  // Menu Home
  const menuTrangChu = (
    <Menu.Item key={"/home"} icon={<HomeOutlined />}>
      <Link to={"/home"}>Home</Link>
    </Menu.Item>
  );
  const menuCommittee = (
    <Menu.Item key={"/s/committee/group"} icon={<SnippetsOutlined />}>
      <Link to={"/s/committee/group"}>Committee</Link>
    </Menu.Item>
  );
  const menuStaff = isStaff && (
    <Menu.Item key={"/staff/semester"} icon={<ApartmentOutlined />}>
      <Link to={"/staff/semester"}>Manager Semester</Link>
    </Menu.Item>
  );
  // Menu đăng ký đồ án
  const menuRegistrationPhase = (
    <Menu.SubMenu
      key={"registration"}
      icon={<CalendarOutlined />}
      title="Registration Phase"
    >
      {isSupervisor && (
        <Menu.Item key={"/registration/std-request"}>
          <Link to={"/registration/std-request"}>
            Registration Group You Recommend
          </Link>
        </Menu.Item>
      )}
      {isStaff && (
        <Menu.Item key={"/student/list"}>
          <Link to={"/student/list"}>Manager Student</Link>
        </Menu.Item>
      )}
      {isStaff && (
        <Menu.Item key={"/supervisor/list"}>
          <Link to={"/supervisor/list"}>Manager Supervisor</Link>
        </Menu.Item>
      )}
      {isStaff && (
        <Menu.Item key={"/group/list"}>
          <Link to={"/group/list"}>Manager Group</Link>
        </Menu.Item>
      )}
      {isSupervisorLeader && (
        <Menu.Item key={"/registration/group-supervisor-leader-request"}>
          <Link to={"/registration/group-supervisor-leader-request"}>
            The Supervisor's proposal
          </Link>
        </Menu.Item>
      )}
      {isSupervisorLeader && (
        <Menu.Item key={"/registration/list-all-group-spLeader"}>
          <Link to={"/registration/list-all-group-spLeader"}>All groups</Link>
        </Menu.Item>
      )}
    </Menu.SubMenu>
  );
  // Menu hướng dẫn đồ án
  const menuGuidancePhase = (
    <Menu.SubMenu
      key={"guidance"}
      icon={<ProfileOutlined />}
      title="Guidance Phase"
    >
      {isStaff && (
        <Menu.Item key={"/committee"}>
          <Link to={"/committee"}>Manager Committee</Link>
        </Menu.Item>
      )}
      {isStaff && (
        <Menu.Item key={"/groupSubmission"}>
          <Link to={"/groupSubmission"}>Manager Group submission</Link>
        </Menu.Item>
      )}
      {isSupervisor && (
        <Menu.Item key={"/guidance/group"} icon={<BarChartOutlined />} title>
          <Link to={"/guidance/group"}>Manager Group</Link>
        </Menu.Item>
      )}
      {isSupervisor && (
        <Menu.Item
          key={"/guidance/schedule-supervisor"}
          icon={<ScheduleOutlined />}
          title
        >
          <Link to={"/guidance/schedule-supervisor"}>Schedule</Link>
        </Menu.Item>
      )}
    </Menu.SubMenu>
  );
  const menuSubmit = isSupervisor && (
    <Menu.Item key={"/cp/submit"} icon={<SolutionOutlined />}>
      <Link to={"/cp/submit"}>Project Submit</Link>
    </Menu.Item>
  );

  const onSubMenuToggle = useCallback((keys) => {
    setOpenedSubMenu(keys);
  }, []);

  const onClickMenuItem = ({ keyPath }) => {
    setOpenedSubMenu([keyPath[1]]);
  };

  useEffect(() => {
    // Home
    if (location.pathname.includes("/home")) {
      commonStore.setPage(["/home"]);
      setOpenedSubMenu([]);
      return;
    }
    if (location.pathname.includes("/staff/semester")) {
      commonStore.setPage(["/staff/semester"]);
      setOpenedSubMenu([]);
      return;
    }
    if (location.pathname.includes("/s/committee/group")) {
      commonStore.setPage(["/s/committee/group"]);
      setOpenedSubMenu([]);
      return;
    }
    // Đăng ký đồ án
    if (location.pathname.includes("/registration/std-request")) {
      commonStore.setPage(["/registration/std-request"]);
      setOpenedSubMenu(["registration"]);
      return;
    }
    if (location.pathname.includes("/student/list")) {
      commonStore.setPage(["/student/list"]);
      setOpenedSubMenu(["registration"]);
      return;
    }
    if (location.pathname.includes("/group/list")) {
      commonStore.setPage(["/group/list"]);
      setOpenedSubMenu(["registration"]);
      return;
    }
    if (location.pathname.includes("/supervisor/list")) {
      commonStore.setPage(["/supervisor/list"]);
      setOpenedSubMenu(["registration"]);
      return;
    }
    if (location.pathname.includes("/committee")) {
      commonStore.setPage(["/committee"]);
      setOpenedSubMenu(["guidance"]);
      return;
    }
    if (location.pathname.includes("/guidance/group")) {
      commonStore.setPage(["/guidance/group"]);
      setOpenedSubMenu(["guidance"]);
      return;
    }
    if (location.pathname.includes("/guidance/schedule-supervisor")) {
      commonStore.setPage(["/guidance/schedule-supervisor"]);
      setOpenedSubMenu(["guidance"]);
      return;
    }

    commonStore.setPage([location.pathname]);
  }, [location.pathname]);

  return (
    <SiderbarWrapper>
      <Menu
        mode="inline"
        selectedKeys={commonStore.pageName}
        openKeys={openedSubMenu}
        onOpenChange={onSubMenuToggle}
        inlineCollapsed={!collapsedMenu}
        onClick={onClickMenuItem}
      >
        {menuTrangChu}
        {menuStaff}
        {menuRegistrationPhase}
        {menuGuidancePhase}
        {menuSubmit}
        {isCommittee && menuCommittee}
      </Menu>
    </SiderbarWrapper>
  );
};

export default memo(
  withRouter(
    inject(
      "commandStore",
      "accountStore",
      "authenticationStore",
      "commonStore",
      "loadingAnimationStore",
      "notificationStore",
      "moduleStore",
      "aclStore"
    )(observer(MainSidebar))
  )
);
