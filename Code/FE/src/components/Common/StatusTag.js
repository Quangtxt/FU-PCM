import React from "react";

import { Tag } from "antd";
import { NOTIFICATION_STATUS } from "../../constants";

export const StatusTag = (typePage, status, isText = false) => {
  let text = "Unclear";
  let color = null;

  if (status === NOTIFICATION_STATUS.REQUESTGROUP) {
    color = "green";
    text = "REQUEST_GROUP";
  }
  if (status === NOTIFICATION_STATUS.GROUP) {
    color = "orange";
    text = "GROUP";
  }
  if (status === NOTIFICATION_STATUS.NEWS) {
    color = "purple";
    text = "NEWS";
  }
  if (status === NOTIFICATION_STATUS.RECHECKMILESTONE) {
    color = "#69c0ff";
    text = "RE_CHECK_MILESTONE";
  }
  if (status === NOTIFICATION_STATUS.RESPONSECHECKMILESTONE) {
    color = "#597ef7";
    text = "RESPONSE_CHECK_MILESTONE";
  }
  if (status === NOTIFICATION_STATUS.MEETING) {
    color = "#d03bff";
    text = "MEETING";
  }

  // if ([NOTIFICATION_STATUS.PROPOSAL_SALARY].includes(status)) {
  //   color = "cyan";
  //   text = "Duyệt lương";
  // }

  // if (status === NOTIFICATION_STATUS.POLICY) {
  //   color = "#69c0ff";
  //   text = "Xin chủ chương";
  // }

  // if (status === NOTIFICATION_STATUS.GENERAL) {
  //   color = "blue";
  //   text = "Thông báo chung";
  // }

  if (isText) {
    return text;
  }

  return (
    <span style={{ display: "flex", alignItems: "center" }}>
      {color && (
        <Tag color={color} style={{ borderRadius: 10, margin: 0 }}>
          {text}
        </Tag>
      )}
    </span>
  );
};
