import HomePage from "./pages/HomePage/HomePage";
import GroupInvitedSupervisorPage from "./pages/GroupInvitedSupervisorPage/GroupInvitedSupervisorPage";
import ProfilePage from "./pages/ProfilePage/ProfilePage";
import ProfileSupervisorPage from "./pages/ProfileSupervisorPage";
import CreateNotePage from "./pages/Meeting/Note/CreateNotePage";
import DetailProfileSupervisorPage from "./pages/DetailProfileSupervisorPage";
import ListStudentPage from "./pages/ListStudentPage";
import ListSupervisorPage from "./pages/ListSupervisorPage";
import ListGroupPage from "./pages/ListGroupPage";
import NotePage from "./pages/Meeting/Note/NotePage";
import ManageTaskPage from "./pages/Guidance/ManageTaskPage";
import ManageGroupPage from "./pages/Guidance/ManageGroupPage";
import ScheduleSupervisorPage from "./pages/Guidance/ScheduleSupervisorPage";
import SemesterPage from "./pages/SemesterPage";
import NotificationPage from "./pages/NotificationPage/NotificationPage";
import SemesterPageDemo from "./pages/SemesterPage";
import ManageGroupProgressPage from "./pages/Guidance/ManageGroupProgressPage/ManageGroupProgressPage";
import ProgressPage from "./pages/ProgressPage";
import ManagerCommitteePage from "./pages/ManagerCommitteePage/ManagerCommitteePage";
import CommitteeGroupPage from "./pages/CommitteeGroupPage/CommitteeGroupPage";
import DownloadReportPage from "./pages/Guidance/ManageGroupProgressPage/DownloadReportPage";
import GroupInvitedSupervisorLeaderPage from "./pages/GroupInvitedSupervisorLeaderPage/GroupInvitedSupervisorLeaderPage";
import AllGroupSupervisorLeaderPage from "./pages/AllGroupSupervisorLeaderPage/AllGroupSupervisorLeaderPage";
import CapstoneProjectSubmitPage from "./pages/CapstoneProjectSubmitPage/CapstoneProjectSubmitPage";
import GroupSubmissionPage from "./pages/GroupSubmissionPage/GroupSubmissionPage";

export const normalRoutes = [
  {
    path: "/login",
    component: HomePage,
    name: "Login",
  },
];

export const routes = [
  {
    path: "/",
    component: HomePage,
    name: "Home",
  },
  {
    path: "/home",
    component: HomePage,
    name: "HomePage",
  },
  {
    path: "/profile/:id",
    component: ProfilePage,
    name: "ProfilePage",
  },
  {
    path: "/profile",
    component: ProfilePage,
    name: "ProfilePage",
  },
  {
    path: "/registration/supervisor/detail",
    component: DetailProfileSupervisorPage,
    name: "DetailProfileSupervisorPage",
  },

  //student-phase-2

  {
    path: "/guidance/meeting/note/create",
    component: CreateNotePage,
    name: "CreateNotePage",
  },
  {
    path: "/guidance/meeting/notes/:meetingId",
    component: NotePage,
    name: "NotePage",
  },
  {
    path: "/guidance/task",
    component: ManageTaskPage,
    name: "ManageTaskPage",
  },
  {
    path: "/notification",
    component: NotificationPage,
    name: "NotificationPage",
  },
  {
    path: "/student/progress",
    component: ProgressPage,
    name: "ProgressPage",
  },

  //supervisor
  {
    path: "/registration/std-request",
    component: GroupInvitedSupervisorPage,
    name: "GroupInvitedSupervisorPage",
  },
  {
    path: "/guidance/schedule-supervisor",
    component: ScheduleSupervisorPage,
    name: "ScheduleSupervisorPage",
  },
  {
    path: "/guidance/group",
    component: ManageGroupPage,
    name: "Manager Group",
  },
  {
    path: "/guidance/group/progress/:id",
    component: ManageGroupProgressPage,
    name: "Manager Group Progress",
  },
  {
    path: "/supervisor/profile",
    component: ProfileSupervisorPage,
    name: "Profile Supervisor",
  },
  {
    path: "/s/committee/group",
    component: CommitteeGroupPage,
    name: "Group committee",
  },
  {
    path: "/download-report/:groupId/:id",
    component: DownloadReportPage,
    name: "Group committee",
  },

  //staff
  {
    path: "/student/list",
    component: ListStudentPage,
    name: "ListStudentPage",
  },
  {
    path: "/group/list",
    component: ListGroupPage,
    name: "ListGroupPage",
  },
  {
    path: "/supervisor/list",
    component: ListSupervisorPage,
    name: "ListSupervisorPage",
  },
  {
    path: "/committee",
    component: ManagerCommitteePage,
    name: "ManagerCommitteePage",
  },
  {
    path: "/groupSubmission",
    component: GroupSubmissionPage,
    name: "groupSubmission",
  },
  //admin
  {
    path: "/staff/semester",
    component: SemesterPage,
    name: "SemesterPage",
  },
  //supervisorLeader
  {
    path: "/registration/group-supervisor-leader-request",
    component: GroupInvitedSupervisorLeaderPage,
    name: "GroupInvitedSupervisorLeaderPage",
  },
  {
    path: "/registration/list-all-group-spLeader",
    component: AllGroupSupervisorLeaderPage,
    name: "AllGroupSupervisorLeaderPage",
  },
  {
    path: "/cp/submit",
    component: CapstoneProjectSubmitPage,
    name: "CapstoneProjectSubmitPage",
  },
];
