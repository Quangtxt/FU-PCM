
package com.pcms.be.service;
import com.pcms.be.domain.Committee;
import com.pcms.be.domain.CommitteeAssignment;
import com.pcms.be.domain.Semester;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.DTO.CommitteeAssignmentDTO;
import com.pcms.be.pojo.DTO.CommitteeDTO;
import com.pcms.be.pojo.DTO.GroupDTO;
import com.pcms.be.pojo.DTO.SemesterDTO;
import com.pcms.be.repository.CommitteeAssignmentRepository;
import com.pcms.be.repository.CommitteeRepository;
import com.pcms.be.repository.SemesterRepository;
import com.pcms.be.service.impl.CommitteeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CommitteeServiceTest {
    @Mock
    private CommitteeRepository committeeRepository;
    @Mock
    private SemesterRepository semesterRepository;
    @Mock
    private UserService userService;
    @Mock
    private CommitteeAssignmentRepository committeeAssignmentRepository;
    @Mock
    private ModelMapper modelMapper;
    @InjectMocks
    private CommitteeServiceImpl committeeService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    @Test
    void testGetCommitteesThrowSemesterNotFound() throws ServiceException {
        Long semesterId = 1L;
        Pageable pageable = PageRequest.of(1,1);
        when(semesterRepository.findById(semesterId)).thenReturn(Optional.empty());

        ServiceException exceptionResponse = assertThrows(ServiceException.class, ()
                -> committeeService.getCommittees(pageable,1));
        assertEquals(ErrorCode.SEMESTER_NOT_FOUND, exceptionResponse.getErrorCode());
    }

    @Test
    void testGetCommitteeSuccessfully() throws ServiceException {
        int semesterId = 2;
        Pageable pageable = Mockito.mock(Pageable.class);

        Semester semester = new Semester();
        semester.setId(1L);

        Committee committee1 = new Committee();
        committee1.setId(1L);
        committee1.setCommitteeCode("C001");
        committee1.setCommitteeName("Committee 1");
        committee1.setSemester(semester);

        Committee committee2 = new Committee();
        committee2.setId(2L);
        committee2.setCommitteeCode("C002");
        committee2.setCommitteeName("Committee 2");
        committee2.setSemester(semester);

        Page<Committee> committees = new PageImpl<>(Arrays.asList(committee1, committee2));

        CommitteeDTO committeeDTO1 = new CommitteeDTO();
        committeeDTO1.setId(1L);
        committeeDTO1.setCommitteeCode("C001");
        committeeDTO1.setCommitteeName("Committee 1");
        committeeDTO1.setSemester(new SemesterDTO());
        CommitteeDTO committeeDTO2 = new CommitteeDTO();
        committeeDTO2.setId(2L);
        committeeDTO2.setCommitteeCode("C002");
        committeeDTO2.setCommitteeName("Committee 2");
        committeeDTO2.setSemester(new SemesterDTO());

        Mockito.when(semesterRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(semester));
        Mockito.when(committeeRepository.findAllBySemester(Mockito.any(Pageable.class), Mockito.any(Semester.class))).thenReturn(committees);
        Mockito.when(modelMapper.map(Mockito.any(Committee.class), Mockito.eq(CommitteeDTO.class))).thenReturn(committeeDTO1, committeeDTO2);

        // Act
        ResponseEntity<Map<String, Object>> response = committeeService.getCommittees(pageable, semesterId);

        // Assert
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        Map<String, Object> result = response.getBody();
        assertNotNull(result);
        assertEquals(2, ((java.util.List<?>) result.get("data")).size());
    }

//    @Test
//    public void testGetCommitteeAssignments_Success() {
//        // Arrange
//        int semesterId = 1;
//        Pageable pageable = Mockito.mock(Pageable.class);
//        Committee committee1 = new Committee();
//        committee1.setId(1L);
//        committee1.setCommitteeCode("C001");
//        committee1.setCommitteeName("Committee 1");
//        Group group1 = new Group();
//        group1.setId(1L);
//        group1.setName("Group 1");
//        CommitteeAssignment assignment1 = new CommitteeAssignment();
//        assignment1.setId(1L);
//        assignment1.setCommittee(committee1);
//        assignment1.setGroup(group1);
//        assignment1.setTime("10:00 AM");
//        assignment1.setDate("2023-08-16");
//        Committee committee2 = new Committee();
//        committee2.setId(2L);
//        committee2.setCommitteeCode("C002");
//        committee2.setCommitteeName("Committee 2");
//        Group group2 = new Group();
//        group2.setId(2L);
//        group2.setName("Group 2");
//        CommitteeAssignment assignment2 = new CommitteeAssignment();
//        assignment2.setId(2L);
//        assignment2.setCommittee(committee2);
//        assignment2.setGroup(group2);
//        assignment2.setTime("2:00 PM");
//        assignment2.setDate("2023-08-17");
//        Page<CommitteeAssignment> assignments = new PageImpl<>(Arrays.asList(assignment1, assignment2));
//        CommitteeAssignmentDTO assignmentDTO1 = new CommitteeAssignmentDTO();
//        assignmentDTO1.setId(1L);
//        assignmentDTO1.setCommittee(new CommitteeDTO());
//        assignmentDTO1.setGroup(new GroupDTO());
//        assignmentDTO1.setTime("10:00 AM");
//        assignmentDTO1.setDate("2023-08-16");
//        CommitteeAssignmentDTO assignmentDTO2 = new CommitteeAssignmentDTO();
//        assignmentDTO2.setId(2L);
//        assignmentDTO2.setCommittee(new CommitteeDTO());
//        assignmentDTO2.setGroup(new GroupDTO());
//        assignmentDTO2.setTime("2:00 PM");
//        assignmentDTO2.setDate("2023-08-17");
//
//        Mockito.when(committeeAssignmentRepository.findAllBySemesterId(Mockito.any(Pageable.class), Mockito.anyInt())).thenReturn(assignments);
//        Mockito.when(modelMapper.map(Mockito.any(CommitteeAssignment.class), Mockito.eq(CommitteeAssignmentDTO.class))).thenReturn(assignmentDTO1, assignmentDTO2);
//
//        // Act
//        ResponseEntity<Map<String, Object>> response = committeeService.getCommitteeAssignments(pageable, semesterId);
//
//        // Assert
//        assertNotNull(response);
//        assertEquals(200, response.getStatusCodeValue());
//        Map<String, Object> result = response.getBody();
//        assertNotNull(result);
//        assertEquals(2, ((java.util.List<?>) result.get("data")).size());
//    }

    @Test
    void testGetCommitteeAssignmentsByCommittee_UserNotFound() throws ServiceException {
        User currentUser = new User();
        int semesterId = 1;
        currentUser.setId(1L);
        when(userService.getCurrentUser()).thenThrow(new ServiceException(ErrorCode.USER_NOT_FOUND));
        ServiceException exceptionResponse = assertThrows(ServiceException.class, () -> committeeService.getCommitteeAssignmentsByCommittee(semesterId));
        assertEquals(ErrorCode.USER_NOT_FOUND, exceptionResponse.getErrorCode());
    }

    @Test
    void testGetCommitteeAssignmentsByCommittee_SemesterNotFound() throws ServiceException {
        // Arrange
        User currentUser = new User();
        currentUser.setId(1L);
        when(userService.getCurrentUser()).thenReturn(currentUser);
        int semesterId = 1;
        when(semesterRepository.findById(anyLong())).thenReturn(Optional.empty());

        // Act and Assert
        ServiceException exceptionResponse = assertThrows(ServiceException.class, () -> committeeService.getCommitteeAssignmentsByCommittee(semesterId));
        assertEquals(ErrorCode.SEMESTER_NOT_FOUND, exceptionResponse.getErrorCode());
    }
    @Test
    void testGetCommitteeAssignmentsByCommittee_Success() throws ServiceException {
        // Arrange
        User currentUser = new User();
        currentUser.setId(2L);
        when(userService.getCurrentUser()).thenReturn(currentUser);

        int semesterId = 1;
        Semester semester = new Semester();
        semester.setId(1L);
        when(semesterRepository.findById(anyLong())).thenReturn(Optional.of(semester));

        Committee committee1 = new Committee();
        committee1.setId(1L);
        Committee committee2 = new Committee();
        committee2.setId(2L);
        List<Committee> committees = Arrays.asList(committee1, committee2);
        when(committeeRepository.findAllByEmployeeSemester(anyLong(), anyLong())).thenReturn(committees);

        CommitteeAssignment assignment1 = new CommitteeAssignment();
        assignment1.setId(1L);
        CommitteeAssignment assignment2 = new CommitteeAssignment();
        assignment2.setId(2L);
        List<CommitteeAssignment> assignments = Arrays.asList(assignment1, assignment2);
        when(committeeAssignmentRepository.findAllByCommittees(anyList())).thenReturn(assignments);

        CommitteeAssignmentDTO dto1 = new CommitteeAssignmentDTO();
        dto1.setId(1L);
        CommitteeAssignmentDTO dto2 = new CommitteeAssignmentDTO();
        dto2.setId(2L);
        List<CommitteeAssignmentDTO> expectedResult = Arrays.asList(dto1, dto2);
        when(modelMapper.map(any(CommitteeAssignment.class), eq(CommitteeAssignmentDTO.class))).thenReturn(dto1, dto2);

        // Act
        List<CommitteeAssignmentDTO> result = committeeService.getCommitteeAssignmentsByCommittee(semesterId);

        // Assert
        assertNotNull(result);
        assertEquals(expectedResult.size(), result.size());
        for (int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i).getId(), result.get(i).getId());
        }
        verify(userService, times(1)).getCurrentUser();
        verify(semesterRepository, times(1)).findById(anyLong());
        verify(committeeRepository, times(1)).findAllByEmployeeSemester(anyLong(), anyLong());
        verify(committeeAssignmentRepository, times(1)).findAllByCommittees(anyList());
        verify(modelMapper, times(2)).map(any(CommitteeAssignment.class), eq(CommitteeAssignmentDTO.class));
    }


}
