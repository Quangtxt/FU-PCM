package com.pcms.be.service;

import com.pcms.be.domain.Semester;
import com.pcms.be.domain.Semester_Milestone;
import com.pcms.be.pojo.DTO.SemesterDTO;
import com.pcms.be.pojo.DTO.SemesterMilestone2DTO;
import com.pcms.be.pojo.request.CreatedSemesterRequest;
import com.pcms.be.repository.SemesterRepository;
import com.pcms.be.repository.SemesterMilestoneRepository;
import com.pcms.be.service.impl.SemesterServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.*;

import java.time.OffsetDateTime;
import java.util.*;

import static org.mockito.Mockito.*;

public class SemesterServiceTest {
    @Mock
    private SemesterRepository semesterRepository;

    @Mock
    private SemesterMilestoneRepository semesterMilestoneRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private SemesterServiceImpl semesterServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAll() {
        // Arrange
        Semester semester = new Semester();
        semester.setId(1L);
        SemesterDTO semesterDTO = new SemesterDTO();
        semesterDTO.setId(1L);

        Semester_Milestone milestone = new Semester_Milestone();
        SemesterMilestone2DTO milestoneDTO = new SemesterMilestone2DTO();

        when(semesterRepository.findAll()).thenReturn(List.of(semester));
        when(modelMapper.map(semester, SemesterDTO.class)).thenReturn(semesterDTO);
        when(semesterRepository.findById(1L)).thenReturn(Optional.of(semester));
        when(semesterMilestoneRepository.findAllBySemester(semester)).thenReturn(List.of(milestone));
        when(modelMapper.map(milestone, SemesterMilestone2DTO.class)).thenReturn(milestoneDTO);

        // Act
        ResponseEntity<List<SemesterDTO>> response = semesterServiceImpl.getAll();

        // Assert
        assertEquals(1, response.getBody().size());
        assertEquals(semesterDTO, response.getBody().get(0));
        verify(semesterRepository, times(1)).findAll();
        verify(semesterRepository, times(1)).findById(1L);
        verify(semesterMilestoneRepository, times(1)).findAllBySemester(semester);
    }
    @Test
    public void testGetAll_noSemesters() {
        // Arrange
        when(semesterRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        ResponseEntity<List<SemesterDTO>> response = semesterServiceImpl.getAll();

        // Assert
        assertEquals(0, response.getBody().size());
        verify(semesterRepository, times(1)).findAll();
        verify(semesterRepository, never()).findById(anyLong());
        verify(semesterMilestoneRepository, never()).findAllBySemester(any());
    }
    //----------------------------------------------------------------------------------
    @Test
    public void testGetById_Success() {
        // Arrange
        Long id = 1L;
        Semester semester = new Semester();
        SemesterDTO semesterDTO = new SemesterDTO();
        when(semesterRepository.findById(id)).thenReturn(Optional.of(semester));
        when(modelMapper.map(semester, SemesterDTO.class)).thenReturn(semesterDTO);

        // Act
        ResponseEntity<SemesterDTO> response = semesterServiceImpl.getById(1);

        // Assert
        assertEquals(semesterDTO, response.getBody());
    }

    @Test
    public void testGetById_NotFound() {
        // Arrange
        Long id = 1L;
        Semester semester = new Semester();
        SemesterDTO semesterDTO = new SemesterDTO();
        when(semesterRepository.findById(id)).thenReturn(Optional.empty());
        when(modelMapper.map(semester, SemesterDTO.class)).thenReturn(semesterDTO);

        // Act
        ResponseEntity<SemesterDTO> response = semesterServiceImpl.getById(1);

        // Assert
        assertEquals(404, response.getStatusCodeValue());

    }
    //----------------------------------------------------------------------------------


}
