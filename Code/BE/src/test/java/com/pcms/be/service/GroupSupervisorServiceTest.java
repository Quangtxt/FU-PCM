package com.pcms.be.service;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.GroupSupervisor;
import com.pcms.be.domain.user.Supervisor;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.DTO.Group1DTO;
import com.pcms.be.pojo.DTO.SupervisorDTO;
import com.pcms.be.pojo.request.AssignTeacherRequest;
import com.pcms.be.pojo.response.GroupSupervisorResponse;
import com.pcms.be.repository.GroupRepository;
import com.pcms.be.repository.GroupSupervisorRepository;
import com.pcms.be.repository.SupervisorRepository;
import com.pcms.be.service.impl.GroupServiceImpl;
import com.pcms.be.service.impl.GroupSupervisorServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class GroupSupervisorServiceTest {
    @Mock
    private GroupSupervisorRepository groupSupervisorRepository;

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserService userService;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private SupervisorRepository supervisorRepository;
    @InjectMocks
    private GroupSupervisorServiceImpl groupSupervisorService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testPutStatusThrowFailedEditGroup(){
        when(groupSupervisorRepository.findById(anyLong())).thenReturn(Optional.empty());
        ServiceException response = assertThrows(ServiceException.class, ()
        -> groupSupervisorService.putStatus(1L, anyString()));
        assertEquals(ErrorCode.FAILED_EDIT_GROUP, response.getErrorCode());
    }

    @Test
    void testPutStatus_RejectSupervisor() throws ServiceException {
        // Arrange
        Long id = 1L;
        String newStatus = Constants.SupervisorStatus.REJECT_SUPERVISOR;
        GroupSupervisor groupSupervisor = new GroupSupervisor();
        groupSupervisor.setId(id);
        when(groupSupervisorRepository.findById(id)).thenReturn(Optional.of(groupSupervisor));

        // Act
        GroupSupervisor result = groupSupervisorService.putStatus(id, newStatus);

        // Assert
        assertSame(groupSupervisor, result);
        verify(groupSupervisorRepository, times(1)).findById(id);
        verify(groupSupervisorRepository, times(1)).delete(groupSupervisor);
        verify(groupSupervisorRepository, never()).save(any());
    }

    @Test
    void testPutStatus_RejectSupervisorLeader() throws ServiceException {
        // Arrange
        Long id = 1L;
        String newStatus = Constants.SupervisorStatus.REJECT_SUPERVISOR_LEADER;
        GroupSupervisor groupSupervisor = new GroupSupervisor();
        groupSupervisor.setId(id);
        when(groupSupervisorRepository.findById(id)).thenReturn(Optional.of(groupSupervisor));

        // Act
        GroupSupervisor result = groupSupervisorService.putStatus(id, newStatus);

        // Assert
        assertSame(groupSupervisor, result);
        verify(groupSupervisorRepository, times(1)).findById(id);
        verify(groupSupervisorRepository, times(1)).delete(groupSupervisor);
        verify(groupSupervisorRepository, never()).save(any());
    }

//    @Test
//    void testPutStatus_UpdateStatus() throws ServiceException {
//        // Arrange
//        Long id = 1L;
//        String newStatus = "NEW_STATUS";
//        GroupSupervisor groupSupervisor = new GroupSupervisor();
//        groupSupervisor.setId(id);
//        groupSupervisor.setStatus("OLD_STATUS");
//        when(groupSupervisorRepository.findById(id)).thenReturn(Optional.of(groupSupervisor));
//
//        // Act
//        GroupSupervisor result = groupSupervisorService.putStatus(id, newStatus);
//
//        // Assert
//        assertSame(groupSupervisor, result);
//        assertEquals(newStatus, groupSupervisor.getStatus());
//        verify(groupSupervisorRepository, times(1)).findById(id);
//        verify(groupSupervisorRepository, times(1)).save(groupSupervisor);
//        verify(groupSupervisorRepository, never()).delete(any());
//    }

    @Test
    void testGetByStatus_PendingSupervisor() throws ServiceException {
        // Arrange
        String status = Constants.SupervisorStatus.PENDING_SUPERVISOR;
        User currentUser = new User();
        currentUser.setSupervisor(new Supervisor());
        when(userService.getCurrentUser()).thenReturn(currentUser);
        List<GroupSupervisor> groupSupervisors = new ArrayList<>();
        GroupSupervisor groupSupervisor1 = new GroupSupervisor();
        groupSupervisor1.setId(1L);
        groupSupervisor1.setGroup(new Group());
        GroupSupervisor groupSupervisor2 = new GroupSupervisor();
        groupSupervisor2.setId(2L);
        groupSupervisor2.setGroup(new Group());
        groupSupervisors.add(groupSupervisor1);
        groupSupervisors.add(groupSupervisor2);
        when(groupSupervisorRepository.findBySupervisorAndStatus(currentUser.getSupervisor(), status)).thenReturn(groupSupervisors);

        // Mock modelMapper.map()
        GroupSupervisorResponse response1 = new GroupSupervisorResponse();
        response1.setId(1L);
        response1.setGroup(new Group1DTO());
        response1.setSupervisor(new SupervisorDTO());
        response1.setStatus(Constants.SupervisorStatus.PENDING_SUPERVISOR);
        GroupSupervisorResponse response2 = new GroupSupervisorResponse();
        response2.setId(2L);
        response2.setGroup(new Group1DTO());
        response2.setSupervisor(new SupervisorDTO());
        response2.setStatus(Constants.SupervisorStatus.PENDING_SUPERVISOR);
        when(modelMapper.map(groupSupervisor1, GroupSupervisorResponse.class)).thenReturn(response1);
        when(modelMapper.map(groupSupervisor2, GroupSupervisorResponse.class)).thenReturn(response2);

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getByStatus(status);

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(userService, times(1)).getCurrentUser();
        verify(groupSupervisorRepository, times(1)).findBySupervisorAndStatus(currentUser.getSupervisor(), status);
        verify(modelMapper, times(2)).map(any(GroupSupervisor.class), eq(GroupSupervisorResponse.class));
        for (GroupSupervisorResponse response : result) {
            assertNotNull(response.getId());
            assertNotNull(response.getGroup());
            assertNotNull(response.getSupervisor());
            assertNotNull(response.getStatus());
        }
    }

    @Test
    void testGetByStatus_PendingLeaderTeacher() throws ServiceException {
        // Arrange
        String status = Constants.SupervisorStatus.PENDING_LEADER_TEACHER;

        List<GroupSupervisor> groupSupervisors = new ArrayList<>();
        GroupSupervisor groupSupervisor1 = new GroupSupervisor();
        groupSupervisor1.setId(1L);
        GroupSupervisor groupSupervisor2 = new GroupSupervisor();
        groupSupervisor2.setId(2L);
        groupSupervisors.add(groupSupervisor1);
        groupSupervisors.add(groupSupervisor2);
        when(groupSupervisorRepository.findAllByStatus(status)).thenReturn(groupSupervisors);

        // Mock modelMapper.map()
        GroupSupervisorResponse response1 = new GroupSupervisorResponse();
        response1.setId(1L);
        response1.setGroup(new Group1DTO());
        response1.setSupervisor(new SupervisorDTO());
        response1.setStatus(Constants.SupervisorStatus.PENDING_LEADER_TEACHER);
        GroupSupervisorResponse response2 = new GroupSupervisorResponse();
        response2.setId(2L);
        response2.setGroup(new Group1DTO());
        response2.setSupervisor(new SupervisorDTO());
        response2.setStatus(Constants.SupervisorStatus.PENDING_LEADER_TEACHER);
        when(modelMapper.map(groupSupervisor1, GroupSupervisorResponse.class)).thenReturn(response1);
        when(modelMapper.map(groupSupervisor2, GroupSupervisorResponse.class)).thenReturn(response2);

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getByStatus(status);

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(groupSupervisorRepository, times(1)).findAllByStatus(status);
        verify(modelMapper, times(2)).map(any(GroupSupervisor.class), eq(GroupSupervisorResponse.class));
        for (GroupSupervisorResponse response : result) {
            assertNotNull(response.getId());
            assertNotNull(response.getGroup());
            assertNotNull(response.getSupervisor());
            assertNotNull(response.getStatus());
        }
    }

    @Test
    void testGetByStatus_AcceptSupervisor() throws ServiceException {
        // Arrange
        String status = Constants.SupervisorStatus.ACCEPT_SUPERVISOR;

        List<GroupSupervisor> groupSupervisors = new ArrayList<>();
        GroupSupervisor groupSupervisor1 = new GroupSupervisor();
        groupSupervisor1.setId(1L);
        GroupSupervisor groupSupervisor2 = new GroupSupervisor();
        groupSupervisor2.setId(2L);
        groupSupervisors.add(groupSupervisor1);
        groupSupervisors.add(groupSupervisor2);
        when(groupSupervisorRepository.findAllByStatus(status)).thenReturn(groupSupervisors);

        // Mock modelMapper.map()
        GroupSupervisorResponse response1 = new GroupSupervisorResponse();
        response1.setId(1L);
        response1.setGroup(new Group1DTO());
        response1.setSupervisor(new SupervisorDTO());
        response1.setStatus(Constants.SupervisorStatus.ACCEPT_SUPERVISOR);
        GroupSupervisorResponse response2 = new GroupSupervisorResponse();
        response2.setId(2L);
        response2.setGroup(new Group1DTO());
        response2.setSupervisor(new SupervisorDTO());
        response2.setStatus(Constants.SupervisorStatus.ACCEPT_SUPERVISOR);
        when(modelMapper.map(groupSupervisor1, GroupSupervisorResponse.class)).thenReturn(response1);
        when(modelMapper.map(groupSupervisor2, GroupSupervisorResponse.class)).thenReturn(response2);

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getByStatus(status);

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(groupSupervisorRepository, times(1)).findAllByStatus(status);
        verify(modelMapper, times(2)).map(any(GroupSupervisor.class), eq(GroupSupervisorResponse.class));
        for (GroupSupervisorResponse response : result) {
            assertNotNull(response.getId());
            assertNotNull(response.getGroup());
            assertNotNull(response.getSupervisor());
            assertNotNull(response.getStatus());
        }
    }

    @Test
    void testGetByStatus_ReturnNull() throws ServiceException {
        // Arrange
        String status = "";
        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getByStatus(status);
        // Assert
        assertNull(result);
    }

    @Test
    void testGroupBySemester_ThrowNotFound() throws ServiceException {
        User currentUser = new User();
        currentUser.setId(1L);
        Supervisor supervisor = new Supervisor();
        supervisor.setId(1L);
        currentUser.setSupervisor(supervisor);
        List<GroupSupervisor> groupSupervisors = new ArrayList<>();
        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(groupSupervisorRepository.findBySupervisorIdAndStatus(Integer.parseInt(currentUser.getSupervisor().getId().toString()), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER)).thenReturn(groupSupervisors);
        ServiceException response = assertThrows(ServiceException.class, ()
                -> groupSupervisorService.getGroupBySemester(1));
        assertEquals(ErrorCode.NOT_FOUND, response.getErrorCode());
    }



    @Test
    void testChangeStatusGroupSupervisorByGroupId_ThrowNotFound(){
        Long groupId = 1L;
        List<GroupSupervisor> groupSupervisors = new ArrayList<>();
        GroupSupervisor groupSupervisor1 = new GroupSupervisor();
        groupSupervisor1.setId(1L);
        groupSupervisors.add(groupSupervisor1);
        GroupSupervisor groupSupervisor2 = new GroupSupervisor();
        groupSupervisor2.setId(2L);
        groupSupervisors.add(groupSupervisor2);

        when(groupSupervisorRepository.findByGroupId(groupId)).thenReturn(groupSupervisors);
        when(groupRepository.findById(groupId)).thenReturn((Optional.empty()));

        ServiceException response = assertThrows(ServiceException.class, ()
        -> groupSupervisorService.changeStatusGroupSupervisorByGroupId(groupId, "anyString()"));
        assertEquals(ErrorCode.GROUP_NOT_FOUND, response.getErrorCode());
    }

    @Test
    void testChangeStatusGroupSupervisorByGroupId_Successful() throws ServiceException {
        // Arrange
        Long groupId = 2L;
        String newStatus = Constants.GroupStatus.PENDING;
        Group group = new Group();
        group.setStatus(Constants.GroupStatus.PENDING);

        GroupSupervisor supervisor1 = new GroupSupervisor();
        supervisor1.setStatus(Constants.SupervisorStatus.PENDING_SUPERVISOR);
        GroupSupervisor supervisor2 = new GroupSupervisor();
        supervisor2.setStatus(Constants.SupervisorStatus.PENDING_SUPERVISOR);

        when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));
        when(groupSupervisorRepository.findByGroupId(groupId)).thenReturn(Arrays.asList(supervisor1, supervisor2));

        // Act
        groupSupervisorService.changeStatusGroupSupervisorByGroupId(groupId, newStatus);

        // Assert
        verify(groupRepository, times(1)).findById(groupId);
        verify(groupSupervisorRepository, times(1)).findByGroupId(groupId);
        verify(groupRepository, times(1)).save(group);
        verify(groupSupervisorRepository, times(1)).delete(supervisor2);
        assertEquals(Constants.GroupStatus.PENDING, group.getStatus());
    }

    @Test
    void testChangeStatusGroupSupervisorByGroupId_NoGroupSupervisors() throws ServiceException {
        // Arrange
        Long groupId = 2L;
        String newStatus = Constants.GroupStatus.PENDING;
        Group group = new Group();
        group.setStatus(Constants.GroupStatus.PENDING);

        when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));
        when(groupSupervisorRepository.findByGroupId(groupId)).thenReturn(List.of());

        // Act
        groupSupervisorService.changeStatusGroupSupervisorByGroupId(groupId, newStatus);

        // Assert
        verify(groupRepository, times(1)).findById(groupId);
        verify(groupSupervisorRepository, times(1)).findByGroupId(groupId);
        verify(groupRepository, times(1)).save(group);
        verify(groupSupervisorRepository, never()).delete(any(GroupSupervisor.class));
    }

//    @Test
//    void testChangeStatusGroupSupervisorByGroupId_SupervisorNotInPendingStatus() throws ServiceException {
//        // Arrange
//        Long groupId = 1L;
//        String newStatus = "1q2w";
//        Group group = new Group();
//        group.setStatus(Constants.GroupStatus.PENDING);
//
//        GroupSupervisor supervisor1 = new GroupSupervisor();
//        supervisor1.setStatus("123");
//        GroupSupervisor supervisor2 = new GroupSupervisor();
//        supervisor2.setStatus("123");
//
//        when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));
//        when(groupSupervisorRepository.findByGroupId(groupId)).thenReturn(Arrays.asList(supervisor1, supervisor2));
//
//        ServiceException serviceException = assertThrows(ServiceException.class, () ->
//                groupSupervisorService.changeStatusGroupSupervisorByGroupId(groupId, newStatus));
//        assertEquals(ErrorCode.FAILED_SEND_GROUP, serviceException.getErrorCode());
//
//    }

    @Test
    void testGetGroupSupervisorByStatusAndSupervisorId_InvalidStatus() throws ServiceException {
        // Arrange
        int supervisorId = 1;
        String status = "INVALID_STATUS";

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getGroupSupervisorByStatusAndSupervisorId(status, supervisorId);

        // Assert
        assertNull(result);
        verify(groupSupervisorRepository, never()).findBySupervisorIdAndStatus(supervisorId, status);
    }

    @Test
    void testGetGroupSupervisorByStatusAndSupervisorId_NoDataFound() throws ServiceException {
        // Arrange
        int supervisorId = 1;
        String status = Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER;
        when(groupSupervisorRepository.findBySupervisorIdAndStatus(supervisorId, status)).thenReturn(new ArrayList<>());

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getGroupSupervisorByStatusAndSupervisorId(status, supervisorId);

        // Assert
        assertEquals(0, result.size());
        verify(groupSupervisorRepository, times(1)).findBySupervisorIdAndStatus(supervisorId, status);
    }

    @Test
    void testGetGroupSupervisorByStatusAndSupervisorId_ValidData() throws ServiceException {
        // Arrange
        int supervisorId = 2;
        String status = Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER;
        List<GroupSupervisor> groupSupervisors = new ArrayList<>();
        GroupSupervisor groupSupervisor1 = new GroupSupervisor();
        groupSupervisor1.setId(1L);
        groupSupervisor1.setStatus(status);
        GroupSupervisor groupSupervisor2 = new GroupSupervisor();
        groupSupervisor2.setId(2L);
        groupSupervisor2.setStatus(status);
        groupSupervisors.add(groupSupervisor1);
        groupSupervisors.add(groupSupervisor2);

        when(groupSupervisorRepository.findBySupervisorIdAndStatus(supervisorId, status)).thenReturn(groupSupervisors);
        GroupSupervisorResponse groupSupervisorResponse1 = new GroupSupervisorResponse();
        groupSupervisorResponse1.setId(1L);
        groupSupervisorResponse1.setStatus(status);

        GroupSupervisorResponse groupSupervisorResponse2 = new GroupSupervisorResponse();
        groupSupervisorResponse2.setId(2L);
        groupSupervisorResponse2.setStatus(status);

        when(modelMapper.map(groupSupervisor1, GroupSupervisorResponse.class)).thenReturn(groupSupervisorResponse1);
        when(modelMapper.map(groupSupervisor2, GroupSupervisorResponse.class)).thenReturn(groupSupervisorResponse2);

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getGroupSupervisorByStatusAndSupervisorId(status, supervisorId);

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(groupSupervisorRepository, times(1)).findBySupervisorIdAndStatus(supervisorId, status);
    }

    @Test
    void testGetByTwoStatus_ValidData() throws ServiceException {
        // Arrange
        String acceptSupervisorStatus = Constants.SupervisorStatus.ACCEPT_SUPERVISOR;
        String acceptSupervisorLeaderStatus = Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER;
        List<GroupSupervisor> groupSupervisors = new ArrayList<>();
        GroupSupervisor groupSupervisor1 = new GroupSupervisor();
        groupSupervisor1.setId(1L);
        GroupSupervisor groupSupervisor2 = new GroupSupervisor();
        groupSupervisor2.setId(2L);
        groupSupervisors.add(groupSupervisor1);
        groupSupervisors.add(groupSupervisor2);

        when(groupSupervisorRepository.findByTwoStatus(acceptSupervisorStatus, acceptSupervisorLeaderStatus)).thenReturn(groupSupervisors);

        GroupSupervisorResponse groupSupervisorResponse1 = new GroupSupervisorResponse();
        groupSupervisorResponse1.setId(1L);

        GroupSupervisorResponse groupSupervisorResponse2 = new GroupSupervisorResponse();
        groupSupervisorResponse2.setId(2L);

        when(modelMapper.map(groupSupervisor1, GroupSupervisorResponse.class)).thenReturn(groupSupervisorResponse1);
        when(modelMapper.map(groupSupervisor2, GroupSupervisorResponse.class)).thenReturn(groupSupervisorResponse2);

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getByTwoStatus(acceptSupervisorStatus, acceptSupervisorLeaderStatus);

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(groupSupervisor1.getId(), result.get(0).getId());
        assertEquals(groupSupervisor2.getId(), result.get(1).getId());
    }

    @Test
    void testGetByTwoStatus_NoDataFound() throws ServiceException {
        // Arrange
        String acceptSupervisorStatus = Constants.SupervisorStatus.ACCEPT_SUPERVISOR;
        String acceptSupervisorLeaderStatus = Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER;
        when(groupSupervisorRepository.findByTwoStatus(acceptSupervisorStatus, acceptSupervisorLeaderStatus)).thenReturn(new ArrayList<>());

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getByTwoStatus(acceptSupervisorStatus, acceptSupervisorLeaderStatus);

        // Assert
        assertEquals(0, result.size());
    }

    @Test
    void testGetByTwoStatus_InvalidStatus() throws ServiceException {
        // Arrange
        String acceptSupervisorStatus = "INVALID_STATUS";
        String acceptSupervisorLeaderStatus = "INVALID_STATUS";

        // Act
        List<GroupSupervisorResponse> result = groupSupervisorService.getByTwoStatus(acceptSupervisorStatus, acceptSupervisorLeaderStatus);

        // Assert
        assertNull(result);
    }

    @Test
    public void testAssignTeacherSuccessfully() throws ServiceException {
        // Arrange
        List<AssignTeacherRequest> requests = new ArrayList<>();
        AssignTeacherRequest request1 = new AssignTeacherRequest();
        request1.setGroupId(1);
        request1.setSupervisorId(1);
        requests.add(request1);

        Supervisor supervisor = new Supervisor();
        supervisor.setId(1L);
        Optional<Supervisor> supervisorOptional = Optional.of(supervisor);

        Group group = new Group();
        group.setId(1L);
        Optional<Group> groupOptional = Optional.of(group);

        when(supervisorRepository.findById(Long.valueOf(request1.getSupervisorId()))).thenReturn(supervisorOptional);
        when(groupRepository.findById(Long.valueOf(request1.getGroupId()))).thenReturn(groupOptional);

        // Act
        ResponseEntity<String> result = groupSupervisorService.assignTeacher(requests);

        // Assert
        assertNotNull(result);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Assign teacher successfully", result.getBody());
        verify(groupSupervisorRepository, times(1)).save(any(GroupSupervisor.class));
    }

    @Test
    void testAssignTeacher_SupervisorNotFound() {
        // Arrange
        List<AssignTeacherRequest> assignTeacherRequests = new ArrayList<>();
        AssignTeacherRequest request = new AssignTeacherRequest();
        request.setGroupId(1);
        request.setSupervisorId(2);
        assignTeacherRequests.add(request);

        when(supervisorRepository.findById(Long.valueOf(request.getSupervisorId()))).thenReturn(Optional.empty());

        // Act
        Assertions.assertThrows(ServiceException.class, () -> {
            groupSupervisorService.assignTeacher(assignTeacherRequests);
        });

        // Assert
        verify(groupSupervisorRepository, never()).save(any(GroupSupervisor.class));
    }

    @Test
    void testAssignTeacher_GroupNotFound() {
        // Arrange
        List<AssignTeacherRequest> assignTeacherRequests = new ArrayList<>();
        AssignTeacherRequest request = new AssignTeacherRequest();
        request.setGroupId(2);
        request.setSupervisorId(1);
        assignTeacherRequests.add(request);

        Supervisor supervisor = new Supervisor();
        supervisor.setId(1L);
        when(supervisorRepository.findById(1L)).thenReturn(Optional.of(supervisor));
        when(groupRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        Assertions.assertThrows(ServiceException.class, () -> {
            groupSupervisorService.assignTeacher(assignTeacherRequests);
        });

        // Assert
        verify(groupSupervisorRepository, never()).save(any(GroupSupervisor.class));
    }


    @Test
    void testCheckGroupSizeOfSupervisor_AllSupervisorsValid() {
        // Arrange
        List<AssignTeacherRequest> assignTeacherRequests = new ArrayList<>();
        AssignTeacherRequest request1 = new AssignTeacherRequest();
        request1.setGroupId(1);
        request1.setSupervisorId(1);
        AssignTeacherRequest request2 = new AssignTeacherRequest();
        request2.setGroupId(2);
        request2.setSupervisorId(2);
        assignTeacherRequests.add(request1);
        assignTeacherRequests.add(request2);

        Supervisor supervisor1 = new Supervisor();
        supervisor1.setId(1L);
        Supervisor supervisor2 = new Supervisor();
        supervisor2.setId(2L);
        List<Supervisor> sampleSupervisors = Arrays.asList(supervisor1, supervisor2);
        when(supervisorRepository.findAllByHavingCountTotalGroupLessThanFour()).thenReturn(sampleSupervisors);
        when(supervisorRepository.findAllById(anyList())).thenReturn(sampleSupervisors);

        // Act
        boolean result = groupSupervisorService.checkGroupSizeOfSupervisor(assignTeacherRequests);

        // Assert
        assertTrue(result);
        verify(supervisorRepository, times(1)).findAllByHavingCountTotalGroupLessThanFour();
        verify(supervisorRepository, times(1)).findAllById(anyList());
    }


}
