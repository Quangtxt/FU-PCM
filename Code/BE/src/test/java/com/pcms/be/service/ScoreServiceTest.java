package com.pcms.be.service;

import com.pcms.be.domain.Report;
import com.pcms.be.domain.user.Member;
import com.pcms.be.domain.user.Score;
import com.pcms.be.pojo.DTO.ScoreDTO;
import com.pcms.be.pojo.request.ScoreEvaluateRequest;
import com.pcms.be.repository.MemberRepository;
import com.pcms.be.repository.ScoreRepository;
import com.pcms.be.repository.ReportRepository;
import com.pcms.be.repository.ScoreHistoryRepository;
import com.pcms.be.service.impl.ScoreServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import com.pcms.be.domain.user.ScoreHistory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ScoreServiceTest {
    @Mock
    private ScoreRepository scoreRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private ScoreHistoryRepository scoreHistoryRepository;

    @Mock
    private MemberRepository memberRepository;

    @Mock
    private ReportRepository reportRepository;

    @InjectMocks
    private ScoreServiceImpl scoreService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetScore() {
        // Arrange
        int groupId = 1;
        int milestoneId = 1;
        int semesterId = 6;

        Score score = new Score();
        ScoreDTO scoreDTO = new ScoreDTO();

        when(scoreRepository.getScoreByGroupIdAndMileStoneIdAndSemesterId(groupId, milestoneId, semesterId))
                .thenReturn(List.of(score));
        when(modelMapper.map(score, ScoreDTO.class)).thenReturn(scoreDTO);

        // Act
        List<ScoreDTO> result = scoreService.getScore(groupId, milestoneId, semesterId);

        // Assert
        assertEquals(1, result.size());
        assertEquals(scoreDTO, result.get(0));
        verify(scoreRepository, times(1))
                .getScoreByGroupIdAndMileStoneIdAndSemesterId(groupId, milestoneId, semesterId);
        verify(modelMapper, times(1)).map(score, ScoreDTO.class);
    }

    @Test
    public void testGetScore_noScores() {
        // Arrange
        int groupId = 2;
        int milestoneId = 2;
        int semesterId = 7;

        when(scoreRepository.getScoreByGroupIdAndMileStoneIdAndSemesterId(groupId, milestoneId, semesterId))
                .thenReturn(Collections.emptyList());

        // Act
        List<ScoreDTO> result = scoreService.getScore(groupId, milestoneId, semesterId);

        // Assert
        assertEquals(0, result.size());
        verify(scoreRepository, times(1))
                .getScoreByGroupIdAndMileStoneIdAndSemesterId(groupId, milestoneId, semesterId);
        verify(modelMapper, never()).map(any(Score.class), eq(ScoreDTO.class));
    }
    //---------------------------------------------------------------------------------------------------------
    @Test
    void getScoreByGroupId_whenScoresExist_returnScoreDTOs() {
        // Arrange
        int groupId = 1;
        Score score1 = new Score();
        Score score2 = new Score();
        List<Score> scores = Arrays.asList(score1, score2);
        ScoreDTO scoreDTO1 = new ScoreDTO();
        ScoreDTO scoreDTO2 = new ScoreDTO();
        List<ScoreDTO> expectedScoreDTOs = Arrays.asList(scoreDTO1, scoreDTO2);

        when(scoreRepository.getScoreByGroupId(groupId)).thenReturn(scores);
        when(modelMapper.map(score1, ScoreDTO.class)).thenReturn(scoreDTO1);
        when(modelMapper.map(score2, ScoreDTO.class)).thenReturn(scoreDTO2);

        // Act
        List<ScoreDTO> actualScoreDTOs = scoreService.getScoreByGroupId(groupId);

        // Assert
        assertEquals(expectedScoreDTOs, actualScoreDTOs);
    }
    @Test
    public void testGetScoreByGroupId_WithNonExistingGroupId_ShouldReturnEmptyList() {
        // Arrange
        int nonExistingGroupId = 999;
        when(scoreRepository.getScoreByGroupId(nonExistingGroupId)).thenReturn(Collections.emptyList());

        // Act
        List<ScoreDTO> result = scoreService.getScoreByGroupId(nonExistingGroupId);

        // Assert
        assertTrue(result.isEmpty(), "The result should be an empty list when the group ID does not exist.");
    }

    @Test
    public void testGetScoreByGroupId_WithExceptionThrown_ShouldHandleException() {
        // Arrange
        int groupId = 0;
        when(scoreRepository.getScoreByGroupId(groupId)).thenThrow(new RuntimeException("Database error"));

        // Act & Assert
        Exception exception = assertThrows(RuntimeException.class, () -> {
            scoreService.getScoreByGroupId(groupId);
        });
        assertEquals("Database error", exception.getMessage(), "The exception message should match.");
    }
    //------------------------------------------------------------------------------
    @Test
    public void testSaveEvaluation_WhenScoreExistsAndIsUpdated_ShouldSaveNewScoreAndHistory() {
        // Arrange
        ScoreEvaluateRequest request = new ScoreEvaluateRequest();
        request.setMemberId(1L);
        request.setReportId(1L);
        request.setScore(8.0);
        request.setComment("Updated comment");
        request.setReason("Reason for change");

        Score existingScore = new Score();
        existingScore.setScore(7.0);
        existingScore.setComment("Old comment");

        when(scoreRepository.findByMemberIdAndReportId(1L, 1L)).thenReturn(Optional.of(existingScore));

        // Act
        scoreService.saveEvaluation(Collections.singletonList(request));

        // Assert
        verify(scoreRepository).save(any(Score.class));
        verify(scoreHistoryRepository).save(any(ScoreHistory.class));
    }

    @Test
    public void testSaveEvaluation_WhenScoreExistsButIsNotUpdated_ShouldNotSaveNewScoreAndHistory() {
        // Arrange
        ScoreEvaluateRequest request = new ScoreEvaluateRequest();
        request.setMemberId(2L);
        request.setReportId(2L);
        request.setScore(7.0);
        request.setComment("Old comment");
        request.setReason("Reason for change");

        Score existingScore = new Score();
        existingScore.setScore(7.0);
        existingScore.setComment("Old comment");

        when(scoreRepository.findByMemberIdAndReportId(2L, 2L)).thenReturn(Optional.of(existingScore));

        // Act
        scoreService.saveEvaluation(Collections.singletonList(request));

        // Assert
        verify(scoreRepository, never()).save(any(Score.class));
        verify(scoreHistoryRepository, never()).save(any(ScoreHistory.class));
    }

    @Test
    public void testSaveEvaluation_WhenScoreDoesNotExist_ShouldSaveNewScore() {
        // Arrange
        ScoreEvaluateRequest request = new ScoreEvaluateRequest();
        request.setMemberId(1L);
        request.setReportId(1L);
        request.setScore(9.0);
        request.setComment("New comment");
        request.setReason("Reason for new score");

        when(scoreRepository.findByMemberIdAndReportId(1L, 1L)).thenReturn(Optional.empty());
        when(memberRepository.findById(1L)).thenReturn(Optional.of(new Member()));
        when(reportRepository.findById(1L)).thenReturn(Optional.of(new Report()));

        // Act
        scoreService.saveEvaluation(Collections.singletonList(request));

        // Assert
        verify(scoreRepository).save(any(Score.class));
    }

    @Test
    public void testSaveEvaluation_WhenMemberOrReportNotFound_ShouldThrowException() {
        // Arrange
        ScoreEvaluateRequest request = new ScoreEvaluateRequest();
        request.setMemberId(4L);
        request.setReportId(3L);
        request.setScore(9.0);
        request.setComment("New comment");
        request.setReason("Reason for new score");

        when(scoreRepository.findByMemberIdAndReportId(1L, 1L)).thenReturn(Optional.empty());
        when(memberRepository.findById(1L)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(RuntimeException.class, () -> {
            scoreService.saveEvaluation(Collections.singletonList(request));
        });
    }

}
