//package com.pcms.be.service;
//
//
//import com.pcms.be.domain.meeting.Meeting;
//import com.pcms.be.domain.user.*;
//import com.pcms.be.errors.ErrorCode;
//import com.pcms.be.errors.ServiceException;
//import com.pcms.be.functions.Constants;
//import com.pcms.be.pojo.DTO.MeetingDTO;
//import com.pcms.be.pojo.request.CreateMeetingRequest;
//import com.pcms.be.pojo.request.EditMeetingRequest;
//import com.pcms.be.pojo.request.SendEmailRequest;
//import com.pcms.be.pojo.response.EmailResponse;
//import com.pcms.be.repository.GroupRepository;
//import com.pcms.be.repository.GroupSupervisorRepository;
//import com.pcms.be.repository.MeetingRepository;
//import com.pcms.be.service.Email.EmailService;
//import com.pcms.be.service.impl.MeetingServiceImpl;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.modelmapper.ModelMapper;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//
//import java.time.LocalDateTime;
//import java.time.OffsetDateTime;
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.*;
//public class MeetingServiceTest {
//    @Mock
//    private MeetingRepository meetingRepository;
//
//    @Mock
//    private GroupRepository groupRepository;
//
//    @Mock
//    private UserService userService;
//
//    @Mock
//    private GroupSupervisorRepository groupSupervisorRepository;
//
//    @Mock
//    private ModelMapper modelMapper;
//    @Mock
//    private EmailService emailService;
//
//    @Mock
//    private NotificationService notificationService;
//
//    @InjectMocks
//    private MeetingServiceImpl meetingService;
//
//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    public void testCreateMeetingSuccess() throws ServiceException {
//        // Arrange
//        List<CreateMeetingRequest> createMeetingRequests = new ArrayList<>();
//        CreateMeetingRequest request1 = new CreateMeetingRequest();
//        request1.setStartAt(OffsetDateTime.now().plusHours(1));
//        request1.setEndAt(OffsetDateTime.now().plusHours(2));
//        request1.setType("type1");
//        request1.setLocation("location1");
//        request1.setGroupId(1);
//        createMeetingRequests.add(request1);
//
//        Set<Member> members = new HashSet<>();
//        Member member = new Member();
//        member.setId(1L);
//        Student student = new Student();
//        student.setId(1L);
//        member.setStudent(student);
//        User user = new User();
//        user.setId(1L);
//        user.setEmail("");
//        student.setUser(user);
//        members.add(member);
//
//        Group group = new Group();
//        group.setId(1L);
//        group.setMembers(members);
//        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
//
//        User currentUser = new User();
//        user.setSupervisor(new Supervisor());
//        when(userService.getCurrentUser()).thenReturn(currentUser);
//
//        GroupSupervisor groupSupervisor = new GroupSupervisor();
//        groupSupervisor.setStatus(Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
//        when(groupSupervisorRepository.findByGroupAndSupervisorAndStatus(group, currentUser.getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER)).thenReturn(groupSupervisor);
//
//        Meeting newMeeting = new Meeting();
//        newMeeting.setId(1L);
//        newMeeting.setStartAt(request1.getStartAt());
//        newMeeting.setEndAt(request1.getEndAt());
//        newMeeting.setType(request1.getType());
//        newMeeting.setLocation(request1.getLocation());
//        newMeeting.setGroup(group);
//        newMeeting.setStatus(Constants.MeetingStatus.PENDING);
//        when(meetingRepository.save(any(Meeting.class))).thenReturn(newMeeting);
//
//        MeetingDTO meetingDTO = new MeetingDTO();
//        meetingDTO.setId(1L);
//        meetingDTO.setStartAt(request1.getStartAt());
//        meetingDTO.setEndAt(request1.getEndAt());
//        meetingDTO.setType(request1.getType());
//        meetingDTO.setLocation(request1.getLocation());
////        meetingDTO.setGroup(group);
//        when(modelMapper.map(newMeeting, MeetingDTO.class)).thenReturn(meetingDTO);
//        when(notificationService.createContentNotification(anyString(), any(Map.class))).thenReturn("");
//
//        EmailResponse emailResponse = new EmailResponse();
//        when(emailService.sendEmail(any(SendEmailRequest.class))).thenReturn(emailResponse);
//
//        // Act
//        List<MeetingDTO> result = meetingService.createMeeting(createMeetingRequests);
//
//        // Assert
//        assertNotNull(result);
//        assertEquals(1, result.size());
//        verify(meetingRepository, times(1)).save(any(Meeting.class));
////        verify(notificationService, times(1)).createContentNotification(any(), any());
////        verify(notificationService, times(1)).saveNotification(any(), any());
////        verify(emailService, times(1)).sendEmail(any());
//    }
//
//    @Test
//    public void testCreateMeetingWithDuplicateTimeRange() {
//        // Arrange
//        List<CreateMeetingRequest> createMeetingRequests = new ArrayList<>();
//        CreateMeetingRequest request1 = new CreateMeetingRequest();
//        request1.setStartAt(OffsetDateTime.now().plusHours(1));
//        request1.setEndAt(OffsetDateTime.now().plusHours(2));
//        request1.setType("type1");
//        request1.setLocation("location1");
//        request1.setGroupId(1);
//        createMeetingRequests.add(request1);
//
//        CreateMeetingRequest request2 = new CreateMeetingRequest();
//        request2.setStartAt(OffsetDateTime.now().plusHours(1));
//        request2.setEndAt(OffsetDateTime.now().plusHours(2));
//        request2.setType("type2");
//        request2.setLocation("location2");
//        request2.setGroupId(1);
//        createMeetingRequests.add(request2);
//
//        // Act and Assert
//        assertThrows(ServiceException.class, () -> meetingService.createMeeting(createMeetingRequests));
//    }
//
//    @Test
//    public void testCreateMeetingWithGroupNotFound() {
//        // Arrange
//        List<CreateMeetingRequest> createMeetingRequests = new ArrayList<>();
//        CreateMeetingRequest request1 = new CreateMeetingRequest();
//        request1.setStartAt(OffsetDateTime.now().plusHours(1));
//        request1.setEndAt(OffsetDateTime.now().plusHours(2));
//        request1.setType("type1");
//        request1.setLocation("location1");
//        request1.setGroupId(1);
//        createMeetingRequests.add(request1);
//
//        when(groupRepository.findById(1L)).thenReturn(Optional.empty());
//
//        // Act and Assert
//        assertThrows(ServiceException.class, () -> meetingService.createMeeting(createMeetingRequests));
//    }
//    @Test
//    public void testCreateMeetingWithUserNotAllowed() throws ServiceException {
//        // Arrange
//        List<CreateMeetingRequest> createMeetingRequests = new ArrayList<>();
//        CreateMeetingRequest request1 = new CreateMeetingRequest();
//        request1.setStartAt(OffsetDateTime.now().plusHours(1));
//        request1.setEndAt(OffsetDateTime.now().plusHours(2));
//        request1.setType("type1");
//        request1.setLocation("location1");
//        request1.setGroupId(1);
//        createMeetingRequests.add(request1);
//
//        Group group = new Group();
//        group.setId(1L);
//        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
//
//        User user = new User();
//        user.setSupervisor(new Supervisor());
//        when(userService.getCurrentUser()).thenReturn(user);
//
//        when(groupSupervisorRepository.findByGroupAndSupervisorAndStatus(group, user.getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER)).thenReturn(null);
//
//        // Act and Assert
//        assertThrows(ServiceException.class, () -> meetingService.createMeeting(createMeetingRequests));
//    }
//
//    @Test
//    void testViewMeetingsWithValidGroupId() throws ServiceException {
//        // Arrange
//        int groupId = 2;
//        Group group = new Group();
//        group.setId(Long.valueOf(groupId));
//        List<Meeting> meetings = new ArrayList<>();
//        meetings.add(new Meeting());
//        meetings.add(new Meeting());
//
//        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.of(group));
//        when(meetingRepository.findAllByGroup(group)).thenReturn(meetings);
//        when(modelMapper.map(any(Meeting.class), eq(MeetingDTO.class))).thenReturn(new MeetingDTO());
//
//        // Act
//        List<MeetingDTO> result = meetingService.viewMeetings(groupId);
//
//        // Assert
//        assertNotNull(result);
//        assertEquals(2, result.size());
////        verify(groupRepository, times(1)).findById(Long.valueOf(groupId));
////        verify(meetingRepository, times(1)).findAllByGroup(group);
//        verify(modelMapper, times(2)).map(any(Meeting.class), eq(MeetingDTO.class));
//    }
//
//    @Test
//    void testViewMeetingsWithInvalidGroupId() {
//        // Arrange
//        int groupId = 2;
//
//        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.empty());
//
//        // Act and Assert
//        ServiceException response = assertThrows(ServiceException.class, () -> meetingService.viewMeetings(groupId),
//                "Expected ServiceException to be thrown");
//        assertEquals(ErrorCode.GROUP_NOT_FOUND, response.getErrorCode());
//        verify(groupRepository, times(1)).findById(Long.valueOf(groupId));
//        verify(meetingRepository, never()).findAllByGroup(any());
//        verify(modelMapper, never()).map(any(Meeting.class), eq(MeetingDTO.class));
//    }
//
////    @Test
////    void testViewMeetingsWithException() {
////        // Arrange
////        int groupId = 3;
////        Group group = new Group();
////        group.setId(Long.valueOf(groupId));
////
////        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.of(group));
////        when(meetingRepository.findAllByGroup(group)).thenThrow(new RuntimeException());
////
////        // Act and Assert
////        assertThrows(ServiceException.class, () -> meetingService.viewMeetings(groupId),
////                "Expected ServiceException to be thrown");
////        verify(groupRepository, times(1)).findById(Long.valueOf(groupId));
////        verify(meetingRepository, times(1)).findAllByGroup(group);
////        verify(modelMapper, never()).map(any(Meeting.class), eq(MeetingDTO.class));
////    }
//
//    @Test
//    void testUpdateMeeting_TimeRequestsDuplicate(){
//        // Arrange
//        List<EditMeetingRequest> editMeetingRequests = new ArrayList<>();
//        EditMeetingRequest request1 = new EditMeetingRequest(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type1", "location1");
//        EditMeetingRequest request2 = new EditMeetingRequest(2L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type2", "location2");
//        editMeetingRequests.add(request1);
//        editMeetingRequests.add(request2);
//
//// Act
//        Assertions.assertThrows(ServiceException.class, () -> {
//            meetingService.updateMeeting(editMeetingRequests);
//        });
//
//        ServiceException response = assertThrows(ServiceException.class, () ->
//                meetingService.updateMeeting(editMeetingRequests));
//        assertEquals(ErrorCode.TIME_RANGE_DUPLICATED, response.getErrorCode());
//// Assert
//    }
//
//    @Test
//    void testUpdateMeeting_TimeRequestAndDatabaseDuplicate(){
//        // Arrange
//        List<EditMeetingRequest> editMeetingRequests = new ArrayList<>();
//        EditMeetingRequest request = new EditMeetingRequest(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type", "location");
//        editMeetingRequests.add(request);
//        List<Meeting> existingMeetings = new ArrayList<>();
//        Meeting existingMeeting = new Meeting(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type", "location", Constants.MeetingStatus.PENDING, null, null);
//        existingMeetings.add(existingMeeting);
//        when(meetingRepository.findAll()).thenReturn(existingMeetings);
//
//// Act
//        Assertions.assertThrows(ServiceException.class, () -> {
//            meetingService.updateMeeting(editMeetingRequests);
//        });
//
//// Assert
//        ServiceException response = assertThrows(ServiceException.class, () ->
//                meetingService.updateMeeting(editMeetingRequests));
//        assertEquals(ErrorCode.TIME_RANGE_DUPLICATED, response.getErrorCode());
//    }
//
//    @Test
//    void testUpdateMeeting_MeetingNotFound(){
//        List<EditMeetingRequest> editMeetingRequests = new ArrayList<>();
//        EditMeetingRequest request = new EditMeetingRequest(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type", "location");
//        editMeetingRequests.add(request);
//        when(meetingRepository.findById(1L)).thenReturn(Optional.empty());
//
//// Act
//        Assertions.assertThrows(ServiceException.class, () -> {
//            meetingService.updateMeeting(editMeetingRequests);
//        });
//
//// Assert
//        ServiceException response = assertThrows(ServiceException.class, () ->
//                meetingService.updateMeeting(editMeetingRequests));
//        assertEquals(ErrorCode.NOT_FOUND, response.getErrorCode());
//    }
//
//    @Test
//    void testUpdateMeeting_UserNotAllow() throws ServiceException {
//        // Arrange
//        List<EditMeetingRequest> editMeetingRequests = new ArrayList<>();
//        EditMeetingRequest request = new EditMeetingRequest(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type", "location");
//        editMeetingRequests.add(request);
//        Optional<Meeting> meeting = Optional.of(new Meeting(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type", "location", Constants.MeetingStatus.PENDING, null, null));
//        when(meetingRepository.findById(1L)).thenReturn(meeting);
//        when(userService.getCurrentUser()).thenReturn(new User());
//        when(groupSupervisorRepository.findByGroupAndSupervisorAndStatus(any(), any(), any())).thenReturn(null);
//
//// Act
//        Assertions.assertThrows(ServiceException.class, () -> {
//            meetingService.updateMeeting(editMeetingRequests);
//        });
//
//// Assert
//        ServiceException response = assertThrows(ServiceException.class, () ->
//                meetingService.updateMeeting(editMeetingRequests));
//        assertEquals(ErrorCode.USER_NOT_ALLOW, response.getErrorCode());
//    }
//
//    @Test
//    void testUpdateMeeting_StatusIsNotPending() throws ServiceException {
//        // Arrange
//        List<EditMeetingRequest> editMeetingRequests = new ArrayList<>();
//        EditMeetingRequest request = new EditMeetingRequest(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type", "location");
//        editMeetingRequests.add(request);
//        Optional<Meeting> meeting = Optional.of(new Meeting(1L, OffsetDateTime.now(), OffsetDateTime.now().plusHours(1), "type", "location", "PENDING1", null, null));
//        when(meetingRepository.findById(1L)).thenReturn(meeting);
//        when(userService.getCurrentUser()).thenReturn(new User());
//        when(groupSupervisorRepository.findByGroupAndSupervisorAndStatus(any(), any(), any())).thenReturn(new GroupSupervisor());
//
//// Act
//        Assertions.assertThrows(ServiceException.class, () -> {
//            meetingService.updateMeeting(editMeetingRequests);
//        });
//
//// Assert
//        ServiceException response = assertThrows(ServiceException.class, () ->
//                meetingService.updateMeeting(editMeetingRequests));
//        assertEquals(ErrorCode.FAILED_EDIT_MEETING, response.getErrorCode());
//    }
//    @Test
//    void testUpdateMeeting_StartAtAfterEndAt() throws ServiceException {
//        // Arrange
//        List<EditMeetingRequest> editMeetingRequests = new ArrayList<>();
//        EditMeetingRequest request = new EditMeetingRequest(1L, OffsetDateTime.now().plusHours(2), OffsetDateTime.now().plusHours(1), "type", "location");
//        editMeetingRequests.add(request);
//        Optional<Meeting> meeting = Optional.of(new Meeting(1L, OffsetDateTime.now().plusHours(1), OffsetDateTime.now().plusHours(2), "type", Constants.MeetingStatus.PENDING, "", null, null));
//        when(meetingRepository.findById(1L)).thenReturn(meeting);
//        when(userService.getCurrentUser()).thenReturn(new User());
//        when(groupSupervisorRepository.findByGroupAndSupervisorAndStatus(any(), any(), any())).thenReturn(new GroupSupervisor());
//
//// Act
//        Assertions.assertThrows(ServiceException.class, () -> {
//            meetingService.updateMeeting(editMeetingRequests);
//        });
//
//// Assert
//        ServiceException response = assertThrows(ServiceException.class, () ->
//                meetingService.updateMeeting(editMeetingRequests));
//        assertEquals(ErrorCode.STARTAT_MUST_BE_BEFORE_ENDAT, response.getErrorCode());
//    }
//
//    @Test
//    void testUpdateMeeting_StartAtBeforeNow() throws ServiceException {
//        // Arrange
//        List<EditMeetingRequest> editMeetingRequests = new ArrayList<>();
//        EditMeetingRequest request = new EditMeetingRequest(1L, OffsetDateTime.now().minusHours(1), OffsetDateTime.now().plusHours(1), "type", "location");
//        editMeetingRequests.add(request);
//        Optional<Meeting> meeting = Optional.of(new Meeting(1L, OffsetDateTime.now().minusHours(1), OffsetDateTime.now().plusHours(1), "type", Constants.MeetingStatus.PENDING, "", null, null));
//        when(meetingRepository.findById(1L)).thenReturn(meeting);
//        when(userService.getCurrentUser()).thenReturn(new User());
//        when(groupSupervisorRepository.findByGroupAndSupervisorAndStatus(any(), any(), any())).thenReturn(new GroupSupervisor());
//
//// Act
//        Assertions.assertThrows(ServiceException.class, () -> {
//            meetingService.updateMeeting(editMeetingRequests);
//        });
//
//// Assert
//        ServiceException response = assertThrows(ServiceException.class, () ->
//                meetingService.updateMeeting(editMeetingRequests));
//        assertEquals(ErrorCode.STARTAT_MUST_BE_AFTER_CURRENT_TIME, response.getErrorCode());
//    }
//
//    @Test
//    void testRemoveMeeting_MeetingNotFound() throws ServiceException {
//        // Arrange
//        int meetingId = 1;
//        when(meetingRepository.findById(Long.valueOf(meetingId))).thenReturn(Optional.empty());
//
//        // Act
//        assertThrows(ServiceException.class, () -> meetingService.removeMeeting(meetingId));
//
//        // Assert
//        verify(meetingRepository, times(1)).findById(Long.valueOf(meetingId));
//        verify(userService, never()).getCurrentUser();
//        verify(groupSupervisorRepository, never()).findByGroupAndSupervisorAndStatus(any(), any(), anyString());
//        verify(notificationService, never()).createContentNotification(any(), any());
//        verify(emailService, never()).sendEmail(any());
//        verify(meetingRepository, never()).delete(any());
//    }
//
//    @Test
//    void testRemoveMeeting_MeetingNotPending() throws ServiceException {
//        // Arrange
//        int meetingId = 2;
//        Meeting meeting = new Meeting();
//        meeting.setStatus("Constants.MeetingStatus.PENDING");
//        when(meetingRepository.findById(anyLong())).thenReturn(Optional.of(meeting));
//
//        // Act
//        ServiceException response = assertThrows(ServiceException.class, ()
//        -> meetingService.removeMeeting(meetingId));
//        assertEquals(ErrorCode.USER_NOT_ALLOW, response.getErrorCode());
//
//        // Assert
//    }
//
//    @Test
//    void testRemoveMeeting_UserNotAllowed() throws ServiceException {
//        // Arrange
//        int meetingId = 1;
//        Meeting meeting = new Meeting();
//        meeting.setStatus(Constants.MeetingStatus.PENDING);
//        meeting.setGroup(new Group());
//        when(meetingRepository.findById(anyLong())).thenReturn(Optional.of(meeting));
//        when(userService.getCurrentUser()).thenReturn(new User());
//        when(groupSupervisorRepository.findByGroupAndSupervisorAndStatus(any(), any(), anyString())).thenReturn(null);
//
//        // Act
//        assertThrows(ServiceException.class, () -> meetingService.removeMeeting(meetingId));
//
//        // Assert
//        verify(meetingRepository, times(1)).findById(Long.valueOf(meetingId));
//        verify(userService, times(1)).getCurrentUser();
//        verify(groupSupervisorRepository, times(1)).findByGroupAndSupervisorAndStatus(meeting.getGroup(), new User().getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
//        verify(notificationService, never()).createContentNotification(any(), any());
//        verify(emailService, never()).sendEmail(any());
//        verify(meetingRepository, never()).delete(any());
//    }
//
//    @Test
//    void testGetByMeetingId_MeetingFound() throws ServiceException {
//        // Arrange
//        long meetingId = 1L;
//        Meeting meeting = new Meeting();
//        meeting.setId(meetingId);
//        MeetingDTO meetingDTO = new MeetingDTO();
//
//        when(meetingRepository.findById(meetingId)).thenReturn(Optional.of(meeting));
//        when(modelMapper.map(meeting, MeetingDTO.class)).thenReturn(meetingDTO);
//
//        // Act
//        ResponseEntity<MeetingDTO> response = meetingService.getByMeetingId((int) meetingId);
//
//        // Assert
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(meetingDTO, response.getBody());
//    }
//
//    @Test
//    void testGetByMeetingId_MeetingNotFound() {
//        // Arrange
//        int meetingId = 2;
//        when(meetingRepository.findById(Long.valueOf(meetingId))).thenReturn(Optional.empty());
//
//        // Act and Assert
//        assertThrows(ServiceException.class, () -> meetingService.getByMeetingId(meetingId));
//    }
//
//    @Test
//    void testGetByMeetingId_ExceptionThrown() {
//        // Arrange
//        int meetingId = 1;
//        when(meetingRepository.findById(Long.valueOf(meetingId))).thenThrow(new RuntimeException());
//
//        // Act and Assert
//        assertThrows(ServiceException.class, () -> meetingService.getByMeetingId(meetingId));
//    }
//
//
//}
