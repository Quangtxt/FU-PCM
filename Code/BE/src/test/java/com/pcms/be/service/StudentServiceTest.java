package com.pcms.be.service;


import com.pcms.be.domain.Campus;
import com.pcms.be.domain.user.Role;
import com.pcms.be.domain.user.Student;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.DTO.CampusDTO;
import com.pcms.be.pojo.DTO.RoleDTO;
import com.pcms.be.pojo.DTO.StudentDTO;
import com.pcms.be.pojo.DTO.UserDTO;
import com.pcms.be.pojo.request.AddStudentRequest;
import com.pcms.be.pojo.request.EditStudentProfileRequest;
import com.pcms.be.pojo.request.SetActiveStudentRequest;
import com.pcms.be.repository.*;
import com.pcms.be.service.impl.StudentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.jose.jwk.JWK;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private StudentRepository studentRepository;

    @Mock
    private UserService userService;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private StudentServiceImpl studentServiceImpl;

    private User currentUser;
    private Campus campus;
    private Role studentRole;
    private AddStudentRequest addStudentRequest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        campus = new Campus();
        campus.setCampusCode("HL");
        campus.setName("Hanoi");

        currentUser = new User();
        currentUser.setUsername("admin");
        currentUser.setCampus(campus);

        studentRole = new Role();
        studentRole.setName(Constants.RoleConstants.STUDENT);

        addStudentRequest = new AddStudentRequest();
        addStudentRequest.setEmail("newstudent@example.com");
        addStudentRequest.setFullName("New Student");
    }

    //---------------------------GetStudentProfile--------------------------------------------
    @Test
    void testGetStudentProfile_UserNotFound() {
        // Arrange
        int studentId = 999;

        when(userRepository.findById((long) studentId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ServiceException.class, () -> {
            studentServiceImpl.getStudentProfile(studentId);
        });
    }

    @Test
    void testGetStudentProfile_AuthorityLevelNotFound() throws ServiceException {
        // Arrange
        int studentId = 1;
        User user = new User();
        user.setId((long) studentId);
        user.setName("John Doe");
        user.setEmail("john.doe@example.com");
        user.setStudent(null); // No student associated

        when(userRepository.findById((long) studentId)).thenReturn(Optional.of(user));

        // Act & Assert
        assertThrows(ServiceException.class, () -> {
            studentServiceImpl.getStudentProfile(studentId);
        });
    }
    //---------------------------EditStudentProfile--------------------------------------------

    @Test
    void testEditStudentProfile_UserNotFound() throws ServiceException {
        // Arrange
        EditStudentProfileRequest request = new EditStudentProfileRequest();

        User mockUser = new User();
        mockUser.setId(999L);

        when(userService.getCurrentUser()).thenReturn(mockUser);

        when(userRepository.findById(999L)).thenReturn(Optional.empty());

        // Act & Assert
        ServiceException thrownException = assertThrows(ServiceException.class, () -> studentServiceImpl.editStudentProfile(request));
        assertEquals(ErrorCode.USER_NOT_FOUND, thrownException.getErrorCode());
    }

    @Test
    void testEditStudentProfile_AuthorityLevelNotFound() throws ServiceException {
        // Arrange
        Long userId = 55L;
        User currentUser = new User();
        currentUser.setId(userId);
        currentUser.setName("John Doe");
        currentUser.setEmail("john.doe@example.com");
        currentUser.setStudent(null);

        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(userRepository.findById(userId)).thenReturn(Optional.of(currentUser));

        EditStudentProfileRequest request = new EditStudentProfileRequest();

        // Act & Assert
        assertThrows(ServiceException.class, () -> studentServiceImpl.editStudentProfile(request));
    }
    //---------------------------AddStudent--------------------------------------------

    @Test
    void testAddStudent_EmailAlreadyExists() {
        // Arrange
        AddStudentRequest request = new AddStudentRequest();
        request.setEmail("existing.email@example.com");
        request.setFullName("John Doe");

        when(studentRepository.findByEmail(request.getEmail())).thenReturn(Optional.of(new Student()));

        /// Act & Assert
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            studentServiceImpl.addStudent(request);
        });

        assertTrue(exception.getCause() instanceof ServiceException);
        ServiceException serviceException = (ServiceException) exception.getCause();
        assertEquals(ErrorCode.USER_DUPLICATE_EMAIL, serviceException.getErrorCode());
    }

    @Test
    void testAddStudent() {
        // Arrange
        AddStudentRequest request = new AddStudentRequest();
        request.setEmail("anhlqhe163875@fpt.edu.vn");
        request.setFullName("quang anh");

        User user = new User();
        user.setName("quang anh");
        user.setEmail("anhlqhe163875@fpt.edu.vn");

        Student st = new Student();
        st.setUser(user);

        when(studentRepository.findByEmail(request.getEmail())).thenReturn(Optional.of(st));

        /// Act & Assert
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            studentServiceImpl.addStudent(request);
        });

        assertTrue(exception.getCause() instanceof ServiceException);
        ServiceException serviceException = (ServiceException) exception.getCause();
        assertEquals(ErrorCode.USER_DUPLICATE_EMAIL, serviceException.getErrorCode());
    }


    //---------------------------SetActiveStudent--------------------------------------------

    @Test
    void testSetActiveStudent_StudentNotFound() {
        // Arrange
        SetActiveStudentRequest request = new SetActiveStudentRequest();
        request.setId(999);
        request.setStatus(true);

        when(studentRepository.findById(anyLong())).thenReturn(Optional.empty());

        // Act
        ResponseEntity<StudentDTO> response = studentServiceImpl.setActiveStudent(request);

        // Assert
        assertEquals(ResponseEntity.notFound().build(), response);
        verify(studentRepository, never()).save(any(Student.class));
    }

    @Test
    void testSetActiveStudent() {
        // Arrange
        SetActiveStudentRequest request = new SetActiveStudentRequest();
        request.setId(999);
        request.setStatus(false);

        when(studentRepository.findById(anyLong())).thenReturn(Optional.empty());

        // Act
        ResponseEntity<StudentDTO> response = studentServiceImpl.setActiveStudent(request);

        // Assert
        assertEquals(ResponseEntity.notFound().build(), response);
        verify(studentRepository, never()).save(any(Student.class));
    }



}
