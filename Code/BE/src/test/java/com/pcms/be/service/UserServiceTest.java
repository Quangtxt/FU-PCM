package com.pcms.be.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.pcms.be.domain.Campus;
import com.pcms.be.domain.user.Role;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.DTO.GoogleUserInfo;
import com.pcms.be.pojo.DTO.TokenDTO;
import com.pcms.be.repository.UserRepository;
import com.pcms.be.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private JWTService jwtService;

    @Mock
    private TokenService tokenService;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private UserServiceImpl userService;
    private User user;
    private Campus campus;
    private TokenDTO TokenDTO;

    @BeforeEach
    public void setUp() {
        campus = new Campus();
        campus.setCampusCode("HL");

        user = new User();
        user.setEmail("Anhlqhe163875@fpt.edu.vn");
        user.setUsername("quanhlq");
        user.setCampus(campus);

        user = new User();
        user.setEmail("Anhlqhe163875@fpt.edu.vn");
        user.setUsername("ABC");
        user.setCampus(campus);

        TokenDTO = new TokenDTO();
        TokenDTO.setUserName("quanhlq");
    }

    // ----------------------------------Get Current User------------------------
    @Test
    public void testGetCurrentUser_Success() throws ServiceException {
        // Giả lập token hợp lệ và người dùng tồn tại
        when(tokenService.getToken()).thenReturn(TokenDTO);
        when(userRepository.findByUsernameIgnoreCase("quanhlq"))
                .thenReturn(Optional.of(user));

        User result = userService.getCurrentUser();

        assertEquals(user, result);
        verify(tokenService, times(1)).getToken();
        verify(userRepository, times(1)).findByUsernameIgnoreCase("quanhlq");
    }

    @Test
    public void testGetCurrentUser_TokenIsNull() {
        // Giả lập token không hợp lệ (null)
        when(tokenService.getToken()).thenReturn(null);
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            userService.getCurrentUser();
        });

        assertEquals(ErrorCode.USER_NOT_FOUND, exception.getErrorCode());
        verify(tokenService, times(1)).getToken();
        verify(userRepository, times(0)).findByUsernameIgnoreCase(anyString());
    }

    @Test
    public void testGetCurrentUser_UserNotFound() {
        // Giả lập token hợp lệ nhưng người dùng không tồn tại
        when(tokenService.getToken()).thenReturn(TokenDTO);
        when(userRepository.findByUsernameIgnoreCase("quanhlq"))
                .thenReturn(Optional.empty());

        ServiceException exception = assertThrows(ServiceException.class, () -> {
            userService.getCurrentUser();
        });

        assertEquals(ErrorCode.USER_NOT_FOUND, exception.getErrorCode());
        verify(tokenService, times(1)).getToken();
        verify(userRepository, times(1)).findByUsernameIgnoreCase("quanhlq");
    }
}
