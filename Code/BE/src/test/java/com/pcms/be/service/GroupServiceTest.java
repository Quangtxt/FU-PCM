package com.pcms.be.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.pcms.be.domain.Campus;
import com.pcms.be.domain.user.*;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.functions.Git;
import com.pcms.be.pojo.DTO.GitFolder;
import com.pcms.be.pojo.DTO.GroupDTO;
import com.pcms.be.pojo.DTO.GroupSupervisorDTO;
import com.pcms.be.pojo.DTO.MemberDTO;
import com.pcms.be.pojo.request.CreateGroupRequest;
import com.pcms.be.pojo.request.EditGroupRequest;
import com.pcms.be.pojo.request.SubmitGroupRequest;
import com.pcms.be.pojo.response.GroupResponse;
import com.pcms.be.pojo.response.SubmitGroupResponse;
import com.pcms.be.repository.*;
import com.pcms.be.service.impl.GroupServiceImpl;
import com.pcms.be.service.impl.MilestoneServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.objectweb.asm.TypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Pageable;

import java.time.OffsetDateTime;
import java.util.*;

class GroupServiceTest {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserService userService;

    @Mock
    private MemberRepository memberRepository;

    @Mock
    private StudentRepository studentRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private SupervisorRepository supervisorRepository;

    @Mock
    private GroupSupervisorRepository groupSupervisorRepository;

    @Mock
    private MilestoneServiceImpl milestoneService;

    @InjectMocks
    private GroupServiceImpl groupService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    //-------------------------------------CreateGroup---------------------------------------------
    @Test
    void testCreateGroupThrowsStudentAlreadyInGroupException() throws ServiceException {
        CreateGroupRequest request = new CreateGroupRequest();
        request.setListStudentID(List.of(2, 3,4,5));
        User currentUser = mock(User.class);
        Student student = mock(Student.class);

        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(currentUser.getStudent()).thenReturn(student);
        when(student.getId()).thenReturn(1L);
        when(memberRepository.findByStudentIdAndStatus(1L, Constants.MemberStatus.INGROUP)).thenReturn(new Member());

        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.createGroup(request);
        });

        assertEquals(ErrorCode.STUDENT_ALREADY_IN_A_GROUP, exception.getErrorCode());
    }

    @Test
    void testCreateGroupThrowsMaximumSizeOfAGroupException() throws ServiceException {
        CreateGroupRequest request = new CreateGroupRequest();
        request.setListStudentID(List.of(2, 3, 4, 5, 6));
        User currentUser = mock(User.class);
        Student student = mock(Student.class);

        when(userService.getCurrentUser()).thenReturn(currentUser);
        when(currentUser.getStudent()).thenReturn(student);
        when(student.getId()).thenReturn(1L);
        when(memberRepository.findByStudentIdAndStatus(1L, Constants.MemberStatus.INGROUP)).thenReturn(null);
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.createGroup(request);
        });

        assertEquals(ErrorCode.MAXIMUM_SIZE_OF_A_GROUP, exception.getErrorCode());
    }

    @Test
    void testCreateGroupThrowsStudentNotFountException() throws ServiceException {
        CreateGroupRequest request = new CreateGroupRequest();
        request.setListStudentID(List.of(1));
        User currentUser = mock(User.class);
        Student student = mock(Student.class);

        when(userService.getCurrentUser()).thenReturn(null);

        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.createGroup(request);
        });

        assertEquals(ErrorCode.FAILED_CREATE_GROUP, exception.getErrorCode());
    }
    //-------------------------------------EditGroup-------------------------------------------
    @Test
    void testEditGroup_Success() throws ServiceException {
        // Arrange
        EditGroupRequest editGroupDTO = new EditGroupRequest();
        editGroupDTO.setGroupId(1);

        Group group = new Group();
        group.setId(1L);
        group.setStatus(Constants.GroupStatus.PENDING);

        User user = new User();
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        Member member = new Member();
        member.setRole(Constants.MemberRole.OWNER);

        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(userService.getCurrentUser()).thenReturn(user);
        when(memberRepository.findByStudentIdAndGroupId(1L, 1)).thenReturn(member);
        when(groupRepository.save(any(Group.class))).thenReturn(group);

        GroupResponse expectedResponse = new GroupResponse();
        expectedResponse.setId(1L);
        when(modelMapper.map(group, GroupResponse.class)).thenReturn(expectedResponse);

        // Act
        GroupResponse result = groupService.editGroup(editGroupDTO);

        // Assert
        assertNotNull(result);
        assertEquals(expectedResponse, result);
        verify(groupRepository, times(1)).save(group);
    }

    @Test
    void testEditGroup_GroupNotPending() throws ServiceException {
        // Arrange
        EditGroupRequest editGroupDTO = new EditGroupRequest();
        editGroupDTO.setGroupId(1);

        Group group = new Group();
        group.setId(1L);
        group.setStatus(Constants.GroupStatus.SUBMITTED);

        User user = new User();
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        Member member = new Member();
        member.setRole(Constants.MemberRole.OWNER);

        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(userService.getCurrentUser()).thenReturn(user);
        when(memberRepository.findByStudentIdAndGroupId(1L, 1)).thenReturn(member);

        // Act & Assert
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.editGroup(editGroupDTO);
        });

        assertEquals(ErrorCode.FAILED_EDIT_GROUP, exception.getErrorCode());
        verify(groupRepository, never()).save(any(Group.class));
    }

    @Test
    void testEditGroup_UserNotOwner() throws ServiceException {
        // Arrange
        EditGroupRequest editGroupDTO = new EditGroupRequest();
        editGroupDTO.setGroupId(1);

        Group group = new Group();
        group.setId(1L);
        group.setStatus(Constants.GroupStatus.PENDING);

        User user = new User();
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        Member member = new Member();
        member.setRole(Constants.MemberRole.MEMBER); // Not the owner

        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(userService.getCurrentUser()).thenReturn(user);
        when(memberRepository.findByStudentIdAndGroupId(1L, 1)).thenReturn(member);

        // Act & Assert
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.editGroup(editGroupDTO);
        });

        assertEquals(ErrorCode.FAILED_EDIT_GROUP, exception.getErrorCode());
        verify(groupRepository, never()).save(any(Group.class));
    }

    @Test
    void testEditGroup_GroupNotFound() throws ServiceException {
        // Arrange
        EditGroupRequest editGroupDTO = new EditGroupRequest();
        editGroupDTO.setGroupId(1);

        Group group = new Group();
        group.setId(1L);
        group.setStatus(Constants.GroupStatus.PENDING);

        User user = new User();
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        Member member = new Member();
        member.setRole(Constants.MemberRole.MEMBER);

        when(groupRepository.findById(1L)).thenReturn(Optional.empty());
        when(userService.getCurrentUser()).thenReturn(user);
        when(memberRepository.findByStudentIdAndGroupId(1L, 1)).thenReturn(member);

        // Act & Assert
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.editGroup(editGroupDTO);
        });

        assertEquals(ErrorCode.GROUP_NOT_FOUND, exception.getErrorCode());
        verify(groupRepository, never()).save(any(Group.class));
    }
    //-------------------------------GetGroupById-------------------------------------
    @Test
    void testGetGroupById_Success() throws ServiceException {
        // Arrange
        int groupId = 1;

        Group group = new Group();
        group.setId(1L);
        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.of(group));

        GroupResponse expectedResponse = new GroupResponse();
        expectedResponse.setId(1L);
        when(modelMapper.map(group, GroupResponse.class)).thenReturn(expectedResponse);

        // Act
        GroupResponse result = groupService.getGroupById(groupId);

        // Assert
        assertNotNull(result);
        assertEquals(expectedResponse, result);
        verify(groupRepository, times(1)).findById(Long.valueOf(groupId));
        verify(modelMapper, times(1)).map(group, GroupResponse.class);
    }

    @Test
    void testGetGroupById_GroupNotFound() {
        // Arrange
        int groupId = 0;

        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.empty());

        // Act & Assert
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.getGroupById(groupId);
        });

        assertEquals(ErrorCode.GROUP_NOT_FOUND, exception.getErrorCode());
        verify(groupRepository, times(1)).findById(Long.valueOf(groupId));
        verify(modelMapper, never()).map(any(), eq(GroupResponse.class));
    }
    //-------------------------------GetGroupsBySupervisor-------------------------------
    @Test
    void testGetGroupsBySupervisor_Success() throws ServiceException {
        // Arrange
        int supervisorId = 1;

        Group group = new Group();
        group.setId(1L);

        when(groupRepository.findById(Long.valueOf(supervisorId))).thenReturn(Optional.of(group));

        GroupResponse groupResponse = new GroupResponse();
        groupResponse.setId(1L);
        when(modelMapper.map(group, GroupResponse.class)).thenReturn(groupResponse);

        Member member = new Member();
        member.setId(1L);
        List<Member> members = List.of(member);
        when(memberRepository.findAllByGroupIdAndStatus(1L, Constants.MemberStatus.INGROUP)).thenReturn(members);

        MemberDTO memberDTO = new MemberDTO();
        memberDTO.setId(1L);
        when(modelMapper.map(member, MemberDTO.class)).thenReturn(memberDTO);

        // Act
        List<GroupResponse> result = groupService.getGroupsBySupervisor(supervisorId, 1);

        // Assert
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals(1, result.get(0).getMembers().size());
        assertEquals(1L, result.get(0).getMembers().get(0).getId());
        verify(groupRepository, times(1)).findById(Long.valueOf(supervisorId));
        verify(memberRepository, times(1)).findAllByGroupIdAndStatus(1L, Constants.MemberStatus.INGROUP);
    }

    @Test
    void testGetGroupsBySupervisor_GroupNotFound() {
        // Arrange
        int supervisorId = 1;

        when(groupRepository.findById(Long.valueOf(supervisorId))).thenReturn(Optional.empty());

        // Act

        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.getGroupsBySupervisor(supervisorId, 1);
        });
        // Assert

        assertEquals(ErrorCode.GROUP_NOT_FOUND, exception.getErrorCode());
    }

    //-----------------------------------------SubmitGroup---------------------------------------------------------
    @Test
    void testSubmitGroup_Success() throws ServiceException {
        // Arrange
        SubmitGroupRequest submitGroupRequest = new SubmitGroupRequest();
        submitGroupRequest.setGroupId(1);
        submitGroupRequest.setListSupervisorId(List.of(1, 2));

        Group group = new Group();
        group.setId(1L);
        group.setStatus(Constants.GroupStatus.PENDING);

        List<Member> pendingMembers = new ArrayList<>();
        pendingMembers.add(new Member()); // Add a pending member to be removed

        List<Member> inGroupMembers = new ArrayList<>();
        inGroupMembers.add(new Member()); // Add an in-group member

        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(memberRepository.findAllByGroupIdAndStatus(1L, Constants.MemberStatus.PENDING)).thenReturn(pendingMembers);
        when(memberRepository.findAllByGroupIdAndStatus(1L, Constants.MemberStatus.INGROUP)).thenReturn(inGroupMembers);
        when(modelMapper.map(any(), eq(MemberDTO.class))).thenReturn(new MemberDTO());
        when(supervisorRepository.findById(1L)).thenReturn(Optional.of(new Supervisor()));
        when(supervisorRepository.findById(2L)).thenReturn(Optional.of(new Supervisor()));
        when(modelMapper.map(any(), eq(GroupSupervisorDTO.class))).thenReturn(new GroupSupervisorDTO());
        when(modelMapper.map(group, SubmitGroupResponse.class)).thenReturn(new SubmitGroupResponse());

        // Act
        SubmitGroupResponse result = groupService.submitGroup(submitGroupRequest);

        // Assert
        assertNotNull(result);
        assertEquals(Constants.GroupStatus.SUBMITTED, group.getStatus());
        verify(memberRepository, times(1)).save(pendingMembers.get(0));
        verify(groupRepository, times(1)).save(group);
        verify(groupSupervisorRepository, times(2)).save(any(GroupSupervisor.class));
    }

    @Test
    void testSubmitGroup_GroupNotFound() {
        // Arrange
        SubmitGroupRequest submitGroupRequest = new SubmitGroupRequest();
        submitGroupRequest.setGroupId(999);

        when(groupRepository.findById(999L)).thenReturn(Optional.empty());

        // Act & Assert
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.submitGroup(submitGroupRequest);
        });

        assertEquals(ErrorCode.GROUP_NOT_FOUND, exception.getErrorCode());
        verify(groupRepository, times(1)).findById(999L);
        verify(memberRepository, never()).save(any(Member.class));
        verify(groupSupervisorRepository, never()).save(any(GroupSupervisor.class));
    }

    @Test
    void testSubmitGroup_TooManySupervisors() {
        // Arrange
        SubmitGroupRequest submitGroupRequest = new SubmitGroupRequest();
        submitGroupRequest.setGroupId(1);
        submitGroupRequest.setListSupervisorId(List.of(1, 2, 3));

        Group group = new Group();
        group.setId(1L);
        group.setStatus(Constants.GroupStatus.PENDING);

        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));

        // Act & Assert
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            groupService.submitGroup(submitGroupRequest);
        });

        assertEquals(ErrorCode.MAXIMUM_SIZE_SUPERVISOR_OF_A_GROUP, exception.getErrorCode());
        verify(groupRepository, times(1)).findById(1L);
        verify(memberRepository, never()).save(any(Member.class));
        verify(groupSupervisorRepository, never()).save(any(GroupSupervisor.class));
    }

    //--------------------------------------------------------------------------------
    @Test
    void testGetGroup() {
        // Arrange
        Pageable pageable = Mockito.mock(Pageable.class);
        String keyword = "anhlq";

        Group group1 = new Group();
        group1.setName("Group 1");
        Group group2 = new Group();
        group2.setName("Group 2");
        List<Group> groupList = Arrays.asList(group1, group2);

        Page<Group> groupPage = new PageImpl<>(groupList);
        Mockito.when(groupRepository.findAllByNameContaining(pageable, keyword)).thenReturn(groupPage);

        GroupDTO groupDTO1 = new GroupDTO();
        groupDTO1.setName("Group 1");
        GroupDTO groupDTO2 = new GroupDTO();
        groupDTO2.setName("Group 2");
        List<GroupDTO> groupDTOs = Arrays.asList(groupDTO1, groupDTO2);
        Mockito.when(modelMapper.map(group1, GroupDTO.class)).thenReturn(groupDTO1);
        Mockito.when(modelMapper.map(group2, GroupDTO.class)).thenReturn(groupDTO2);

        // Act
        ResponseEntity<Map<String, Object>> response = groupService.getGroups(pageable, keyword);

        // Assert
        assertNotNull(response);
        assertEquals(ResponseEntity.class, response.getClass());

        Map<String, Object> responseBody = response.getBody();
        assertNotNull(responseBody);
        assertEquals(3, responseBody.size());
        assertEquals(2L, responseBody.get("totalCount"));
        assertEquals(2L, responseBody.get("totalPage"));
        assertEquals(groupDTOs, responseBody.get("data"));
    }
    @Test
    void testGetGroupsWithEmptyResults() {
        // Arrange
        Pageable pageable = Mockito.mock(Pageable.class);
        String keyword = "non-existent-keyword";

        Page<Group> emptyGroupPage = new PageImpl<>(Collections.emptyList());
        Mockito.when(groupRepository.findAllByNameContaining(pageable, keyword)).thenReturn(emptyGroupPage);

        // Act
        ResponseEntity<Map<String, Object>> response = groupService.getGroups(pageable, keyword);

        // Assert
        assertNotNull(response);
        assertEquals(ResponseEntity.class, response.getClass());

        Map<String, Object> responseBody = response.getBody();
        assertNotNull(responseBody);
        assertEquals(3, responseBody.size());
        assertEquals(0L, responseBody.get("totalCount"));
        assertEquals(0L, responseBody.get("totalPage"));
        assertEquals(Collections.emptyList(), responseBody.get("data"));
    }
    //------------------------------------------------------------------------------------------------------
//    @Test
//    void testAddGit() throws ServiceException {
//        // Arrange
//        int groupId = 1;
//        String gitId = "123456";
//
//        Group group = new Group();
//        group.setId(Long.valueOf(groupId));
//        group.setGitId("123456");
//        Mockito.when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.of(group));
//
//        List<GitFolder> gitFolders = Arrays.asList(new GitFolder(), new GitFolder());
//        Mockito.when(MilestoneServiceImpl.getObjectByCallApiToGit(Mockito.anyString())).thenReturn(gitFolders);
//
//        // Act
//        ResponseEntity<String> response = groupService.addGit(groupId, gitId);
//
//        // Assert
//        assertNotNull(response);
//        assertEquals(ResponseEntity.class, response.getClass());
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals("Add Git Id successfully", response.getBody());
//
//        Mockito.verify(groupRepository, Mockito.times(1)).findById(Long.valueOf(groupId));
//        Mockito.verify(groupRepository, Mockito.times(1)).save(group);
//    }



}