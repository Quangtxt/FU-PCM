package com.pcms.be.service;

import com.pcms.be.domain.*;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.Member;
import com.pcms.be.domain.user.Student;
import com.pcms.be.domain.user.User;
import com.pcms.be.pojo.DTO.MilestoneDTO;
import com.pcms.be.pojo.DTO.SemesterMilestone2DTO;
import com.pcms.be.pojo.DTO.SubmitReportResponse;
import com.pcms.be.repository.*;
import com.pcms.be.service.impl.MemberServiceImpl;
import com.pcms.be.service.impl.MilestoneServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
public class MilestoneServiceTest {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private ReportRepository reportRepository;
    @Mock
    private MilestoneGroupRepository milestoneGroupRepository;

    @Mock
    private SemesterRepository semesterRepository;

    @Mock
    private NotificationService notificationService;

    @Mock
    private SemesterMilestoneRepository semesterMilestoneRepository;
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private MilestoneRepository milestoneRepository;
    @InjectMocks
    private MilestoneServiceImpl milestoneService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    @Test
    void testGetByIdWithExistingMilestone() {
        Milestone milestone = new Milestone();
        milestone.setId(1L);
        // Arrange

        MilestoneDTO milestoneDTO = new MilestoneDTO();
        milestone.setId(1L);
        when(milestoneRepository.findById(1L)).thenReturn(Optional.of(milestone));
        when(modelMapper.map(milestone, MilestoneDTO.class)).thenReturn(milestoneDTO);

        // Act
        ResponseEntity<MilestoneDTO> response = milestoneService.getById(1);

        // Assert
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
    }

    @Test
    void testGetByIdWithMissingMilestone() {
        // Arrange
        when(milestoneRepository.findById(2L)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<MilestoneDTO> response = milestoneService.getById(2);

        // Assert
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    void testGetMilestoneGuidancePhase() {
        Semester semester = new Semester();
        semester.setId(1L);

        Milestone milestone1 = new Milestone();
        milestone1.setId(1L);
        milestone1.setParent(5L);

        Milestone milestone2 = new Milestone();
        milestone2.setId(2L);
        milestone2.setParent(6L);

        Semester_Milestone semesterMilestone1 = new Semester_Milestone();
        semesterMilestone1.setMilestone(milestone1);

        Semester_Milestone semesterMilestone2 = new Semester_Milestone();
        semesterMilestone2.setMilestone(milestone2);

        SemesterMilestone2DTO semesterMilestone2DTO1 = new SemesterMilestone2DTO();

        SemesterMilestone2DTO semesterMilestone2DTO2 = new SemesterMilestone2DTO();
        // Arrange
        when(semesterRepository.findById(1L)).thenReturn(Optional.of(semester));
        when(semesterMilestoneRepository.findAllBySemester(semester)).thenReturn(Arrays.asList(semesterMilestone1, semesterMilestone2));
        when(modelMapper.map(semesterMilestone1, SemesterMilestone2DTO.class)).thenReturn(semesterMilestone2DTO1);

        // Act
        List<SemesterMilestone2DTO> result = milestoneService.getMilestoneGuidancePhase(1L);

        // Assert
        assertNotNull(result);
        assertEquals(1, result.size());
    }

//    @Test
//    void testGetProcessOfMilestone() {
//
//        Group group = new Group();
//        group.setId(1L);
//        Semester semester = new Semester();
//        semester.setId(1L);
//        group.setSemester(semester);
//
//        Report report1 = new Report();
//        report1.setId(1L);
//        Milestone milestone = new Milestone();
//        milestone.setId(1L);
//        report1.setMilestone(milestone);
//
//        Report report2 = new Report();
//        report2.setId(2L);
//        Milestone milestone2 = new Milestone();
//        milestone2.setId(2L);
//        report2.setMilestone(milestone2);
//
//        MilestoneGroup milestoneGroup1 = new MilestoneGroup();
//        milestoneGroup1.setGroup(group);
//        milestoneGroup1.setMilestone(milestone);
//        milestoneGroup1.setStatus(true);
//
//        MilestoneGroup milestoneGroup2 = new MilestoneGroup();
//        milestoneGroup2.setGroup(group);
//        milestoneGroup2.setMilestone(milestone2);
//        milestoneGroup2.setStatus(false);
//        // Arrange
//        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
//        when(reportRepository.findAllBySemesterId(1)).thenReturn(List.of(report1, report2));
//        when(milestoneGroupRepository.findByGroupIdAndMilestoneId(1, 1)).thenReturn(Optional.of(milestoneGroup1));
//        when(milestoneGroupRepository.findByGroupIdAndMilestoneId(1, 2)).thenReturn(Optional.of(milestoneGroup2));
//
//        // Act
//        List<SubmitReportResponse> result = milestoneService.getProcessOfMilestone(1);
//
//        // Assert
//        assertNotNull(result);
//        assertEquals(2, result.size());
//        assertEquals(1L, result.get(0).getMilestoneId());
//        assertTrue(result.get(0).isStatus());
//        assertEquals(2L, result.get(1).getMilestoneId());
//        assertFalse(result.get(1).isStatus());
//    }

//    @Test
//    void testSetProcessOfMilestone() {
//        Group group = new Group();
//        group.setId(1L);
//        Semester semester = new Semester();
//        semester.setId(1L);
//        group.setSemester(semester);
//
//        Milestone milestone1 = new Milestone();
//        milestone1.setId(1L);
//        Milestone milestone2 = new Milestone();
//        milestone2.setId(2L);
//
//        MilestoneGroup milestoneGroup1 = new MilestoneGroup();
//        milestoneGroup1.setGroup(group);
//        milestoneGroup1.setMilestone(milestone1);
//        milestoneGroup1.setStatus(false);
//
//        MilestoneGroup milestoneGroup2 = new MilestoneGroup();
//        milestoneGroup2.setGroup(group);
//        milestoneGroup2.setMilestone(milestone2);
//        milestoneGroup2.setStatus(false);
//        // Arrange
//        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
//        when(milestoneRepository.findById(1L)).thenReturn(Optional.of(milestone1));
//        when(milestoneRepository.findById(2L)).thenReturn(Optional.of(milestone2));
//        when(milestoneGroupRepository.findByGroupIdAndMilestoneId(1, 1)).thenReturn(Optional.of(milestoneGroup1));
//        when(milestoneGroupRepository.findByGroupIdAndMilestoneId(1, 2)).thenReturn(Optional.of(milestoneGroup2));
//
//        List<SubmitReportResponse> submitReportResponses = new ArrayList<>();
//        SubmitReportResponse response1 = new SubmitReportResponse(1L, true);
//        SubmitReportResponse response2 = new SubmitReportResponse(2L, true);
//        submitReportResponses.add(response1);
//        submitReportResponses.add(response2);
//
//        // Act
//        milestoneService.setProcessOfMilestone(1, submitReportResponses);
//
//        // Assert
//        verify(milestoneGroupRepository, times(2)).save(any(MilestoneGroup.class));
//        assertEquals(true, milestoneGroup1.isStatus());
//        assertEquals(true, milestoneGroup2.isStatus());
//    }
}
