package com.pcms.be.service;

import com.pcms.be.domain.meeting.Meeting;
import com.pcms.be.domain.meeting.Note;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.request.CreateNoteRequest;
import com.pcms.be.pojo.request.EditNoteRequest;
import com.pcms.be.repository.MeetingRepository;
import com.pcms.be.repository.NoteRepository;
import com.pcms.be.service.impl.NoteServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class NoteServiceTest {
    @Mock
    private UserService userService;
    @Mock
    private MeetingRepository meetingRepository;
    @Mock
    private NoteRepository noteRepository;
    @InjectMocks
    private NoteServiceImpl noteService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateNote_Success() throws ServiceException {
        // Arrange
        CreateNoteRequest noteRequest = new CreateNoteRequest();
        noteRequest.setMeetingId(1);
        noteRequest.setTitle("Test Note");
        noteRequest.setContent("This is a test note.");

        Meeting meeting = new Meeting();
        meeting.setId(1L);

        User currentUser = new User();
        currentUser.setId(1L);

        Note newNote = new Note();
        newNote.setTitle("Test Note");
        newNote.setContent("This is a test note.");
        newNote.setAuthor(currentUser);
        newNote.setMeeting(meeting);

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(meeting));
        Mockito.when(userService.getCurrentUser()).thenReturn(currentUser);
        Mockito.when(noteRepository.save(Mockito.any(Note.class))).thenReturn(newNote);

        // Act
        Note createdNote = noteService.createNote(noteRequest);

        // Assert
        Assertions.assertNotNull(createdNote);
        Assertions.assertEquals("Test Note", createdNote.getTitle());
        Assertions.assertEquals("This is a test note.", createdNote.getContent());
        Assertions.assertSame(currentUser, createdNote.getAuthor());
        Assertions.assertSame(meeting, createdNote.getMeeting());
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(1)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(1)).save(Mockito.any(Note.class));
    }

    @Test
    void testCreateNote_MeetingNotFound() throws ServiceException {
        // Arrange
        CreateNoteRequest noteRequest = new CreateNoteRequest();
        noteRequest.setMeetingId(1);
        noteRequest.setTitle("Test Note");
        noteRequest.setContent("This is a test note.");

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        // Act and Assert
        Assertions.assertThrows(ServiceException.class, () -> noteService.createNote(noteRequest), "Meeting not found");
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(0)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(0)).save(Mockito.any(Note.class));
    }

    @Test
    void testCreateNote_Exception() throws ServiceException {
        // Arrange
        CreateNoteRequest noteRequest = new CreateNoteRequest();
        noteRequest.setMeetingId(1);
        noteRequest.setTitle("Test Note");
        noteRequest.setContent("This is a test note.");

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenThrow(new RuntimeException());

        // Act and Assert
        Assertions.assertThrows(ServiceException.class, () -> noteService.createNote(noteRequest), "Failed to create note");
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(0)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(0)).save(Mockito.any(Note.class));
    }
    //-------------------------------------------------------------------------------------------
    @Test
    void testEditNote_Success() throws ServiceException {
        // Arrange
        EditNoteRequest editNoteRequest = new EditNoteRequest();
        editNoteRequest.setMeetingId(1);
        editNoteRequest.setNoteId(1);
        editNoteRequest.setAuthorId(1);
        editNoteRequest.setTitle("Edited Note");
        editNoteRequest.setContent("This is an edited note.");

        Meeting meeting = new Meeting();
        meeting.setId(1L);

        User user = new User();
        user.setId(1L);

        Note existingNote = new Note();
        existingNote.setId(1L);
        existingNote.setTitle("Test Note");
        existingNote.setContent("This is a test note.");
        existingNote.setAuthor(user);

        Note editedNote = new Note();
        editedNote.setId(1L);
        editedNote.setTitle("Edited Note");
        editedNote.setContent("This is an edited note.");
        editedNote.setAuthor(user);

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(meeting));
        Mockito.when(noteRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingNote));
        Mockito.when(userService.getCurrentUser()).thenReturn(existingNote.getAuthor());
        Mockito.when(noteRepository.save(Mockito.any(Note.class))).thenReturn(editedNote);

        // Act
        Note updatedNote = noteService.editNote(editNoteRequest);

        // Assert
        Assertions.assertNotNull(updatedNote);
        Assertions.assertEquals("Edited Note", updatedNote.getTitle());
        Assertions.assertEquals("This is an edited note.", updatedNote.getContent());
        Assertions.assertSame(existingNote.getAuthor(), updatedNote.getAuthor());
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(noteRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(1)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(1)).save(Mockito.any(Note.class));
    }

    @Test
    void testEditNote_MeetingNotFound() throws ServiceException {
        // Arrange
        EditNoteRequest editNoteRequest = new EditNoteRequest();
        editNoteRequest.setMeetingId(99);
        editNoteRequest.setNoteId(1);
        editNoteRequest.setAuthorId(1);
        editNoteRequest.setTitle("Edited Note");
        editNoteRequest.setContent("This is an edited note.");

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        // Act and Assert
        Assertions.assertThrows(ServiceException.class, () -> noteService.editNote(editNoteRequest), "Meeting not found");
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(noteRepository, Mockito.times(0)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(0)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(0)).save(Mockito.any(Note.class));
    }

    @Test
    void testEditNote_NoteNotFound() throws ServiceException {
        // Arrange
        EditNoteRequest editNoteRequest = new EditNoteRequest();
        editNoteRequest.setMeetingId(1);
        editNoteRequest.setNoteId(99);
        editNoteRequest.setAuthorId(1);
        editNoteRequest.setTitle("Edited Note");
        editNoteRequest.setContent("This is an edited note.");

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(new Meeting()));
        Mockito.when(noteRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        // Act and Assert
        Assertions.assertThrows(ServiceException.class, () -> noteService.editNote(editNoteRequest), "Note not found");
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(noteRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(0)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(0)).save(Mockito.any(Note.class));
    }

    @Test
    void testEditNote_UserNotAllowed() throws ServiceException {
        // Arrange
        EditNoteRequest editNoteRequest = new EditNoteRequest();
        editNoteRequest.setMeetingId(1);
        editNoteRequest.setNoteId(1);
        editNoteRequest.setAuthorId(2);
        editNoteRequest.setTitle("Edited Note");
        editNoteRequest.setContent("This is an edited note.");

        User user = new User();
        user.setId(1L);

        Note existingNote = new Note();
        existingNote.setId(1L);
        existingNote.setTitle("Test Note");
        existingNote.setContent("This is a test note.");
        existingNote.setAuthor(user);

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(new Meeting()));
        Mockito.when(noteRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingNote));
        Mockito.when(userService.getCurrentUser()).thenReturn(user);

        // Act and Assert
        Assertions.assertThrows(ServiceException.class, () -> noteService.editNote(editNoteRequest), "User not allowed to edit the note");
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(noteRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(1)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(0)).save(Mockito.any(Note.class));
    }

    @Test
    void testEditNote_Exception() throws ServiceException {
        // Arrange
        EditNoteRequest editNoteRequest = new EditNoteRequest();
        editNoteRequest.setMeetingId(1);
        editNoteRequest.setNoteId(1);
        editNoteRequest.setAuthorId(1);
        editNoteRequest.setTitle("Edited Note");
        editNoteRequest.setContent("This is an edited note.");

        Mockito.when(meetingRepository.findById(Mockito.anyLong())).thenThrow(new RuntimeException());

        // Act and Assert
        Assertions.assertThrows(ServiceException.class, () -> noteService.editNote(editNoteRequest), "Failed to edit note");
        Mockito.verify(meetingRepository, Mockito.times(1)).findById(Mockito.anyLong());
        Mockito.verify(noteRepository, Mockito.times(0)).findById(Mockito.anyLong());
        Mockito.verify(userService, Mockito.times(0)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(0)).save(Mockito.any(Note.class));
    }

    //----------------------------------------------------------------------------------------------------

    @Test
    void testRemoveNote_NoteNotFound() {
        Mockito.when(noteRepository.findById(99L)).thenReturn(Optional.empty());

        Assertions.assertThrows(ServiceException.class, () -> noteService.removeNote(99));
        Mockito.verify(noteRepository, Mockito.times(1)).findById(99L);
    }

    @Test
    void testRemoveNote_UserNotAllowed() throws ServiceException {
        User test_user = new User();
        test_user.setId(2L);
        test_user.setName("test_user");

        User current_user = new User();
        current_user.setId(3L);
        current_user.setName("current_user");
        Note note = new Note();
        note.setId(1L);
        note.setAuthor(test_user);

        Mockito.when(noteRepository.findById(1L)).thenReturn(Optional.of(note));
        Mockito.when(userService.getCurrentUser()).thenReturn(current_user);

        Assertions.assertThrows(ServiceException.class, () -> noteService.removeNote(1));
        Mockito.verify(noteRepository, Mockito.times(1)).findById(1L);
        Mockito.verify(userService, Mockito.times(1)).getCurrentUser();
    }

    @Test
    void testRemoveNote_Success() throws ServiceException {
        User user = new User();
        user.setId(3L);
        Note note = new Note();
        note.setId(3L);
        note.setAuthor(user);

        Mockito.when(noteRepository.findById(3L)).thenReturn(Optional.of(note));
        Mockito.when(userService.getCurrentUser()).thenReturn(user);

        Note removedNote = noteService.removeNote(3);

        Assertions.assertNotNull(removedNote);
        Assertions.assertEquals(note, removedNote);
        Mockito.verify(noteRepository, Mockito.times(1)).findById(3L);
        Mockito.verify(userService, Mockito.times(1)).getCurrentUser();
        Mockito.verify(noteRepository, Mockito.times(1)).delete(note);
    }

    //----------------------------------------------------------------------------------------
    @Test
    void testViewNotes_NoteListEmpty() throws ServiceException {
        Mockito.when(noteRepository.findAllByMeeting_Id(99L)).thenReturn(new ArrayList<>());

        List<Note> notes = noteService.viewNotes(99);

        Assertions.assertNotNull(notes);
        Assertions.assertTrue(notes.isEmpty());
        Mockito.verify(noteRepository, Mockito.times(1)).findAllByMeeting_Id(99L);
    }

    @Test
    void testViewNotes_NoteListPopulated() throws ServiceException {
        List<Note> expectedNotes = new ArrayList<>();

        Meeting meeting = new Meeting();
        meeting.setId(1L);

        Note note1 = new Note();
        note1.setId(1L);
        note1.setContent("content1");
        note1.setTitle("note1");
        note1.setMeeting(meeting);


        Note note2 = new Note();
        note2.setId(2L);
        note2.setContent("content2");
        note2.setTitle("note2");
        note2.setMeeting(meeting);

        expectedNotes.add(note1);
        expectedNotes.add(note2);

        Mockito.when(noteRepository.findAllByMeeting_Id(1L)).thenReturn(expectedNotes);

        List<Note> notes = noteService.viewNotes(1);

        Assertions.assertNotNull(notes);
        Assertions.assertEquals(expectedNotes.size(), notes.size());
        Assertions.assertEquals(expectedNotes, notes);
        Mockito.verify(noteRepository, Mockito.times(1)).findAllByMeeting_Id(1L);
    }
}