package com.pcms.be.service;

import com.pcms.be.domain.meeting.Meeting;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.Member;
import com.pcms.be.domain.user.Student;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.DTO.MeetingDTO;
import com.pcms.be.pojo.DTO.MemberDTO;
import com.pcms.be.pojo.DTO.StudentDTO;
import com.pcms.be.pojo.request.InviteMemberRequest;
import com.pcms.be.pojo.request.UpdateInvitationStatusRequest;
import com.pcms.be.pojo.response.MemberResponse;
import com.pcms.be.repository.GroupRepository;
import com.pcms.be.repository.MeetingRepository;
import com.pcms.be.repository.MemberRepository;
import com.pcms.be.repository.StudentRepository;
import com.pcms.be.service.impl.MeetingServiceImpl;
import com.pcms.be.service.impl.MemberServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
public class MemberServiceTest
{

//    @Mock
//    private MemberServiceImpl memberService;

    @Mock
    private MemberRepository memberRepository;

    @Mock
    private UserService userService;

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private NotificationService notificationService;

    @Mock
    private StudentRepository studentRepository;

    @Mock
    private ModelMapper modelMapper;
    @InjectMocks
    private MemberServiceImpl memberService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testUpdateStatusWhenStudentAcceptsJoinGroup() throws ServiceException {
        // Arrange
        UpdateInvitationStatusRequest request = new UpdateInvitationStatusRequest();
        request.setGroupId(1);
        request.setStudentId(2);
        request.setStatus(Constants.MemberStatus.INGROUP);

        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(2L);
        user.setStudent(student);

        Group group = new Group();
        group.setId(1L);

        Member member = new Member();
        member.setStatus(Constants.MemberStatus.PENDING);
        member.setStudent(user.getStudent());

        when(userService.getCurrentUser()).thenReturn(user);
        when(groupRepository.findById(group.getId())).thenReturn(Optional.of(group));
        when(memberRepository.findByStudentIdAndGroupId(Long.valueOf(request.getStudentId()), request.getGroupId())).thenReturn(member);
        doNothing().when(notificationService).createMemberJoinGroupNotification(member);
        when(memberRepository.save(member)).thenReturn(member);
        when(modelMapper.map(member, MemberResponse.class)).thenReturn(new MemberResponse());

        // Act
        MemberResponse response = memberService.updateStatus(request);

        // Assert
        assertEquals(Constants.MemberStatus.INGROUP, member.getStatus());
        assertEquals(MemberResponse.class, response.getClass());
    }

    @Test
    void testUpdateStatusWhenStudentRejectsJoinGroup() throws ServiceException {
        // Arrange
        UpdateInvitationStatusRequest request = new UpdateInvitationStatusRequest();
        request.setGroupId(1);
        request.setStudentId(2);
        request.setStatus(Constants.MemberStatus.OUTGROUP);

        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        Group group = new Group();
        group.setId(1L);

        Member member = new Member();
        member.setStatus(Constants.MemberStatus.PENDING);
        member.setStudent(user.getStudent());

        when(userService.getCurrentUser()).thenReturn(user);
        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(memberRepository.findByStudentIdAndGroupId(Long.valueOf(request.getStudentId()), request.getGroupId())).thenReturn(member);
        doNothing().when(notificationService).createRejectJoinGroupNotification(member);
        when(memberRepository.save(member)).thenReturn(member);
        when(modelMapper.map(member, MemberResponse.class)).thenReturn(new MemberResponse());

        // Act
        MemberResponse response = memberService.updateStatus(request);

        // Assert
        assertEquals(Constants.MemberStatus.OUTGROUP, member.getStatus());
        assertEquals(MemberResponse.class, response.getClass());
    }

    @Test
    void testUpdateStatusWhenOwnerRemovesMember() throws ServiceException {
        // Arrange
        UpdateInvitationStatusRequest request = new UpdateInvitationStatusRequest();
        request.setGroupId(1);
        request.setStudentId(2);
        request.setStatus(Constants.MemberStatus.PENDING);

        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        Group group = new Group();
        group.setId(1L);

        Member member = new Member();
        member.setStatus(Constants.MemberStatus.OUTGROUP);
        Student student1 = new Student();
        student1.setId(2L);
        member.setStudent(student1);

        member.setRole(Constants.MemberRole.OWNER);

        when(userService.getCurrentUser()).thenReturn(user);
        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(memberRepository.findByStudentIdAndGroupId(2L, 1)).thenReturn(member);
        when(memberRepository.findByStudentIdAndGroupId(1L, 1)).thenReturn(member);
        doNothing().when(notificationService).removeMemberNotification(member);
        when(memberRepository.save(member)).thenReturn(member);
        when(modelMapper.map(member, MemberResponse.class)).thenReturn(new MemberResponse());

        // Act
        MemberResponse response = memberService.updateStatus(request);

        // Assert
        assertEquals(Constants.MemberStatus.OUTGROUP, member.getStatus());
        assertEquals(MemberResponse.class, response.getClass());
    }

    @Test
    void testUpdateStatusWhenGroupNotFound() throws ServiceException {
        // Arrange
        UpdateInvitationStatusRequest request = new UpdateInvitationStatusRequest();
        request.setGroupId(1);
        request.setStudentId(3);
        request.setStatus(Constants.MemberStatus.INGROUP);

        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        when(userService.getCurrentUser()).thenReturn(user);
        when(groupRepository.findById(1L)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.updateStatus(request), ErrorCode.GROUP_NOT_FOUND.toString());
    }

    @Test
    void testUpdateStatusWhenStudentNotFound() throws ServiceException {
        // Arrange
        UpdateInvitationStatusRequest request = new UpdateInvitationStatusRequest();
        request.setGroupId(1);
        request.setStudentId(4);
        request.setStatus(Constants.MemberStatus.INGROUP);

        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(4L);
        user.setStudent(student);

        Group group = new Group();
        group.setId(1L);

        when(userService.getCurrentUser()).thenReturn(user);
        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(memberRepository.findByStudentIdAndGroupId(Long.valueOf(request.getStudentId()), 1)).thenReturn(null);

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.updateStatus(request), ErrorCode.STUDENT_NOT_FOUND.toString());
    }

    @Test
    void testUpdateStatusWhenUserNotAllowed() throws ServiceException {
        // Arrange
        UpdateInvitationStatusRequest request = new UpdateInvitationStatusRequest();
        request.setGroupId(1);
        request.setStudentId(2);
        request.setStatus(Constants.MemberStatus.INGROUP);

        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);

        Group group = new Group();
        group.setId(1L);

        Member member = new Member();
        member.setRole(Constants.MemberRole.MEMBER);

        when(userService.getCurrentUser()).thenReturn(user);
        when(groupRepository.findById(1L)).thenReturn(Optional.of(group));
        when(memberRepository.findByStudentIdAndGroupId(1L, 1)).thenReturn(member);
        when(memberRepository.findByStudentIdAndGroupId(2L, 1)).thenReturn(member);

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.updateStatus(request), ErrorCode.USER_NOT_ALLOW.toString());
    }

    @Test
    public void getGroupMember_ShouldReturnListOfMemberDTO_WhenGroupIdExists() throws ServiceException {
        // Arrange
        int groupId = 1;
        List<Member> members = new ArrayList<>();
        Member member1 = new Member();
        member1.setId(1L);
        member1.setStatus(Constants.MemberStatus.INGROUP);
        Student student = new Student();
        student.setId(1L);
        member1.setStudent(student);
        member1.setRole("MEMBER");
        members.add(member1);
        when(memberRepository.findAllByGroupIdAndStatus(anyLong(), any())).thenReturn(members);
        MemberDTO member = new MemberDTO();
        member.setId(1L);

        when(modelMapper.map(any(Member.class), eq(MemberDTO.class))).thenReturn(member);

        // Act
        List<MemberDTO> result = memberService.getGroupMember(groupId);

        // Assert
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void getGroupMember_ShouldThrowServiceException_WhenExceptionOccurs() {
        // Arrange
        int groupId = 2;
        when(memberRepository.findAllByGroupIdAndStatus(anyLong(), any())).thenThrow(new RuntimeException("Some error occurred"));

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.getGroupMember(groupId));
        verify(memberRepository, times(1)).findAllByGroupIdAndStatus(anyLong(), any());
    }

    @Test
    public void getGroupMemberIncludePending_ShouldReturnListOfMemberDTO_WhenGroupIdExists() throws ServiceException {
        // Arrange
        int groupId = 1;
        List<Member> members = new ArrayList<>();
        Member member1 = new Member();
        member1.setId(1L);
        member1.setStatus(Constants.MemberStatus.INGROUP);
        Student student = new Student();
        student.setId(1L);
        member1.setStudent(student);
        member1.setRole("MEMBER");
        members.add(member1);

        Member member2 = new Member();
        member2.setId(2L);
        member2.setStatus(Constants.MemberStatus.PENDING);
        student = new Student();
        student.setId(2L);
        member2.setStudent(student);
        member2.setRole("PENDING");
        members.add(member2);

        when(memberRepository.findAllByGroupIdAndStatusIn(anyLong(), anyList())).thenReturn(members);
        when(modelMapper.map(any(Member.class), eq(MemberDTO.class))).thenAnswer(invocation -> {
            Member member = invocation.getArgument(0);
            StudentDTO studentDTO = new StudentDTO();
            studentDTO.setId(member.getStudent().getId());
            return new MemberDTO(member.getId(), studentDTO, member.getRole(), member.getStatus());
        });

        // Act
        List<MemberDTO> result = memberService.getGroupMemberIncludePending(groupId);

        // Assert
        assertEquals(2, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals(1L, result.get(0).getStudent().getId());
        assertEquals("MEMBER", result.get(0).getRole());
        assertEquals(Constants.MemberStatus.INGROUP, result.get(0).getStatus());
        assertEquals(2L, result.get(1).getId());
        assertEquals(2L, result.get(1).getStudent().getId());
        assertEquals("PENDING", result.get(1).getRole());
        assertEquals(Constants.MemberStatus.PENDING, result.get(1).getStatus());
        verify(memberRepository, times(1)).findAllByGroupIdAndStatusIn(anyLong(), anyList());
        verify(modelMapper, times(2)).map(any(Member.class), eq(MemberDTO.class));
    }

    @Test
    public void getGroupMemberIncludePending_ShouldThrowServiceException_WhenExceptionOccurs() {
        // Arrange
        int groupId = 2;
        when(memberRepository.findAllByGroupIdAndStatusIn(anyLong(), anyList())).thenThrow(new RuntimeException("Some error occurred"));

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.getGroupMemberIncludePending(groupId));
        verify(memberRepository, times(1)).findAllByGroupIdAndStatusIn(anyLong(), anyList());
    }

//    @Test
//    void testInviteMemberSuccess() throws ServiceException {
//        // Arrange
//        InviteMemberRequest inviteMemberRequest = new InviteMemberRequest();
//        inviteMemberRequest.setGroupId(1);
//        inviteMemberRequest.setListStudentID(Arrays.asList(1, 2, 3));
//
//        User user = new User();
//        user.setId(1L);
//        Student student = new Student();
//        student.setId(1L);
//        user.setStudent(student);
//        when(userService.getCurrentUser()).thenReturn(user);
//
//        Group group = new Group();
//        group.setId(1L);
//        when(groupRepository.findById(anyLong())).thenReturn(Optional.of(group));
//        Member member = new Member();
//        member.setId(1L);
//        member.setRole(Constants.MemberRole.OWNER);
//        member.setStatus(Constants.MemberStatus.PENDING);
//        when(memberRepository.findByStudentIdAndGroupId(user.getStudent().getId(), Integer.parseInt(Long.toString(group.getId())))).thenReturn(member);
//
//        when(memberRepository.findAllByGroupIdAndStatus(anyLong(), any())).thenReturn(Collections.emptyList());
//
//        Student invitedStudent = new Student();
//        invitedStudent.setId(2L);
//        when(studentRepository.findById(anyLong())).thenReturn(Optional.of(invitedStudent));
//
//        Member newMember = new Member();
//        member.setId(2L);
//        member.setRole(Constants.MemberRole.OWNER);
//        member.setStatus(Constants.MemberStatus.PENDING);
//        when(memberRepository.findByStudentIdAndStatus(anyLong(), any())).thenReturn(null);
//        when(memberRepository.save(any(Member.class))).thenReturn(newMember);
//
//        MemberResponse memberResponse = new MemberResponse();
//        when(modelMapper.map(any(Member.class), eq(MemberResponse.class))).thenReturn(memberResponse);
//
//        // Act
//        List<MemberResponse> result = memberService.inviteMember(inviteMemberRequest);
//
//        // Assert
//        assertNotNull(result);
//        assertEquals(3, result.size());
//        verify(notificationService, times(3)).inviteMemberNotification(any(Member.class));
//    }

    @Test
    void testInviteMemberGroupNotFound() {
        // Arrange
        InviteMemberRequest inviteMemberRequest = new InviteMemberRequest();
        inviteMemberRequest.setGroupId(2);
        inviteMemberRequest.setListStudentID(Arrays.asList(1, 2, 3));

        when(groupRepository.findById(anyLong())).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.inviteMember(inviteMemberRequest));
        verify(notificationService, never()).inviteMemberNotification(any(Member.class));
    }

    @Test
    void testInviteMemberUserNotOwner() throws ServiceException {
        // Arrange
        InviteMemberRequest inviteMemberRequest = new InviteMemberRequest();
        inviteMemberRequest.setGroupId(1);
        inviteMemberRequest.setListStudentID(Arrays.asList(5, 6, 7));

        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);
        when(userService.getCurrentUser()).thenReturn(user);

        Group group = new Group();
        group.setId(1L);
        when(groupRepository.findById(anyLong())).thenReturn(Optional.of(group));

        Member member = new Member();
        member.setId(1L);
        member.setRole(Constants.MemberRole.MEMBER);
        member.setStatus(Constants.MemberStatus.INGROUP);
        when(memberRepository.findByStudentIdAndGroupId(user.getStudent().getId(), Integer.parseInt(Long.toString(group.getId())))).thenReturn(member);

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.inviteMember(inviteMemberRequest));
        verify(notificationService, never()).inviteMemberNotification(any(Member.class));
    }

    @Test
    void testEmpowerOwner_Success() throws ServiceException {
        // Arrange
        int groupId = 1;
        int studentId = 2;
        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);
        when(userService.getCurrentUser()).thenReturn(user);

        Group group = new Group();
        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.of(group));

        Member owner = new Member();
        owner.setRole(Constants.MemberRole.OWNER);
        when(memberRepository.findByStudentIdAndGroupId(user.getStudent().getId(), groupId)).thenReturn(owner);

        Member member = new Member();
        member.setStatus(Constants.MemberStatus.INGROUP);
        when(memberRepository.findByStudentIdAndGroupId(Long.valueOf(studentId), groupId)).thenReturn(member);

        // Act
        List<MemberResponse> result = memberService.empowerOwner(groupId, studentId);

        // Assert
        assertEquals(2, result.size());
        assertEquals(Constants.MemberRole.MEMBER, owner.getRole());
        assertEquals(Constants.MemberRole.OWNER, member.getRole());
    }

    @Test
    void testEmpowerOwner_GroupNotFound() {
        // Arrange
        int groupId = 1;
        int studentId = 1;
        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.empowerOwner(groupId, studentId),
                ErrorCode.GROUP_NOT_FOUND.toString());
    }

    @Test
    void testEmpowerOwner_StudentNotOwner() throws ServiceException {
        // Arrange
        int groupId = 1;
        int studentId = 2;
        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);
        when(userService.getCurrentUser()).thenReturn(user);

        Group group = new Group();
        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.of(group));

        Member owner = new Member();
        owner.setRole(Constants.MemberRole.MEMBER);
        when(memberRepository.findByStudentIdAndGroupId(user.getStudent().getId(), groupId)).thenReturn(owner);

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.empowerOwner(groupId, studentId),
                ErrorCode.STUDENT_IS_INVALID_ROLE.toString());
    }

    @Test
    void testEmpowerOwner_StudentNotInGroup() throws ServiceException {
        // Arrange
        int groupId = 1;
        int studentId = 2;
        User user = new User();
        user.setId(1L);
        Student student = new Student();
        student.setId(1L);
        user.setStudent(student);
        when(userService.getCurrentUser()).thenReturn(user);

        Group group = new Group();
        when(groupRepository.findById(Long.valueOf(groupId))).thenReturn(Optional.of(group));

        Member owner = new Member();
        owner.setRole(Constants.MemberRole.OWNER);
        when(memberRepository.findByStudentIdAndGroupId(user.getStudent().getId(), groupId)).thenReturn(owner);

        Member member = new Member();
        member.setStatus(Constants.MemberStatus.PENDING);
        when(memberRepository.findByStudentIdAndGroupId(Long.valueOf(studentId), groupId)).thenReturn(member);

        // Act and Assert
        assertThrows(ServiceException.class, () -> memberService.empowerOwner(groupId, studentId),
                ErrorCode.STUDENT_IS_INVALID_ROLE.toString());
    }
}
