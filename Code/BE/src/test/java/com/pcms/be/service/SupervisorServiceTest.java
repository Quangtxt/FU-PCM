package com.pcms.be.service;


import com.pcms.be.domain.Campus;
import com.pcms.be.domain.user.Role;
import com.pcms.be.domain.user.Supervisor;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.functions.ValidateData;
import com.pcms.be.pojo.DTO.SupervisorDTO;

import com.pcms.be.pojo.DTO.UserDTO;
import com.pcms.be.pojo.request.AddSupervisorRequest;
import com.pcms.be.pojo.request.EditSupervisorProfileRequest;
import com.pcms.be.pojo.response.SupervisorPageResponse;
import com.pcms.be.pojo.response.SupervisorProfileResponse;
import com.pcms.be.repository.RoleRepository;
import com.pcms.be.repository.SupervisorRepository;
import com.pcms.be.repository.UserRepository;
import com.pcms.be.service.impl.SupervisorServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SupervisorServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private SupervisorRepository supervisorRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private SupervisorServiceImpl supervisorService;

    @BeforeEach
    public void setUp() throws IOException, ServiceException {
        MockitoAnnotations.openMocks(this);
    }
    //-------------------------------GetSupervisor---------------------------------------

//    @Test
//    void testGetSupervisor_UserNotFound() {
//        // Arrange
//        String keyword = "Invalid";
//        PageRequest pageRequest = PageRequest.of(0, 10);
//
//        when(supervisorRepository.findSupervisorsByEmailOrName(keyword, pageRequest)).thenReturn(Page.empty());
//
//        // Act & Assert
//        assertThrows(ServiceException.class, () -> {
//            supervisorService.getSupervisor(keyword, pageRequest);
//        });
//
//        verify(supervisorRepository, times(1)).findSupervisorsByEmailOrName(keyword, pageRequest);
//    }

    //    @Test
//    void testGetSupervisor_NullKeyword() throws ServiceException {
//        // Arrange
//        String keyword = null;
//        PageRequest pageRequest = PageRequest.of(0, 10);
//        Supervisor supervisor = new Supervisor();
//        SupervisorDTO supervisorDTO = new SupervisorDTO();
//        List<Supervisor> supervisors = List.of(supervisor);
//        Page<Supervisor> supervisorPage = new PageImpl<>(supervisors, pageRequest, supervisors.size());
//
//        when(supervisorRepository.findSupervisorsByEmailOrName(keyword, pageRequest)).thenReturn(supervisorPage);
//        when(modelMapper.map(supervisor, SupervisorDTO.class)).thenReturn(supervisorDTO);
//
//        SupervisorPageResponse expectedResponse = new SupervisorPageResponse();
//        expectedResponse.setTotalPage(1);
//        expectedResponse.setTotalCount(1L);
//        expectedResponse.setData(List.of(supervisorDTO));
//
//        // Act
//        SupervisorPageResponse actualResponse = supervisorService.getSupervisor(keyword, pageRequest);
//
//        // Assert
//        assertNotNull(actualResponse);
//        assertEquals(expectedResponse.getTotalPage(), actualResponse.getTotalPage());
//        assertEquals(expectedResponse.getTotalCount(), actualResponse.getTotalCount());
//        assertEquals(expectedResponse.getData(), actualResponse.getData());
//    }


    //-------------------------------GetSupervisorProfile---------------------------------------
//    @Test
//    void testGetSupervisorProfile_UserNotFound() {
//        // Arrange
//        int userId = 1;
//        when(userRepository.findById((long) userId)).thenReturn(Optional.empty());
//
//        // Act & Assert
//        ServiceException exception = assertThrows(ServiceException.class, () -> {
//            supervisorService.getSupervisorProfile(userId);
//        });
//        assertEquals(ErrorCode.USER_NOT_FOUND, exception.getErrorCode());
//
//        // Verify interactions
//        verify(userRepository, times(1)).findById((long) userId);
//    }

//    @Test
//    void testGetSupervisorProfile_AuthorityLevelNotFound() {
//        // Arrange
//        int userId = 1;
//        User user = new User();
//        user.setId((long) userId);
//        user.setSupervisor(null); // No supervisor associated
//
//        when(userRepository.findById((long) userId)).thenReturn(Optional.of(user));
//
//        // Act & Assert
//        ServiceException exception = assertThrows(ServiceException.class, () -> {
//            supervisorService.getSupervisorProfile(userId);
//        });
//        assertEquals(ErrorCode.AUTHORITY_LEVEL_NOT_FOUND, exception.getErrorCode());
//
//        // Verify interactions
//        verify(userRepository, times(1)).findById((long) userId);
//    }

//    @Test
//    void testGetSupervisorProfile_Success() throws ServiceException {
//        // Arrange
//        int userId = 1;
//        User user = new User();
//        user.setId((long) userId);
//        user.setName("John Doe");
//        user.setEmail("john.doe@example.com");
//
//        Supervisor supervisor = new Supervisor();
//        supervisor.setPersonalEmail("john.doe.personal@example.com");
//        supervisor.setPhone("123-456-7890");
//        supervisor.setSelfDescription("A dedicated supervisor");
//
//        user.setSupervisor(supervisor);
//
//        when(userRepository.findById((long) userId)).thenReturn(Optional.of(user));
//
//        // Act
//        SupervisorProfileResponse response = supervisorService.getSupervisorProfile(userId);
//
//        // Assert
//        assertNotNull(response);
//        assertEquals("john.doe@example.com", response.getFptEmail());
//        assertEquals("john.doe.personal@example.com", response.getPersionalEmail());
//        assertEquals("123-456-7890", response.getPhone());
//        assertEquals("John Doe", response.getFullName());
//        assertEquals("A dedicated supervisor", response.getSelfDescription());
//
//        // Verify interactions
//        verify(userRepository, times(1)).findById((long) userId);
//    }

    //-----------------------------------EditSupervisorProfile-------------------------------------------------
//    @Test
//    void testEditSupervisorProfile_UserNotFound() {
//        // Arrange
//        EditSupervisorProfileRequest request = new EditSupervisorProfileRequest();
//        request.setUserId(1L);
//
//        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
//
//        // Act & Assert
//        ServiceException exception = assertThrows(ServiceException.class, () -> {
//            supervisorService.editSupervisorProfile(request);
//        });
//        assertEquals(ErrorCode.USER_NOT_FOUND, exception.getErrorCode());
//
//        // Verify interactions
//        verify(userRepository, times(1)).findById(anyLong());
//        verify(userRepository, never()).save(any());
//    }

//    @Test
//    void testEditSupervisorProfile_AuthorityLevelNotFound() {
//        // Arrange
//        EditSupervisorProfileRequest request = new EditSupervisorProfileRequest();
//        request.setUserId(1L);
//
//        User user = new User();
//        user.setId(1L);
//        user.setSupervisor(null); // No supervisor associated
//
//        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
//
//        // Act & Assert
//        ServiceException exception = assertThrows(ServiceException.class, () -> {
//            supervisorService.editSupervisorProfile(request);
//        });
//        assertEquals(ErrorCode.AUTHORITY_LEVEL_NOT_FOUND, exception.getErrorCode());
//
//        // Verify interactions
//        verify(userRepository, times(1)).findById(anyLong());
//        verify(userRepository, never()).save(any());
//    }

//    @Test
//    void testEditSupervisorProfile_Success() throws ServiceException {
//        // Arrange
//        EditSupervisorProfileRequest request = new EditSupervisorProfileRequest();
//        request.setUserId(1L);
//        request.setFullName("John Doe");
//        request.setFptEmail("john.doe@example.com");
//        request.setPersionalEmail("john.doe.personal@example.com");
//        request.setPhone("123-456-7890");
//        request.setSelfDescription("A dedicated supervisor");
//
//        User user = new User();
//        user.setId(1L);
//        user.setName("Old Name");
//        user.setEmail("old.email@example.com");
//
//        Supervisor supervisor = new Supervisor();
//        supervisor.setPersonalEmail("old.personal.email@example.com");
//        supervisor.setPhone("987-654-3210");
//        supervisor.setSelfDescription("Old description");
//
//        user.setSupervisor(supervisor);
//
//        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
//
//        // Act
//        SupervisorProfileResponse response = supervisorService.editSupervisorProfile(request);
//
//        // Assert
//        assertNotNull(response);
//        assertEquals("john.doe@example.com", response.getFptEmail());
//        assertEquals("john.doe.personal@example.com", response.getPersionalEmail());
//        assertEquals("123-456-7890", response.getPhone());
//        assertEquals("John Doe", response.getFullName());
//
//        // Verify interactions
//        verify(userRepository, times(1)).findById(anyLong());
//        verify(userRepository, times(1)).save(user);
//
//        // Verify the updated fields in user and supervisor
//        assertEquals("John Doe", user.getName());
//        assertEquals("john.doe@example.com", user.getEmail());
//        assertEquals("john.doe.personal@example.com", supervisor.getPersonalEmail());
//        assertEquals("123-456-7890", supervisor.getPhone());
//        assertEquals("A dedicated supervisor", supervisor.getSelfDescription());
//    }
    //-----------------------------------------------------------------------------------

}
