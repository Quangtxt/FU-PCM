package com.pcms.be.service;

import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.response.SupervisorPageResponse;
import org.springframework.data.domain.PageRequest;

public interface UserService {
    String checkUser(String token, String campusCode,int type)  throws ServiceException;
    User getCurrentUser() throws ServiceException;

}
