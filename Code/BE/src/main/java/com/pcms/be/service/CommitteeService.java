package com.pcms.be.service;

import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.DTO.CommitteeAssignmentDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface CommitteeService {
    ResponseEntity<Map<String, Object>> getCommittees(Pageable pageable,int semesterId) throws ServiceException;
    ResponseEntity<Map<String, Object>> getCommitteeAssignments(Pageable pageable,int semesterId);
    List<CommitteeAssignmentDTO> getCommitteeAssignmentsByCommittee(int semesterId) throws ServiceException;
    boolean checkIsCommittee(Long userId) throws ServiceException;

}
