package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExcelSupervisorDTO {
    String emailFE;
    String emailFPT;
    String userName;
    String fullName;
    String phoneNumber;
    String genderTxt;
    String note;
    String branch;
    String parentDepartment;
    String childDepartment;
    String jobTitle;
    String contractType;
    double emId;
}
