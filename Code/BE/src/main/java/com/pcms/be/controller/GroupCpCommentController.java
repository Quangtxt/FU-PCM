package com.pcms.be.controller;

import com.pcms.be.domain.user.GroupCpComment;
import com.pcms.be.pojo.DTO.GroupCpCommentDTO;
import com.pcms.be.pojo.request.GroupCpCommentRequest;
import com.pcms.be.service.GroupCpCommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/g/comment")
public class GroupCpCommentController {

    private final GroupCpCommentService groupCpCommentService;

    @PostMapping
    public ResponseEntity<GroupCpCommentDTO> addGroupCpComment(@RequestBody GroupCpCommentRequest req) {
        GroupCpCommentDTO createdComment = groupCpCommentService.addGroupCpComment(req);
        return ResponseEntity.ok(createdComment);
    }

    @GetMapping("/{groupId}")
    public ResponseEntity<GroupCpCommentDTO> getGroupCpCommentByGroupId(@PathVariable Long groupId) {
        GroupCpCommentDTO groupCpCommentDTO = groupCpCommentService.getGroupCpCommentByGroupId(groupId);
        return ResponseEntity.ok(groupCpCommentDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<GroupCpCommentDTO> updateGroupCpComment(@PathVariable Long id, @RequestBody GroupCpCommentRequest req) {
        GroupCpCommentDTO updatedComment = groupCpCommentService.updateGroupCpComment(id, req);
        if (updatedComment != null) {
            return ResponseEntity.ok(updatedComment);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}

