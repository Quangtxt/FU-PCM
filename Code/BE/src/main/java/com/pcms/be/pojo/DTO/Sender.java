package com.pcms.be.pojo.DTO;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Sender {
    private String name;
    private String email;
}
