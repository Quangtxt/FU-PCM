package com.pcms.be.repository;

import com.pcms.be.domain.user.GroupCpComment;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface GroupCpCommentRepository extends JpaRepository<GroupCpComment, Long> {
    Optional<GroupCpComment> findByGroupId(Long groupId);
}
