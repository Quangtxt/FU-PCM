package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommitteeEDTO {
    String committeeName;
    String employeeName;
    String email;
    String mission;
    String committeeCode;
    String note;
}
