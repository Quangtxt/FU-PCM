package com.pcms.be.service.impl;

import com.pcms.be.domain.user.Supervisor;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.DTO.GoogleUserInfo;
import com.pcms.be.pojo.DTO.SupervisorDTO;
import com.pcms.be.pojo.response.SupervisorPageResponse;
import com.pcms.be.pojo.DTO.TokenDTO;
import com.pcms.be.repository.SupervisorRepository;
import com.pcms.be.repository.UserRepository;
import com.pcms.be.service.EncryptionService;
import com.pcms.be.service.JWTService;
import com.pcms.be.service.TokenService;
import com.pcms.be.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final SupervisorRepository supervisorRepository;
    private final EncryptionService encryptionService;
    private final JWTService jwtService;
    private final TokenService tokenService;
    private final ModelMapper modelMapper;

    @Autowired
    ConfigurableApplicationContext applicationContext;

    @Override
    public String checkUser(String token, String campusCode, int type) throws ServiceException {
        String url = "https://www.googleapis.com/oauth2/v3/userinfo";
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<GoogleUserInfo> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                entity,
                GoogleUserInfo.class
        );

        GoogleUserInfo userInfo = response.getBody();
        Optional<User>    optionalUser = userRepository.findByEmail(userInfo.getEmail());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            boolean isStudent = user.getRoles().stream()
                    .anyMatch(role -> role.getName().equals(Constants.RoleConstants.STUDENT));
            if (type == 0) {
                if(!isStudent) {
                    throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
                }
            } else {
                if(isStudent) {
                    throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
                }
            }
            if (user.getCampus().getCampusCode().equals(campusCode)) {
                return jwtService.generateJWT(user);
            }
        }
        throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
    }
    @Override
    @Transactional
    public User getCurrentUser() throws ServiceException {
        TokenDTO tokenDTO = tokenService.getToken();
        if (tokenDTO != null) {
            Optional<User> userOptional = userRepository.findByUsernameIgnoreCase(tokenDTO.getUserName());
            if (userOptional.isPresent()) {
                return userOptional.get();
            }
        }
        throw new ServiceException(ErrorCode.USER_NOT_FOUND);
    }
}
