package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupCpCommentStudentDTO {
    private Long id;
    private StudentDTO student;
    private Integer defenseStatus;
    private String note;
}
