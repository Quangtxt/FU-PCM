package com.pcms.be.pojo.response;

import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.Supervisor;
import com.pcms.be.pojo.DTO.GroupDTO;
import com.pcms.be.pojo.DTO.GroupSupervisorDTO;
import com.pcms.be.pojo.DTO.SupervisorDTO;
import com.pcms.be.pojo.DTO.SupervisorDTO1;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class GroupAllRespone {
    private List<GroupDTO> groupNoSupervisor;
    private List<GroupSupervisorDTO> groupHaveSupervisor;
    private List<SupervisorDTO1> supervisorDTO1List;

}
