package com.pcms.be.service;

import com.pcms.be.domain.user.GroupCpComment;
import com.pcms.be.pojo.DTO.GroupCpCommentDTO;
import com.pcms.be.pojo.request.GroupCpCommentRequest;

import java.util.Optional;

public interface GroupCpCommentService {
    GroupCpCommentDTO addGroupCpComment(GroupCpCommentRequest groupCpComment);
    GroupCpCommentDTO getGroupCpCommentByGroupId(Long groupId);
    GroupCpCommentDTO updateGroupCpComment(Long id, GroupCpCommentRequest groupCpComment);
}
