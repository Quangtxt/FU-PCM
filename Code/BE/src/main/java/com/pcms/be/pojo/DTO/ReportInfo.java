package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportInfo {
    private String name;
    private double percentScore;
    private Long milestoneId;
    private Long milestoneIdSubmit;

    public ReportInfo(String name, double percentScore, Long milestoneId, Long milestoneIdSubmit) {
        this.name = name;
        this.percentScore = percentScore;
        this.milestoneId = milestoneId;
        this.milestoneIdSubmit = milestoneIdSubmit;
    }
}
