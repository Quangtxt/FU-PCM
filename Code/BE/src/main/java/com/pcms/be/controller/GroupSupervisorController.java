package com.pcms.be.controller;


import com.pcms.be.domain.user.GroupSupervisor;
import com.pcms.be.errors.ApiException;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.request.AssignTeacherRequest;
import com.pcms.be.pojo.response.GroupAllRespone;
import com.pcms.be.pojo.response.GroupSupervisorResponse;
import com.pcms.be.pojo.response.PageGroupHaveSupervisorResponse;
import com.pcms.be.pojo.response.PageUnsupervisedGroupResponse;
import com.pcms.be.service.GroupSupervisorService;
import com.pcms.be.service.GroupService;
import com.pcms.be.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.awt.print.Pageable;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/group-supervisor")

public class GroupSupervisorController {
    private final GroupSupervisorService groupSupervisorService;

    @GetMapping("/list/invitation")
    public ResponseEntity<List<GroupSupervisorResponse>> getGroupInvitation() {
        try {
            List<GroupSupervisorResponse> groupSupervisorResponse = groupSupervisorService.getByStatus(Constants.SupervisorStatus.PENDING_SUPERVISOR);
            return ResponseEntity.ok(groupSupervisorResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/groups/pending")
    public ResponseEntity<List<GroupSupervisorResponse>> getGroupSupervisorRegisted() {
        try {
            List<GroupSupervisorResponse> groupSupervisorResponse = groupSupervisorService.getByStatus(Constants.SupervisorStatus.ACCEPT_SUPERVISOR);
            return ResponseEntity.ok(groupSupervisorResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<GroupSupervisor> putStatus(@PathVariable Long id, @RequestParam String status) {
        try {
            GroupSupervisor editGroupSupervisor = groupSupervisorService.putStatus(id, status);
            return ResponseEntity.ok(editGroupSupervisor);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PutMapping("/put/{groupId}/status")
    public ResponseEntity<List<GroupSupervisor>> changeStatusGroupSupervisorByGroupId(@PathVariable Long groupId, @RequestParam String status) {
        try {
            List<GroupSupervisor> listEdiGroupSupervisor = groupSupervisorService.changeStatusGroupSupervisorByGroupId(groupId, status);
            return ResponseEntity.ok(listEdiGroupSupervisor);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/list/{semesterId}")
    public ResponseEntity<List<GroupSupervisorResponse>> getGroupsOfSupervisor(@PathVariable int semesterId) {
        try {
            List<GroupSupervisorResponse> groupSupervisorResponse = groupSupervisorService.getGroupBySemester(semesterId);
            return ResponseEntity.ok(groupSupervisorResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/groups-of-supervisor/{supervisorId}")
    public ResponseEntity<List<GroupSupervisorResponse>> getGroupOfSupervisor(@PathVariable int supervisorId) {
        try {
            List<GroupSupervisorResponse> groupSupervisorResponse = groupSupervisorService.getGroupSupervisorByStatusAndSupervisorId(Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER, supervisorId);
            return ResponseEntity.ok(groupSupervisorResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/list-group-supervisor/by-supervisor-id/{supervisorId}")
    public ResponseEntity<List<GroupSupervisorResponse>> getGroupSupervisorBySupervisorId(@PathVariable int supervisorId) {
        try {
            List<GroupSupervisorResponse> groupSupervisorResponse = groupSupervisorService.getBySupervisorId(supervisorId);
            return ResponseEntity.ok(groupSupervisorResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/list-group-supervisor/by-two-status")
    public ResponseEntity<List<GroupSupervisorResponse>> getGroupSupervisorByTwoAcceptStatus() {
        try {
            List<GroupSupervisorResponse> groupSupervisorResponse = groupSupervisorService.getByTwoStatus(Constants.SupervisorStatus.ACCEPT_SUPERVISOR, Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
            return ResponseEntity.ok(groupSupervisorResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/all-group")
    public ResponseEntity<GroupAllRespone> getAllGroup() {
        try {
            GroupAllRespone groupAllRespone = groupSupervisorService.getAllGroup();
            return ResponseEntity.ok(groupAllRespone);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PostMapping("/assignTeacher")
    public ResponseEntity<String> assignTeacherRequest(@RequestBody List<AssignTeacherRequest> assignTeacherRequests) {
        try {
            return groupSupervisorService.assignTeacher(assignTeacherRequests);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/group-have-supervisor")
    public ResponseEntity<PageGroupHaveSupervisorResponse> getAllGroupHaveSupervisor(@Valid @RequestParam(value = "page", required = true) String page,
                                                                                     @Valid @RequestParam(value = "size", required = true) String size) {
        try {
            PageRequest pageRequest = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            return ResponseEntity.ok(groupSupervisorService.getAllGroupHaveSupervisor(pageRequest));
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }
    @GetMapping("/unsupervised-group")
    public ResponseEntity<PageUnsupervisedGroupResponse> getAllUnsupervisedGroup(@Valid @RequestParam(value = "page", required = true) String page,
                                                                                 @Valid @RequestParam(value = "size", required = true) String size) {
        try {
            PageRequest pageRequest = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            return ResponseEntity.ok(groupSupervisorService.getAllUnsupervisedGroup(pageRequest));
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

//    @GetMapping("/list-group/by-group")
//    public ResponseEntity<GroupAllRespone> getAllGroup(){
//        try {
//            GroupAllRespone groupAllRespone = groupSupervisorService.getAllGroup();
//            return ResponseEntity.ok(groupAllRespone);
//        }catch (ServiceException e){
//            throw new ApiException(e.getErrorCode(), e.getParams());
//        }
//    }
}
