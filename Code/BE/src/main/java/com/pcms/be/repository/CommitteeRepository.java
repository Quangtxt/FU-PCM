package com.pcms.be.repository;

import com.pcms.be.domain.Committee;
import com.pcms.be.domain.Semester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommitteeRepository extends JpaRepository<Committee, Long> {

    Optional<Committee> findByCommitteeCodeAndSemester(String committeeCode, Semester semester);
    Page<Committee> findAllBySemester(Pageable pageable, Semester semester);
    @Query(value = "SELECT c.* " +
            "FROM committee c " +
            "INNER JOIN committee_detail cd " +
            "WHERE c.id = cd.committee_id " +
            "AND c.semester_id = :semesterId " +
            "AND cd.employee_id = :userId", nativeQuery = true)
    List<Committee> findAllByEmployeeSemester(Long userId, Long semesterId);
}
