package com.pcms.be.functions;

public interface NotificationTemplate {
    interface UserNotification{
        String joinGroupTemplate = "You have been assigned to the group _GroupName-txt_ in the subject SEP490 by the school." +
                "Your group consists of the following members: _ListMember-txt_ in which _Leader-txt_ is the group leader.\n";
        String memberAcceptJoinGroup = "_User-txt_ đã tham gia vào nhóm của bạn.";
        String memberOutGroup = "_User-txt_ đã rời khỏi nhóm của bạn.";
        String memberRejectJoinGroup = "_User-txt_ has left your group.";
        String removeMember = "_User-txt_ has left your group.";
        String removeMember1 = "Group leader _Leader-txt_ has removed you from the group.";
        String inviteMemberJoinGroup = "_Leader-txt_ has invited you to join their group _Group-txt_.";


    }

    interface NotificationType {
        String GROUP = "GROUP";
        String REQUESTGROUP = "REQUESTGROUP";
        String NEWS = "NEWS";
        String REQUEST_CHECK_MILESTONE = "REQUEST_CHECK_MILESTONE";
        String RECHECKMILESTONE = "RECHECKMILESTONE";
        String RESPONSECHECKMILESTONE = "RESPONSECHECKMILESTONE";
        String MEETING = "MEETING";
    }
    interface MeetingNotification{
        String createMeetingTemplate = "You and your team have a meeting with supervisor: _SupervisorName-txt_ at _Time-txt_. \n" +
                "Location: _Location-txt_";
        String updateMeetingTemplate = "The meeting with your supervisor: _SupervisorName-txt_ and the group at _OldTime-txt_ has been updated.\n" +
                "Meeting Schedule: _Time-txt\n"+
                "Location: _Location-txt_";
        String deleteMeetingTemplate = "The meeting with your supervisor and team at _Time-txt_ has been canceled.";
    }
    interface MilestoneProcessNotification{
        String cannotFindInGit = "Hệ thống không thể tìm thấy folder của Milestone: _MilestoneName-txt_ trong Gitlab của dự án nhóm các bạn.  ";
        String reCheckSubmission = "Group _GroupName-txt_ requests to recheck the progress of Milestone _MilestoneName-txt_.";
        String reDoSubmission = "Instructor asks group to redo Milestone _MilestoneName-txt_.";
        String passSubmission = "Milestone Review Request Milestone Name-txt has been reviewed and updated by the instructor.";
        String reqCheckSubmission = "Group _GroupName-txt_ requests to check the progress of Milestone _MilestoneName-txt_.";
    }
}
