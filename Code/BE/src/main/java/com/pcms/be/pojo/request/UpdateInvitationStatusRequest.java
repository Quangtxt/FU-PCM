package com.pcms.be.pojo.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateInvitationStatusRequest {
    int groupId;
    String status;
    int studentId;
}
