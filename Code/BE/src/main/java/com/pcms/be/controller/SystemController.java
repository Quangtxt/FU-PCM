package com.pcms.be.controller;

import com.pcms.be.configuration.ScheduleConfig;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.service.GitService;
import com.pcms.be.service.MilestoneService;
import jakarta.annotation.PostConstruct;
import jakarta.mail.MessagingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.OffsetDateTime;

//@RequiredArgsConstructor
@Slf4j
@Component
public class SystemController {
    @Autowired
    private  ScheduleConfig scheduleConfig;
    @Autowired
    private MilestoneService milestoneService;

    @Autowired
    private GitService gitService;

    @PostConstruct
    public void setCronForSchedule() throws ServiceException{
        log.info("setCronForSchedule");
        gitService.setCronForSchedule();
        gitService.setCronForCheckingProcessCommit();
        gitService.setCronToReminder();
        log.info( "@scheduleConfig.getUpdateStatusMilestoneCron(): "+scheduleConfig.getUpdateStatusMilestoneCron());
        log.info( "@scheduleConfig.getCheckingTotalCommitCron(): "+scheduleConfig.getCheckingTotalCommitCron());
        log.info( "@scheduleConfig.getReminderToSubmitReportCron(): "+scheduleConfig.getReminderToSubmitReportCron());

    }

    @Scheduled(cron = "#{@scheduleConfig.getUpdateStatusMilestoneCron()}")
    public void UpdateStatusMilestone() throws ServiceException {
        gitService.updateStatusMilestone();
        log.info( "@scheduleConfig.getUpdateStatusMilestoneCron(): "+scheduleConfig.getUpdateStatusMilestoneCron());
    }


    @Scheduled(cron = "#{@scheduleConfig.getCheckingTotalCommitCron()}")
    public void checkingCommitProcess() throws ServiceException, IOException {
        gitService.checkCommit();
        log.info( "@scheduleConfig.getCheckingTotalCommitCron(): "+scheduleConfig.getCheckingTotalCommitCron());

    }

    @Scheduled(cron = "#{@scheduleConfig.getReminderToSubmitReportCron()}")
    public void reminderToSubmitReport() throws ServiceException {
        gitService.reminderToSubmitReport();
    }

    private String getCronExpressionFromOffsetDateTime(OffsetDateTime offsetDateTime) {
        // Lấy các thành phần của OffsetDateTime
        int second = offsetDateTime.getSecond();
        int minute = offsetDateTime.getMinute();
        int hour = offsetDateTime.getHour();
        int dayOfMonth = offsetDateTime.getDayOfMonth();
        int month = offsetDateTime.getMonthValue();

        // Tạo cron expression
        // Cấu trúc cron expression: "seconds minutes hours dayOfMonth month *"
        return String.format("%d %d %d %d %d *", second, minute, hour, dayOfMonth, month);
    }

}
