package com.pcms.be.pojo.response;

import com.pcms.be.pojo.DTO.GroupDTO;
import com.pcms.be.pojo.DTO.GroupSupervisorDTO;
import com.pcms.be.pojo.DTO.SupervisorDTO1;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PageUnsupervisedGroupResponse {
    private Long totalCount;
    private Integer totalPage;
    private List<GroupDTO> data;
    private List<SupervisorDTO1> supervisorDTO1List;
}
