package com.pcms.be.pojo.DTO;

import com.pcms.be.domain.user.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommitteeDetailDTO {
    Long id;
    UserDTO employee;
    String mission;
}
