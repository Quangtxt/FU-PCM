package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExcelCommitteeDTO {
    List<CommitteeEDTO> committees;
    List<CommitteeAssignmentEDTO> committeeAssignments;
}
