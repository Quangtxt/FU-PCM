package com.pcms.be.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pcms.be.configuration.ScheduleConfig;
import com.pcms.be.domain.*;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.Member;
import com.pcms.be.domain.user.Student;
import com.pcms.be.domain.user.User;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Git;
import com.pcms.be.functions.MailTemplate;
import com.pcms.be.functions.NotificationTemplate;
import com.pcms.be.pojo.DTO.CommitProfile;
import com.pcms.be.pojo.DTO.GitFolder;
import com.pcms.be.pojo.DTO.Recipient;
import com.pcms.be.pojo.request.SendEmailRequest;
import com.pcms.be.repository.*;
import com.pcms.be.service.Email.EmailService;
import com.pcms.be.service.impl.MilestoneServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GitService {
    
    private final GroupRepository groupRepository;
    private final EmailService emailService;
    private final SemesterRepository semesterRepository;
    private final ScheduleConfig scheduleConfig;
    private final MilestoneRepository milestoneRepository;
    private final SemesterMilestoneRepository semesterMilestoneRepository;
    private final MilestoneServiceImpl milestoneService;
    private final ReportRepository reportRepository;
    private final MilestoneGroupRepository milestoneGroupRepository;
    private final NotificationService notificationService;
    private final MemberRepository memberRepository;
    private final StudentRepository studentRepository;
    private final UserRepository userRepository;
    private final List<String> milestoneNeedToCheckCommit = List.of("Solution", "Implementation","Verification");

    public void setCronForSchedule() throws ServiceException {
//        Optional<Semester_Milestone> milestone = semesterMilestoneRepository.findLatestSemesterMilestone();
//        milestone.ifPresent(value -> scheduleConfig.setUpdateStatusMilestoneCron(getCronExpressionFromOffsetDateTime(value.getEndDate())));

        OffsetDateTime now = OffsetDateTime.now();
        Optional<Semester> optSemester = semesterRepository.findByCurrent(now);
        if (optSemester.isPresent()) {
            List<Report> reports = reportRepository.findAllBySemesterId(Integer.parseInt(optSemester.get().getId().toString()));
            reports.sort(Comparator.comparing(Report::getPossibleDate));
            for (Report r : reports) {
                if (!now.isAfter(r.getPossibleDate())) {
                    scheduleConfig.setUpdateStatusMilestoneCron(getCronExpressionFromOffsetDateTime(r.getPossibleDate()));
                    break;
                }
            }
        }
    }
    public void updateStatusMilestone() throws ServiceException {
        OffsetDateTime now = OffsetDateTime.now();
        Optional<Semester> optSemester = semesterRepository.findByCurrent(now);
        if (optSemester.isEmpty()) {
            throw new ServiceException(ErrorCode.SEMESTER_NOT_FOUND_BY_CURRENT);
        }
        OffsetDateTime cron = getOffsetDateTimeFromCronExpression(scheduleConfig.getUpdateStatusMilestoneCron());
        List<Report> reports = reportRepository.findByPossibleDate(cron.toLocalDateTime());
        List<Group> groups = groupRepository.findBySemester(Integer.parseInt(optSemester.get().getId().toString()));
        for (Report r : reports) {
            Milestone milestone = r.getMilestone();
            String path = "";
            while (milestone.getParent() != null) {
                milestone = milestoneRepository.findById(milestone.getParent()).orElseThrow();
                path = "/" + milestone.getName();
            }
            path = path .substring(1);
            for (Group gr : groups) {
                String href = Git.GitSrc.repositorySubTree.replace("_projectId-txt_", gr.getGitId()).replace("_SubtreeName-txt_", path);
                List<GitFolder> gitFolder = getGitFolderByCallApiToGit(href);
                Optional<MilestoneGroup> milestoneGroup = milestoneGroupRepository.findByGroupIdAndMilestoneId(Integer.parseInt(gr.getId().toString()), Integer.parseInt(r.getMilestone().getId().toString()));
                if (milestoneGroup.isEmpty()) {
                    continue;
                }
                if (gitFolder == null || gitFolder.isEmpty()) {
                    SendEmailRequest sendEmailRequest = new SendEmailRequest();
                    List<Recipient> recipients = new ArrayList<>();
                    for (Member m : gr.getMembers()) {
                        Map<String, String> map = new HashMap<>();
                        map.put("_MilestoneName-txt_", milestone.getName());
                        String content = notificationService.createContentNotification(NotificationTemplate.MilestoneProcessNotification.cannotFindInGit, map);
                        notificationService.saveNotification(m.getStudent().getUser(), content);
                        Recipient recipient = new Recipient();
                        recipient.setEmail(m.getStudent().getUser().getEmail());
                        recipients.add(recipient);
                    }
                    sendEmailRequest.setTo(recipients);
                    sendEmailRequest.setSubject(MailTemplate.MilestoneUnfinished.subject);
                    sendEmailRequest.setHtmlContent(MailTemplate.MilestoneUnfinished.htmlContent.replace("_MilestoneName-txt_", r.getMilestone().getProduct()).replace("_GroupName-txt_", gr.getName()));
                    emailService.sendEmail(sendEmailRequest);
                    milestoneGroup.get().setStudentRequestGrading(false);
                    milestoneGroupRepository.save(milestoneGroup.get());
                } else {
                    milestoneGroup.get().setStudentRequestGrading(checkSubmission(gitFolder, r.getMilestone()));
                    milestoneGroupRepository.save(milestoneGroup.get());
                }
            }
        }


    }
    private boolean checkSubmission(List<GitFolder> gitFolders, Milestone milestone) {
        String strCheck = sanitizeFileName(milestone.getProduct()).toLowerCase();
        for (GitFolder g : gitFolders) {
            if (g.getName().toLowerCase().contains(strCheck) && g.getType().equals("blob")) {
                return true;
            }
        }
        return false;
    }
    public static String sanitizeFileName(String fileName) {
        // Thay thế khoảng trắng bằng dấu gạch dưới
        fileName = fileName.replace(" ", "_");

        // Chuyển đổi chữ hoa sang chữ thường
        fileName = fileName.toLowerCase();


        // Loại bỏ các ký tự đặc biệt
        fileName = fileName.replaceAll("[/\\\\:*?\"<>|]", "_");

        // Thay thế các ký tự không được hỗ trợ bằng dấu gạch dưới
        fileName = fileName.replaceAll("[^a-zA-Z0-9_.-]", "_");

        return fileName;
    }

    public void setCronForCheckingProcessCommit() throws ServiceException{
        OffsetDateTime now = OffsetDateTime.now();

        Optional<Semester> semester = semesterRepository.findByCurrent(now);
        if (semester.isEmpty()){
            return;
        }
        List<Semester_Milestone> semesterMilestones = getListSemesterMilestoneToCheckGit();
        if (semesterMilestones == null || semesterMilestones.isEmpty()){
            return;
        }else{
            String cron = "";
            for (Semester_Milestone semesterMilestone : semesterMilestones){
                if (now.isBefore(semesterMilestone.getStartDate())){
//                    OffsetDateTime newDateTime = semesterMilestone.getStartDate()
//                            .with(TemporalAdjusters.nextOrSame(DayOfWeek.TUESDAY))
//                            .withHour(4)
//                            .withMinute(12)
//                            .withSecond(0)
//                            .withNano(0);
                    OffsetDateTime newDateTime = semesterMilestone.getStartDate()
                            .with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY))
                            .withHour(23)
                            .withMinute(55)
                            .withSecond(0)
                            .withNano(0);
                    cron = getCronExpressionFromOffsetDateTime(newDateTime);
                }
            }
            if (cron.equalsIgnoreCase("")){
                return;
            }
            scheduleConfig.setCheckingTotalCommitCron(cron);
        }

    }
    public void Test(){
        OffsetDateTime now = OffsetDateTime.now();
        Optional<Semester> semester = semesterRepository.findByCurrent(OffsetDateTime.now());
        if (semester.isEmpty()){
            return;
        }
        List<Report> reports = reportRepository.findAllBySemesterId(Integer.parseInt(semester.get().getId().toString()));
        reports.sort(Comparator.comparing(Report::getPossibleDate));
        for (Report r : reports){
            if (now.isBefore(r.getPossibleDate())){
                OffsetDateTime ofCron = r.getPossibleDate().minusDays(3);
                scheduleConfig.setReminderToSubmitReportCron(getCronExpressionFromOffsetDateTime(ofCron));
            }
        }
    }
//    public Milestone getByCh

    public void setCronToReminder() throws ServiceException {
        OffsetDateTime now = OffsetDateTime.now();
        Optional<Semester> semester = semesterRepository.findByCurrent(OffsetDateTime.now());
        if (semester.isEmpty()){
            return;
        }
        List<Report> reports = reportRepository.findAllBySemesterId(Integer.parseInt(semester.get().getId().toString()));
        reports.sort(Comparator.comparing(Report::getPossibleDate));
        for (Report r : reports){
            if (now.isBefore(r.getPossibleDate())){
                OffsetDateTime ofCron = r.getPossibleDate().minusDays(3);
                scheduleConfig.setReminderToSubmitReportCron(getCronExpressionFromOffsetDateTime(ofCron));
            }
        }
    }
    public List<Semester_Milestone> getListSemesterMilestoneToCheckGit(){
        Semester semester = semesterRepository.findByCurrent(OffsetDateTime.now()).orElseThrow();
        List<Semester_Milestone> semesterMilestones = new ArrayList<>();
        List<Semester_Milestone> sm = new ArrayList<>();
        for(String str : milestoneNeedToCheckCommit){
            Optional<Milestone> milestone = milestoneRepository.findByName(str);
            if (milestone.isEmpty()){
                break;
            }
            Optional<Semester_Milestone> semesterMilestone = semesterMilestoneRepository.findBySemesterIdAndMilestoneId(Integer.parseInt(semester.getId().toString()), Integer.parseInt(milestone.get().getId().toString()));
            if (semesterMilestone.isEmpty()){
                break;
            }
            semesterMilestones.add(semesterMilestone.orElseThrow());
        }
        semesterMilestones.sort(Comparator.comparing(Semester_Milestone::getEndDate));

        return semesterMilestones;
    }

    public void checkCommit() throws ServiceException, IOException {
        Optional<Semester> semester = semesterRepository.findByCurrent(OffsetDateTime.now());
        if (semester.isEmpty()){
            return;
        }
        List<Group> groups = groupRepository.findBySemester(Integer.parseInt(semester.get().getId().toString()));
        List<Semester_Milestone> semesterMilestones = getListSemesterMilestoneToCheckGit();
        OffsetDateTime now =OffsetDateTime.now();

        // Lấy ngày hôm nay
        LocalDate today = LocalDate.now();

        // Xác định ngày đầu tuần hiện tại
        LocalDate startOfCurrentWeek = today.with(DayOfWeek.MONDAY);

        // Xác định ngày cuối tuần hiện tại
        LocalDate endOfCurrentWeek = today.with(DayOfWeek.SUNDAY);

        // Xác định ngày đầu tuần trước
        LocalDate startOfPreviousWeek = startOfCurrentWeek.minusWeeks(1);

        // Xác định ngày cuối tuần trước
        LocalDate endOfPreviousWeek = endOfCurrentWeek.minusWeeks(1);
//
//        if (now.isAfter(semesterMilestones.get(semesterMilestones.size()-1).getEndDate())){
//            return;
//        }

        // Định dạng hiển thị
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (Group gr : groups){
            if (gr.getId() == null){
                break;
            }
            String hrefGitCurrentWeek = Git.GitSrc.repositoryCommit.replace("_projectId-txt_", gr.getGitId()).replace("_Since-txt_", startOfCurrentWeek.format(formatter)).replace("_Until-txt_", endOfCurrentWeek.format(formatter));
            String hrefGitPreviousWeek = Git.GitSrc.repositoryCommit.replace("_projectId-txt_", gr.getGitId()).replace("_Since-txt_", startOfPreviousWeek.format(formatter)).replace("_Until-txt_", endOfPreviousWeek.format(formatter));
            int totalCommitOfCurrentWeek = totalCommitPerWeek(hrefGitCurrentWeek);
            int totalCommitOfPreviousWeek = totalCommitPerWeek(hrefGitPreviousWeek);
            SendEmailRequest sendEmailRequest = new SendEmailRequest();
            List<Recipient> recipients = new ArrayList<>();
            List<Member> members = memberRepository.findAllByGroupId(Integer.parseInt(gr.getId().toString()));
            for (Member member : members){
                Recipient recipient = new Recipient();
                Optional<Student> student = studentRepository.findById(member.getStudent().getId());
                if (student.isEmpty()){
                    return;
                }
                Optional<User> user = userRepository.findByStudentId(Integer.parseInt(student.get().getId().toString()));
                if (user.isEmpty()){
                    return;
                }
                recipient.setEmail(user.get().getEmail());
                recipients.add(recipient);
            }
            sendEmailRequest.setTo(recipients);
            if (totalCommitOfCurrentWeek == 0 && totalCommitOfPreviousWeek == 0){
                break;
            } else if (totalCommitOfCurrentWeek < totalCommitOfPreviousWeek){
                sendEmailRequest.setSubject(MailTemplate.TotalCommitsDecreased.subject);
                sendEmailRequest.setHtmlContent(MailTemplate.TotalCommitsDecreased.htmlContent.replace("_GroupName-txt_", gr.getName()));
            }else{
                sendEmailRequest.setSubject(MailTemplate.TotalCommitsIncreased.subject);
                sendEmailRequest.setHtmlContent(MailTemplate.TotalCommitsIncreased.htmlContent.replace("_GroupName-txt_", gr.getName()));
            }
            emailService.sendEmail(sendEmailRequest);
        }
        OffsetDateTime currentCron = getOffsetDateTimeFromCronExpression(scheduleConfig.getCheckingTotalCommitCron());
        OffsetDateTime newCron = currentCron.plusDays(7);
        newCron = newCron.withHour(0).withMinute(0).withSecond(0).withNano(0);
        scheduleConfig.setCheckingTotalCommitCron(getCronExpressionFromOffsetDateTime(newCron));
    }
    public void reminderToSubmitReport() throws ServiceException {
        if (scheduleConfig.getReminderToSubmitReportCron().equalsIgnoreCase("0 0 * * * ?")){
            return;
        }
        OffsetDateTime cron = getOffsetDateTimeFromCronExpression(scheduleConfig.getReminderToSubmitReportCron());
        cron = cron.plusDays(3);
        List<Report> reports = reportRepository.findByPossibleDate(cron.toLocalDateTime());
        Semester semester = semesterRepository.findByCurrent(OffsetDateTime.now()).orElseThrow();
        for (Report report : reports){
            List<Group> groups = groupRepository.findBySemester(Integer.parseInt(semester.getId().toString()));
            Milestone milestone = report.getMilestone();
            String path = "";
            while (milestone.getParent() != null) {
                milestone = milestoneRepository.findById(milestone.getParent()).orElseThrow();
                path = milestone.getName() + "/" +path;
            }
            String realPath = path.substring(0, path.length()-1);
            for (Group group : groups){

                String href = Git.GitSrc.repositorySubTree.replace("_projectId-txt_", group.getGitId()).replace("_SubtreeName-txt_", realPath);
                List<GitFolder> gitFolder = getGitFolderByCallApiToGit(href);
                Optional<MilestoneGroup> milestoneGroup = milestoneGroupRepository.findByGroupIdAndMilestoneId(Integer.parseInt(group.getId().toString()), Integer.parseInt(report.getMilestone().getId().toString()));
                if (milestoneGroup.isEmpty()) {
                    continue;
                }
                if (gitFolder == null || gitFolder.isEmpty()) {
                    SendEmailRequest sendEmailRequest = new SendEmailRequest();
                    List<Recipient> recipients = new ArrayList<>();
                    for (Member m : group.getMembers()) {
                        Map<String, String> map = new HashMap<>();
                        map.put("_MilestoneName-txt_", milestone.getName());
                        String content = notificationService.createContentNotification(NotificationTemplate.MilestoneProcessNotification.cannotFindInGit, map);
                        notificationService.saveNotification(m.getStudent().getUser(), content);
                        Recipient recipient = new Recipient();
                        recipient.setEmail(m.getStudent().getUser().getEmail());
                        recipients.add(recipient);
                    }
                    sendEmailRequest.setTo(recipients);
                    sendEmailRequest.setSubject(MailTemplate.MilestoneUnfinished.subject);
                    sendEmailRequest.setHtmlContent(MailTemplate.MilestoneUnfinished.htmlContent.replace("_MilestoneName-txt_", report.getMilestone().getProduct()).replace("_GroupName-txt_", group.getName()));
                    emailService.sendEmail(sendEmailRequest);
                    milestoneGroup.get().setStudentRequestGrading(false);
                    milestoneGroupRepository.save(milestoneGroup.get());
                }
            }

        }
    }

    public int totalCommitPerWeek(String hrefGit) throws ServiceException, IOException {
        int result = 0;
        boolean flag = true;
        int page = 1;
        while (flag){
            String href = new String(hrefGit);
            href = href.replace("_Page-txt_", String.valueOf(page));
            List<CommitProfile> commitProfiles = getCommitProfileByCallApiToGit(href);
            if (commitProfiles != null && !commitProfiles.isEmpty()){
                result += commitProfiles.size();
            }else{
                flag = false;
            }
            page++;
        }
        return result;
    }
    public static List<CommitProfile> getCommitProfileByCallApiToGit(String hrefGit) throws ServiceException {
        try {
            RestTemplate restTemplate = new RestTemplate();

            // Thiết lập header
            HttpHeaders headers = new HttpHeaders();
            // Tạo httpEntity
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(headers);
            ParameterizedTypeReference<List<CommitProfile>> typeRef = new ParameterizedTypeReference<List<CommitProfile>>() {
            };
            ResponseEntity<List<CommitProfile>> response = restTemplate.exchange(
                    hrefGit,
                    HttpMethod.GET,
                    entity,
                    typeRef
            );
            return response.getBody();
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            // Xử lý trường hợp API không tồn tại hoặc trả về lỗi
//            throw new ServiceException(ErrorCode.GITLAB_IS_INCORRECT_FORMAT);
            return null;
        } catch (Exception e) {
            // Xử lý các ngoại lệ khác
//            throw new ServiceException(ErrorCode.GITLAB_IS_INCORRECT_FORMAT);
            return null;
        }
    }
    public static List<GitFolder> getGitFolderByCallApiToGit(String hrefGit) throws ServiceException {
        try {
            RestTemplate restTemplate = new RestTemplate();

            // Thiết lập header
            HttpHeaders headers = new HttpHeaders();
            // Tạo httpEntity
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(headers);
            ParameterizedTypeReference<List<GitFolder>> typeRef = new ParameterizedTypeReference<List<GitFolder>>() {
            };
            ResponseEntity<List<GitFolder>> response = restTemplate.exchange(
                    hrefGit,
                    HttpMethod.GET,
                    entity,
                    typeRef
            );
            return response.getBody();
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }
    public String getCronExpressionFromOffsetDateTime(OffsetDateTime offsetDateTime) {
        // Lấy các thành phần của OffsetDateTime
        int second = offsetDateTime.getSecond();
        int minute = offsetDateTime.getMinute();
        int hour = offsetDateTime.getHour();
        int dayOfMonth = offsetDateTime.getDayOfMonth();
        int month = offsetDateTime.getMonthValue();

        // Tạo cron expression
        // Cấu trúc cron expression: "seconds minutes hours dayOfMonth month *"
        return String.format("%d %d %d %d %d *", second, minute, hour, dayOfMonth, month);
    }

    public OffsetDateTime getOffsetDateTimeFromCronExpression(String cronExpression) {
        String[] parts = cronExpression.split(" ");
        if (parts.length != 6) {
            throw new IllegalArgumentException("Invalid cron expression format");
        }

        int second = Integer.parseInt(parts[0]);
        int minute = Integer.parseInt(parts[1]);
        int hour = Integer.parseInt(parts[2]);
        int dayOfMonth = Integer.parseInt(parts[3]);
        int month = Integer.parseInt(parts[4]);
        int year = LocalDateTime.now().getYear(); // Assuming the current year

        LocalDateTime localDateTime = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
        return localDateTime.atOffset(ZoneOffset.UTC);
    }




}
