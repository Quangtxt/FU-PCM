package com.pcms.be.pojo.DTO;

import com.pcms.be.domain.user.GroupSubmission;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class SubmissionFileDTO {
    private Long id;
    private String filename;
    private String filePath;
    private String fileType;
    private String submissionType;
    private Long fileSize;
    private LocalDateTime uploadDate;
}
