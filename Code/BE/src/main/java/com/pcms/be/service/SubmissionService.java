package com.pcms.be.service;

import com.pcms.be.domain.user.GroupSubmission;
import com.pcms.be.domain.user.SubmissionFile;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.DTO.SubmissionDTO;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

public interface SubmissionService {

    List<SubmissionFile> uploadFiles(List<MultipartFile> files, Long groupId,String type) throws IOException;
    public void deleteFile(Long submissionFileId) throws IOException, ServiceException;
    SubmissionDTO getSubmissionByGroup(Long groupId) throws IOException;
    void confirmGroupSubmission(Long groupId,String status) throws IOException;
    List<SubmissionDTO> getGroupSubmissionBySemester(Long semesterId) throws IOException, ServiceException;

    List<SubmissionDTO> getGroupSubmissionFinal(int semesterId) throws ServiceException;

    List<SubmissionDTO> setAllForGroupTimeSubmit(int semesterId, OffsetDateTime startSubmit,OffsetDateTime endSubmit) throws ServiceException;

    SubmissionDTO setForGroupTimeSubmit( OffsetDateTime startSubmit, OffsetDateTime endSubmit, int groupId) throws  ServiceException;

    List<SubmissionDTO> setAllForGroupTimeApprove(int semesterId, OffsetDateTime startApprove, OffsetDateTime endApprove) throws ServiceException;
}
