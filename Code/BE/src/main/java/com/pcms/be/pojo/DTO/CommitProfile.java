package com.pcms.be.pojo.DTO;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommitProfile {
    private String id;
    private String shortId;
    private String createdAt;
    private List<String> parentIds;
    private String title;
    private String message;
    private String authorName;
    private String authorEmail;
    private String authoredDate;
    private String committerName;
    private String committerEmail;
    private String committedDate;
    private JsonObject trailers;
    private JsonObject extendedTrailers;
    private String webUrl;
}
