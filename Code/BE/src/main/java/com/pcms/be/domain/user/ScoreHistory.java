package com.pcms.be.domain.user;

import java.io.Serializable;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import java.time.OffsetDateTime;
@Getter
@Setter
@Entity
@Table(name = "v_score_history")
public class ScoreHistory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "score_id", nullable = false)
    private Score score;

    @Column(name = "old_score")
    private Double oldScore;

    @Column(name = "new_score")
    private Double newScore;

    @Column(name = "reason")
    private String reason;

    @CreationTimestamp
    @Column(name = "created_at")
    private OffsetDateTime createdAt;
}