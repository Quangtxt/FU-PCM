package com.pcms.be.service.Email;

import com.pcms.be.pojo.request.EmailRequest;
import com.pcms.be.pojo.response.EmailResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "email-client", url = "https://api.brevo.com")
public interface EmailClient {
    @PostMapping(value = "/v3/smtp/email", produces = "application/json")
    ResponseEntity<EmailResponse> sendEmail(@RequestHeader("api-key") String apiKey, @RequestBody EmailRequest body);
}
