package com.pcms.be.repository;

import com.pcms.be.domain.user.Score;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ScoreRepository extends JpaRepository<Score, Long> {
    @Query(value = "SELECT s.*\n" +
            "FROM v_scores s\n" +
            "INNER JOIN v_members m ON s.member_id = m.id\n" +
            "INNER JOIN report r ON s.report_id = r.id\n" +
            "WHERE m.group_id = :groupId\n" +
            "  AND m.status = 'INGROUP'\n" +
            "  AND r.semester_id = :semesterId\n" +
            "  AND r.milestone_id_submit = :milestoneId", nativeQuery = true)
    List<Score> getScoreByGroupIdAndMileStoneIdAndSemesterId(int groupId,int milestoneId,int semesterId);
    @Query(value = "SELECT s.*\n" +
            "FROM v_scores s\n" +
            "INNER JOIN v_members m ON s.member_id = m.id\n" +
            "WHERE m.group_id = :groupId\n" +
            "  AND m.status = 'INGROUP'", nativeQuery = true)
    List<Score> getScoreByGroupId(int groupId);

    Optional<Score> findByMemberIdAndReportId(Long memberId, Long reportId);
}
