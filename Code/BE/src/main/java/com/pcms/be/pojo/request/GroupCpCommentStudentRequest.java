package com.pcms.be.pojo.request;

import com.pcms.be.pojo.DTO.StudentDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupCpCommentStudentRequest {
    private Long studentId;
    private Integer defenseStatus;
    private String note;
}
