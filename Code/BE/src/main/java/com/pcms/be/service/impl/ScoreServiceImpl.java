package com.pcms.be.service.impl;

import com.pcms.be.domain.user.Score;
import com.pcms.be.domain.user.ScoreHistory;
import com.pcms.be.pojo.DTO.ScoreDTO;
import com.pcms.be.pojo.request.ScoreEvaluateRequest;
import com.pcms.be.repository.*;
import com.pcms.be.service.ScoreService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ScoreServiceImpl implements ScoreService {
    private final ScoreRepository scoreRepository;
    private final ScoreHistoryRepository scoreHistoryRepository;
    private final MemberRepository memberRepository;
    private final ReportRepository reportRepository;
    private final SemesterRepository semesterRepository;
    private final ModelMapper modelMapper;
    @Override
    public List<ScoreDTO> getScore(int groupId, int milestoneId,int semesterId) {
        List<Score> scores = scoreRepository.getScoreByGroupIdAndMileStoneIdAndSemesterId(groupId,milestoneId,semesterId);
        List<ScoreDTO> scoreDTOS = scores.stream()
                .map(s -> modelMapper.map(s, ScoreDTO.class))
                .collect(Collectors.toList());
        return scoreDTOS;
    }

    @Override
    public List<ScoreDTO> getScoreByGroupId(int groupId) {
        List<Score> scores = scoreRepository.getScoreByGroupId(groupId);
        List<ScoreDTO> scoreDTOS = scores.stream()
                .map(s -> modelMapper.map(s, ScoreDTO.class))
                .collect(Collectors.toList());
        return scoreDTOS;
    }

    @Transactional
    @Override
    public void saveEvaluation(List<ScoreEvaluateRequest> scoreEvaluateRequests) {
        for (ScoreEvaluateRequest request : scoreEvaluateRequests) {
            Optional<Score> scoreOptional = scoreRepository.findByMemberIdAndReportId(request.getMemberId(), request.getReportId());
            if (scoreOptional.isPresent()) {
                Score score = scoreOptional.get();
                Double oldScore = score.getScore();
                if (!Objects.equals(oldScore, request.getScore())) {
                    score.setScore(request.getScore());
                    score.setComment(request.getComment());
                    scoreRepository.save(score);

                    ScoreHistory scoreHistory = new ScoreHistory();
                    scoreHistory.setScore(score);
                    scoreHistory.setOldScore(oldScore);
                    scoreHistory.setNewScore(request.getScore());
                    scoreHistory.setReason(request.getReason());
                    scoreHistoryRepository.save(scoreHistory);
                }
            } else {
                Score newScore = new Score();
                newScore.setMember(memberRepository.findById(request.getMemberId()).orElseThrow());
                newScore.setReport(reportRepository.findById(request.getReportId()).orElseThrow());
                newScore.setScore(request.getScore());
                newScore.setComment(request.getComment());
                scoreRepository.save(newScore);
            }
        }
    }
}
