package com.pcms.be.pojo.response;

import com.pcms.be.pojo.DTO.NotificationDTO;
import com.pcms.be.pojo.DTO.UserDTO;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserNotificationResponse {
    private Long id;
    private UserDTO user;
    private Boolean status;
    private NotificationDTO notification;
}
