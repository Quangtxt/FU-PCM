package com.pcms.be.pojo.DTO;

import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.GroupCpCommentStudent;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GroupCpCommentDTO {
    private Long id;
    private GroupDTO group;
    private String contentEvaluation;
    private String formatEvaluation;
    private String attitudeEvaluation;
    private String achievementLevel;
    private String limitation;
    private List<GroupCpCommentStudentDTO> groupCpCommentStudents;
}
