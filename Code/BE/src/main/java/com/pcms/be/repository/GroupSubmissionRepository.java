package com.pcms.be.repository;

import com.pcms.be.domain.user.GroupSubmission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupSubmissionRepository extends JpaRepository<GroupSubmission, Long> {
    Optional<GroupSubmission> findByGroupId(Long groupId);
    @Query(value = "SELECT * FROM v_group_submission where group_id in " +
            "(SELECT gs.group_id FROM v_group_supervisor gs " +
            "INNER JOIN v_group g ON gs.group_id = g.id " +
            "WHERE g.semester_id = :semesterId AND gs.supervisor_id = :supervisorId AND gs.status = 'ACCEPT_SUPERVISOR_LEADER')",
            nativeQuery = true)
    List<GroupSubmission> findAllBySupervisorIdAndSemesterId(Long supervisorId, Long semesterId);

    @Query(value = "SELECT * FROM v_group_submission gs WHERE gs.group_id IN (SELECT g.id FROM v_group g WHERE g.semester_id = :semesterId)", nativeQuery = true)
    List<GroupSubmission> findAllBySemesterId(int semesterId);
}
