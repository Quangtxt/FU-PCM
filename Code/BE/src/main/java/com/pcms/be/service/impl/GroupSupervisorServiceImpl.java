package com.pcms.be.service.impl;

import com.pcms.be.pojo.response.*;
import org.springframework.data.domain.Page;
import com.pcms.be.domain.user.*;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.DTO.GroupDTO;
import com.pcms.be.pojo.DTO.GroupSupervisorDTO;
import com.pcms.be.pojo.DTO.SupervisorDTO;
import com.pcms.be.pojo.DTO.SupervisorDTO1;
import com.pcms.be.pojo.request.AssignTeacherRequest;
import com.pcms.be.repository.GroupRepository;
import com.pcms.be.repository.GroupSupervisorRepository;
import com.pcms.be.repository.SupervisorRepository;
import com.pcms.be.repository.UserRepository;
import com.pcms.be.service.GroupSupervisorService;
import com.pcms.be.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.internal.bytebuddy.implementation.bind.annotation.Super;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class GroupSupervisorServiceImpl implements GroupSupervisorService {
    private final GroupSupervisorRepository groupSupervisorRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final GroupRepository groupRepository;
    private final SupervisorRepository supervisorRepository;

    @Override
    public List<GroupSupervisorResponse> getByStatus(String status) throws ServiceException {
        if (status.equals(Constants.SupervisorStatus.PENDING_SUPERVISOR)) {
            User currentUser = userService.getCurrentUser();
            List<GroupSupervisor> listInvitation = groupSupervisorRepository.findBySupervisorAndStatus(currentUser.getSupervisor(), status);
            List<GroupSupervisorResponse> groupSupervisorResponse = new ArrayList<>();
            for (GroupSupervisor groupSupervisor : listInvitation
            ) {
                groupSupervisorResponse.add(modelMapper.map(groupSupervisor, GroupSupervisorResponse.class));
            }
            return groupSupervisorResponse;
        } else if (status.equals(Constants.SupervisorStatus.PENDING_LEADER_TEACHER)) {
            List<GroupSupervisor> listInvitation = groupSupervisorRepository.findAllByStatus(status);
            List<GroupSupervisorResponse> groupSupervisorResponse = new ArrayList<>();
            for (GroupSupervisor groupSupervisor : listInvitation
            ) {
                groupSupervisorResponse.add(modelMapper.map(groupSupervisor, GroupSupervisorResponse.class));
            }
            return groupSupervisorResponse;
        } else if (status.equals(Constants.SupervisorStatus.ACCEPT_SUPERVISOR)) {
            List<GroupSupervisor> listInvitation = groupSupervisorRepository.findAllByStatus(status);
            List<GroupSupervisorResponse> groupSupervisorResponse = new ArrayList<>();
            for (GroupSupervisor groupSupervisor : listInvitation
            ) {
                groupSupervisorResponse.add(modelMapper.map(groupSupervisor, GroupSupervisorResponse.class));
            }
            return groupSupervisorResponse;
        }
        return null;
    }

    @Override
    public GroupSupervisor putStatus(Long id, String newStatus) throws ServiceException {
        Optional<GroupSupervisor> optionalInvitation = groupSupervisorRepository.findById(id);
        User user = userService.getCurrentUser();
        if (optionalInvitation.isPresent()) {
            GroupSupervisor invitation = optionalInvitation.get();
            if (newStatus.equals(Constants.SupervisorStatus.REJECT_SUPERVISOR) || newStatus.equals(Constants.SupervisorStatus.REJECT_SUPERVISOR_LEADER)) {
                groupSupervisorRepository.delete(invitation);
            } else {
                invitation.setStatus(newStatus);
                groupSupervisorRepository.save(invitation);
                if (groupSupervisorRepository.findBySupervisorIdAndStatus(Integer.parseInt(user.getSupervisor().getId().toString()),
                        Constants.SupervisorStatus.ACCEPT_SUPERVISOR).size() == 4) {
                    for (GroupSupervisor gs : groupSupervisorRepository.findBySupervisorIdAndStatus(Integer.parseInt(user.getSupervisor().getId().toString()),
                            Constants.SupervisorStatus.PENDING_SUPERVISOR)) {
                        groupSupervisorRepository.delete(gs);
                    }
                }
            }
            return invitation;
        } else {
            throw new ServiceException(ErrorCode.FAILED_EDIT_GROUP);
        }
    }

    @Override
    public List<GroupSupervisorResponse> getGroupBySemester(int semesterId) throws ServiceException {
        User currentUser = userService.getCurrentUser();
        List<GroupSupervisor> groupSupervisors = groupSupervisorRepository.findBySupervisorAndStatus(currentUser.getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
        if (groupSupervisors.size() == 0) {
            throw new ServiceException(ErrorCode.NOT_FOUND);
        }
        List<GroupSupervisorResponse> groupSupervisorResponse = new ArrayList<>();
        for (GroupSupervisor groupSupervisor : groupSupervisors
        ) {
            if (groupSupervisor.getGroup().getSemester().getId() == semesterId) {
                groupSupervisorResponse.add(modelMapper.map(groupSupervisor, GroupSupervisorResponse.class));
            }
        }
        return groupSupervisorResponse;
    }

    @Transactional
    @Override
    public List<GroupSupervisor> changeStatusGroupSupervisorByGroupId(Long groupId, String newStatus) throws ServiceException {

        Optional<Group> group1 = groupRepository.findById(groupId);
        if (group1.isEmpty()) {
            throw new ServiceException(ErrorCode.GROUP_NOT_FOUND);
        }
        List<GroupSupervisor> groupSupervisors = groupSupervisorRepository.findByGroupId(groupId);
        if (!groupSupervisors.isEmpty()) {
            for (GroupSupervisor supervisor : groupSupervisors) {
                if (!supervisor.getStatus().equals(Constants.SupervisorStatus.PENDING_SUPERVISOR)) {
                    throw new ServiceException(ErrorCode.FAILED_CANCEL_REQUEST_BECAUSE_SUPERVISOR_ACCEPT);
                }
            }
        }
        for (GroupSupervisor groupSupervisor : groupSupervisors) {
            groupSupervisorRepository.delete(groupSupervisor);
        }
        Group group = group1.get();
        group.setStatus(Constants.GroupStatus.PENDING);
        groupRepository.save(group);
        return groupSupervisors;
    }

    @Override
    public List<GroupSupervisorResponse> getGroupSupervisorByStatusAndSupervisorId(String status, int supervisorId) throws ServiceException {
        if (status.equals(Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER)) {
            List<GroupSupervisor> listInvitation = groupSupervisorRepository.findBySupervisorIdAndStatus(supervisorId, status);
            List<GroupSupervisorResponse> groupSupervisorResponse = new ArrayList<>();
            for (GroupSupervisor groupSupervisor : listInvitation
            ) {
                groupSupervisorResponse.add(modelMapper.map(groupSupervisor, GroupSupervisorResponse.class));
            }
            return groupSupervisorResponse;
        }
        return null;
    }

    @Override
    public List<GroupSupervisorResponse> getBySupervisorId(int supervisorId) throws ServiceException {
        User user = userRepository.findById(Long.valueOf(Integer.toString(supervisorId))).orElseThrow(() -> new org.hibernate.service.spi.ServiceException(ErrorCode.USER_NOT_FOUND));
        List<GroupSupervisor> listInvitation = groupSupervisorRepository.findBySupervisorId(Integer.parseInt(user.getSupervisor().getId().toString()));
        if (!listInvitation.isEmpty()) {
            List<GroupSupervisorResponse> groupSupervisorResponse = new ArrayList<>();
            for (GroupSupervisor groupSupervisor : listInvitation
            ) {
                groupSupervisorResponse.add(modelMapper.map(groupSupervisor, GroupSupervisorResponse.class));
            }
            return groupSupervisorResponse;
        }
        return null;
    }

    @Override
    public List<GroupSupervisorResponse> getByTwoStatus(String acceptSupervisorStatus, String acceptSupervisorLeaderStatus) throws ServiceException {
        if (acceptSupervisorStatus.equals(Constants.SupervisorStatus.ACCEPT_SUPERVISOR) && acceptSupervisorLeaderStatus.equals(Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER)) {
            List<GroupSupervisor> listInvitation = groupSupervisorRepository.findByTwoStatus(acceptSupervisorStatus, acceptSupervisorLeaderStatus);
            List<GroupSupervisorResponse> groupSupervisorResponse = new ArrayList<>();
            for (GroupSupervisor groupSupervisor : listInvitation
            ) {
                groupSupervisorResponse.add(modelMapper.map(groupSupervisor, GroupSupervisorResponse.class));
            }
            return groupSupervisorResponse;
        }
        return null;
    }

    @Override
    @Transactional
    public ResponseEntity<String> assignTeacher(List<AssignTeacherRequest> assignTeacherRequests) throws ServiceException {
        try {
            if (!checkGroupSizeOfSupervisor(assignTeacherRequests)) {
                throw new ServiceException(ErrorCode.SUPERVISOR_HAS_MAX_GROUP);
            }
            for (AssignTeacherRequest a : assignTeacherRequests) {
                Optional<Supervisor> supervisor = supervisorRepository.findById(Long.valueOf(a.getSupervisorId()));
                Optional<Group> group = groupRepository.findById(Long.valueOf(a.getGroupId()));
                if (supervisor.isEmpty()) {
                    throw new ServiceException(ErrorCode.SUPERVISOR_NOT_FOUND);
                }
                if (group.isEmpty()) {
                    throw new ServiceException(ErrorCode.GROUP_NOT_FOUND);
                }

                GroupSupervisor groupSupervisor = new GroupSupervisor();
                groupSupervisor.setSupervisor(supervisor.get());
                groupSupervisor.setGroup(group.get());
                groupSupervisor.setStatus(Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
                groupSupervisorRepository.save(groupSupervisor);

            }
            return ResponseEntity.ok("Assign teacher successfully");
        } catch (ServiceException e) {
            throw new ServiceException(e.getErrorCode());
        }
    }
    @Override
    public PageGroupHaveSupervisorResponse getAllGroupHaveSupervisor(PageRequest pageRequest) throws ServiceException {
        Pageable pageable = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize());
        Page<GroupSupervisor> groupSupervisors = groupSupervisorRepository.findByStatusAccept(pageable);
        PageGroupHaveSupervisorResponse pageGroupHaveSupervisorResponse = new PageGroupHaveSupervisorResponse();
        pageGroupHaveSupervisorResponse.setTotalPage(groupSupervisors.getTotalPages());
        pageGroupHaveSupervisorResponse.setTotalCount(groupSupervisors.getTotalElements());
        List<GroupSupervisorDTO> groupSupervisorDTOS = groupSupervisors.stream()
                .map(groupSupervisor -> modelMapper.map(groupSupervisor, GroupSupervisorDTO.class))
                .collect(Collectors.toList());
        pageGroupHaveSupervisorResponse.setData(groupSupervisorDTOS);
        return pageGroupHaveSupervisorResponse;

    }

    @Override
    public PageUnsupervisedGroupResponse getAllUnsupervisedGroup(PageRequest pageRequest) throws ServiceException {
        Pageable pageable = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize());
        PageUnsupervisedGroupResponse pageUnsupervisedGroupResponse = new PageUnsupervisedGroupResponse();
        Page<Group> groups = groupRepository.findByUnsupervisedGrour(pageable);
        List<GroupDTO> groupDTOS = groups.stream()
                .map(group -> modelMapper.map(group, GroupDTO.class))
                .collect(Collectors.toList());
        List<SupervisorDTO1> supervisorDTO1List = new ArrayList<>();
        List<Supervisor> allSupervisor = supervisorRepository.findAll();
        for (Supervisor supervisor : allSupervisor) {
            List<GroupSupervisor> groupHaveSupervisor = groupSupervisorRepository.findBySupervisorAndStatus(supervisor, Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
            SupervisorDTO1 supervisorDTO1 = new SupervisorDTO1();
            supervisorDTO1.setSupervisor(modelMapper.map(supervisor, SupervisorDTO.class));
            supervisorDTO1.setCount(groupHaveSupervisor.size());
            supervisorDTO1List.add(supervisorDTO1);
        }
        pageUnsupervisedGroupResponse.setTotalPage(groups.getTotalPages());
        pageUnsupervisedGroupResponse.setTotalCount(groups.getTotalElements());
        pageUnsupervisedGroupResponse.setData(groupDTOS);
        pageUnsupervisedGroupResponse.setSupervisorDTO1List(supervisorDTO1List);
        return pageUnsupervisedGroupResponse;
    }
    public boolean checkGroupSizeOfSupervisor(List<AssignTeacherRequest> assignTeacherRequests) {
        List<Supervisor> sampleSupervisors = supervisorRepository.findAllByHavingCountTotalGroupLessThanFour();
        List<Long> listId = new ArrayList<>();
        List<Supervisor> supervisors = new ArrayList<>();
        for (AssignTeacherRequest a : assignTeacherRequests) {
            listId.add(Long.valueOf(a.getGroupId()));
        }
        supervisors = supervisorRepository.findAllById(listId);
        return new HashSet<>(sampleSupervisors).containsAll(supervisors);
    }

    public GroupAllRespone getAllGroup() throws ServiceException {
        List<Group> allgroup = groupRepository.findAll();
        List<Supervisor> allSupervisor = supervisorRepository.findAll();
        GroupAllRespone response = new GroupAllRespone();
        List<GroupSupervisorDTO> listGroupHaveSupervisor = new ArrayList<>();
        List<GroupDTO> listGroupNoSupervisor = new ArrayList<>();
        List<SupervisorDTO1> supervisorDTO1List = new ArrayList<>();
        for (Group group : allgroup) {
            GroupSupervisor groupHaveSupervisor = groupSupervisorRepository.findByGroupAndStatus(group, Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
            if (!(groupHaveSupervisor == null)) {
                listGroupHaveSupervisor.add(modelMapper.map(groupHaveSupervisor, GroupSupervisorDTO.class));
            } else {
                listGroupNoSupervisor.add(modelMapper.map(group, GroupDTO.class));
            }
        }
        for (Supervisor supervisor : allSupervisor) {
            List<GroupSupervisor> groupHaveSupervisor = groupSupervisorRepository.findBySupervisorAndStatus(supervisor, Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
            SupervisorDTO1 supervisorDTO1 = new SupervisorDTO1();
            supervisorDTO1.setSupervisor(modelMapper.map(supervisor, SupervisorDTO.class));
            supervisorDTO1.setCount(groupHaveSupervisor.size());
            supervisorDTO1List.add(supervisorDTO1);
        }
        response.setSupervisorDTO1List(supervisorDTO1List);
        response.setGroupHaveSupervisor(listGroupHaveSupervisor);
        response.setGroupNoSupervisor(listGroupNoSupervisor);
        return response;
    }

}
