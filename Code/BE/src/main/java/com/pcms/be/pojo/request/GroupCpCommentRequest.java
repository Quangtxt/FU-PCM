package com.pcms.be.pojo.request;

import com.pcms.be.pojo.DTO.GroupCpCommentStudentDTO;
import com.pcms.be.pojo.DTO.GroupDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GroupCpCommentRequest {
    private Long groupId;
    private String contentEvaluation;
    private String formatEvaluation;
    private String attitudeEvaluation;
    private String achievementLevel;
    private String limitation;
    private List<GroupCpCommentStudentRequest> groupCpCommentStudents;
}
