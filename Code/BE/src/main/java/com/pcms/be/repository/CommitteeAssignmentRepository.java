package com.pcms.be.repository;

import com.pcms.be.domain.Committee;
import com.pcms.be.domain.CommitteeAssignment;
import com.pcms.be.domain.Semester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommitteeAssignmentRepository extends JpaRepository<CommitteeAssignment, Long> {
    @Query(value = "SELECT ca.*\n" +
            "FROM committee_assignment ca\n" +
            "INNER JOIN committee c ON ca.committee_id = c.id\n" +
            "WHERE c.semester_id = :semesterId", nativeQuery = true)
    Page<CommitteeAssignment> findAllBySemesterId(Pageable pageable, int semesterId);
    @Query(value = "SELECT ca.* \n" +
            "FROM committee_assignment ca \n" +
            "WHERE ca.committee_id  in (:committees) \n" ,
            nativeQuery = true)
    List<CommitteeAssignment> findAllByCommittees(List<Long> committees);
}
