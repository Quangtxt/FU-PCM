package com.pcms.be.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupTimeSubmitRequest {
    int groupId;
    OffsetDateTime startSubmit;
    OffsetDateTime endSubmit;
}
