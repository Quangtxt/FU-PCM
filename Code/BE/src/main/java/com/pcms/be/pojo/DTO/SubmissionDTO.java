package com.pcms.be.pojo.DTO;

import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.SubmissionFile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubmissionDTO {
    private Long id;
    private GroupDTO group;
    private String approvedBySupervisor;
    private boolean confirmedBySecretary;
    public OffsetDateTime startSubmit;
    public OffsetDateTime endSubmit;
    public OffsetDateTime startApprove;
    public OffsetDateTime endApprove;
    private boolean isComment;
    private List<SubmissionFileDTO> files;
}
