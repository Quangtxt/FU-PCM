package com.pcms.be.service;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
public class GoogleCloudStorageService {

    private final String bucketName = "cpss-v1";

    public String uploadFile(String groupCode,MultipartFile file) throws IOException {
        Storage storage = StorageOptions.newBuilder()
                .setCredentials(ServiceAccountCredentials.fromStream(
                        getClass().getClassLoader().getResourceAsStream("key.json")))
                .setProjectId("secure-approach-424011-j8")
                .build().getService();
        Bucket bucket = storage.get(bucketName);
        Blob blob = bucket.create(groupCode+"_"+file.getOriginalFilename(), file.getBytes(), file.getContentType());

        return blob.getName();
    }
    public Blob getFile(String blobId) throws IOException {
        Storage storage = StorageOptions.newBuilder()
                .setCredentials(ServiceAccountCredentials.fromStream(
                        getClass().getClassLoader().getResourceAsStream("key.json")))
                .setProjectId("secure-approach-424011-j8")
                .build().getService();
        Bucket bucket = storage.get(bucketName);
        return bucket.get(blobId);
    }
    public void deleteFile(String blobId) throws IOException {
        Storage storage = StorageOptions.newBuilder()
                .setCredentials(ServiceAccountCredentials.fromStream(
                        getClass().getClassLoader().getResourceAsStream("key.json")))
                .setProjectId("secure-approach-424011-j8")
                .build().getService();
        Bucket bucket = storage.get(bucketName);
        Blob blob = bucket.get(blobId);

        if (blob != null) {
            blob.delete();
        } else {
            throw new RuntimeException("Blob not found with ID: " + blobId);
        }
    }
}
