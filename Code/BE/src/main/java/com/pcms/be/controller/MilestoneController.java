package com.pcms.be.controller;

import com.pcms.be.domain.Milestone;
import com.pcms.be.domain.Report;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.DTO.ReportDTO;
import com.pcms.be.pojo.DTO.SemesterMilestone2DTO;
import com.pcms.be.pojo.DTO.SubmitReportResponse;
import com.pcms.be.pojo.request.MilestoneStatusRequest;
import com.pcms.be.pojo.response.MilestoneTemplateRp;
import com.pcms.be.repository.MilestoneRepository;
import com.pcms.be.repository.ReportRepository;
import com.pcms.be.service.MilestoneService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/milestones")
public class MilestoneController {
    private final MilestoneRepository milestoneRepository;
    private final ReportRepository reportRepository;
    private final MilestoneService milestoneService;
    private final ModelMapper modelMapper;
    private final SimpMessagingTemplate messagingTemplate;

    @GetMapping()
    public ResponseEntity<List<MilestoneTemplateRp>> getMilestoneTemplate() {
        List<Milestone> milestones = milestoneRepository.findAll();
        List<MilestoneTemplateRp> milestoneTemplateRps = new ArrayList<>();
        for (Milestone m : milestones
        ) {
            milestoneTemplateRps.add(modelMapper.map(m, MilestoneTemplateRp.class));
        }
        return ResponseEntity.ok(milestoneTemplateRps);
    }

    @GetMapping("/report/{semesterId}")
    public ResponseEntity<List<ReportDTO>> getReports(@PathVariable int semesterId) {
        List<Report> reports = reportRepository.findAllBySemesterId(semesterId);
        List<ReportDTO> reportDTOS = new ArrayList<>();
        for (Report r : reports
        ) {
            reportDTOS.add(modelMapper.map(r, ReportDTO.class));
        }
        return ResponseEntity.ok(reportDTOS);
    }

    @GetMapping("/report/m/{id}")
    public ResponseEntity<ReportDTO> getReportByMilestoneId(@PathVariable int id) {
        Report report = reportRepository.findFirstByMilestoneId(id);
        ReportDTO reportDTO = modelMapper.map(report, ReportDTO.class);
        return ResponseEntity.ok(reportDTO);
    }

    @GetMapping("/guidance/{id}")
    public ResponseEntity<List<SemesterMilestone2DTO>> getMilestoneGuidancePhase(@PathVariable Long id) {
        return ResponseEntity.ok(milestoneService.getMilestoneGuidancePhase(id));
    }

    @PostMapping("/process")//De giao vien submit cai check
    public ResponseEntity<String> setProcessOfMilestone(@RequestBody MilestoneStatusRequest milestoneStatus) {
        milestoneService.setProcessOfMilestone(milestoneStatus.getGroupId(), milestoneStatus.getMilestoneStatus());
        messagingTemplate.convertAndSend("/topic/notification","Update successfully" );
        return ResponseEntity.ok("Update successfully");
    }

    @PostMapping("/check")// sinh vien gui yeu cau check cho giao vien
    public ResponseEntity<String> requestCheckMilestone(@RequestBody MilestoneStatusRequest milestoneStatus) {
        milestoneService.requestCheckMilestone(milestoneStatus.getGroupId(), milestoneStatus.getMilestoneStatus());
        messagingTemplate.convertAndSend("/topic/notification","Update successfully" );
        return ResponseEntity.ok("Update successfully");
    }

    @GetMapping("/status")// Lay status
    public ResponseEntity<List<SubmitReportResponse>> getMilestoneStatus(@RequestParam int groupId) throws ServiceException {
        return ResponseEntity.ok(milestoneService.getStatusOfMilestone(groupId));
    }
}
