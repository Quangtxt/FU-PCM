package com.pcms.be.domain.user;

import com.pcms.be.domain.Milestone;
import com.pcms.be.domain.Report;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "v_group_cp_comment")
public class GroupCpComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    @JoinColumn(name = "group_id")
    private Group group;
    @Column(name = "content_evaluation")
    private String contentEvaluation;
    @Column(name = "format_evaluation")
    private String formatEvaluation;
    @Column(name = "attitude_evaluation")
    private String attitudeEvaluation;
    @Column(name = "achievement_level")
    private String achievementLevel;
    @Column(name = "limitation")
    private String limitation;

    @OneToMany(mappedBy = "groupCpComment", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<GroupCpCommentStudent> groupCpCommentStudents;
}
