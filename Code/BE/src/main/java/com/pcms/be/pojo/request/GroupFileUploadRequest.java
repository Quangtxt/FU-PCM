package com.pcms.be.pojo.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
public class GroupFileUploadRequest {
    private Long groupId;
    private String submissionType;
    private List<MultipartFile> files;
}
