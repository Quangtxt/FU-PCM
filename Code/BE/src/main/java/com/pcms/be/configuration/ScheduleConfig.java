package com.pcms.be.configuration;

import com.pcms.be.domain.Milestone;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.time.OffsetDateTime;

@Configuration
@PropertySource("classpath:schedule-config.properties")
@Getter
@Setter
public class ScheduleConfig {
    @Value("${updateStatusMilestone.cron}")
    private String updateStatusMilestoneCron;

    @Value("${checkingTotalCommit.cron}")
    private String checkingTotalCommitCron;

    @Value("${checkingTotalCommit.cron}")
    private String reminderToSubmitReportCron;


}
