package com.pcms.be.repository;

import com.pcms.be.domain.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameIgnoreCase(String username);
    Optional<User> findByEmail(String email);

    @Query(value = "SELECT u.*\n" +
            "FROM v_user u\n" +
            "INNER JOIN v_student s ON u.id = s.user_id\n" +
            "WHERE s.id = :studentId", nativeQuery = true)
    Optional<User> findByStudentId(int studentId);

}