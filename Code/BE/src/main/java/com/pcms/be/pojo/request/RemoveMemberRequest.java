package com.pcms.be.pojo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RemoveMemberRequest {
    int GroupId;
    int StudentId;
}
