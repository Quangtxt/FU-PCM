package com.pcms.be.domain;

import com.pcms.be.domain.user.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@Entity
@Table(name = "committee")
public class Committee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "committee_code")
    private String committeeCode;

    @Column(name = "committee_name")
    private String committeeName;

    @ManyToOne
    @JoinColumn(name = "semester_id", nullable = false)
    private Semester semester ;

    @OneToMany(mappedBy = "committee", cascade = CascadeType.ALL)
    private List<CommitteeDetail> committeeDetails = new ArrayList<>();
}
