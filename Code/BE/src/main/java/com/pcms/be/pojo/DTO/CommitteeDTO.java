package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CommitteeDTO {
    Long id;
    String committeeCode;
    String committeeName;
    SemesterDTO semester;
    List<CommitteeDetailDTO> committeeDetails;
}
