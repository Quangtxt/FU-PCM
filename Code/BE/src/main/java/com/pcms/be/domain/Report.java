package com.pcms.be.domain;

import com.pcms.be.domain.meeting.Meeting;
import com.pcms.be.domain.user.Member;
import com.pcms.be.domain.user.Supervisor;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "report")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "milestone_id_submit")
    private Milestone milestoneSubmit;

    @OneToOne
    @JoinColumn(name = "milestone_id", referencedColumnName = "id", nullable = false)
    private Milestone milestone;

    @Column(name = "percent_score")
    private Double percentScore;

    @ManyToOne
    @JoinColumn(name = "semester_id")
    private Semester semester;

    @Column(name = "possible_date")
    public OffsetDateTime possibleDate;

}
