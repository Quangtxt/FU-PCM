package com.pcms.be.repository;

import org.springframework.data.domain.Page;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.GroupSupervisor;
import com.pcms.be.domain.user.Supervisor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupSupervisorRepository extends JpaRepository<GroupSupervisor, Long> {
//    Optional<GroupSupervisor> findById(Long id);
    List<GroupSupervisor> findBySupervisorAndStatus(Supervisor supervisor, String status);
    List<GroupSupervisor> findAllByStatus(String status);
    GroupSupervisor findByGroupAndSupervisorAndStatus(Group group, Supervisor supervisor, String status);
    List<GroupSupervisor> findByGroupId(Long groupId);
    List<GroupSupervisor> findBySupervisorIdAndStatus(int supervisorId,String status);
    List<GroupSupervisor> findBySupervisorId(int supervisorId);
    @Query(value = "SELECT g.* \n" +
            "FROM v_group_supervisor g\n" +
            "WHERE g.status = :acceptSupervisorStatus \n" +
            "   OR g.status = :acceptSupervisorLeaderStatus", nativeQuery = true)
    List<GroupSupervisor> findByTwoStatus(String acceptSupervisorStatus, String acceptSupervisorLeaderStatus);

    GroupSupervisor findByGroupAndStatus(Group group, String status);

    @Query(value = "SELECT gs.* \n" +
            "FROM v_group_supervisor gs\n" +
            "WHERE gs.status = 'ACCEPT_SUPERVISOR_LEADER'", nativeQuery = true)
    Page<GroupSupervisor> findByStatusAccept(Pageable pageable);
}
