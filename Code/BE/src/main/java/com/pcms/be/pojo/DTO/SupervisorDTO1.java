package com.pcms.be.pojo.DTO;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupervisorDTO1 {
    private SupervisorDTO supervisor;
    private int count;
}
