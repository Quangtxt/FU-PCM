package com.pcms.be.domain.user;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "v_group_submission")
public class GroupSubmission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false,unique = true)
    private Group group;

    @Column(name = "approved_by_supervisor")
    private String approvedBySupervisor;

    @Column(name = "confirmed_by_secretary")
    private boolean confirmedBySecretary;

    @Column(name = "start_submit_at")
    public OffsetDateTime startSubmit;

    @Column(name = "end_submit_at")
    public OffsetDateTime endSubmit;

    @Column(name = "start_approve")
    public OffsetDateTime startApprove;

    @Column(name = "end_approve")
    public OffsetDateTime endApprove;

    @OneToMany(mappedBy = "submission", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SubmissionFile> files;

}

