package com.pcms.be.pojo.DTO;

import com.pcms.be.pojo.response.GroupResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MemberDTO {
    private Long id;
    private StudentDTO student;
    private String role;
    private String status;
}
