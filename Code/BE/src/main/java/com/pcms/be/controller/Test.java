package com.pcms.be.controller;

import com.pcms.be.domain.Milestone;
import com.pcms.be.domain.Report;
import com.pcms.be.domain.Semester_Milestone;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.GroupSupervisor;
import com.pcms.be.domain.user.Student;
import com.pcms.be.domain.user.Supervisor;
import com.pcms.be.errors.ApiException;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.DTO.GroupDTO;
import com.pcms.be.pojo.DTO.StudentDTO;
import com.pcms.be.pojo.request.SendEmailRequest;
import com.pcms.be.pojo.response.EmailResponse;
import com.pcms.be.pojo.response.GroupResponse;
import com.pcms.be.repository.*;
import com.pcms.be.service.Email.EmailService;
import com.pcms.be.service.GitService;
import com.pcms.be.service.MilestoneService;
import com.pcms.be.service.impl.GroupServiceImpl;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Slf4j
@RestController
@RequestMapping("/api/v1/test")
@RequiredArgsConstructor
public class Test {
    private final MilestoneService milestoneService;
    private final EmailService emailService;
    private final ReportRepository reportRepository;
    private final GroupServiceImpl groupService;
    private final GroupSupervisorRepository groupSupervisorRepository;
    private final SupervisorRepository supervisorRepository;
    private final ModelMapper modelMapper;
    private final GroupRepository groupRepository;
    private final StudentRepository studentRepository;
    private final GitService gitService;
    @GetMapping("/a")
    public ResponseEntity<String> test() throws ServiceException {
       try {
           gitService.updateStatusMilestone();
           return ResponseEntity.ok("a");
       } catch (ServiceException e) {
           throw new ApiException(e.getErrorCode(), e.getParams());
       }
    }

    @PostMapping("/email/send")
    ResponseEntity<EmailResponse> sendEmail(@RequestBody SendEmailRequest request){
        return ResponseEntity.ok(emailService.sendEmail(request));
    }

    @GetMapping("/test/aa")
    ResponseEntity<Integer> testaa(){
        List<Supervisor> supervisors = supervisorRepository.findAll();
        
        return ResponseEntity.ok(supervisors.size());
    }

    @GetMapping("/test/v2")
    ResponseEntity<StudentDTO> testGroupDTO(){
        Student student = studentRepository.findById(1L).get();
        return ResponseEntity.ok(modelMapper.map(student, StudentDTO.class));
    }

    @GetMapping("/clear")
    ResponseEntity<String> clearGroup() throws ServiceException {
        List<Group> groups = groupRepository.findGroupsWithMemberCountLessThan(5);
        groupService.clearGroup(groups);
        return ResponseEntity.ok("");
    }

    @GetMapping("/setStoG")
    ResponseEntity<String> setStudentsToGroups() throws ServiceException {
        List<Group> groups = groupRepository.findGroupsWithMemberCountLessThan(5);
        groupService.clearGroup(groups);
        List<Student> listStudentNoneGroup = studentRepository.findStudentsNotInMemberOrInactive();
        groupService.setStudentsToGroup(groups, listStudentNoneGroup);
        return ResponseEntity.ok("");
    }
    @GetMapping("/checkReminder")
    ResponseEntity<String> checkReminder() throws ServiceException, IOException {
        gitService.checkCommit();
        return ResponseEntity.ok("");
    }
    @GetMapping("/testCheckGitt")
    ResponseEntity<String> checkReminder2() throws ServiceException, IOException {
        gitService.reminderToSubmitReport();
        return ResponseEntity.ok("");
    }
}
