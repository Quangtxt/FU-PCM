package com.pcms.be.repository;

import com.pcms.be.domain.user.ScoreHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScoreHistoryRepository extends JpaRepository<ScoreHistory, Long> {
}
