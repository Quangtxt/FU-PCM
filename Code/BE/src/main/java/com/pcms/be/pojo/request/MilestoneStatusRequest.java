package com.pcms.be.pojo.request;

import com.pcms.be.pojo.DTO.SubmitReportResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MilestoneStatusRequest {
    int GroupId;
    List<SubmitReportResponse> milestoneStatus;
}
