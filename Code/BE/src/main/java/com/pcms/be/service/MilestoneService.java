package com.pcms.be.service;

import com.pcms.be.domain.Milestone;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.DTO.MilestoneDTO;
import com.pcms.be.pojo.DTO.SemesterMilestone2DTO;
import com.pcms.be.pojo.DTO.SubmitReportResponse;
import com.pcms.be.pojo.request.CreatedMilestoneRequest;
import com.pcms.be.pojo.request.EditMilestoneRequest;
import jakarta.mail.MessagingException;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface MilestoneService {
    ResponseEntity<MilestoneDTO> getById(int id);

    List<SemesterMilestone2DTO> getMilestoneGuidancePhase(Long semester_id);
    void setProcessOfMilestone(int groupId,List<SubmitReportResponse> submitReportResponses);
    void requestCheckMilestone(int groupId,List<SubmitReportResponse> submitReportResponses);
    List<SubmitReportResponse> getStatusOfMilestone(int groupId) throws ServiceException;
}
