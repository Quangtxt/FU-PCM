package com.pcms.be.domain.user;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "v_group_cp_comment_student")
public class GroupCpCommentStudent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "group_cp_comment_id")
    private GroupCpComment groupCpComment;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @Column(name = "defense_status")
    private Integer defenseStatus;

    @Column(name = "note")
    private String note;
}
