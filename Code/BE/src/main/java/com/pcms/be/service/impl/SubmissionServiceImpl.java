package com.pcms.be.service.impl;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.pcms.be.domain.MilestoneGroup;
import com.pcms.be.domain.Semester;
import com.pcms.be.domain.user.*;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.pojo.DTO.SubmissionDTO;
import com.pcms.be.pojo.response.GroupResponse;
import com.pcms.be.pojo.response.UserNotificationResponse;
import com.pcms.be.repository.*;
import com.pcms.be.service.GoogleCloudStorageService;
import com.pcms.be.service.SubmissionService;
import com.pcms.be.service.UserService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class SubmissionServiceImpl implements SubmissionService {
    private final SemesterRepository semesterRepository;
    private final GroupRepository groupRepository;
    private final GoogleCloudStorageService googleCloudStorageService;
    private final GroupSubmissionRepository groupSubmissionRepository;
    private final SubmissionFileRepository submissionFileRepository;
    private final GroupCpCommentRepository groupCpCommentRepository;
    private final ModelMapper modelMapper;
    private final UserService userService;


    @Override
    public List<SubmissionFile> uploadFiles(List<MultipartFile> files, Long groupId, String type) throws IOException {
        GroupSubmission submission = groupSubmissionRepository.findByGroupId(groupId)
                .orElseThrow(() -> new RuntimeException("Submission not found"));

        List<SubmissionFile> submissionFiles = new ArrayList<>();

        for (MultipartFile file : files) {
            String blobId = googleCloudStorageService.uploadFile(submission.getGroup().getGroupCode(),file);

            SubmissionFile submissionFile = new SubmissionFile();
            submissionFile.setFilename(submission.getGroup().getGroupCode()+"_"+file.getOriginalFilename());
            submissionFile.setFileType(file.getContentType());
            submissionFile.setSubmissionType(type);
            submissionFile.setFileSize(file.getSize());
            submissionFile.setBlobId(blobId);
            submissionFile.setSubmission(submission);
            submissionFiles.add(submissionFile);
        }

        return submissionFileRepository.saveAll(submissionFiles);
    }

    @Override
    public void deleteFile(Long submissionFileId) throws IOException, ServiceException {
        SubmissionFile submissionFile = submissionFileRepository.findById(submissionFileId)
                .orElseThrow(() -> new ServiceException(ErrorCode.SUBMISSION_NOT_FOUND));

        googleCloudStorageService.deleteFile(submissionFile.getBlobId());

        submissionFileRepository.delete(submissionFile);
    }

    @Override
    public SubmissionDTO getSubmissionByGroup(Long groupId) throws IOException {
        GroupSubmission submission = groupSubmissionRepository.findByGroupId(groupId)
                .orElseThrow(() -> new RuntimeException("Submission not found"));

        SubmissionDTO submissionDTO = modelMapper.map(submission, SubmissionDTO.class);
        return submissionDTO;
    }

    @Override
    public void confirmGroupSubmission(Long groupId, String status) throws IOException {
        GroupSubmission submission = groupSubmissionRepository.findByGroupId(groupId)
                .orElseThrow(() -> new RuntimeException("Submission not found"));
        submission.setApprovedBySupervisor(status);
        groupSubmissionRepository.save(submission);
    }

    @Override
    public List<SubmissionDTO> getGroupSubmissionBySemester(Long semesterId) throws IOException, ServiceException {
        User currentUser = userService.getCurrentUser();

        List<GroupSubmission> submissions = groupSubmissionRepository.findAllBySupervisorIdAndSemesterId(currentUser.getSupervisor().getId(),semesterId);

        List<SubmissionDTO> submissionDTOs = submissions.stream()
                .map(submission -> {
                    SubmissionDTO dto = modelMapper.map(submission, SubmissionDTO.class);
                    Optional<GroupCpComment> groupCpComment = groupCpCommentRepository.findByGroupId(submission.getGroup().getId());
                    dto.setComment(groupCpComment.isPresent());
                    return dto;
                })
                .collect(Collectors.toList());
        return submissionDTOs;
    }
    @Override
    public List<SubmissionDTO> getGroupSubmissionFinal(int semesterId) throws ServiceException {
        List<SubmissionDTO> submissionDTOS = new ArrayList<>();
        Semester semester = semesterRepository.findById(Long.valueOf(semesterId)).orElseThrow(()-> new ServiceException(ErrorCode.NOT_FOUND));
        List<Group> groups = groupRepository.findBySemester(Integer.parseInt(semester.getId().toString()));
        for (Group group: groups) {
            GroupSubmission groupSubmission = groupSubmissionRepository.findByGroupId(group.getId()).orElseGet(() -> {
                        GroupSubmission newGr = new GroupSubmission();
                        newGr.setGroup(group);
                        return newGr;
                    }
            );
            submissionDTOS.add(modelMapper.map(groupSubmission,SubmissionDTO.class ));
        }
        return submissionDTOS;
    }

    @Override
    public List<SubmissionDTO> setAllForGroupTimeSubmit(int semesterId, OffsetDateTime startSubmit,OffsetDateTime endSubmit) throws ServiceException {
        List<SubmissionDTO> submissionDTOS = new ArrayList<>();
        Semester semester = semesterRepository.findById(Long.valueOf(semesterId)).orElseThrow(()-> new ServiceException(ErrorCode.NOT_FOUND));
        List<Group> groups = groupRepository.findBySemester(Integer.parseInt(semester.getId().toString()));
        for (Group group: groups) {
            GroupSubmission groupSubmission = groupSubmissionRepository.findByGroupId(group.getId()).orElseGet(() -> {
                        GroupSubmission newGr = new GroupSubmission();
                        newGr.setGroup(group);
                        newGr.setStartSubmit(startSubmit);
                        newGr.setEndSubmit(endSubmit);
                        newGr.setApprovedBySupervisor("PENDING");
                        groupSubmissionRepository.save(newGr);
                        return newGr;
                    }
            );
            groupSubmission.setStartSubmit(startSubmit);
            groupSubmission.setEndSubmit(endSubmit);
            groupSubmission.setApprovedBySupervisor("PENDING");
            groupSubmissionRepository.save(groupSubmission);
            submissionDTOS.add(modelMapper.map(groupSubmission,SubmissionDTO.class));
        }
        return submissionDTOS;
    }

    @Override
    public SubmissionDTO setForGroupTimeSubmit(OffsetDateTime startSubmit, OffsetDateTime endSubmit, int groupId) throws ServiceException {
        Group group = groupRepository.findById(Long.valueOf(groupId)).orElseThrow(()-> new ServiceException(ErrorCode.NOT_FOUND));
        GroupSubmission groupSubmission = groupSubmissionRepository.findByGroupId(group.getId()).orElseGet(() -> {
                    GroupSubmission newGr = new GroupSubmission();
                    newGr.setGroup(group);
//                    newGr.setStartSubmit(startSubmit);
//                    newGr.setEndSubmit(endSubmit);
                    groupSubmissionRepository.save(newGr);
                    return newGr;
                }
        );
        groupSubmission.setStartSubmit(startSubmit);
        groupSubmission.setEndSubmit(endSubmit);
        groupSubmission.setApprovedBySupervisor("PENDING");

        groupSubmissionRepository.save(groupSubmission);
        return modelMapper.map(groupSubmission, SubmissionDTO.class);
    }

    @Override
    public List<SubmissionDTO> setAllForGroupTimeApprove(int semesterId, OffsetDateTime startApprove, OffsetDateTime endApprove) throws ServiceException {
        List<SubmissionDTO> submissionDTOS = new ArrayList<>();
        Semester semester = semesterRepository.findById(Long.valueOf(semesterId)).orElseThrow(()-> new ServiceException(ErrorCode.NOT_FOUND));
        List<Group> groups = groupRepository.findBySemester(Integer.parseInt(semester.getId().toString()));
        for (Group group: groups) {
            GroupSubmission groupSubmission = groupSubmissionRepository.findByGroupId(group.getId()).orElseGet(() -> {
                        GroupSubmission newGr = new GroupSubmission();
                        newGr.setGroup(group);
                        newGr.setStartSubmit(startApprove);
                        newGr.setEndSubmit(endApprove);
                        groupSubmissionRepository.save(newGr);
                        return newGr;
                    }
            );
            groupSubmission.setStartSubmit(startApprove);
            groupSubmission.setEndSubmit(endApprove);
            groupSubmissionRepository.save(groupSubmission);
            submissionDTOS.add(modelMapper.map(groupSubmission,SubmissionDTO.class));
        }
        return submissionDTOS;
    }

}
