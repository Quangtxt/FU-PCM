package com.pcms.be.pojo.DTO;

import java.util.Arrays;
import java.util.List;

public class ReportTemplate {
    public static List<ReportInfo> getReportInfoList() {
        return Arrays.asList(
                new ReportInfo("Report1_Project_Introduction", 4, Long.parseLong("13"),Long.parseLong("7")),
                new ReportInfo("Report2_Project_Management_Plan", 8, Long.parseLong("14"),Long.parseLong("8")),
                new ReportInfo("Report3_Software_Requirement_Specification", 16, Long.parseLong("15"),Long.parseLong("10")),
                new ReportInfo("Report4_Software_Design_Document", 18, Long.parseLong("18"),Long.parseLong("10")),
                new ReportInfo("Report5_Test_Documentation", 18, Long.parseLong("37"),Long.parseLong("10")),
                new ReportInfo("Report6_Software_User_Guides", 4, Long.parseLong("38"),Long.parseLong("11")),
                new ReportInfo("Report7_Final_Project_Report", 32, Long.parseLong("40"),Long.parseLong("12"))
        );
    }
}
