package com.pcms.be.service.impl;

import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.GroupCpComment;
import com.pcms.be.domain.user.GroupCpCommentStudent;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.pojo.DTO.GroupCpCommentDTO;
import com.pcms.be.pojo.request.GroupCpCommentRequest;
import com.pcms.be.pojo.request.GroupCpCommentStudentRequest;
import com.pcms.be.repository.GroupCpCommentRepository;
import com.pcms.be.repository.GroupCpCommentStudentRepository;
import com.pcms.be.repository.GroupRepository;
import com.pcms.be.repository.StudentRepository;
import com.pcms.be.service.GroupCpCommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.service.spi.ServiceException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class GroupCpCommentServiceImpl implements GroupCpCommentService {

    private final GroupCpCommentRepository groupCpCommentRepository;
    private final GroupCpCommentStudentRepository groupCpCommentStudentRepository;
    private final GroupRepository groupRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public GroupCpCommentDTO addGroupCpComment(GroupCpCommentRequest groupCpCommentRequest) {
        GroupCpComment groupCpComment = new GroupCpComment();
        Optional<Group> group = groupRepository.findById(groupCpCommentRequest.getGroupId());

        if (group.isPresent()) {
            groupCpComment.setGroup(group.get());
            groupCpComment.setContentEvaluation(groupCpCommentRequest.getContentEvaluation());
            groupCpComment.setFormatEvaluation(groupCpCommentRequest.getFormatEvaluation());
            groupCpComment.setAttitudeEvaluation(groupCpCommentRequest.getAttitudeEvaluation());
            groupCpComment.setAchievementLevel(groupCpCommentRequest.getAchievementLevel());
            groupCpComment.setLimitation(groupCpCommentRequest.getLimitation());

            List<GroupCpCommentStudent> studentCommentList = groupCpCommentRequest.getGroupCpCommentStudents()
                    .stream()
                    .map(studentRequest -> {
                        GroupCpCommentStudent studentComment = new GroupCpCommentStudent();
                        studentComment.setGroupCpComment(groupCpComment);
                        studentComment.setStudent(studentRepository.findById(studentRequest.getStudentId())
                                .orElseThrow(() -> new ServiceException(ErrorCode.STUDENT_NOT_FOUND)));
                        studentComment.setDefenseStatus(studentRequest.getDefenseStatus());
                        studentComment.setNote(studentRequest.getNote());
                        return studentComment;
                    })
                    .collect(Collectors.toList());

            groupCpComment.setGroupCpCommentStudents(studentCommentList);
            groupCpCommentRepository.save(groupCpComment);
            return modelMapper.map(groupCpComment, GroupCpCommentDTO.class);
        }
        return null;
    }


    @Override
    public GroupCpCommentDTO getGroupCpCommentByGroupId(Long groupId) {
        Optional<GroupCpComment> groupCpComment = groupCpCommentRepository.findByGroupId(groupId);
        if(groupCpComment.isPresent()) {
            GroupCpCommentDTO groupCpCommentDTO =  modelMapper.map(groupCpComment.get(), GroupCpCommentDTO.class);
            return groupCpCommentDTO;
        }
        return null;
    }

    @Override
    @Transactional
    public GroupCpCommentDTO updateGroupCpComment(Long id, GroupCpCommentRequest groupCpCommentRequest) {
        Optional<GroupCpComment> existingComment = groupCpCommentRepository.findById(id);

        if (existingComment.isPresent()) {
            GroupCpComment groupCpComment = existingComment.get();
            groupCpComment.setContentEvaluation(groupCpCommentRequest.getContentEvaluation());
            groupCpComment.setFormatEvaluation(groupCpCommentRequest.getFormatEvaluation());
            groupCpComment.setAttitudeEvaluation(groupCpCommentRequest.getAttitudeEvaluation());
            groupCpComment.setAchievementLevel(groupCpCommentRequest.getAchievementLevel());
            groupCpComment.setLimitation(groupCpCommentRequest.getLimitation());

            for (GroupCpCommentStudentRequest studentRequest : groupCpCommentRequest.getGroupCpCommentStudents()) {
                Optional<GroupCpCommentStudent> existingStudentComment = groupCpCommentStudentRepository
                        .findByGroupCpCommentIdAndStudentId(id, studentRequest.getStudentId());

                if (existingStudentComment.isPresent()) {
                    GroupCpCommentStudent studentComment = existingStudentComment.get();
                    studentComment.setDefenseStatus(studentRequest.getDefenseStatus());
                    studentComment.setNote(studentRequest.getNote());
                    groupCpCommentStudentRepository.save(studentComment);
                } else {
                    GroupCpCommentStudent newStudentComment = new GroupCpCommentStudent();
                    newStudentComment.setGroupCpComment(groupCpComment);
                    newStudentComment.setStudent(studentRepository.findById(studentRequest.getStudentId())
                            .orElseThrow(() -> new ServiceException(ErrorCode.STUDENT_NOT_FOUND)));
                    newStudentComment.setDefenseStatus(studentRequest.getDefenseStatus());
                    newStudentComment.setNote(studentRequest.getNote());
                    groupCpCommentStudentRepository.save(newStudentComment);
                }
            }
            groupCpCommentRepository.save(groupCpComment);
            return modelMapper.map(groupCpComment, GroupCpCommentDTO.class);
        }
        return null;
    }
}

