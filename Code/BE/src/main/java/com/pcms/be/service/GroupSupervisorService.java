package com.pcms.be.service;

import com.pcms.be.domain.user.GroupSupervisor;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.request.AssignTeacherRequest;
import com.pcms.be.pojo.response.GroupAllRespone;
import com.pcms.be.pojo.response.GroupSupervisorResponse;
import com.pcms.be.pojo.response.PageGroupHaveSupervisorResponse;
import com.pcms.be.pojo.response.PageUnsupervisedGroupResponse;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface GroupSupervisorService {

    GroupSupervisor putStatus(Long id, String newStatus) throws ServiceException;
    List<GroupSupervisorResponse> getByStatus(String status) throws ServiceException;
    List<GroupSupervisorResponse> getGroupBySemester(int semesterId) throws ServiceException;
    List<GroupSupervisor> changeStatusGroupSupervisorByGroupId(Long groupId, String status) throws ServiceException ;
    List<GroupSupervisorResponse> getGroupSupervisorByStatusAndSupervisorId(String status, int supervisorId) throws ServiceException;
    List<GroupSupervisorResponse> getBySupervisorId(int supervisorId) throws ServiceException;
    List<GroupSupervisorResponse> getByTwoStatus(String acceptSupervisorStatus, String acceptSupervisorLeaderStatus) throws ServiceException;
    GroupAllRespone getAllGroup() throws ServiceException;
    ResponseEntity<String> assignTeacher (List<AssignTeacherRequest> assignTeacherRequests) throws ServiceException;

    PageGroupHaveSupervisorResponse getAllGroupHaveSupervisor(PageRequest pageRequest) throws ServiceException;

    PageUnsupervisedGroupResponse getAllUnsupervisedGroup(PageRequest pageRequest) throws ServiceException;
}
