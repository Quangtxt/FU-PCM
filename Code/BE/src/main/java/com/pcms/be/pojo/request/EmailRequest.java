package com.pcms.be.pojo.request;

import com.pcms.be.pojo.DTO.Recipient;
import com.pcms.be.pojo.DTO.Sender;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class EmailRequest {
    private Sender sender;
    private List<Recipient> to;
    private String subject;
    private String htmlContent;

}
