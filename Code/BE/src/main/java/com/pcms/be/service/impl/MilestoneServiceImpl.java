package com.pcms.be.service.impl;

import com.pcms.be.configuration.ScheduleConfig;
import com.pcms.be.domain.*;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.Member;
import com.pcms.be.domain.user.Student;
import com.pcms.be.domain.user.Supervisor;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.functions.Git;
import com.pcms.be.functions.MailTemplate;
import com.pcms.be.functions.NotificationTemplate;
import com.pcms.be.pojo.DTO.*;
import com.pcms.be.pojo.request.CreatedMilestoneRequest;
import com.pcms.be.pojo.request.EditMilestoneRequest;
import com.pcms.be.pojo.request.SendEmailRequest;
import com.pcms.be.repository.*;
import com.pcms.be.service.Email.EmailService;
import com.pcms.be.service.MilestoneService;
import com.pcms.be.service.NotificationService;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MilestoneServiceImpl implements MilestoneService {
    @Autowired
    private ScheduleConfig scheduleConfig;
    private final MilestoneRepository milestoneRepository;
    private final SemesterRepository semesterRepository;
    private final SemesterMilestoneRepository semesterMilestoneRepository;
    private final ModelMapper modelMapper;
    private final GroupRepository groupRepository;
    private final MilestoneGroupRepository milestoneGroupRepository;
    private final EmailService emailService;
    private final NotificationService notificationService;
    private final ReportRepository reportRepository;

    @Override
    public ResponseEntity<MilestoneDTO> getById(int id) {
        Optional<Milestone> optMilestone = milestoneRepository.findById(Long.valueOf(id));
        if (optMilestone.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            Milestone milestone = optMilestone.orElseThrow();
            MilestoneDTO milestoneDTO = modelMapper.map(milestone, MilestoneDTO.class);
            return ResponseEntity.ok(milestoneDTO);
        }
    }






    @Override
    public List<SemesterMilestone2DTO> getMilestoneGuidancePhase(Long semester_id) {
        Semester semester = semesterRepository.findById(semester_id).orElseThrow();
        List<Semester_Milestone> semesterMilestones = semesterMilestoneRepository.findAllBySemester(semester);
        List<Semester_Milestone> filteredMilestones = semesterMilestones.stream()
                .filter(s -> s.getMilestone().getParent() != null && s.getMilestone().getParent() == 5)
                .collect(Collectors.toList());

        List<SemesterMilestone2DTO> semesterMilestone2DTOS = filteredMilestones.stream()
                .map(s -> modelMapper.map(s, SemesterMilestone2DTO.class))
                .collect(Collectors.toList());
        return semesterMilestone2DTOS;
    }

    @Override
    public void setProcessOfMilestone(int groupId, List<SubmitReportResponse> submitReportResponses) {
        Group g = groupRepository.findById(Long.parseLong(Integer.toString(groupId))).orElseThrow();
        for (SubmitReportResponse rq : submitReportResponses) {
            Optional<MilestoneGroup> milestoneGroupOptional = milestoneGroupRepository.findByGroupIdAndMilestoneId(groupId, (int) rq.getMilestoneId());
            MilestoneGroup milestoneGroup = milestoneGroupOptional.orElseGet(() -> {
                MilestoneGroup newGroup = new MilestoneGroup();
                newGroup.setGroup(g);
                newGroup.setMilestone(milestoneRepository.findById(rq.getMilestoneId()).orElseThrow());
                if (rq.getStatus() != null && rq.getStatus().equals(Constants.MilestoneGroupStatus.REDO)) {
                    newGroup.setStudentRequestGrading(false);
                }
                newGroup.setStatus(rq.getStatus());
                return newGroup;
            });

            if (rq.getStatus() != null){
                if (!rq.getStatus().equals(milestoneGroup.getStatus())){
                    String content = "";
                    Map<String, String> map = new HashMap<>();
                    map.put("_MilestoneName-txt_", milestoneGroup.getMilestone().getProduct());
                    if (rq.getStatus().equalsIgnoreCase(Constants.MilestoneGroupStatus.REDO)){
                        milestoneGroup.setStudentRequestGrading(false);
                        content = notificationService.createContentNotification(NotificationTemplate.MilestoneProcessNotification.reDoSubmission, map);
                    } else if (rq.getStatus().equalsIgnoreCase(Constants.MilestoneGroupStatus.DONE)) {
                        content = notificationService.createContentNotification(NotificationTemplate.MilestoneProcessNotification.passSubmission, map);
                    }
                    notificationService.saveResponseCheckMilestone(g.getMembers(), content);

                    milestoneGroup.setStatus(rq.getStatus());
                    milestoneGroupRepository.save(milestoneGroup);
                }
            }
        }
    }

    @Override
    @Transactional
    public void requestCheckMilestone(int groupId, List<SubmitReportResponse> submitReportResponses) {
        Group g = groupRepository.findById(Long.parseLong(Integer.toString(groupId))).orElseThrow();
        for (SubmitReportResponse rq : submitReportResponses) {
            Optional<MilestoneGroup> milestoneGroupOptional = milestoneGroupRepository.findByGroupIdAndMilestoneId(groupId, (int) rq.getMilestoneId());
            MilestoneGroup milestoneGroup = milestoneGroupOptional.orElseGet(() -> {
                MilestoneGroup newGroup = new MilestoneGroup();
                newGroup.setGroup(g);
                newGroup.setMilestone(milestoneRepository.findById(rq.getMilestoneId()).orElseThrow());
                newGroup.setStudentRequestGrading(rq.isStudentRequestGrading());
                if (rq.getStatus() != null && rq.getStatus().equals(Constants.MilestoneGroupStatus.REDO) && rq.isStudentRequestGrading()) {
                    newGroup.setStatus(Constants.MilestoneGroupStatus.PENDING);
                } else {
                    newGroup.setStatus(rq.getStatus());
                }
                return newGroup;
            });
            milestoneGroup.setStudentRequestGrading(rq.isStudentRequestGrading());
            if (rq.getStatus() != null && rq.getStatus().equals(Constants.MilestoneGroupStatus.REDO) && rq.isStudentRequestGrading()) {
                milestoneGroup.setStatus(Constants.MilestoneGroupStatus.PENDING);
                Map<String, String> map = new HashMap<>();
                map.put("_GroupName-txt_", g.getName());
                map.put("_MilestoneName-txt_", milestoneGroup.getMilestone().getProduct());
                String content = notificationService.createContentNotification(NotificationTemplate.MilestoneProcessNotification.reCheckSubmission, map);
                notificationService.saveCheckProgressNotification(g.getSupervisors(), content);
            } else if(rq.getStatus() == null){
                if(rq.isStudentRequestGrading()){
                    Map<String, String> map = new HashMap<>();
                    map.put("_GroupName-txt_", g.getName());
                    map.put("_MilestoneName-txt_", milestoneGroup.getMilestone().getProduct());
                    String content = notificationService.createContentNotification(NotificationTemplate.MilestoneProcessNotification.reCheckSubmission, map);
                    notificationService.saveCheckProgressNotification(g.getSupervisors(), content);
                }
                milestoneGroup.setStatus(rq.getStatus());
            }
            if (rq.getStatus() != null && rq.getStatus().equals(Constants.MilestoneGroupStatus.PENDING) && rq.isStudentRequestGrading()) {

                milestoneGroup.setStatus(rq.getStatus());
                Map<String, String> map = new HashMap<>();
                map.put("_GroupName-txt_", g.getName());
                map.put("_MilestoneName-txt_", milestoneGroup.getMilestone().getProduct());
                String content = notificationService.createContentNotification(NotificationTemplate.MilestoneProcessNotification.reqCheckSubmission, map);
                notificationService.saveRequestCheckProgressNotification(g.getSupervisors(), content);
            }
            milestoneGroupRepository.save(milestoneGroup);

        }
    }

    @Override
    public List<SubmitReportResponse> getStatusOfMilestone(int groupId) throws ServiceException {
        List<SubmitReportResponse> submitReportResponses = new ArrayList<>();
        Group g = groupRepository.findById(Long.parseLong(Integer.toString(groupId))).orElseThrow();
        List<MilestoneGroup> milestoneGroup = milestoneGroupRepository.findAllByGroupId(g.getId());
        for (MilestoneGroup m : milestoneGroup
        ) {
            SubmitReportResponse submitReportResponse = new SubmitReportResponse();
            submitReportResponse.setMilestoneId(m.getMilestone().getId());
            submitReportResponse.setStudentRequestGrading(m.isStudentRequestGrading());
            submitReportResponse.setStatus(m.getStatus());
            submitReportResponses.add(submitReportResponse);
        }

        return submitReportResponses;
    }


    public List<Milestone> getTotalSubmissionByRootMilestone(int id) {
        List<Milestone> milestones = milestoneRepository.findAllByParent(id);
        List<Milestone> result = new ArrayList<>();
        for (Milestone m : milestones) {
            if (m.getName() == null) {
                result.add(m);
            } else {
                result.addAll(getTotalSubmissionByRootMilestone(Integer.parseInt(m.getId().toString())));
            }
        }
        return result;
    }

    public static List<GitFolder> getObjectByCallApiToGit(String hrefGit) throws ServiceException {
        try {
            RestTemplate restTemplate = new RestTemplate();

            // Thiết lập header
            HttpHeaders headers = new HttpHeaders();
            // Tạo httpEntity
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(headers);
            ParameterizedTypeReference<List<GitFolder>> typeRef = new ParameterizedTypeReference<List<GitFolder>>() {
            };
            ResponseEntity<List<GitFolder>> response = restTemplate.exchange(
                    hrefGit,
                    HttpMethod.GET,
                    entity,
                    typeRef
            );
            return response.getBody();
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }








}
