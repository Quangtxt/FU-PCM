package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommitteeAssignmentEDTO {
    String groupCode;
    String committee;
    String time;
    String date;
    String note;
}
