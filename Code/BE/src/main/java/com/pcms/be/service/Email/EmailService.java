package com.pcms.be.service.Email;

import com.pcms.be.pojo.DTO.Sender;
import com.pcms.be.pojo.request.EmailRequest;
import com.pcms.be.pojo.request.SendEmailRequest;
import com.pcms.be.pojo.response.EmailResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class EmailService {
    private final EmailClient emailClient;

    public EmailResponse sendEmail(SendEmailRequest request){
        String apiKey = "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";
        Sender sender = new Sender();
        sender.setEmail("boycghd989@gmail.com");
        sender.setName("FU-PCMS Operation");
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setSender(sender);
        emailRequest.setTo(request.getTo());
        emailRequest.setSubject(request.getSubject());
        emailRequest.setHtmlContent(request.getHtmlContent());
        return emailClient.sendEmail(apiKey, emailRequest).getBody();
//        return null;
    }
}
