package com.pcms.be.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface Git {
    interface GitSrc{
        String repositoryTree = "https://gitlab.com/api/v4/projects/_projectId-txt_/repository/tree";
        String repositorySubTree = "https://gitlab.com/api/v4/projects/_projectId-txt_/repository/tree?path=Report/_SubtreeName-txt_";
        String repositoryCommit = "https://gitlab.com/api/v4/projects/_projectId-txt_/repository/commits?since=_Since-txt_&until=_Until-txt_&per_page=200&page=_Page-txt_";
    }
    interface GitNameFile{
        List<String> listNameRootFolder = new ArrayList<>(Arrays.asList("Report","Code"));
        String rootFileRoport = "Report";
    }
}
