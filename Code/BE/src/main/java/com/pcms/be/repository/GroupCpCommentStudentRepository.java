package com.pcms.be.repository;

import com.pcms.be.domain.user.GroupCpCommentStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;

public interface GroupCpCommentStudentRepository extends JpaRepository<GroupCpCommentStudent, Long> {
    List<GroupCpCommentStudent> findByGroupCpCommentId(Long groupCpCommentId);
    Optional<GroupCpCommentStudent> findByGroupCpCommentIdAndStudentId(Long groupCpCommentId, Long studentId);
}

