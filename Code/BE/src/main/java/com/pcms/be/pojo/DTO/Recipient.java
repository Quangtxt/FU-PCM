package com.pcms.be.pojo.DTO;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Recipient {
    private String name;
    private String email;
}
