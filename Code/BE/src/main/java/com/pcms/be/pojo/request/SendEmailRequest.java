package com.pcms.be.pojo.request;

import com.pcms.be.pojo.DTO.Recipient;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SendEmailRequest {
    private List<Recipient> to;
    private String subject;
    private String htmlContent;
}
