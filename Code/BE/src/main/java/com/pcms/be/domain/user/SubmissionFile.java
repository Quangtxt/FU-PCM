package com.pcms.be.domain.user;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "v_submission_files")
public class SubmissionFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id")
    private GroupSubmission submission;

    @Column(name = "filename")
    private String filename;

    @Column(name = "blobId")
    private String blobId;

    @Column(name = "file_type")
    private String fileType;
    @Column(name = "submission_type")
    private String submissionType;

    @Column(name = "file_size")
    private Long fileSize;

    @UpdateTimestamp
    @Column(name = "upload_date")
    private LocalDateTime uploadDate;

    // Getters and Setters
}
