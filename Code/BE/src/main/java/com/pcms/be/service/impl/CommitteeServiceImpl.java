
package com.pcms.be.service.impl;

import com.pcms.be.domain.*;
import com.pcms.be.domain.user.*;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.functions.ValidateData;
import com.pcms.be.pojo.DTO.*;
import com.pcms.be.repository.*;
import com.pcms.be.service.CommitteeService;
import com.pcms.be.service.ExcelService;
import com.pcms.be.service.UserService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class CommitteeServiceImpl implements CommitteeService {
    private final CommitteeRepository committeeRepository;
    private final UserService userService;
    private final CommitteeAssignmentRepository committeeAssignmentRepository;
    private final GroupSubmissionRepository groupSubmissionRepository;
    private final SemesterRepository semesterRepository;
    private final ModelMapper modelMapper;

    @Override
    public ResponseEntity<Map<String, Object>> getCommittees(Pageable pageable, int semesterId) throws ServiceException {
        Semester semester = semesterRepository.findById(Long.parseLong(Integer.toString(semesterId))).orElseThrow(() -> new ServiceException(ErrorCode.SEMESTER_NOT_FOUND));
        Page<Committee> committees = committeeRepository.findAllBySemester(pageable, semester);
        List<CommitteeDTO> committeeDTOS = new ArrayList<>();
        Map<String, Object> result = new HashMap<>();
        for (Committee committee : committees.getContent()) {
            committeeDTOS.add(modelMapper.map(committee, CommitteeDTO.class));
        }
        result.put("totalCount", committees.getTotalElements());
        result.put("totalPage", committees.getTotalElements());
        result.put("data", committeeDTOS);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Map<String, Object>> getCommitteeAssignments(Pageable pageable, int semesterId) {
        Page<CommitteeAssignment> committeeAssignments = committeeAssignmentRepository.findAllBySemesterId(pageable, semesterId);
        List<CommitteeAssignmentDTO> committeeAssignmentDTOS = new ArrayList<>();
        Map<String, Object> result = new HashMap<>();
        for (CommitteeAssignment committeeAssignment : committeeAssignments.getContent()) {
            CommitteeAssignmentDTO dto = modelMapper.map(committeeAssignment, CommitteeAssignmentDTO.class);
            GroupDTO groupDTO = dto.getGroup();
            GroupSubmission submission = groupSubmissionRepository.findByGroupId(groupDTO.getId())
                    .orElseThrow(() -> new RuntimeException("Submission not found"));
            groupDTO.setApprovedBySupervisor(submission.getApprovedBySupervisor());
            dto.setGroup(groupDTO);
            committeeAssignmentDTOS.add(dto);
        }
        result.put("totalCount", committeeAssignments.getTotalElements());
        result.put("totalPage", committeeAssignments.getTotalElements());
        result.put("data", committeeAssignmentDTOS);
        return ResponseEntity.ok(result);
    }

    @Override
    public List<CommitteeAssignmentDTO> getCommitteeAssignmentsByCommittee(int semesterId) throws ServiceException {
        User currentUser  = userService.getCurrentUser();
        Semester semester = semesterRepository.findById(Long.parseLong(Integer.toString(semesterId))).orElseThrow(() -> new ServiceException(ErrorCode.SEMESTER_NOT_FOUND));

        List<Committee> committees= committeeRepository.findAllByEmployeeSemester(currentUser.getId(),semester.getId());
        List<Long> committeeIds = committees.stream()
                .map(Committee::getId)
                .collect(Collectors.toList());

        List<CommitteeAssignment> committeeAssignments = committeeAssignmentRepository.findAllByCommittees(committeeIds);
        List<CommitteeAssignmentDTO> committeeAssignmentDTOS = committeeAssignments.stream()
                .map(s -> modelMapper.map(s, CommitteeAssignmentDTO.class))
                .collect(Collectors.toList());
        return  committeeAssignmentDTOS;
    }

    @Override
    public boolean checkIsCommittee(Long userId) throws ServiceException {
        Optional<Semester> currentSemester = semesterRepository.findByCurrent(OffsetDateTime.now());
        if (!currentSemester.isPresent()) return false;
        Semester semester = currentSemester.get();
        List<Committee> c = committeeRepository.findAllByEmployeeSemester(userId, semester.getId());
        return !c.isEmpty();
    }
}
