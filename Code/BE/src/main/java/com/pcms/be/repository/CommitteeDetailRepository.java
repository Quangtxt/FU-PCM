package com.pcms.be.repository;

import com.pcms.be.domain.Committee;
import com.pcms.be.domain.CommitteeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommitteeDetailRepository extends JpaRepository<CommitteeDetail, Long> {
}
