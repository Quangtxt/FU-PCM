package com.pcms.be.pojo.DTO;

import com.pcms.be.domain.Milestone;
import com.pcms.be.domain.Semester;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportDTO {
    public Long id;
    private String name;
    private MilestoneDTO milestone;
    private MilestoneDTO milestoneSubmit;
    private Double percentScore;
    private SemesterDTO semester;
    public OffsetDateTime possibleDate;
}
