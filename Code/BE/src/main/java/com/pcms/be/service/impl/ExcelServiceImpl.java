package com.pcms.be.service.impl;

import com.pcms.be.domain.*;
import com.pcms.be.domain.user.*;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.functions.ValidateData;
import com.pcms.be.pojo.DTO.*;
import com.pcms.be.repository.*;
import com.pcms.be.service.ExcelService;
import com.pcms.be.service.UserService;
import com.pcms.be.utils.StringUtils;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.pcms.be.service.impl.GroupServiceImpl.generateRandomString;


@Service
@RequiredArgsConstructor
@Slf4j
public class ExcelServiceImpl implements ExcelService {
    private final MemberRepository memberRepository;
    private final GroupRepository groupRepository;
    private final CommitteeRepository committeeRepository;
    private final CommitteeAssignmentRepository committeeAssignmentRepository;
    private final SemesterRepository semesterRepository;
    public static final List<String> formatExcel_listStudent = new ArrayList<>(List.of("RollNumber", "Email", "MemberCode", "FullName", "Status", "Note"));
    private final UserRepository userRepository;
    private final UserService userService;
    private final ValidateData validateData;
    private final RoleRepository roleRepository;
    private final StudentRepository studentRepository;
    private final SupervisorRepository supervisorRepository;
    private final SpecificMajorRepository specificMajorRepository;


    @Override
    public List<ExcelStudentDTO> getStudentsFromFile(MultipartFile file) throws IOException, ServiceException {
        User user = null;
        if (!validateData.checkFormatExcel(file, formatExcel_listStudent)) {
            throw new ServiceException(ErrorCode.EXCEL_IS_NOT_IN_THE_CORRECT_FORMAT);
        }
        try {
            user = userService.getCurrentUser();
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }
        List<ExcelStudentDTO> data = new ArrayList<>();
        if (file.isEmpty()) {
            return data;
        }

        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Campus campus = user.getCampus();
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            String rollNumber = sheet.getRow(i).getCell(0).getStringCellValue();
            String memberCode = sheet.getRow(i).getCell(2).getStringCellValue();
            String fullName = sheet.getRow(i).getCell(3).getStringCellValue();
            String email = sheet.getRow(i).getCell(1).getStringCellValue();
            String userName = email.split("@")[0];

//userRepository.findByUsernameIgnoreCase(userName).isEmpty() && userRepository.findByEmail(email).isEmpty() && validateData.isValidEmail(email)
            ExcelStudentDTO excelStudentDTO = new ExcelStudentDTO();
            excelStudentDTO.setUsername(userName);
            excelStudentDTO.setFullName(fullName);
            excelStudentDTO.setEmail(email);
            excelStudentDTO.setMemberCode(memberCode);
            excelStudentDTO.setRollNumber(rollNumber);
            String note = "";
            if (userRepository.findByEmail(email).isPresent()) {
                note = note.concat("Email: " + email + " already exists.");
            }
            if (!validateData.isValidEmail(email)) {
                note = note.concat("Email: " + email + " is not in correct format.");
            }
            if (!validateData.validateFPTEmail(email)){
                note = note.concat("Email: " + email + " is not FPT email.");
            }
            ExcelStudentDTO checkDuplicateEmail = data.stream()
                            .filter(dto -> dto.getEmail().equalsIgnoreCase(email))
                                    .findFirst().orElse(null);
            if (checkDuplicateEmail!= null){
                String checkDuplicateEmailNote = checkDuplicateEmail.getNote().concat("Email: " + email + "is duplicate. ");
                checkDuplicateEmail.setNote(checkDuplicateEmailNote);
                note = note.concat("Email: " + email + "is duplicate. ");
            }

            data.add(excelStudentDTO);
            excelStudentDTO.setNote(note);
        }
        return data;
    }

    @Override
    public List<ExcelSupervisorDTO> getSupervisorsFromFile(MultipartFile file) throws IOException, ServiceException {
        User user = null;
        try {
            user = userService.getCurrentUser();
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }
        List<ExcelSupervisorDTO> data = new ArrayList<>();
        if (file.isEmpty()) {
            return data;
        }
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Campus campus = user.getCampus();
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

            String emailFE = sheet.getRow(i).getCell(8).getStringCellValue();
            String emailFPT = sheet.getRow(i).getCell(7).getStringCellValue();
            String userName = emailFPT.split("@")[0];
            String fullName = sheet.getRow(i).getCell(1).getStringCellValue();
            String phoneNumber = sheet.getRow(i).getCell(9).getStringCellValue();
            String genderTxt = sheet.getRow(i).getCell(2).getStringCellValue();
            String branch = sheet.getRow(i).getCell(3).getStringCellValue();
            String parentDepartment = sheet.getRow(i).getCell(4).getStringCellValue();
            String childDepartment = sheet.getRow(i).getCell(5).getStringCellValue();
            String jobTitle = sheet.getRow(i).getCell(6).getStringCellValue();
            String contractType = sheet.getRow(i).getCell(10).getStringCellValue();
            String emId = sheet.getRow(i).getCell(0).getStringCellValue();


            //Doi van Quang tao bang Staff de lay Campus

//            userRepository.findByUsernameIgnoreCase(userName).isEmpty() && userRepository.findByEmail(emailFE).isEmpty()
//                    && validateData.isValidEmail(emailFE) && validateData.isValidPhoneNumber(phoneNumber) && validateData.isValidGender(genderTxt)
            ExcelSupervisorDTO excelSupervisorDTO = new ExcelSupervisorDTO();
            excelSupervisorDTO.setEmailFE(emailFE);
            excelSupervisorDTO.setEmailFPT(emailFPT);
            excelSupervisorDTO.setUserName(userName);
            excelSupervisorDTO.setFullName(fullName);
            excelSupervisorDTO.setPhoneNumber(phoneNumber);
            excelSupervisorDTO.setGenderTxt(genderTxt);
            excelSupervisorDTO.setBranch(branch);
            excelSupervisorDTO.setParentDepartment(parentDepartment);
            excelSupervisorDTO.setChildDepartment(childDepartment);
            excelSupervisorDTO.setJobTitle(jobTitle);
            excelSupervisorDTO.setContractType(contractType);
            excelSupervisorDTO.setEmId(Double.parseDouble(emId));
            String note = "";
            if (userRepository.findByEmail(emailFE).isPresent()) {
                note = note.concat("Email: " + emailFE + " already exists.");
            }
            if (!validateData.isValidEmail(emailFE)) {
                note = note.concat("Email: " + emailFE + " is not in correct format.");
            }

            if (!validateData.isValidPhoneNumber(phoneNumber)) {
                note = note.concat("Phone: " + phoneNumber + " is not in correct format.");
            }
            if (!validateData.isValidGender(genderTxt)) {
                note = note.concat("Gender is not in correct format.");
            }
            ExcelSupervisorDTO checkDuplicateEmail = data.stream()
                    .filter(dto -> dto.getEmailFE().equalsIgnoreCase(emailFE))
                    .findFirst().orElse(null);
            if (checkDuplicateEmail!= null){
                String checkDuplicateEmailNote = checkDuplicateEmail.getNote().concat("Email: " + emailFE + "is duplicate. ");
                checkDuplicateEmail.setNote(checkDuplicateEmailNote);
                note = note.concat("Email: " + emailFE + " is duplicate. ");
            }

            excelSupervisorDTO.setNote(note);
            data.add(excelSupervisorDTO);
        }
        return data;
    }

    @Override
    public List<ExcelGroupDTO> getGroupFromFile(MultipartFile file) throws IOException, ServiceException {
        List<ExcelGroupDTO> data = new ArrayList<>();
        if (file.isEmpty()) {
            return data;
        }
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

            String group = sheet.getRow(i).getCell(0).getStringCellValue();
            String rollNumber = sheet.getRow(i).getCell(1).getStringCellValue();
            String email = sheet.getRow(i).getCell(2).getStringCellValue();
            String memberCode = sheet.getRow(i).getCell(3).getStringCellValue();
            String fullName = sheet.getRow(i).getCell(4).getStringCellValue();

            ExcelGroupDTO excelGroupDTO = new ExcelGroupDTO();
            excelGroupDTO.setGroup(group);
            excelGroupDTO.setRollNumber(rollNumber);
            excelGroupDTO.setEmail(email);
            excelGroupDTO.setMemberCode(memberCode);
            excelGroupDTO.setFullName(fullName);
            data.add(excelGroupDTO);
        }

        Set<String> frequentNameSet = getFrequentName(data);
        for (ExcelGroupDTO excelGroupDTO : data){
            String note = "";
            if (userRepository.findByEmail(excelGroupDTO.getEmail()).isPresent()) {
                note = note.concat("Email: " + excelGroupDTO.getEmail() + " already exists.");
            }
            if (!validateData.isValidEmail(excelGroupDTO.getEmail())) {
                note = note.concat("Email: " + excelGroupDTO.getEmail() + " is not in correct format.");
            }
            if (frequentNameSet.contains(excelGroupDTO.getGroup())){
                note = note.concat("Group have total member bigger than 6.");
            }
            excelGroupDTO.setNote(note);
        }

        return data;
    }
    public Set<String> getFrequentName(List<ExcelGroupDTO> data){
        return data.stream()
                .collect(Collectors.toMap(
                        ExcelGroupDTO::getGroup,
                        excelGroupDTO -> 1L,
                        Long::sum
                ))
                .entrySet().stream()
                .filter(entry -> entry.getValue() > 6)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    @Override
    public ExcelCommitteeDTO getCommitteeFromFile(MultipartFile file) throws IOException, ServiceException {
        ExcelCommitteeDTO data = new ExcelCommitteeDTO();
        List<CommitteeEDTO> data1 = new ArrayList<>();
        List<CommitteeAssignmentEDTO> data2 = new ArrayList<>();
        if (file.isEmpty()) {
            return data;
        }
        Semester currentSemester = semesterRepository.findByCurrent(OffsetDateTime.now()).orElseThrow(() -> new ServiceException(ErrorCode.SEMESTER_NOT_FOUND_BY_CURRENT));
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

            String committeeName = sheet.getRow(i).getCell(0).getStringCellValue();
            String employeeName = sheet.getRow(i).getCell(1).getStringCellValue();
            String email = sheet.getRow(i).getCell(2).getStringCellValue();
            String mission = sheet.getRow(i).getCell(3).getStringCellValue();
            String committeeCode = sheet.getRow(i).getCell(4).getStringCellValue();

            CommitteeEDTO committeeEDTO = new CommitteeEDTO();
            committeeEDTO.setCommitteeName(committeeName);
            committeeEDTO.setEmployeeName(employeeName);
            committeeEDTO.setEmail(email);
            committeeEDTO.setMission(mission);
            committeeEDTO.setCommitteeCode(committeeCode);
            String note = "";
            if (!userRepository.findByEmail(email).isPresent()) {
                note = note.concat("Email: " + email + "does not exist.");
            }
            committeeEDTO.setNote(note);
            data1.add(committeeEDTO);
        }
        Sheet sheet1 = workbook.getSheetAt(1);
        for (int i = 1; i < sheet1.getPhysicalNumberOfRows(); i++) {

            String groupCode = sheet1.getRow(i).getCell(0).getStringCellValue();
            String committee = sheet1.getRow(i).getCell(1).getStringCellValue();
            String time = sheet1.getRow(i).getCell(2).getStringCellValue();
            String date = sheet1.getRow(i).getCell(3).getStringCellValue();

            CommitteeAssignmentEDTO committeeAssignmentEDTO = new CommitteeAssignmentEDTO();
            committeeAssignmentEDTO.setGroupCode(groupCode);
            committeeAssignmentEDTO.setCommittee(committee);
            committeeAssignmentEDTO.setTime(time);
            committeeAssignmentEDTO.setDate(date);
            String note = "";
            if (!groupRepository.findByGroupCodeAndSemester(groupCode,currentSemester).isPresent()) {
                note = note.concat("Group: " + groupCode + " does not exist.");
            }
            committeeAssignmentEDTO.setNote(note);
            data2.add(committeeAssignmentEDTO);
        }
        data.setCommittees(data1);
        data.setCommitteeAssignments(data2);
        return data;
    }

    @Override
    public void saveStudents(List<ExcelStudentDTO> data) throws ServiceException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        User user = null;
        try {
            user = userService.getCurrentUser();
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }
        Campus campus = user.getCampus();
        for (ExcelStudentDTO s : data) {
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName(Constants.RoleConstants.STUDENT).orElseThrow());
            User newUser = new User();
            newUser.setUsername(s.getUsername());
            newUser.setName(s.getFullName());
            newUser.setEmail(s.getEmail());
            newUser.setStatus(true);
            newUser.setIsAdmin(false);
            newUser.setRoles(roles);
            newUser.setCampus(campus);
            userRepository.save(newUser);

            Student newStudent = new Student();
            newStudent.setUser(newUser);
            newStudent.setSpecificMajor(specificMajorRepository.findById(1L).orElseThrow());
            studentRepository.save(newStudent);
        }
    }

    @Override
    public void saveSupervisors(List<ExcelSupervisorDTO> data) throws ServiceException {
        User user = null;
        try {
            user = userService.getCurrentUser();
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }
        Campus campus = user.getCampus();
        for (ExcelSupervisorDTO m : data) {
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName(Constants.RoleConstants.SUPERVISOR).orElseThrow());
            User newUser = new User();
            newUser.setUsername(m.getUserName());
            newUser.setName(m.getFullName());
            newUser.setEmail(m.getEmailFE());
            newUser.setStatus(true);
            newUser.setIsAdmin(false);
            newUser.setRoles(roles);
            newUser.setCampus(campus);
            userRepository.save(newUser);
            Supervisor supervisor = new Supervisor();
            supervisor.setUser(newUser);
            boolean gender = m.getGenderTxt().trim().equalsIgnoreCase("m");
            String phone = m.getPhoneNumber();
            supervisor.setGender(gender);
            supervisor.setPhone(phone);
            supervisorRepository.save(supervisor);
        }
    }

    @Override
    @Transactional
    public void saveGroups(List<ExcelGroupDTO> data) throws ServiceException {
        Set<String> identifyGroupCodeSet = data.stream()
                .map(ExcelGroupDTO::getGroup)
                .collect(Collectors.toSet());

        OffsetDateTime now = OffsetDateTime.now();
        Optional<Semester> semester = semesterRepository.findByCurrent(now);
        if (semester.isEmpty()) {
            throw new ServiceException(ErrorCode.SEMESTER_NOT_FOUND_BY_CURRENT);
        } else {
            for (String groupName : identifyGroupCodeSet){
                List<ExcelGroupDTO> excelGroupDTOS = data.stream()
                        .filter(excelGroupDTO -> groupName.equals(excelGroupDTO.getGroup()))
                        .toList();
                Group group = new Group();
                group.setName(groupName);
                group.setGroupCode("SEP490_"+semester.get().getCode()+"_"+group.getId()+"_"+generateRandomString());
                group.setSemester(semester.get());
                groupRepository.save(group);
                for (ExcelGroupDTO excelGroupDTO : excelGroupDTOS){
                    Optional<Student> student = studentRepository.findByEmail(excelGroupDTO.getEmail());
                    Member member = new Member();
                    member.setRole(Constants.MemberRole.MEMBER);
                    member.setGroup(group);
                    member.setStatus(Constants.MemberStatus.INGROUP);
                    student.ifPresent(member::setStudent);
                    memberRepository.save(member);
                }
            }
        }
    }

    @Override
    @Transactional
    public void saveCommittees(ExcelCommitteeDTO data) throws ServiceException {
        Semester currentSemester = semesterRepository.findByCurrent(OffsetDateTime.now())
                .orElseThrow(() -> new ServiceException(ErrorCode.SEMESTER_NOT_FOUND_BY_CURRENT));

        Map<String, Committee> committeeMap = new HashMap<>();

        for (CommitteeEDTO committeeDTO : data.getCommittees()) {
            Committee committee = committeeMap.get(committeeDTO.getCommitteeCode());

            if (committee == null) {
                committee = committeeRepository.findByCommitteeCodeAndSemester(committeeDTO.getCommitteeCode(), currentSemester)
                        .orElse(null);

                if (committee == null) {
                    committee = new Committee();
                    committee.setCommitteeCode(committeeDTO.getCommitteeCode());
                    committee.setCommitteeName(committeeDTO.getCommitteeName());
                    committee.setSemester(currentSemester);
                    committeeRepository.save(committee);
                }
                committeeMap.put(committeeDTO.getCommitteeCode(), committee);
            }

            CommitteeDetail committeeDetail = new CommitteeDetail();
            committeeDetail.setCommittee(committee);

            User employee = userRepository.findByEmail(committeeDTO.getEmail())
                    .orElseThrow(() -> new ServiceException(ErrorCode.USER_NOT_FOUND));

            committeeDetail.setEmployee(employee);
            committeeDetail.setMission(committeeDTO.getMission());

            committee.getCommitteeDetails().add(committeeDetail);
        }

        committeeRepository.saveAll(committeeMap.values());

        // Lưu các committee assignment
        List<CommitteeAssignment> assignments = new ArrayList<>();
        for (CommitteeAssignmentEDTO assignmentDTO : data.getCommitteeAssignments()) {
            CommitteeAssignment assignment = new CommitteeAssignment();
            assignment.setCommittee(committeeMap.values().stream()
                    .filter(c -> c.getCommitteeCode().equals(assignmentDTO.getCommittee()))
                    .findFirst()
                    .orElseThrow(() -> new ServiceException(ErrorCode.COMMITTEE_NOT_FOUND)));
            assignment.setGroup(groupRepository.findByGroupCodeAndSemester(assignmentDTO.getGroupCode(), currentSemester)
                    .orElseThrow(() -> new ServiceException(ErrorCode.GROUP_NOT_FOUND)));
            assignment.setTime(assignmentDTO.getTime());
            assignment.setDate(assignmentDTO.getDate());
            assignments.add(assignment);
        }
        committeeAssignmentRepository.saveAll(assignments);
    }


    @Override
    public void downloadTemplateStudent(HttpServletResponse response) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Student");


        CellStyle boldCellStyle = workbook.createCellStyle();
        Font boldFont = workbook.createFont();
        boldFont.setBold(true);
        boldCellStyle.setFont(boldFont);
        boldCellStyle.setAlignment(HorizontalAlignment.CENTER);

        Row headerRow = sheet.createRow(0);
        headerRow.setRowStyle(boldCellStyle);

        Cell cell = headerRow.createCell(0);
        cell.setCellValue("RollNumber");
        cell = headerRow.createCell(1);
        cell.setCellValue("Email");
        cell = headerRow.createCell(2);
        cell.setCellValue("MemberCode");
        cell = headerRow.createCell(3);
        cell.setCellValue("FullName");
        cell = headerRow.createCell(4);
        cell.setCellValue("Status");
        cell = headerRow.createCell(5);
        cell.setCellValue("Note");

        // Set the values of cells row 2
        Row mergedRow = sheet.createRow(1);
        mergedRow.setRowStyle(boldCellStyle);

        // Thiết lập header và body của response
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=sample-data.xlsx");

        // Ghi workbook vào response
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @Override
    public void downloadTemplateSupervisor(HttpServletResponse response) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Sheet 1");


        CellStyle boldCellStyle = workbook.createCellStyle();
        Font boldFont = workbook.createFont();
        boldFont.setBold(true);
        boldCellStyle.setFont(boldFont);
        boldCellStyle.setAlignment(HorizontalAlignment.CENTER);

        Row headerRow = sheet.createRow(0);
        headerRow.setRowStyle(boldCellStyle);

        Cell cell = headerRow.createCell(0);
        cell.setCellValue("Empl_ID");
        cell = headerRow.createCell(1);
        cell.setCellValue("Name");
        cell = headerRow.createCell(2);
        cell.setCellValue("Gender");
        cell = headerRow.createCell(3);
        cell.setCellValue("Branch");
        cell = headerRow.createCell(4);
        cell.setCellValue("Parent Department");
        cell = headerRow.createCell(5);
        cell.setCellValue("Child Department");
        cell = headerRow.createCell(6);
        cell.setCellValue("Job title");
        cell = headerRow.createCell(7);
        cell.setCellValue("Email FPT");
        cell = headerRow.createCell(8);
        cell.setCellValue("Email FE");
        cell = headerRow.createCell(9);
        cell.setCellValue("Telephone");
        cell = headerRow.createCell(10);
        cell.setCellValue("Contract type");

        // Set the values of cells row 2
        Row mergedRow = sheet.createRow(1);
        mergedRow.setRowStyle(boldCellStyle);

        // Thiết lập header và body của response
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=sample-data.xlsx");

        // Ghi workbook vào response
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @Override
    public void downloadTemplateGroup(HttpServletResponse response) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Sheet 1");


        CellStyle boldCellStyle = workbook.createCellStyle();
        Font boldFont = workbook.createFont();
        boldFont.setBold(true);
        boldCellStyle.setFont(boldFont);
        boldCellStyle.setAlignment(HorizontalAlignment.CENTER);

        Row headerRow = sheet.createRow(0);
        headerRow.setRowStyle(boldCellStyle);

        Cell cell = headerRow.createCell(0);
        cell.setCellValue("Class");
        cell = headerRow.createCell(1);
        cell.setCellValue("RollNumber");
        cell = headerRow.createCell(2);
        cell.setCellValue("Email");
        cell = headerRow.createCell(3);
        cell.setCellValue("MemberCode");
        cell = headerRow.createCell(4);
        cell.setCellValue("FullName");

        Row mergedRow = sheet.createRow(1);
        mergedRow.setRowStyle(boldCellStyle);

        // Thiết lập header và body của response
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=sample-data.xlsx");

        // Ghi workbook vào response
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @Override
    public void downloadTemplateCommittee(HttpServletResponse response) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Committee");
        Sheet sheet1 = workbook.createSheet("Committee assignment");

        CellStyle boldCellStyle = workbook.createCellStyle();
        Font boldFont = workbook.createFont();
        boldFont.setBold(true);
        boldCellStyle.setFont(boldFont);
        boldCellStyle.setAlignment(HorizontalAlignment.CENTER);

        Row headerRow = sheet.createRow(0);
        headerRow.setRowStyle(boldCellStyle);

        Cell cell = headerRow.createCell(0);
        cell.setCellValue("Committee name");
        cell = headerRow.createCell(1);
        cell.setCellValue("Employee name");
        cell = headerRow.createCell(2);
        cell.setCellValue("Email");
        cell = headerRow.createCell(3);
        cell.setCellValue("Mission");
        cell = headerRow.createCell(4);
        cell.setCellValue("Committee code");

//        Row mergedRow = sheet.createRow(1);
//        mergedRow.setRowStyle(boldCellStyle);

        Row headerRow1 = sheet1.createRow(0);
        headerRow1.setRowStyle(boldCellStyle);

        Cell cell1 = headerRow1.createCell(0);
        cell1.setCellValue("Group");
        cell1 = headerRow1.createCell(1);
        cell1.setCellValue("Committee");
        cell1 = headerRow1.createCell(2);
        cell1.setCellValue("Time");
        cell1 = headerRow1.createCell(3);
        cell1.setCellValue("Date");

        Row mergedRow1 = sheet1.createRow(1);
        mergedRow1.setRowStyle(boldCellStyle);
        // Thiết lập header và body của response
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=sample-data.xlsx");

        // Ghi workbook vào response
        workbook.write(response.getOutputStream());
        workbook.close();
    }
}
