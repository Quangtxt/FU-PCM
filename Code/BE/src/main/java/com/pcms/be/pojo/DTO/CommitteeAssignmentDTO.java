package com.pcms.be.pojo.DTO;

import com.pcms.be.domain.Committee;
import com.pcms.be.domain.user.Group;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommitteeAssignmentDTO {
    Long id;
    CommitteeDTO committee;
    GroupDTO group;
    String time;
    String date;
}
