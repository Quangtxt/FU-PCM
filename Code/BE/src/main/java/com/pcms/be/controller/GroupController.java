package com.pcms.be.controller;

import com.google.cloud.storage.Blob;
import com.pcms.be.domain.CommitteeAssignment;
import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.GroupSubmission;
import com.pcms.be.domain.user.SubmissionFile;
import com.pcms.be.errors.ApiException;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.pojo.DTO.CommitteeAssignmentDTO;
import com.pcms.be.pojo.DTO.CommitteeAssignmentEDTO;
import com.pcms.be.pojo.DTO.SubmissionDTO;
import com.pcms.be.pojo.request.*;
import com.pcms.be.pojo.response.GroupResponse;
import com.pcms.be.pojo.response.MemberResponse;
import com.pcms.be.pojo.response.StudentResponse;
import com.pcms.be.pojo.response.SubmitGroupResponse;
import com.pcms.be.repository.CommitteeAssignmentRepository;
import com.pcms.be.repository.GroupRepository;
import com.pcms.be.repository.GroupSubmissionRepository;
import com.pcms.be.repository.SubmissionFileRepository;
import com.pcms.be.service.*;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@CrossOrigin(origins = "*")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/group")

public class GroupController {
    private final GroupService groupService;
    private final SubmissionService submissionService;
    private final GroupRepository groupRepository;
    private final GroupSubmissionRepository groupSubmissionRepository;
    private final CommitteeService committeeService;
    private final StudentService studentService;
    private final MemberService memberService;
    private final SubmissionFileRepository submissionFileRepository;
    private final GoogleCloudStorageService googleCloudStorageService;
    private final SimpMessagingTemplate messagingTemplate;

    @PostMapping("/create")
    public ResponseEntity<GroupResponse> createGroup(@Valid @RequestBody CreateGroupRequest createGroupDTO) {
        try {
            GroupResponse createdGroup = groupService.createGroup(createGroupDTO);
            messagingTemplate.convertAndSend("/topic/manageGroup", createdGroup);
            return ResponseEntity.ok(createdGroup);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<GroupResponse> editGroup(@Valid @RequestBody EditGroupRequest editGroupDTO) {
        try {
            GroupResponse editGroup = groupService.editGroup(editGroupDTO);
            return ResponseEntity.ok(editGroup);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/student/invite/allow")
    public ResponseEntity<List<StudentResponse>> getListStudentToInvite() {
        List<StudentResponse> studentResponseList = studentService.getAllStudentToInvite();
        return ResponseEntity.ok(studentResponseList);
    }

    @GetMapping("/invitations")
    public ResponseEntity<List<MemberResponse>> getListInvitationToJoinGroup() {
        try {
            List<MemberResponse> invitation = memberService.getInvitations();
            return ResponseEntity.ok(invitation);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PostMapping("/update/invitation/status")
    public ResponseEntity<MemberResponse> updateInvitationStatus(@Valid @RequestBody UpdateInvitationStatusRequest updateInvitationStatusRequest) {
        try {
            MemberResponse member = memberService.updateStatus(updateInvitationStatusRequest);
            messagingTemplate.convertAndSend("/topic/manageGroup", member);
            messagingTemplate.convertAndSend("/topic/notification", member);
            return ResponseEntity.ok(member);

        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/view")
    public ResponseEntity<GroupResponse> getGroupById(@RequestParam int groupId) {
        try {
            GroupResponse groupResponse = groupService.getGroupById(groupId);
            groupResponse.setMembers(memberService.getGroupMember(groupId));
            return ResponseEntity.ok(groupResponse);

        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/supervisor/view")
    public ResponseEntity<List<GroupResponse>> getGroupsBySupervisor(@RequestParam int supervisorId,@RequestParam int semesterId) {
        try {
            List<GroupResponse> groupResponse = groupService.getGroupsBySupervisor(supervisorId,semesterId);
            return ResponseEntity.ok(groupResponse);

        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }
    @GetMapping("/committee/view")
    public ResponseEntity<List<CommitteeAssignmentDTO>> getGroupsByCommittee(@RequestParam int semesterId) {
        try {
            List<CommitteeAssignmentDTO> allByCommitteeId = committeeService.getCommitteeAssignmentsByCommittee(semesterId);
            return ResponseEntity.ok(allByCommitteeId);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/view-group")
    public ResponseEntity<GroupResponse> getGroupByMemberId() {
        try {
            GroupResponse groupResponse = groupService.getGroupByMemberId();
            groupResponse.setMembers(memberService.getGroupMemberIncludePending(Integer.parseInt(Long.toString(groupResponse.getId()))));
            return ResponseEntity.ok(groupResponse);

        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @GetMapping("/delete")
    public ResponseEntity<Void> Delete(@RequestParam int groupId) {

        Group group = groupRepository.findById(Long.valueOf(groupId))
                .orElseThrow(() -> new IllegalArgumentException("Group not found with id: " + groupId));

        groupRepository.delete(group);
        return ResponseEntity.noContent().build();


    }

    @PostMapping("inviteMember")
    public ResponseEntity<List<MemberResponse>> inviteMember(@Valid @RequestBody InviteMemberRequest inviteMemberRequest) {
        try {
            List<MemberResponse> memberResponse = memberService.inviteMember(inviteMemberRequest);
            messagingTemplate.convertAndSend("/topic/manageGroup", memberResponse);
            messagingTemplate.convertAndSend("/topic/notification", memberResponse);
            return ResponseEntity.ok(memberResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PostMapping("submitGroup")
    public ResponseEntity<SubmitGroupResponse> submitGroup(@Valid @RequestBody SubmitGroupRequest submitGroupRequest) {
        try {
            SubmitGroupResponse submitGroupResponse = groupService.submitGroup(submitGroupRequest);
            messagingTemplate.convertAndSend("/topic/submitGroup", submitGroupResponse);
            return ResponseEntity.ok(submitGroupResponse);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PostMapping("/empowerOwner")
    public ResponseEntity<List<MemberResponse>> empowerOwner(@RequestParam int groupId, @RequestParam int studentId) {
        try {
            List<MemberResponse> memberResponses = memberService.empowerOwner(groupId, studentId);
            messagingTemplate.convertAndSend("/topic/empowerOwner", memberResponses);
            return ResponseEntity.ok(memberResponses);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PostMapping("/addGit")
    public ResponseEntity<String> addGit(@RequestParam String gitId, @RequestParam int groupId) throws ServiceException{
        try {
            return groupService.addGit(groupId, gitId);
        } catch (ServiceException e) {
            throw new ApiException(e.getErrorCode(), e.getParams());
        }
    }

    @PostMapping("/{groupId}/submission/final")
    public ResponseEntity<String> uploadFiles(@PathVariable Long groupId,
                                                            @RequestParam("type") String type,
                                                            @RequestParam("files") List<MultipartFile> files) throws IOException {
       submissionService.uploadFiles(files, groupId, type);
        return ResponseEntity.ok("Upload file successfully");
    }
    @GetMapping ("/{groupId}/submission/final")
    public ResponseEntity<SubmissionDTO> getSubmissionByGroup(@PathVariable Long groupId) throws IOException {
       SubmissionDTO submissionDTO = submissionService.getSubmissionByGroup(groupId);
        return ResponseEntity.ok(submissionDTO);
    }
    @GetMapping ("/submission/final/{semesterId}")
    public ResponseEntity<List<SubmissionDTO>> getGroupSubmissionBySemester(@PathVariable Long semesterId) throws IOException, ServiceException {
       List<SubmissionDTO> submissionDTO = submissionService.getGroupSubmissionBySemester(semesterId);
        return ResponseEntity.ok(submissionDTO);
    }
    @DeleteMapping("/file/{submissionFileId}")
    public ResponseEntity<String> deleteFile(@PathVariable Long submissionFileId) throws IOException, ServiceException {
        submissionService.deleteFile(submissionFileId);
        return ResponseEntity.ok("File deleted successfully");
    }
    @GetMapping("/download/file/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long fileId, HttpServletResponse response) throws IOException {
        SubmissionFile submissionFile = submissionFileRepository.findById(fileId)
                .orElseThrow(() -> new RuntimeException("Submission not found"));

        String blobId = submissionFile.getBlobId();
        Blob blob = googleCloudStorageService.getFile(blobId);
        byte[] content = blob.getContent();
        if (content == null || content.length == 0) {
            throw new RuntimeException("No content available in Blob.");
        }
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(blob.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + submissionFile.getFilename() + "\"")
                .body(new ByteArrayResource((blob.getContent())));
    }
    @GetMapping("/download/all/{groupId}")
    public ResponseEntity<Resource> downloadGroupFiles(@PathVariable Long groupId) throws IOException {
        GroupSubmission groupSubmission = groupSubmissionRepository.findByGroupId(groupId) .orElseThrow(() -> new RuntimeException("Submission not found"));

        List<SubmissionFile> submissionFiles = groupSubmission.getFiles();
        if (submissionFiles.isEmpty()) {
            throw new RuntimeException("No files found for the specified group.");
        }
        ByteArrayOutputStream zipOutputStream = new ByteArrayOutputStream();
        try (ZipOutputStream zipStream = new ZipOutputStream(zipOutputStream)) {
            Map<String, List<SubmissionFile>> filesByType = submissionFiles.stream()
                    .collect(Collectors.groupingBy(SubmissionFile::getSubmissionType));
            for (Map.Entry<String, List<SubmissionFile>> entry : filesByType.entrySet()) {
                String folderName = entry.getKey();
                List<SubmissionFile> files = entry.getValue();
                for (SubmissionFile file : files) {
                    String blobId = file.getBlobId();
                    Blob blob = googleCloudStorageService.getFile(blobId);
                    byte[] content = blob.getContent();
                    if (content != null && content.length > 0) {
                        ZipEntry zipEntry = new ZipEntry(folderName + "/" + file.getFilename());
                        zipStream.putNextEntry(zipEntry);
                        zipStream.write(content);
                        zipStream.closeEntry();
                    }
                }
            }
        }
        ByteArrayResource resource = new ByteArrayResource(zipOutputStream.toByteArray());
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/zip"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+groupSubmission.getGroup().getGroupCode()+"_files.zip\"")
                .body(resource);
    }
    @GetMapping("/download/all/{groupId}/{type}")
    public ResponseEntity<Resource> downloadGroupFilesByType(@PathVariable Long groupId, @PathVariable String type) throws IOException {
        GroupSubmission groupSubmission = groupSubmissionRepository.findByGroupId(groupId)
                .orElseThrow(() -> new RuntimeException("Submission not found"));
        List<SubmissionFile> submissionFiles = groupSubmission.getFiles().stream()
                .filter(file -> type.equals(file.getSubmissionType()))
                .collect(Collectors.toList());
        if (submissionFiles.isEmpty()) {
            throw new RuntimeException("No files found for the specified type: " + type);
        }
        ByteArrayOutputStream zipOutputStream = new ByteArrayOutputStream();
        try (ZipOutputStream zipStream = new ZipOutputStream(zipOutputStream)) {
            for (SubmissionFile file : submissionFiles) {
                String blobId = file.getBlobId();
                Blob blob = googleCloudStorageService.getFile(blobId);
                byte[] content = blob.getContent();
                if (content != null && content.length > 0) {
                    ZipEntry zipEntry = new ZipEntry(file.getFilename());
                    zipStream.putNextEntry(zipEntry);
                    zipStream.write(content);
                    zipStream.closeEntry();
                }
            }
        }
        ByteArrayResource resource = new ByteArrayResource(zipOutputStream.toByteArray());
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/zip"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + groupSubmission.getGroup().getGroupCode() + "_" + type + "_files.zip\"")
                .body(resource);
    }
   @PutMapping ("/confirm/{groupId}")
    public ResponseEntity<String> confirmGroupSubmission(@PathVariable Long groupId,@RequestBody String status) throws IOException, ServiceException {
        submissionService.confirmGroupSubmission(groupId,status);
        return ResponseEntity.ok("Successfully");
    }
}
