package com.pcms.be.domain;

import com.pcms.be.domain.user.Group;
import com.pcms.be.domain.user.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "committee_assignment")
public class CommitteeAssignment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "committee_id", nullable = false)
    private Committee committee;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    private Group group ;

    @Column(name = "time")
    private String time;

    @Column(name = "date")
    private String date;
}
