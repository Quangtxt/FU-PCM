package com.pcms.be.service.impl;

import com.pcms.be.domain.meeting.Meeting;
import com.pcms.be.domain.user.*;
import com.pcms.be.errors.ErrorCode;
import com.pcms.be.errors.ServiceException;
import com.pcms.be.functions.Constants;
import com.pcms.be.functions.MailTemplate;
import com.pcms.be.functions.NotificationTemplate;
import com.pcms.be.pojo.DTO.MeetingDTO;
import com.pcms.be.pojo.DTO.Recipient;
import com.pcms.be.pojo.request.CreateMeetingRequest;
import com.pcms.be.pojo.request.EditMeetingRequest;
import com.pcms.be.pojo.request.SendEmailRequest;
import com.pcms.be.repository.GroupSupervisorRepository;
import com.pcms.be.repository.GroupRepository;
import com.pcms.be.repository.MeetingRepository;
import com.pcms.be.service.Email.EmailService;
import com.pcms.be.service.MeetingService;
import com.pcms.be.service.NotificationService;
import com.pcms.be.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class MeetingServiceImpl implements MeetingService {
    private final GroupRepository groupRepository;
    private final GroupSupervisorRepository groupSupervisorRepository;
    private final MeetingRepository meetingRepository;
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final NotificationService notificationService;
    private final EmailService emailService;

    @Override
    @Transactional
    public List<MeetingDTO> createMeeting(List<CreateMeetingRequest> createMeetingRequests, int groupId) throws ServiceException {
        try {
            Group groupMee = groupRepository.findById(Long.valueOf(groupId)).orElseThrow();
            List<MeetingDTO> meetingDTOS = new ArrayList<>();
            OffsetDateTime currentTime = OffsetDateTime.now();
            // Kiểm tra thời gian không được trùng nhau trong cùng 1 list request
            for (int i = 0; i < createMeetingRequests.size(); i++) {
                for (int j = i + 1; j < createMeetingRequests.size(); j++) {
                    if (areMeetingTimesOverlapping(createMeetingRequests.get(i), createMeetingRequests.get(j))) {
                        throw new ServiceException(ErrorCode.TIME_RANGE_DUPLICATED);
                    }
                }
            }
            List<Meeting> existingMeetings = meetingRepository.findAllByGroup(groupMee);
            // Kiểm tra xem thời gian của meeting request có trùng với thời gian của meeting đã có trong database hay không
            for (CreateMeetingRequest meet : createMeetingRequests) {
                for (Meeting existingMeeting : existingMeetings) {
                    if (areMeetingTimesOverlapping(meet, existingMeeting)) {
                        throw new ServiceException(ErrorCode.TIME_RANGE_DUPLICATED);
                    }
                }
            }
            for (CreateMeetingRequest meet : createMeetingRequests) {
                Optional<Group> group = groupRepository.findById(Long.valueOf(meet.getGroupId()));
                if (!group.isPresent()) {
                    throw new ServiceException(ErrorCode.GROUP_NOT_FOUND);
                }
                Group group1 = group.get();
                User user = userService.getCurrentUser();
                GroupSupervisor groupSupervisor = groupSupervisorRepository.findByGroupAndSupervisorAndStatus(group1, user.getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
                if (groupSupervisor == null) {
                    throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
                }

                // Kiểm tra createAt phải trước endAt
                if (meet.getStartAt().isAfter(meet.getEndAt())) {
                    throw new ServiceException(ErrorCode.STARTAT_MUST_BE_BEFORE_ENDAT);
                }
                // Kiểm tra createAt phải sau currentTime
                if (meet.getStartAt().isBefore(currentTime)) {
                    throw new ServiceException(ErrorCode.STARTAT_MUST_BE_AFTER_CURRENT_TIME);
                }

                Meeting newMeeting = new Meeting();
                newMeeting.setStartAt(meet.getStartAt());
                newMeeting.setEndAt(meet.getEndAt());
                newMeeting.setType(meet.getType());
                newMeeting.setLocation(meet.getLocation());
                newMeeting.setGroup(group1);
                newMeeting.setStatus(Constants.MeetingStatus.PENDING);
                meetingRepository.save(newMeeting);
                meetingDTOS.add(modelMapper.map(newMeeting, MeetingDTO.class));
                List<User> users = new ArrayList<>();
                for (Member m: group1.getMembers()){
                    users.add(m.getStudent().getUser());
                }
                //Tạo notification
                Map<String, String> map = new HashMap<>();
                //Bạn có một cuộc họp với supervisor: _SupervisorName-txt_ vào lúc _Time-txt_.\n" +
                //                "Link Meeting: _Location-txt_
                map.put("_SupervisorName-txt_", group1.getSupervisors().stream().map(Supervisor::getUser).map(User::getName).collect(Collectors.joining(". ")));
                map.put("_Time-txt_", String.valueOf(meet.getStartAt().getHour())+":"+String.valueOf(meet.getStartAt().getMinute()+" "+ meet.getStartAt().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                +" " + meet.getStartAt().getDayOfMonth() + "/" + meet.getStartAt().getMonth()+"/"+meet.getStartAt().getYear());
                map.put("_Location-txt_", meet.getLocation());
                String content = notificationService.createContentNotification(NotificationTemplate.MeetingNotification.createMeetingTemplate, map);
                SendEmailRequest sendEmailRequest = new SendEmailRequest();
                List<Recipient> recipients = new ArrayList<>();
                for (User u : users){
                    Recipient recipient = new Recipient();
                    recipient.setEmail(u.getEmail());
                    recipients.add(recipient);
                }
                notificationService.saveMeetingNotification(users, content);
                sendEmailRequest.setTo(recipients);
                sendEmailRequest.setSubject(MailTemplate.CreateMeeting.subject);
                sendEmailRequest.setHtmlContent(MailTemplate.CreateMeeting.htmlContent.replace("_GroupName-txt_",group.get().getName()).replace("_Type-txt_", meet.getType()).replace("_StartAt-txt_",displayOffSetDatetime(meet.getStartAt())).replace("_EndAt-txt_", displayOffSetDatetime(meet.getEndAt())).replace("_Location-txt_", meet.getLocation()));
                emailService.sendEmail(sendEmailRequest);
            }
            return meetingDTOS;
        } catch (Exception e) {
            throw new ServiceException(ErrorCode.FAILED_CREATE_MEETING);
        }
    }
    public String displayOffSetDatetime(OffsetDateTime offsetDateTime){

        // Định dạng hiển thị
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

        // Hiển thị OffsetDateTime
        String formattedDateTime = offsetDateTime.format(formatter);
        return formattedDateTime;
    }

    @Override
    public List<MeetingDTO> viewMeetings(int groupId) throws ServiceException {
            Optional<Group> group = groupRepository.findById(Long.valueOf(groupId));
            if (!group.isPresent()) {
                throw new ServiceException(ErrorCode.GROUP_NOT_FOUND);
            }
            Group group1 = group.get();
            List<Meeting> meetings = meetingRepository.findAllByGroup(group1);
            List<MeetingDTO> meetingDTOs = new ArrayList<>();
            for (Meeting meet : meetings) {
                meetingDTOs.add(modelMapper.map(meet, MeetingDTO.class));
            }
            return meetingDTOs;
    }

    @Override
    public List<MeetingDTO> updateMeeting(List<EditMeetingRequest> editMeetingRequests) throws ServiceException {
        try {
            List<MeetingDTO> meetingDTOS = new ArrayList<>();
            OffsetDateTime currentTime = OffsetDateTime.now();
            // Kiểm tra thời gian không được trùng nhau trong cùng 1 list request
            for (int i = 0; i < editMeetingRequests.size(); i++) {
                for (int j = i + 1; j < editMeetingRequests.size(); j++) {
                    if (areMeetingTimesOverlapping(editMeetingRequests.get(i), editMeetingRequests.get(j))) {
                        throw new ServiceException(ErrorCode.TIME_RANGE_DUPLICATED);
                    }
                }
            }
            List<Meeting> existingMeetings = meetingRepository.findAll();
            // Kiểm tra xem thời gian của meeting request có trùng với thời gian của meeting đã có trong database hay không
            for (EditMeetingRequest meet : editMeetingRequests) {
                for (Meeting existingMeeting : existingMeetings) {
                    if (areMeetingTimesOverlapping(meet, existingMeeting)) {
                        throw new ServiceException(ErrorCode.TIME_RANGE_DUPLICATED);
                    }
                }
            }

            for (EditMeetingRequest meet : editMeetingRequests) {
                Optional<Meeting> meeting = meetingRepository.findById(Long.valueOf(meet.getId()));
                if (!meeting.isPresent()) {
                    throw new ServiceException(ErrorCode.NOT_FOUND);
                }
                User user = userService.getCurrentUser();
                GroupSupervisor groupSupervisor = groupSupervisorRepository.findByGroupAndSupervisorAndStatus(meeting.get().getGroup(), user.getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
                if (groupSupervisor == null) {
                    throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
                }
                if(!meeting.get().getStatus().equals(Constants.MeetingStatus.PENDING)){
                    throw new ServiceException(ErrorCode.FAILED_EDIT_MEETING);
                }
                // Kiểm tra createAt phải trước endAt
                if (meet.getStartAt().isAfter(meet.getEndAt())) {
                    throw new ServiceException(ErrorCode.STARTAT_MUST_BE_BEFORE_ENDAT);
                }
                // Kiểm tra createAt phải sau currentTime
                if (meet.getStartAt().isBefore(currentTime)) {
                    throw new ServiceException(ErrorCode.STARTAT_MUST_BE_AFTER_CURRENT_TIME);
                }

                Meeting editMeeting = meeting.get();
                OffsetDateTime oldStartAt = editMeeting.getStartAt();
                OffsetDateTime oldEndAt = editMeeting.getEndAt();
                editMeeting.setStartAt(meet.getStartAt());
                editMeeting.setEndAt(meet.getEndAt());
                editMeeting.setType(meet.getType());
                editMeeting.setLocation(meet.getLocation());
                meetingRepository.save(editMeeting);
                meetingDTOS.add(modelMapper.map(editMeeting, MeetingDTO.class));
                Map<String, String> map = new HashMap<>();
                //Bạn có một cuộc họp với supervisor: _SupervisorName-txt_ vào lúc _Time-txt_.\n" +
                //                "Link Meeting: _Location-txt_
                String oldTimeTxt = String.valueOf(oldStartAt.getHour())+":"+String.valueOf(oldStartAt.getMinute()+" "+ oldStartAt.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                        +" " + oldStartAt.getDayOfMonth() + "/" + oldStartAt.getMonth()+"/"+oldStartAt.getYear()
                        + " to " + String.valueOf(oldEndAt.getHour())+":"+String.valueOf(oldEndAt.getMinute()+" "+ oldEndAt.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                        +" " + oldEndAt.getDayOfMonth() + "/" + oldEndAt.getMonth()+"/"+oldEndAt.getYear();
                String newTimeTxt = String.valueOf(editMeeting.getStartAt().getHour())+":"+String.valueOf(editMeeting.getStartAt().getMinute()+" "+ editMeeting.getStartAt().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                        +" " + editMeeting.getStartAt().getDayOfMonth() + "/" + editMeeting.getStartAt().getMonth()+"/"+editMeeting.getStartAt().getYear()
                        + " to " + String.valueOf(editMeeting.getEndAt().getHour())+":"+String.valueOf(editMeeting.getEndAt().getMinute()+" "+ editMeeting.getEndAt().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                        +" " + editMeeting.getEndAt().getDayOfMonth() + "/" + editMeeting.getEndAt().getMonth()+"/"+editMeeting.getEndAt().getYear();;
                map.put("_Location-txt_", meet.getLocation());
                map.put("_OldTime-txt_", oldTimeTxt);
                map.put("_SupervisorName-txt_", meeting.get().getGroup().getSupervisors().stream().map(Supervisor::getUser).map(User::getName).collect(Collectors.joining(". ")));
                map.put("_Time-txt_", newTimeTxt);
                String content = notificationService.createContentNotification(NotificationTemplate.MeetingNotification.updateMeetingTemplate, map);
                SendEmailRequest sendEmailRequest = new SendEmailRequest();
                List<Recipient> recipients = new ArrayList<>();
                List<User> users = new ArrayList<>();
                for (Member m : meeting.get().getGroup().getMembers()){
                    Recipient recipient = new Recipient();
                    recipient.setName(m.getStudent().getUser().getName());
                    recipient.setEmail(m.getStudent().getUser().getEmail());
                    recipients.add(recipient);
                    users.add(m.getStudent().getUser());
                }
                notificationService.saveMeetingNotification(users, content);
                sendEmailRequest.setTo(recipients);
                sendEmailRequest.setSubject(MailTemplate.UpdateMeeting.subject);
                sendEmailRequest.setHtmlContent(MailTemplate.UpdateMeeting.htmlContent.replace("_GroupName-txt_", meeting.get().getGroup().getName()).replace("_OldTime-txt_", oldTimeTxt).replace("_NewTime-txt_", newTimeTxt).replace("_Type-txt_", editMeeting.getType()).replace("_Location-txt_", meet.getLocation()));
                emailService.sendEmail(sendEmailRequest);
            }
            return meetingDTOS;
        } catch (Exception e) {
            throw new ServiceException(ErrorCode.FAILED_EDIT_MEETING);
        }
    }
    @Override
    public MeetingDTO updateOneMeeting(EditMeetingRequest editMeetingRequests) throws ServiceException {
        Optional<Meeting> meeting = meetingRepository.findById(Long.valueOf(editMeetingRequests.getId()));
        if (!meeting.isPresent()) {
            throw new ServiceException(ErrorCode.NOT_FOUND);
        }
        OffsetDateTime oldStartAt = meeting.get().getStartAt();
        OffsetDateTime oldEndAt = meeting.get().getEndAt();
        String oldTime = String.valueOf(oldStartAt.getHour()+":"+oldStartAt.getMinute() + "  "+oldStartAt.getDayOfMonth()+"/"+oldStartAt.getMonth()+"/"+oldStartAt.getYear()+
                "  to  " + oldEndAt.getHour()+":"+oldEndAt.getMinute() + "  "+oldEndAt.getDayOfMonth()+"/"+oldEndAt.getMonth()+"/"+oldEndAt.getYear());

        OffsetDateTime currentTime = OffsetDateTime.now();
        User user = userService.getCurrentUser();
        GroupSupervisor groupSupervisor = groupSupervisorRepository.findByGroupAndSupervisorAndStatus(meeting.get().getGroup(), user.getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
        if (groupSupervisor == null) {
            throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
        }
        if(!meeting.get().getStatus().equals(Constants.MeetingStatus.PENDING)){
            throw new ServiceException(ErrorCode.FAILED_EDIT_MEETING);
        }
        // Kiểm tra createAt phải trước endAt
        if (editMeetingRequests.getStartAt().isAfter(editMeetingRequests.getEndAt())) {
            throw new ServiceException(ErrorCode.STARTAT_MUST_BE_BEFORE_ENDAT);
        }
        // Kiểm tra createAt phải sau currentTime
        if (editMeetingRequests.getStartAt().isBefore(currentTime)) {
            throw new ServiceException(ErrorCode.STARTAT_MUST_BE_AFTER_CURRENT_TIME);
        }
            Meeting editMeeting = meeting.get();
            editMeeting.setStartAt(editMeetingRequests.getStartAt());
            editMeeting.setEndAt(editMeetingRequests.getEndAt());
            editMeeting.setType(editMeetingRequests.getType());
            editMeeting.setLocation(editMeetingRequests.getLocation());
            meetingRepository.save(editMeeting);
            OffsetDateTime newStartAt = editMeeting.getStartAt();
            OffsetDateTime newEndAt = editMeeting.getEndAt();
            String newTime = String.valueOf(newStartAt.getHour()+":"+newStartAt.getMinute() + "  "+newStartAt.getDayOfMonth()+"/"+newStartAt.getMonth()+"/"+newStartAt.getYear()+
                "  to  " + newEndAt.getHour()+":"+newEndAt.getMinute() + "  "+newEndAt.getDayOfMonth()+"/"+newEndAt.getMonth()+"/"+newEndAt.getYear());
            Map<String, String> map = new HashMap<>();
            map.put("_OldTime-txt_", oldTime);
            map.put("_SupervisorName-txt_", meeting.get().getGroup().getSupervisors().stream().map(Supervisor::getUser).map(User::getName).collect(Collectors.joining(". ")));
            map.put("_Time-txt_", String.valueOf(editMeeting.getStartAt().getHour())+":"+String.valueOf(editMeeting.getStartAt().getMinute()+" "+ editMeeting.getStartAt().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                    +" " + editMeeting.getStartAt().getDayOfMonth() + "/" + editMeeting.getStartAt().getMonth()+"/"+editMeeting.getStartAt().getYear());
            map.put("_Location-txt_", editMeeting.getLocation());
            String content = notificationService.createContentNotification(NotificationTemplate.MeetingNotification.updateMeetingTemplate, map);
            SendEmailRequest sendEmailRequest = new SendEmailRequest();
            List<Recipient> recipients = new ArrayList<>();
            for (Member m : meeting.get().getGroup().getMembers()){
                Recipient recipient = new Recipient();
                recipient.setName(m.getStudent().getUser().getName());
                recipient.setEmail(m.getStudent().getUser().getEmail());
                recipients.add(recipient);
                notificationService.saveNotification(m.getStudent().getUser(), content);
            }
            sendEmailRequest.setTo(recipients);
            sendEmailRequest.setSubject(MailTemplate.UpdateMeeting.subject);
            sendEmailRequest.setHtmlContent(MailTemplate.UpdateMeeting.htmlContent.replace("_GroupName-txt_", meeting.get().getGroup().getName())
                    .replace("_OldTime-txt_",oldTime)
                    .replace("_NewTime-txt_", newTime)
                    .replace("_Location-txt_", editMeeting.getLocation()).replace("_Type-txt_", editMeeting.getType()));
            emailService.sendEmail(sendEmailRequest);
            return modelMapper.map(editMeeting, MeetingDTO.class);
    }

    @Override
    public MeetingDTO removeMeeting(int meetingId) throws ServiceException {
        try {
            Optional<Meeting> meeting = meetingRepository.findById(Long.valueOf(meetingId));
            if (!meeting.isPresent()) {
                throw new ServiceException(ErrorCode.NOT_FOUND);
            }
            if(!meeting.get().getStatus().equals(Constants.MeetingStatus.PENDING)){
                throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
            }
            User user = userService.getCurrentUser();
            GroupSupervisor groupSupervisor = groupSupervisorRepository.findByGroupAndSupervisorAndStatus(meeting.get().getGroup(), user.getSupervisor(), Constants.SupervisorStatus.ACCEPT_SUPERVISOR_LEADER);
            if (groupSupervisor == null) {
                throw new ServiceException(ErrorCode.USER_NOT_ALLOW);
            }
            Map<String, String> map = new HashMap<>();
            String timeTxt = String.valueOf(meeting.get().getStartAt().getHour())+":"+String.valueOf(meeting.get().getStartAt().getMinute()+" "+ meeting.get().getStartAt().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                    +" " + meeting.get().getStartAt().getDayOfMonth() + "/" + meeting.get().getStartAt().getMonth()+"/"+meeting.get().getStartAt().getYear()
                    + " to " + String.valueOf(meeting.get().getEndAt().getHour())+":"+String.valueOf(meeting.get().getEndAt().getMinute()+" "+ meeting.get().getEndAt().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("vi")))
                    +" " + meeting.get().getEndAt().getDayOfMonth() + "/" + meeting.get().getEndAt().getMonth()+"/"+meeting.get().getEndAt().getYear();;
            map.put("_Time-txt_", timeTxt);
            String content = notificationService.createContentNotification(NotificationTemplate.MeetingNotification.deleteMeetingTemplate, map);
            SendEmailRequest sendEmailRequest = new SendEmailRequest();
            List<Recipient> recipients = new ArrayList<>();
            for (Member m : meeting.get().getGroup().getMembers()){
                Recipient recipient = new Recipient();
                recipient.setName(m.getStudent().getUser().getName());
                recipient.setEmail(m.getStudent().getUser().getEmail());
                recipients.add(recipient);
                notificationService.saveNotification(m.getStudent().getUser(), content);
            }

            sendEmailRequest.setTo(recipients);
            sendEmailRequest.setSubject(MailTemplate.CancelMeeting.subject);
            sendEmailRequest.setHtmlContent(MailTemplate.CancelMeeting.htmlContent.replace("_Time-Txt_", timeTxt).replace("_Type-txt_", meeting.get().getType()).replace("_Location-txt_", meeting.get().getLocation()));
            emailService.sendEmail(sendEmailRequest);
            meetingRepository.delete(meeting.get());
            return modelMapper.map(meeting.get(), MeetingDTO.class);
        } catch (Exception e) {
            throw new ServiceException(ErrorCode.FAILED_DELETE_MEETING);
        }
    }

    @Override
    public ResponseEntity<MeetingDTO> getByMeetingId(int meetingId) throws ServiceException {
        try {
            Optional<Meeting> meeting = meetingRepository.findById(Long.valueOf(meetingId));
            if (meeting.isEmpty()){
                throw new ServiceException(ErrorCode.MEETING_NOT_FOUND);
            }else{
                MeetingDTO meetingDTO = modelMapper.map(meeting.get(), MeetingDTO.class);
                return ResponseEntity.ok(meetingDTO);
            }
        }catch (Exception e) {
            throw new ServiceException(ErrorCode.MEETING_NOT_FOUND);
        }
    }

    private boolean areMeetingTimesOverlapping(CreateMeetingRequest createMeetingRequest, Meeting existingMeeting) {
        // Kiểm tra xem thời gian của meeting request có trùng với thời gian của meeting đã có trong database hay không
        return (createMeetingRequest.getStartAt().isBefore(existingMeeting.getEndAt()) && createMeetingRequest.getEndAt().isAfter(existingMeeting.getStartAt()));
    }

    private boolean areMeetingTimesOverlapping(EditMeetingRequest createMeetingRequest, Meeting existingMeeting) {
        // Kiểm tra xem thời gian của meeting request có trùng với thời gian của meeting đã có trong database hay không
        return (createMeetingRequest.getStartAt().isBefore(existingMeeting.getEndAt()) && createMeetingRequest.getEndAt().isAfter(existingMeeting.getStartAt()));
    }

    private boolean areMeetingTimesOverlapping(CreateMeetingRequest meeting1, CreateMeetingRequest meeting2) {
        //kiểm tra xem thời gian của meeting request không được đè lên thời gian của nhau
        return (meeting1.getStartAt().isBefore(meeting2.getEndAt()) && meeting2.getStartAt().isBefore(meeting1.getEndAt()));
    }

    private boolean areMeetingTimesOverlapping(EditMeetingRequest meeting1, EditMeetingRequest meeting2) {
        //kiểm tra xem thời gian của meeting request không được đè lên thời gian của nhau
        return (meeting1.getStartAt().isBefore(meeting2.getEndAt()) && meeting2.getStartAt().isBefore(meeting1.getEndAt()));
    }
}