package com.pcms.be.functions;

public interface MailTemplate {
    interface MilestoneUnfinished{
        String subject = "Unfinished Milestone";
        String htmlContent = "<p>Dear _GroupName-txt_,</p>\n" +
                "            <p>I hope this email finds you well. I'm writing to inform you that the _MilestoneName-txt_ milestone has not been completed as scheduled.</p>\n" +
                "            <p>The expected completion date for this milestone was [Milestone Due Date], but the work is still in progress.</p>\n" +
                "            <p>I understand that there may be valid reasons for the delay, and I would like to discuss the status of the project and any potential obstacles or challenges you are facing. Please let me know if you have any updates or if there is anything I can do to assist you in getting the project back on track.</p>\n" +
                "            <p>Thank you for your attention to this matter, and I look forward to hearing from you.</p>\n" +
                "            <p>Best regards,</p>";
    }
    interface InviteJoinGroup{
        String subject = "Invite Join Group";
        String htmlContent = "<p>You have been invited to join their group by _LeaderName-txt_( group: _GroupName-txt_).</p>" +
                "<p>Please enter the CPSS system to respond to their request.</p>"+
                "<p>Best regards,</p>";
    }
    interface CreateMeeting{
        String subject = "Create new meeting";
        String htmlContent = " <p>Dear _GroupName-txt_,</p>" +
                "            <p>I hope this email finds you well. I'm writing to inform you that a new meeting has been scheduled.</p>" +
                "                <h2>Meeting Details</h2>" +
                "                <p><strong>  Date:</strong> _StartAt-txt_ to _EndAt-txt_</p>" +
                "                <p><strong>  Type:</strong> _Type-txt_</p>" +
                "                <p><strong>  Location:</strong> _Location-txt_</p>" +
                "            <p>Thank you for your participation,</p>";
    }
    interface UpdateMeeting{
        String subject = "Update meeting";
        String htmlContent = " <p>Dear _GroupName-txt_,</p>" +
                "            <p>I hope this email finds you well. I'm writing to inform you of an update to the previously scheduled meeting.</p>" +
                "            <div class=meeting-details>" +
                "                <h2>Meeting Details</h2>" +
                "                <p><strong>  Original Date:</strong> _OldTime-txt_</p>" +
                "                <p><strong>  New Date:</strong> _NewTime-txt_</p>" +
                "                <p><strong>  Type:</strong> _Type-txt_</p>" +
                "                <p><strong>  Location:</strong> _Location-txt_</p>" +
                "            </div>" +
                "            <p>Thank you for your participation,</p>";
    }

    interface CancelMeeting{
        String subject = "Cancel meeting";
        String htmlContent = "<p>Dear _GroupName-txt_,</p>" +
                "            <p>I regret to inform you that the upcoming meeting has been cancelled.</p>" +
                "            <div class=meeting-details>" +
                "                <h2>Meeting Details</h2>" +
                "                <p><strong>Date:</strong> _Time-Txt_</p>" +
                "                <p><strong>  Type:</strong> _Type-txt_</p>" +
                "                <p><strong>Location:</strong> _Location-txt_</p>" +
                "            </div>" +
                "            <p>Thank you for your participation,</p>";
    }
    interface TotalCommitsDecreased{
        String subject = "Checking Process";
        String htmlContent = "<p>Dear _GroupName-txt_,</p>" +
                "<p>Your team's total commits this week have decreased compared to last week. Please try to contribute to complete the project.</p>" +
                "<p>Good luck to your team in completing the project.</p>";
    }

    interface TotalCommitsIncreased{
        String subject = "Checking Process";
        String htmlContent = "<p>Dear _GroupName-txt_,</p>" +
                "<p>Your team's total commits this week have increased compared to last week. Keep up the good work.</p>" +
                "<p>Good luck with your project.</p>";
    }
}
