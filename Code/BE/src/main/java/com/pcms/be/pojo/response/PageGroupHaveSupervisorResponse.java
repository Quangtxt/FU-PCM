package com.pcms.be.pojo.response;

import com.pcms.be.pojo.DTO.GroupSupervisorDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class PageGroupHaveSupervisorResponse {
        private Long totalCount;
        private Integer totalPage;
        private List<GroupSupervisorDTO> data;
}
