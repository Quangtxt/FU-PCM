package com.pcms.be.pojo.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExcelStudentDTO {
    String rollNumber;
    String email;
    String memberCode;
    String fullName;
    String username ;
    String note;

}
