package com.pcms.be.repository;

import com.pcms.be.domain.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findAllBySemesterId(int semesterId);
    @Query(value = "SELECT r.* FROM Report r\n" +
            "WHERE r.possible_date = :possibleDate ;", nativeQuery = true)
    List<Report> findByPossibleDate(LocalDateTime possibleDate);
    Report findFirstByMilestoneId(int milestoneId);
}
